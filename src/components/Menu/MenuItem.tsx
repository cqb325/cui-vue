import { HTMLAttributes, PropType, computed, defineComponent, inject, ref, watch } from "vue";
import Popover from "../Popover";
import { FeatherChevronDown } from "cui-vue-icons/feather";
import { MenuContext, MenuTreeContext } from ".";

export interface MenuItemProps extends HTMLAttributes {
    name?: string,
    disabled?: boolean,
    isSubmenuTitle?: boolean,
    data?: any,
    cert?: boolean,
    icon?: any
}

export default defineComponent({
    name: 'MenuItem',
    props: {
        name: {type: String as PropType<MenuItemProps['name']>},
        disabled: {type: Boolean as PropType<MenuItemProps['disabled']>},
        isSubmenuTitle: {type: Boolean as PropType<MenuItemProps['isSubmenuTitle']>},
        data: {type: Object as PropType<MenuItemProps['data']>},
        cert: {type: Boolean as PropType<MenuItemProps['cert']>},
        icon: {type: Object as PropType<MenuItemProps['icon']>},
    },
    emits: ['select'],
    setup (props: MenuItemProps, {slots, emit}) {
        if (!props.isSubmenuTitle && !props.name) {
            console.warn("MenuItem need name prop");
        }

        const ctx: any = inject(MenuContext, {});
        const treeCtx: any = inject(MenuTreeContext, {});
        const parentPadding = treeCtx.parent?.padding ?? 0;
        const padding = ref(ctx?.dir === 'h' ? 16 : props.isSubmenuTitle ? parentPadding : parentPadding + 16);
        const isRootItem = ref(false);

        const classList = computed(() => ({
            'cm-menu-item': true,
            'cm-menu-item-disabled': props.disabled,
            'cm-menu-item-active': !props.isSubmenuTitle && props.name && ctx?.store.activeName === props.name
        }));

        watch(() => ctx.store.min, () => {
            isRootItem.value = ctx.store.min && !props.isSubmenuTitle && !treeCtx.parent;
            padding.value = props.isSubmenuTitle ? treeCtx.parent?.padding : treeCtx.parent?.padding + 16;
            // 根item元素
            if (!props.isSubmenuTitle) {
                if (!treeCtx.parent) {
                    padding.value = 16;
                }
            } else {
                // submenu 或则 menugroup
                if (!treeCtx.parent.parent) {
                    padding.value = 16;
                }
                if (ctx.dir === 'h') {
                    padding.value = 16;
                }
            }
        }, {immediate: true});

        const onSelect = () => {
            if (props.isSubmenuTitle && !ctx.store.min) {
                emit('select');
            } else {
                ctx?.onSelect(props.name, props.data);
            }
        };

        const createNode = () => {
            const item = {
                name: props.name,
                parent: null,
                children: []
            };
            if (ctx && props.name) {
                ctx.treeMap[props.name] = item;
                if (!treeCtx.parentName) {
                    ctx?.tree.push(item);
                } else {
                    const parent = ctx.treeMap[treeCtx.parentName];
                    if (parent) {
                        item.parent = parent;
                        parent.children.push(item);
                    }
                }
            }
        };

        if (!props.isSubmenuTitle) {
            createNode();
        }

        return () => isRootItem.value ? (
            <Popover align="right" arrow theme={ctx.theme} content={<div class="cm-menu-item-text">{slots.default && slots.default()}</div>}>
                <li class={classList.value} onClick={onSelect}>
                    <div class="cm-menu-item-icon">{props.icon}</div>
                </li>
            </Popover>
        ) : (
            <li class={classList.value} onClick={onSelect} style={{paddingLeft: padding.value + 'px'}}>
                <div class="cm-menu-item-icon">{props.icon}</div>
                <div class="cm-menu-item-text">{slots.default && slots.default()}</div>
                {
                    props.cert
                        ? <div class="cm-menu-item-cert">
                            <FeatherChevronDown size={14}/>
                        </div>
                        : null
                }
            </li>
        );
    }
});
