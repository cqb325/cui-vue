import { HTMLAttributes } from "vue";

function BothSide (props: any, { slots }) {
    return <div class="cm-both-side">{slots.default && slots.default()}</div>;
}

BothSide.displayName = 'BothSide';

export default BothSide;
