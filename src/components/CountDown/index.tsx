import dayjs from 'dayjs';
import { PropType, defineComponent, onMounted, onUnmounted, ref, computed } from 'vue';

interface CountDownProps {
    prefix?: string | object; // 前缀内容
    suffix?: string | object; // 后缀内容
    value?: string | number | Date; // 目标时间
    format?: string; // 格式化字符串
    onEnd?: () => void; // 倒计时结束回调
}

// 补零函数
const fixedZero = (val: number): string => val.toString().padStart(2, '0');

export default defineComponent({
    name: 'CountDown',
    props: {
        prefix: { type: [String, Object] as PropType<CountDownProps['prefix']> },
        suffix: { type: [String, Object] as PropType<CountDownProps['suffix']> },
        value: { type: [String, Number, Date] as PropType<CountDownProps['value']>, required: true },
        format: { type: String as PropType<CountDownProps['format']>, default: 'HH:mm:ss' },
    },
    emits: ['end'],
    setup (props: CountDownProps, { emit }) {
        const now = ref(Date.now()); // 当前时间
        const timer = ref<number | null>(null); // 定时器

        // 计算剩余时间
        const remainingTime = computed(() => {
            let targetTime: number;

            if (typeof props.value === 'string' || props.value instanceof Date) {
                targetTime = dayjs(props.value).valueOf();
            } else {
                targetTime = props.value;
            }

            return Math.max(targetTime - now.value, 0); // 确保剩余时间不为负数
        });

        // 格式化倒计时文本
        const formattedText = computed(() => {
            const offset = remainingTime.value;

            if (offset <= 0) {
                stopTimer();
                emit('end');
                props.onEnd?.();
                return '00:00:00'; // 倒计时结束时的默认值
            }

            const days = fixedZero(Math.floor(offset / (1000 * 60 * 60 * 24)));
            const hours = fixedZero(Math.floor((offset / (1000 * 60 * 60)) % 24));
            const minutes = fixedZero(Math.floor((offset / (1000 * 60)) % 60));
            const seconds = fixedZero(Math.floor(offset / 1000) % 60);

            return props.format
                .replace(/D+/g, days)
                .replace(/H+/g, hours)
                .replace(/m+/g, minutes)
                .replace(/s+/g, seconds);
        });

        // 启动定时器
        const startTimer = () => {
            timer.value = window.setInterval(() => {
                now.value = Date.now();
            }, 1000);
        };

        // 停止定时器
        const stopTimer = () => {
            if (timer.value) {
                clearInterval(timer.value);
                timer.value = null;
            }
        };

        // 生命周期钩子
        onMounted(startTimer);
        onUnmounted(stopTimer);

        return () => (
            <span class="cm-count-down">
                {props.prefix && <span class="cm-count-down-prefix">{props.prefix}</span>}
                <span class="cm-count-down-value">{formattedText.value}</span>
                {props.suffix && <span class="cm-count-down-suffix">{props.suffix}</span>}
            </span>
        );
    },
});
