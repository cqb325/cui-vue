import { createApp, reactive } from "vue";
import usePortal from "../use/usePortal";
import { Notices } from "./Notices";
import { createUniqueId } from "../use/createUniqueId";
import usezIndex from "../use/usezIndex";

export type NoticeConfig = {
    dock?: 'topRight'|'topLeft'|'bottomLeft'|'bottomRight',
    key?: string,
    duration?: number,
    content?: any,
    title?: any,
    icon?: any,
    theme?: 'success' | 'warning' | 'error' | 'info' | 'help',
    btn?: any,
    style?: any,
    onClose?: () => void
}

type StoreData = {
    topLeft: NoticeConfig[],
    topRight: NoticeConfig[],
    bottomLeft: NoticeConfig[],
    bottomRight: NoticeConfig[],
}

/**
 * Notice
 * @returns
 */
function Notice () {
    const store = reactive({
        topLeft: [],
        topRight: [],
        bottomLeft: [],
        bottomRight: [],
    } as StoreData);

    const onClose = (key: string, dock: 'topRight'|'topLeft'|'bottomLeft'|'bottomRight') => {
        const arr: NoticeConfig[] = store[dock].filter((item: NoticeConfig) => {
            return item.key !== key;
        });
        store[dock] = arr;
    };
    const ele = usePortal('cm-notice-portal', 'cm-notices-wrap');
    createApp(() => <Notices data={store} onClose={onClose}/>).mount(ele);
    return {
        open (config: NoticeConfig) {
            if (!config.dock) {
                config.dock = 'topRight';
            }
            if (config.key === undefined) {
                config.key = createUniqueId();
            }
            if (config.duration === undefined) {
                config.duration = 4.5;
            }

            store[config.dock].push(config);
            ele.style.zIndex = usezIndex() + '';
        },
        info (config: NoticeConfig) {
            config.theme = 'info';
            this.open(config);
        },
        success (config: NoticeConfig) {
            config.theme = 'success';
            this.open(config);
        },
        warning (config: NoticeConfig) {
            config.theme = 'warning';
            this.open(config);
        },
        error (config: NoticeConfig) {
            config.theme = 'error';
            this.open(config);
        },
        help (config: NoticeConfig) {
            config.theme = 'help';
            this.open(config);
        }
    };
}
export const notice = Notice();
