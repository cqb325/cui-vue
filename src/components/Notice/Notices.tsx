import { NoticeConfig } from ".";
import usezIndex from "../use/usezIndex";
import { computed, createVNode, defineComponent, onMounted, ref } from "vue";
import { F7CheckmarkAltCircleFill, F7ExclamationmarkTriangleFill, F7InfoCircleFill, F7QuestionCircleFill, F7XmarkCircleFill } from "cui-vue-icons/f7";
import { FeatherX } from "cui-vue-icons/feather";

const icons = {
    info: F7InfoCircleFill,
    success: F7CheckmarkAltCircleFill,
    warning: F7ExclamationmarkTriangleFill,
    error: F7XmarkCircleFill,
    help: F7QuestionCircleFill
};

type NoticesProps = {
    data?: any,
    onClose?: (key: any, dock: any) => void,
    docker?: string
}

const NoticePanel = defineComponent({
    name: 'NoticePanel',
    props: {
        data: {type: Object},
        docker: {type: String}
    },
    emits: ['close'],
    setup (props: NoticesProps, { emit }) {
        const visible = ref(false);
        const closed = ref(false);
        const data: NoticeConfig = props.data;
        const { style, icon, btn, theme, title, content } = data;
        const ic = computed(() => icon === undefined ? (icons[theme!] ? createVNode(icons[theme!], {class: `cm-notice-icon-${theme}`}) : null) : icon);
        const hasIcon = computed(() => icon || (icon === undefined ? icons[theme!] : null));
        const classList = computed(() => ({
            'cm-notification-item': true,
            'cm-notification-item-width-icon': hasIcon.value,
            'cm-notification-item-open': visible.value,
            'cm-notification-item-close': closed.value,
            [`cm-notification-item-${theme}`]: theme,
        }));


        onMounted(() => {
            setTimeout(()=>{
                visible.value = true;
            });

            if (data.duration) {
                setTimeout(() => {
                    hide();
                }, data.duration * 1000);
            }
        });

        const hide = () => {
            // 隐藏效果
            if (!closed.value) {
                closed.value = true;
                setTimeout(() => {
                    close();
                }, 250);
            }
        };

        // 关闭事件
        const close = () => {
            emit('close', data.key, data.dock);
            data.onClose && data.onClose();
        };

        return () => <div class={classList.value} style={style}>
            <div class="cm-notification-item-wrap">
                {
                    hasIcon.value ? <div class="cm-notification-icon">
                        {ic.value}
                    </div> : null
                }
                <div class="cm-notification-content">
                    {
                        title ? <div class="cm-notification-head">
                            {title}
                            <a class="cm-notification-close" onClick={hide}><FeatherX/></a>
                        </div> : null
                    }
                    <div class="cm-notification-body">{content}</div>
                    {
                        btn ? <span class="cm-notification-btn-wrap">
                            {btn}
                        </span> : null
                    }
                </div>
            </div>
        </div>;
    }
});

const NoticeBox = defineComponent({
    name: 'NoticeBox',
    props: {
        data: Array,
        docker: {
            type: String
        }
    },
    emits: ['close'],
    setup (props, ctx) {
        const zindex = usezIndex();

        return () => props.data && props.data.length ?
            <div class={`cm-notification-box cm-notification-${props.docker}`} style={{"z-index": zindex}}>
                {
                    props.data.map((item: any) => {
                        return <NoticePanel data={item} onClose={ctx.emit.bind(ctx, 'close')}/>;
                    })
                }
            </div>
            : null;
    },
});

export const Notices = defineComponent({
    name: 'Notices',
    props: {
        data: Object,
    },
    emits: ['close'],
    setup (props, ctx) {
        return () => <div class="cm-notification">
            <NoticeBox data={props.data.topLeft} docker="top-left" onClose={ctx.emit.bind(ctx, 'close')}/>
            <NoticeBox data={props.data.topRight} docker="top-right" onClose={ctx.emit.bind(ctx, 'close')}/>
            <NoticeBox data={props.data.bottomLeft} docker="bottom-left" onClose={ctx.emit.bind(ctx, 'close')}/>
            <NoticeBox data={props.data.bottomRight} docker="bottom-right" onClose={ctx.emit.bind(ctx, 'close')}/>
        </div>;
    }
});
