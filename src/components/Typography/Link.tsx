import { computed, defineComponent, PropType } from "vue";
import type { ParagraphProps } from "./paragraph.d";
import { useStyle } from "../use/useStyle";


export default defineComponent({
    name: 'Link',
    props: {
        type: {type: String as PropType<ParagraphProps['type']>},
        disabled: {type: Boolean as PropType<ParagraphProps['disabled']>},
        link: {type: String as PropType<ParagraphProps['link']>},
        icon: {type: Object as PropType<ParagraphProps['icon']>},
        mark: {type: Boolean as PropType<ParagraphProps['mark']>},
        code: {type: Boolean as PropType<ParagraphProps['code']>},
        underline: {type: Boolean as PropType<ParagraphProps['underline']>},
        deleted: {type: Boolean as PropType<ParagraphProps['deleted']>},
        strong: {type: Boolean as PropType<ParagraphProps['strong']>},
        size: {type: String as PropType<ParagraphProps['size']>},
        gradient: {type: Array as PropType<ParagraphProps['gradient']>},
    },
    setup (props, {slots}) {
        const size = () => props.size || 'normal';
        const classList = computed(() => ({
            'cm-text cm-text-link': true,
            [`cm-text-${props.type}`]: props.type,
            'cm-text-disabled': props.disabled,
            'cm-text-underline': props.underline,
            'cm-text-deleted': props.deleted,
            'cm-text-strong': props.strong,
            [`cm-text-${size()}`]: size(),
        }));

        const style = computed(() => ({
            'background-image': props.gradient ? `linear-gradient(${props.gradient.join(',')})` : '',
            '-webkit-text-fill-color': props.gradient ? 'transparent' : ''
        }));

        return () => <span class={classList.value}><a style={style.value} href={props.link}>{props.icon}{slots.default?.()}</a></span>;
    },
});
