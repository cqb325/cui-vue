export type ParagraphProps = {
    type?: 'default'|'secondary'|'warning'|'error'|'success'|'primary',
    disabled?: boolean,
    link?: string,
    icon?: any,
    mark?: boolean,
    code?: boolean,
    underline?: boolean,
    deleted?: boolean,
    strong?: boolean,
    size?: 'small'|'default'|'large',
    spacing?: 'extended',
    copyable?: boolean,
    gradient?: string[]
    onCopy?: Function,
    copyText?: string,
    inline?: boolean,
    prefix?: "bar"|"dot",
    prefixWidth?: number,
    prefixGap?: number,
    prefixColor?: string | string[],
    heading?: 1|2|3|4|5|6
}