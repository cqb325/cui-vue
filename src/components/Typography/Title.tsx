import type { ParagraphProps } from "./paragraph.d";
import { computed, defineComponent } from "vue";

export default defineComponent({
    name: 'Title',
    props: ['heading', 'type', 'inline', 'prefix', 'gradient', 'prefixWidth', 'prefixGap', 'prefixColor'],
    setup (props: ParagraphProps, {slots}) {
        const heading = computed(() => props.heading || 1);

        const classList = computed(() => ({
            'cm-typograghy-title': true,
            [`cm-typograghy-h${heading.value}`]: true,
            'cm-typograghy-title-inline': props.inline,
            [`cm-typograghy-title-prefix-${props.prefix}`]: props.prefix,
        }));

        const style = computed(() => ({
            'background-image': props.gradient ? `linear-gradient(${props.gradient.join(',')})` : '',
            '-webkit-text-fill-color': props.gradient ? 'transparent' : '',
            [`--cm-title-prefix-width`]: props.prefixWidth ?? (props.prefix === 'bar' ? 4 : 8),
            [`--cm-title-prefix-gap`]: props.prefixGap ?? 16,
            [`--cm-title-prefix-color`]: typeof props.prefixColor === 'string' ? props.prefixColor : "",
            [`--cm-title-prefix-gradient`]: props.prefixColor instanceof Array ? props.prefixColor.join(',') : "",
        }));
        const options = [
            () => <h1 class={classList.value} style={style.value}>{slots.default && slots.default()}</h1>,
            () => <h2 class={classList.value} style={style.value}>{slots.default && slots.default()}</h2>,
            () => <h3 class={classList.value} style={style.value}>{slots.default && slots.default()}</h3>,
            () => <h4 class={classList.value} style={style.value}>{slots.default && slots.default()}</h4>,
            () => <h5 class={classList.value} style={style.value}>{slots.default && slots.default()}</h5>,
            () => <h6 class={classList.value} style={style.value}>{slots.default && slots.default()}</h6>,
        ];
        return () => options[heading.value - 1]();
    }
});
