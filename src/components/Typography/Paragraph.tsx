import useCopy from "../use/useCopy";
import type { ParagraphProps } from "./paragraph.d";
import { computed, defineComponent, ref } from "vue";
import { FeatherCheck, FeatherCopy } from "cui-vue-icons/feather";

export default defineComponent({
    name: 'Paragraph',
    props: ['size', 'type', 'spacing', 'copyText', 'copyable'],
    emits: ['copy'],
    setup (props: ParagraphProps, {slots, emit}) {
        const copyed = ref(false);

        const size = computed(() => props.size || 'normal');
        const classList = computed(() => ({
            'cm-typograghy-paragraph': true,
            [`cm-typograghy-paragraph-${size.value}`]: size.value,
            [`cm-typograghy-paragraph-${props.type}`]: !!props.type,
            'cm-typograghy-extended': props.spacing === 'extended',
        }));
        const target = ref<HTMLParagraphElement>(null);
        async function onCopy () {
            const ret = await useCopy(props.copyText ?? target.value.innerText);
            copyed.value = ret;
            if (ret) {
                emit('copy');
                setTimeout(() => {
                    copyed.value = false;
                }, 4000);
            }
        }

        return () => <p class={classList.value} ref={target}>
            {slots.default && slots.default()}
            {
                props.copyable ? (copyed.value ? <span class="cm-typograghy-copyed"><FeatherCheck/></span>
                    : <span class="cm-typograghy-copy" onClick={onCopy}><FeatherCopy /></span>) : null
            }
        </p>;
    },
});
