import { computed, defineComponent, PropType } from "vue";
import type { ParagraphProps } from "./paragraph.d";

export default defineComponent({
    name: 'Text',
    props: ['type', 'gradient', 'size', 'onCopy', 'mark', 'disabled', 'link', 'code', 'underline', 'deleted', 'strong', 'size', 'icon'],
    setup (props: ParagraphProps, { slots }) {
        const size = () => props.size || 'normal';
        const classList = computed(() => ({
            'cm-text': true,
            [`cm-text-${props.type}`]: props.type,
            'cm-text-disabled': props.disabled,
            'cm-text-underline': props.underline,
            'cm-text-link': props.link,
            'cm-text-deleted': props.deleted,
            'cm-text-strong': props.strong,
            [`cm-text-${size()}`]: size(),
        }));

        const style = computed(() => ({
            'background-image': props.gradient ? `linear-gradient(${props.gradient.join(',')})` : '',
            '-webkit-text-fill-color': props.gradient ? 'transparent' : ''
        }));

        return () => <span class={classList.value} style={style.value}>{props.mark ? <mark>{slots.default && slots.default()}</mark> : props.code ? <code>{slots.default && slots.default()}</code> : props.link ? <a href={props.link}>{props.icon}<span>{slots.default && slots.default()}</span></a> : slots.default && slots.default()}</span>;
    }
});
