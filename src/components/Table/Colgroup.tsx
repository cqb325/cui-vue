import { PropType, defineComponent, inject, watch, watchEffect } from 'vue';
import { TableContextKey, TableStore } from '.';

type ColgroupProps = {
    data: TableStore
}

export default defineComponent({
    name: 'Colgroup',
    props: {
        data: {
            type: Object as PropType<TableStore>,
            required: true
        }
    },
    setup (props: ColgroupProps) {
        const ctx: any = inject(TableContextKey, null);

        return () => <colgroup class="cm-table-colgroup">
            {
                ctx.store.columns.map((col, index) => <col key={index} class="cm-table-col" width={col._width} />)
            }
        </colgroup>;
    }
});
