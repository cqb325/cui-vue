import { PropType, computed, defineComponent, inject, ref, watchEffect } from "vue";
import Icon from "../Icon";
import Collapase from "../inner/Collapase";

export interface AccordionItemProps {
    name?: string,
    title?: any,
    icon?: any,
}

export default defineComponent({
    name: "AccordionItem",
    props: {
        name: {type: String as PropType<AccordionItemProps['name']>},
        title: {type: [String, Object] as PropType<AccordionItemProps['title']>},
        icon: {type: Object as PropType<AccordionItemProps['icon']>},
    },
    setup (props, { slots }) {
        const ctx: any = inject('CMAccordionContext', null);
        const onSelect = ctx?.onSelect;
        const multi = ctx?.flex ? false : ctx?.multi;
        const opened = ref(false);
        const end = ref(false);

        const onTitleClick = () => {
            let v;
            let open = false;
            if (!multi) {
                if (ctx?.activeKey.value === props.name) {
                    if (ctx?.flex) {
                        return;
                    }
                    v = '';
                    open = false;
                } else {
                    v = props.name;
                    open = true;
                }
            } else {
                const currentKey = ctx?.activeKey.value;
                if (currentKey instanceof Array) {
                    if (currentKey.includes(props.name)) {
                        const index = currentKey.indexOf(props.name);
                        currentKey.splice(index, 1);
                        v = [].concat(currentKey);
                        open = false;
                    } else {
                        currentKey.push(props.name);
                        v = [].concat(currentKey);
                        open = true;
                    }
                }
            }
            ctx.activeKey.value = v;
            onSelect && onSelect(props.name, open, v);
        };


        watchEffect(() => {
            const currentKey = ctx.activeKey.value;
            let open = false;
            if (!multi) {
                open = currentKey === props.name;
            } else {
                open = currentKey.includes(props.name);
            }
            end.value = false;
            opened.value = open;
        });

        const classList = computed(() => ({
            'cm-accordion-item': true,
            'cm-accordion-item-active': opened.value,
            'cm-accordion-item-full': opened.value && end.value
        }));

        const onEnd = () => {
            end.value = true;
        };

        return () => <div class={classList.value}>
            <div class="cm-accordion-title" onClick={onTitleClick}>
                {props.icon}
                <div class="cm-accordion-item-title-text">{props.title}</div>
                <Icon class="cm-accordion-title-arrow" name="chevron-right"/>
            </div>
            <Collapase open={opened.value} onEnd={onEnd}>
                <div class="cm-accordion-content">{slots.default?.()}</div>
            </Collapase>
        </div>;
    },
});
