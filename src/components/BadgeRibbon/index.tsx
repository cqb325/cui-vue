import { computed, defineComponent, PropType } from "vue";
import { isColor } from "../utils/assist";

export interface BadgeRibbonProps {
    text?: any
    status?: 'success'|'error'|'warning'|'primary'|'info',
    align?: 'start'|'end'
    color?: string | 'blue'|'green'|'red'|'yellow'|'pink'|'magenta'|'volcano'|'orange'|'gold'|'lime'|'cyan'|'geekblue'|'purple'
}

export default defineComponent({
    name: 'BadgeRibbon',
    props: {
        text: {type: [String, Number, Object] as PropType<BadgeRibbonProps['text']>},
        status: {type: String as PropType<BadgeRibbonProps['status']>},
        align: {type: String as PropType<BadgeRibbonProps['align']>},
        color: {type: String as PropType<BadgeRibbonProps['color']>}
    },
    setup (props: BadgeRibbonProps, {slots}) {
        const align = props.align ?? 'end';
        const classList = computed(() => ({
            'cm-badge-ribbon': true,
            [`cm-badge-ribbon-status-${props.status}`]: !!props.status,
            [`cm-badge-ribbon-${align}`]: !!align,
            [`cm-badge-ribbon-${props.color}`]: !!props.color && !isColor(props.color)
        }));

        const style = computed(() => ({
            '--cui-background-color': isColor(props.color) ? props.color : ''
        }));

        return () => <div class={classList.value} style={style.value}>
            {slots.default?.()}
            <div class="cm-badge-ribbon-inner">
                <span class="cm-badge-ribbon-text">{props.text}</span>
                <div class="cm-badge-ribbon-corner" />
            </div>
        </div>;
    },
});
