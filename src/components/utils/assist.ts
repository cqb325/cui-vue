export function findComponentUpward (wantFind, _context) {
    const parent = _context.parent;
    if (parent && parent.type.name && parent.type.name === wantFind) {
        return parent;
    } else {
        if (parent.parent) {
            return findComponentUpward(wantFind, parent);
        }
    }
    return null;
}

/**
 * 划到顶部
 * @param el
 * @param from
 * @param to
 * @param duration
 * @param endCallback
 */
export function scrollTop (el: any, from = 0, to: number, duration = 500, endCallback?: Function) {
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = (
            function (callback) {
                return window.setTimeout(callback, 1000/60);
            }
        );
    }
    const difference = Math.abs(from - to);
    const step = Math.ceil(difference / duration * 50);

    function scroll (start: number, end: number, step: number) {
        if (start === end) {
            endCallback && endCallback();
            return;
        }

        let d = (start + step > end) ? end : start + step;
        if (start > end) {
            d = (start - step < end) ? end : start - step;
        }

        if (el === window) {
            window.scrollTo(d, d);
        } else {
            el.scrollTop = d;
        }
        window.requestAnimationFrame(() => scroll(d, end, step));
    }
    scroll(from, to, step);
}

/**
 * 获取安全的随机数
 */
export function getRandomIntInclusive (min: number, max: number) {
    const randomBuffer = new Uint32Array(1);
    window.crypto.getRandomValues(randomBuffer);
    const randomNumber = randomBuffer[0] / (0xffffffff + 1);
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(randomNumber * (max - min + 1)) + min;
}

export function getRandomNumber () {
    const randomBuffer = new Uint32Array(1);
    window.crypto.getRandomValues(randomBuffer);
    return randomBuffer[0] / (0xffffffff + 1);
}

export const uuidv4 = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    .replace(/[xy]/g, c => (c === 'x' ? 0xF & getRandomNumber() * 16 : 0x3 & getRandomNumber() * 16 | 8)
        .toString(16)).replace(/-/g, '');

/**
 * 字符串是否为颜色值
 * @param strColor
 * @returns
 */
export function isColor (strColor: string | undefined): boolean {
    if (strColor && (strColor.startsWith('#') || strColor.startsWith('rgb') || strColor.startsWith('hsl'))) {
        const s = new Option().style;
        s.color = strColor as string;
        return s.color.startsWith('rgb');
    }
    return false;
}
