import { FeatherChevronRight } from "cui-vue-icons/feather";
import { defineComponent } from "vue";

export default defineComponent({
    name: 'PageNext',
    props: {
        currentIndex: Number,
        disabled: Boolean,
    },
    emits: ['click'],
    setup (props, { emit }) {
        const onClick = () => {
            emit('click');
        };
        const classList = () => ({
            'cm-pagination-num': true,
            'cm-pagination-next': true,
            'cm-pagination-num-disabled': props.disabled
        });
        return () => <li onClick={onClick} class={classList()}>
            <FeatherChevronRight />
        </li>;
    }
});
