import { FeatherChevronLeft } from "cui-vue-icons/feather";
import { defineComponent } from "vue";

export default defineComponent({
    name: 'PagePrev',
    props: {
        currentIndex: Number,
        current: Number,
    },
    emits: ['click'],
    setup (props, { emit }) {
        const onClick = () => {
            emit('click');
        };
        const classList = () => ({
            'cm-pagination-num': true,
            'cm-pagination-prev': true,
            'cm-pagination-num-disabled': props.current
        });
        return () => <li onClick={onClick} class={classList()}>
            <FeatherChevronLeft/>
        </li>;
    }
});
