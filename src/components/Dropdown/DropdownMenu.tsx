import { computed, defineComponent, inject } from "vue";

export default defineComponent({
    name: "DropdownMenu",
    setup (props, { slots }) {
        const ctx: any = inject('dropdownConext', {});
        const style = computed(() => ({
            'background': ctx?.gradient ? `linear-gradient(${ctx.gradient?.join(',')})` : ''
        }));
        return () => <ul class="cm-dropdown-list" style={style.value}>{slots.default && slots.default()}</ul>;
    }
});
