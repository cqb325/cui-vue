import { computed, defineComponent, inject, PropType } from "vue";
import { isColor } from "../utils/assist";
import { FeatherChevronRight } from "cui-vue-icons/feather";

export interface DropdownItemProps {
    disabled?: boolean
    name?: string
    divided?: boolean
    icon?: JSX.Element
    arrow?: boolean
    data?: any
    theme?: string|'primary'|'secondary'|'tertiary'|'success'|'warning'|'error'|'info'|'light'
    selected?: boolean
}



export default defineComponent({
    name: 'DropdownItem',
    props: {
        disabled: {type: Boolean as PropType<DropdownItemProps['disabled']>, default: false},
        divided: {type: Boolean as PropType<DropdownItemProps['divided']>, default: false},
        name: {type: String as PropType<DropdownItemProps['name']>, default: ''},
        icon: {type: Object as PropType<DropdownItemProps['icon']>, default: null},
        arrow: {type: Boolean as PropType<DropdownItemProps['arrow']>, default: false},
        data: {type: Object as PropType<DropdownItemProps['data']>, default: null},
        theme: {type: String as PropType<DropdownItemProps['theme']>, default: ''},
        selected: {type: Boolean as PropType<DropdownItemProps['selected']>, default: false}
    },
    setup (props: DropdownItemProps, {slots}) {
        const theme = isColor(props.theme) ? '' : props.theme;
        const classList = computed(() => ({
            'cm-dropdown-item': true,
            'cm-dropdown-item-disabled': props.disabled,
            'cm-dropdown-item-divided': props.divided,
            'cm-dropdown-item-selected': props.selected,
            'cm-dropdown-item-with-arrow': props.arrow,
            [`cm-dropdown-item-${theme}`]: theme,
        }));

        const ctx: any = inject("dropdownConext", {});

        const onClick= (e: any) => {
            if (props.disabled) {
                return;
            }
            e.preventDefault();
            e.stopPropagation();
            ctx?.onSelect(props.name);
        };

        const style = computed(() =>({
            '--cui-dropdown-text-color': isColor(props.theme) ? props.theme : '',
        }));

        return () => <li class={classList.value} style={style.value} onClick={onClick}>
            {props.icon ? <span class="cm-dropdown-item-icon">{props.icon}</span> : null}
            {slots.default && slots.default()}
            {props.arrow ? <FeatherChevronRight class="cm-dropdown-item-arrow"/> : null}
        </li>;
    }
});
