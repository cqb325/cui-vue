import { PropType, computed, defineComponent } from "vue";

export interface TimelineItemProps {
    color?: 'blue'|'green'|'red'|'yellow',
    fill?: boolean,
    icon?: any,
    class?: string,
    time?: string
};

export default defineComponent({
    name: 'TimelineItem', // vue component name
    props: {
        color: {type: String as PropType<TimelineItemProps['color']>, default: 'blue'},
        class: {type: String as PropType<TimelineItemProps['class']>},
        fill: {type: Boolean as PropType<TimelineItemProps['fill']>, default: undefined},
        icon: {type: [String, Object] as PropType<TimelineItemProps['icon']>},
        time: {type: String as PropType<TimelineItemProps['time']>},
    },
    setup (props, {slots}) {
        const classList = computed(() => ({
            'cm-timeline-item-head': true,
            [`cm-timeline-item-head-${props.color}`]: props.color,
            'cm-timeline-item-head-custom': props.icon,
            'cm-timeline-item-head-fill': props.fill
        }));

        return () => <div class="cm-timeline-item">
            <div class="cm-timeline-item-tail"></div>
            <div class={classList.value}>
                {props.icon || slots.icon?.()}
            </div>
            <div class="cm-timeline-item-content">
                {slots.default?.()}
                {
                    props.time
                        ? <div class="cm-timeline-time">
                            {props.time}
                        </div>
                        : null
                }
            </div>
        </div>;
    }
});
