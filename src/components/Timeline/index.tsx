import { computed, defineComponent } from "vue";
import TimelineItem from "./TimelineItem";

export interface TimelineProps {
    mode?: 'left'|'right'|'alternate'|'center'
}

const Timeline = defineComponent({
    name: 'Timeline',
    props: {
        mode: {
            type: String as () => 'left'|'right'|'alternate'|'center',
        }
    },
    setup (props, {slots}) {
        const classList = computed(() => ({
            'cm-timeline': true,
            [`cm-timeline-${props.mode}`]: props.mode
        }));
        return () => <div class={classList.value}>
            {slots.default?.()}
        </div>;
    }
});

Timeline.Item = TimelineItem;

export default Timeline;
