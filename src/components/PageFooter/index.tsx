import { defineComponent } from "vue";

export default defineComponent({
    name: 'PageFooter',
    setup (props, { slots }) {
        return () => <div class="cm-page-footer">
            {slots.default?.()}
        </div>;
    }
});

export * from './Floor';
export * from './Navigations';
