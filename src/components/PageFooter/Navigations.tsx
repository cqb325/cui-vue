import { defineComponent } from "vue";

export const FooterNavigations = defineComponent({
    name: "FooterNavigations",
    setup (props, { slots }) {
        return () => <div class="cm-page-footer-navigations">
            {slots.default?.()}
        </div>;
    },
});

export const FooterNavigation = defineComponent({
    name: "FooterNavigation",
    props: {
        head: {
            type: [String, Object],
        },
    },
    setup (props, { slots }) {
        return () => <div class="cm-page-footer-navigation">
            <dl>
                <dt>{props.head || slots.head?.()}</dt>
                {slots.default?.()}
            </dl>
        </div>;
    },
});

export interface FooterNavigationLink {
    icon?: any
    link: string;
    style?: any;
}

const Link = defineComponent({
    name: "Link",
    props: {
        icon: {
            type: Object,
        },
        link: {
            type: String,
            required: true,
        },
        style: {
            type: Object,
        },
    },
    setup (props, {slots}) {
        return () => <dd class="cm-page-footer-navigation-link">
            <a href={props.link} target="_blank" style={props.style}>{props.icon}{slots.default?.()}</a>
        </dd>;
    }
});

FooterNavigation.Link = Link;
