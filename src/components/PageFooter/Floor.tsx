import { PropType, computed, defineComponent } from "vue";

export interface FloorProps {
    center?: boolean;
    padding?: string;
    color?: string;
    dividerTop?: boolean;
    dividerBottom?: boolean;
}

export default defineComponent({
    name: "Floor",
    props: {
        center: {type: Boolean as PropType<FloorProps['center']>, default: false},
        padding: {type: String as PropType<FloorProps['padding']>, default: undefined},
        color: {type: String as PropType<FloorProps['color']>, default: undefined},
        dividerTop: {type: Boolean as PropType<FloorProps['dividerTop']>, default: undefined},
        dividerBottom: {type: Boolean as PropType<FloorProps['dividerBottom']>, default: undefined},
    },
    setup (props: FloorProps, {slots}) {
        const classList = computed(() => ({
            'cm-footer-floor': true,
            'cm-footer-floor-center': props.center,
            'cm-footer-floor-divider-top': props.dividerTop,
            'cm-footer-floor-divider-bottom': props.dividerBottom,
        }));
        return () => <div class={classList.value} style={{padding: props.padding, color: props.color}}>
            {slots.default?.()}
        </div>;
    }
});
