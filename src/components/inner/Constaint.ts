export enum TreeCheckMod {
    // 只返回全选数据，包含父节点和子节点
    FULL = 'FULL',
    // 返回全部选中子节点和部分选中的父节点
    HALF = 'HALF',
    // 只返回选中子节点
    CHILD = 'CHILD',
    // 如果父节点下所有子节点全部选中，只返回父节点
    SHALLOW = 'SHALLOW'
}
