import { PropType, VueElement, computed, defineComponent, ref } from "vue";
import { F7ExclamationmarkTriangleFill, F7CheckmarkAltCircleFill, F7XmarkCircleFill, F7InfoCircleFill } from "cui-vue-icons/f7";
import { FeatherX } from "cui-vue-icons/feather";

export interface BannerProps {
    type: 'warning'|'info'|'success'|'error'
    bordered?: boolean
    icon?: any
    closeIcon?: any
    title?: any
    fullMode?: boolean
    modelValue?: boolean
}

export default defineComponent({
    name: 'Banner',
    props: {
        type: {type: String as PropType<BannerProps['type']>},
        bordered: {type: Boolean as PropType<BannerProps['bordered']>},
        icon: {type: VueElement as PropType<BannerProps['icon']>},
        closeIcon: {type: VueElement as PropType<BannerProps['closeIcon']>},
        title: {type: [String, Object] as PropType<BannerProps['title']>},
        fullMode: {type: Boolean as PropType<BannerProps['fullMode']>},
        modelValue: {type: Boolean as PropType<BannerProps['modelValue']>, default: true},
    },
    setup (props: BannerProps, { emit, slots }) {
        const visible = ref(props.modelValue);
        const classList = computed(() => ({
            'cm-banner': true,
            [`cm-banner-${props.type}`]: props.type,
            [`cm-banner-bordered`]: props.bordered,
            [`cm-banner-full`]: props.fullMode ?? true,
            [`cm-banner-not-full`]: props.fullMode === false,
        }));

        const getIconByType = () => {
            let icon = null;
            switch (props.type) {
            case 'info': {
                icon = <F7InfoCircleFill />;
                break;
            }
            case 'success': {
                icon = <F7CheckmarkAltCircleFill/>;
                break;
            }
            case 'warning': {
                icon = <F7ExclamationmarkTriangleFill />;
                break;
            }
            case 'error': {
                icon = <F7XmarkCircleFill/>;
                break;
            }
            default: {
                icon = <F7InfoCircleFill/>;
            }
            }
            return icon;
        };

        const onClickClose = () => {
            visible.value = false;
            emit('update:modelValue', false);
        };

        const icon = props.icon === null ? null : props.icon ?? getIconByType();

        return () => (
            visible.value ?
                <div class={classList.value}>
                    <div class="cm-banner-body">
                        <div class="cm-banner-content">
                            {
                                icon ? <div class="cm-banner-icon">{icon}</div> : null
                            }
                            <div class="cm-banner-content-body">
                                {
                                    props.title ? <div class="cm-banner-title">
                                        {props.title}
                                    </div> : null
                                }
                                {
                                    slots.default ? <div class="cm-banner-desc">{slots.default()}</div> : null
                                }
                            </div>
                        </div>
                        {
                            props.closeIcon !== null ?
                                <span class="cm-banner-close" onClick={onClickClose}>{props.closeIcon ?? <FeatherX/>}</span>
                                : null
                        }
                    </div>
                    {
                        slots.extra ?
                            <div class="cm-banner-extra">
                                {slots.extra()}
                            </div> : null
                    }
                </div>
                : null
        );
    }
});
