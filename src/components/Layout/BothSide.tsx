import { defineComponent } from "vue";

export default defineComponent({
    name: 'BothSide',
    setup (props, { slots }) {
        return () => <div class="cm-both-side">{slots.default?.()}</div>;
    }
});
