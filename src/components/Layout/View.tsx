import { HTMLAttributes, computed, defineComponent } from "vue";

export interface ViewProps extends HTMLAttributes {
    size?: string,
}

export const View = defineComponent({
    name: "View",
    props: ['size'],
    setup (props, { slots }) {
        const newStyle = computed(()=> ({flex: `0 1 ${props.size}`}));
        return () => <div class="cm-view" style={newStyle.value}>{slots.default && slots.default()}</div>;
    },
});

export const HView = defineComponent({
    name: "HView",
    props: ['size'],
    setup (props, {slots}) {
        return () => <View class="cm-h-view" {...props}>{slots.default && slots.default()}</View>;
    },
});

export const VView = defineComponent({
    name: "VView",
    props: ['size'],
    setup (props, {slots}) {
        return () => <View class="cm-v-view" {...props}>{slots.default && slots.default()}</View>;
    },
});

export const FixedView = defineComponent({
    name: "FixedView",
    props: ['size'],
    setup (props, {slots}) {
        return () => <View class="cm-fixed-view" {...props}>{slots.default && slots.default()}</View>;
    },
});
