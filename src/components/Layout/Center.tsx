import { HTMLAttributes, computed, defineComponent } from "vue";

export interface CenterProps extends HTMLAttributes {
    width?: number,
    height?: number,
}

export default defineComponent({
    name: "Center",
    props: ['width', 'height'],
    setup (props: CenterProps, {slots}) {
        const newStyle = computed(() => ({width: props.width + 'px', height: props.height + 'px'}));
        return () => <div class="cm-view-center" style={newStyle.value}>{slots.default && slots.default()}</div>;
    }
});
