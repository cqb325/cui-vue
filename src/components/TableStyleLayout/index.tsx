import { defineComponent, PropType, provide } from 'vue';

export * from './Row';
export * from './Col';
export * from './Label';
export * from './Value';

export interface TableStyleLayoutProps {
    labelWidth?: number
}

export interface TableStyleLayoutContextProps {
    labelWidth: number
}

export const TableStyleLayoutContextKey = Symbol('TableStyleLayoutContextKey');

export default defineComponent({
    name: 'TableStyleLayout',
    props: {
        labelWidth: { type: Number as PropType<TableStyleLayoutProps['labelWidth']>, default: 100 }
    },
    setup (props, { slots }) {
        provide(TableStyleLayoutContextKey, {
            labelWidth: props.labelWidth
        });
        return () => <div class="cm-table-style-layout">{slots.default?.()}</div>;
    }
});
