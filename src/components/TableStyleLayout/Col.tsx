import { computed, defineComponent, PropType } from "vue";

export interface TableStyleLayoutColProps{
    flex?: number
}

export const TableStyleLayoutCol = defineComponent({
    name: 'TableStyleLayoutCol',
    props: {
        flex: { type: Number as PropType<TableStyleLayoutColProps['flex']>, default: undefined }
    },
    setup (props, { slots }) {
        const style: any = computed(() => {
            const obj: any = {};
            if (props.flex != undefined) {
                obj.flex = `1 1 ${props.flex * 100}%`;
                obj['min-width'] = 0;
            }
            return obj;
        });
        return () => (
            <div class="cm-table-style-layout-col" style={style.value}>{slots.default?.()}</div>
        );
    }
});
