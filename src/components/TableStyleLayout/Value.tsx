import { computed, defineComponent, PropType } from "vue";

export interface TableStyleLayoutValueProps {
    verticalAlign?: 'start' | 'center' | 'end'
    column?: boolean
    row?: boolean
    justify?: 'flex-start' | 'flex-end' | 'center'
}

export const TableStyleLayoutValue = defineComponent({
    name: 'TableStyleLayoutValue',
    props: {
        verticalAlign: {type: String as PropType<TableStyleLayoutValueProps['verticalAlign']>},
        column: {type: Boolean as PropType<TableStyleLayoutValueProps['column']>},
        row: {type: Boolean as PropType<TableStyleLayoutValueProps['row']>},
        justify: {type: String as PropType<TableStyleLayoutValueProps['justify']>}
    },
    setup (props, { slots }) {
        const classList = computed(() => ({
            'cm-table-style-layout-value': true,
            column: !!props.column,
            row: !!props.row,
            [`cm-table-style-layout-value-${props.verticalAlign}`]: !!props.verticalAlign
        }));
        const style = computed(() => ({
            'justify-content': props.justify
        }));
        return () => <div class={classList.value} style={style.value}>{slots.default?.()}</div>;
    }
});
