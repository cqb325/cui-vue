import { defineComponent } from "vue";

export const TableStyleLayoutRow = defineComponent({
    name: 'TableStyleLayoutRow',
    props: {
        class: String,
        classList: Function
    },
    setup (props, { slots }) {
        return () => <div class="cm-table-style-layout-row">{slots.default?.()}</div>;
    }
});
