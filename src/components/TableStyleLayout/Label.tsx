import { computed, defineComponent, inject, PropType } from "vue";
import { TableStyleLayoutContextKey } from ".";

export interface TableStyleLayoutLabelProps {
    verticalAlign?: 'start' | 'center' | 'end'
    width?: number
    required?: boolean
}

export const TableStyleLayoutLabel = defineComponent({
    name: 'TableStyleLayoutLabel',
    props: {
        verticalAlign: {type: String as PropType<TableStyleLayoutLabelProps['verticalAlign']>},
        width: {type: Number as PropType<TableStyleLayoutLabelProps['width']>},
        required: {type: Boolean as PropType<TableStyleLayoutLabelProps['required']>}
    },
    setup (props, { slots }) {
        const ctx: any = inject(TableStyleLayoutContextKey);
        const classList = computed(() => ({
            'cm-table-style-layout-label': true,
            [`cm-table-style-layout-label-${props.verticalAlign}`]: props.verticalAlign,
            required: !!props.required
        }));
        const style = computed(() => {
            const labelWidth: number = ctx.labelWidth;
            return {
                flex: `0 0 ${(props.width || labelWidth) + 'px'}`
            };
        });
        return () => <div class={classList.value} style={style.value}>{slots.default?.()}</div>;
    }
});

