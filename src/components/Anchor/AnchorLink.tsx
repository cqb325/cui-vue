import { PropType, defineComponent, inject } from "vue";

export interface AnchorLinkProps {
    href: string
    title: any,
}

export default defineComponent({
    name: "AnchorLink",
    props: {
        href: {type: String as PropType<AnchorLinkProps['href']>, default: undefined},
        title: {type: [String, Object] as PropType<AnchorLinkProps['title']>, default: undefined},
    },
    setup (props: AnchorLinkProps, {slots}) {
        const ctx = inject('CMAnchorContext', null);

        const gotoAnchor = (e) => {
            ctx?.gotoAnchor(props.href, e);
        };

        ctx.addLink({
            href: props.href,
            title: props.title,
        });

        return () => <div class="cm-anchor-link">
            <a class="cm-anchor-link-title" href={props.href} data-scroll-offset={ctx?.scrollOffset || 0} data-href={props.href}
                onClick={gotoAnchor} title={props.title as string}>{ props.title }</a>
            {slots.default?.()}
        </div>;
    },
});
