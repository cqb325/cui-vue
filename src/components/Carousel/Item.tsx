import { defineComponent, inject } from "vue";
import { CarouselContextKey } from ".";
import { createUniqueId } from "../use/createUniqueId";

export default defineComponent({
    name: 'InnerCarouselItem',
    props: null,
    setup (props, { slots }) {
        const ctx: any = inject(CarouselContextKey);
        const id = createUniqueId();

        ctx?.addItem({
            id,
            ...props,
            content: slots.default
        });

        return () => null;
    }
});
