import { PropType, computed, defineComponent } from "vue";

export interface BadgeProps {
    count?: number,
    dot?: boolean,
    overflowCount?: number,
    text?: string,
    status?: 'success'|'error'|'processing'|'warning'|'default',
    color?: string | 'blue'|'green'|'red'|'yellow'|'pink'|'magenta'|'volcano'|'orange'|'gold'|'lime'|'cyan'|'geekblue'|'purple',
    type?: 'primary'|'success'|'normal'|'info'|'error'|'warning'
}

function isColor (strColor: string | undefined): boolean {
    if (strColor && (strColor.startsWith('#') || strColor.startsWith('rgb') || strColor.startsWith('hsl'))) {
        const s = new Option().style;
        s.color = strColor as string;
        return s.color.startsWith('rgb');
    }
    return false;
}

export default defineComponent({
    name: 'Badge',
    props: {
        count: {type: Number as PropType<BadgeProps['count']>, default: undefined},
        dot: {type: Boolean as PropType<BadgeProps['dot']>},
        overflowCount: {type: Number as PropType<BadgeProps['overflowCount']>},
        text: {type: String as PropType<BadgeProps['text']>, default: undefined},
        status: {type: String as PropType<BadgeProps['status']>},
        color: {type: String as PropType<BadgeProps['color']>},
        type: {type: String as PropType<BadgeProps['type']>}
    },
    setup (props: BadgeProps, { slots }) {
        const overflowCount = props.overflowCount ?? 99;
        const classList = computed(() => ({
            'cm-badge': true,
            'cm-badge-status': props.status
        }));
        const showCount = computed(() => {
            return props.count && props.count > overflowCount ? Math.min(overflowCount, props.count) + '+' : props.count;
        });

        const statusClass = computed(() => ({
            'cm-badge-status-dot': true,
            [`cm-badge-status-${props.status}`]: !!props.status,
            [`cm-badge-status-${props.color}`]: !!props.color && props.color.indexOf('#') === -1
        }));

        const statusStyle = computed(() => ({
            'background-color': isColor(props.color) ? props.color : ''
        }));

        const countClass = computed(() => ({
            'cm-badge-count': true,
            [`cm-badge-count-${props.type}`]: !!props.type
        }));

        return () => <span class={classList.value}>
            {slots.default?.()}
            {
                !props.status && !props.color
                    ? <>
                        {props.count !== undefined || props.text !== undefined
                            ? <sup class={countClass.value}>{showCount.value}{props.text}</sup>: null}
                        {props.dot
                            ? <sup class="cm-badge-dot"></sup> : null}
                    </>
                    : <>
                        <span class={statusClass.value} style={statusStyle.value}></span>
                        <span class="cm-badge-status-text">{props.text}</span>
                    </>
            }
        </span>;
    }
});
