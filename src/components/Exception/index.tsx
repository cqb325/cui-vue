import { typeImages } from "./typeImages";
import { PropType, VNode, computed, defineComponent } from "vue";
import Image from "../Image";
import Text from "../Typography/Text";

type ExceptionProps = {
    type?: '404'|'403'|'500'|'empty'|'fail'|'deny',
    typeImage?: any,
    desc?: string,
    width?: number,
    height?: number,
    showDesc?: boolean,
    action?: VNode,
}
export default defineComponent({
    name: 'Exception',
    props: {
        type: {type: String as PropType<ExceptionProps['type']>, required: true},
        typeImage: {type: Object as PropType<ExceptionProps['typeImage']>},
        desc: {type: String as PropType<ExceptionProps['desc']>},
        showDesc: {type: Boolean as PropType<ExceptionProps['showDesc']>, default: true},
        width: {type: Number as PropType<ExceptionProps['width']>},
        height: {type: Number as PropType<ExceptionProps['height']>},
        action: {type: Object as PropType<ExceptionProps['action']>},
    },
    setup (props) {
        const classList = computed(() => ({
            'cm-exception': true,
            [`cm-exception-${props.type}`]: !!props.type
        }));

        return () => <div class={classList.value}>
            <div class="cm-exception-img">
                {
                    props.typeImage
                        ? <Image src={props.typeImage} width={props.width} height={props.height}/>
                        : <Image src={typeImages(props.type)} width={props.width} height={props.height}/>
                }
            </div>
            <div class="cm-exception-info">
                {
                    props.showDesc
                        ? <div class="cm-exception-desc">
                            {
                                props.type === '403'
                                    ? <Text size="large">{props.desc ?? '抱歉，你无权访问该页面'}</Text>
                                    : null
                            }
                            {
                                props.type === '404'
                                    ? <Text size="large">{props.desc ?? '抱歉，你访问的页面不存在'}</Text>
                                    : null
                            }
                            {
                                props.type === '500'
                                    ? <Text size="large">{props.desc ?? '抱歉，服务器出错了'}</Text>
                                    : null
                            }
                            {
                                props.type === 'empty'
                                    ? <Text size="large">{props.desc ?? '暂无数据'}</Text>
                                    : null
                            }
                            {
                                props.type === 'fail'
                                    ? <Text size="large">{props.desc ?? '授权失败'}</Text>
                                    : null
                            }
                            {
                                props.type === 'deny'
                                    ? <Text size="large">{props.desc ?? '拒绝访问'}</Text>
                                    : null
                            }
                        </div>
                        : null
                }
                {
                    props.action
                        ? <div class="cm-exception-action">
                            {props.action}
                        </div>
                        : null
                }
            </div>
        </div>;
    }
});
