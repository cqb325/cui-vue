import { PropType, computed, defineComponent } from "vue";

export type BasicProps = {
    type?: string;
    width?: string | string[];
    height?: string;
    inline?: boolean;
};

export type AvatarProps = {
    size?: 'extra-small'|'small'|'medium'|'large'|'extra-large'|number,
    shape?: 'circle'|'square'
}

export type GenericProps = BasicProps & AvatarProps;

const GenericCompProps = {
    type: {
        type: String
    },
    width: {
        type: [String, Array] as PropType<BasicProps['width']>
    },
    height: {
        type: String
    },
    inline: {
        type: Boolean
    },
    size: {
        type: String as PropType<AvatarProps['size']>
    },
    shape: {
        type: String as PropType<AvatarProps['shape']>
    },
    rows: {
        type: Number
    }
};
const Generic = defineComponent({
    props: GenericCompProps,
    setup (props: GenericProps) {
        const size = props.size ?? 'medium';
        const shape = props.shape ?? 'circle';
        const classList = computed(() => ({
            'cm-skeleton-item': true,
            [`cm-skeleton-${props.type}`]: props.type,
            [`cm-skeleton-${props.type}-${size}`]: size,
            [`cm-skeleton-${props.type}-${shape}`]: shape,
            [`cm-skeleton-inline`]: props.inline,
        }));
        const style = computed(() => ({
            width: typeof props.size === 'number' ? props.size + 'px' : props.width,
            height: typeof props.size === 'number' ? props.size + 'px' : props.height,
        }));
        return () => <div class={classList.value} style={style.value}></div>;
    }
});

const generator = <T extends BasicProps>(type: string) => (props: T) => <Generic type={type} {...props} />;

export const Avatar = generator<GenericProps>('avatar');
export const Image = generator<GenericProps>('image');
export const Title = generator<GenericProps>('title');
export const Button = generator<GenericProps>('button');
export const Item = generator<GenericProps>('item');

export interface ParagraphProps extends BasicProps {
    rows?: number;
}

export const Paragraph = defineComponent({
    props: GenericCompProps,
    setup (props: ParagraphProps) {
        const rows = props.rows ?? 4;
        const arr = new Array(rows).fill(1);

        const outStyle = computed(() => ({
            width: props.width,
        }));

        return () => <ul class="cm-skeleton-paragraph" style={outStyle.value}>
            {
                arr.map((item, index) => {
                    const style: any = {};
                    if (props.width && props.width instanceof Array) {
                        style.width = props.width[index];
                    }
                    return <li style={style}></li>;
                })
            }
        </ul>;
    }
});
