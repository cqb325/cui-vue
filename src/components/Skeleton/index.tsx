import { PropType, computed, defineComponent } from "vue";
import { Avatar, Image, Title, Button, Paragraph, Item } from "./Item";

export interface SkeletonProps {
    active?: boolean;
    loading?: boolean;
    placeholder?: any;
    width?: string;
    height?: string;
    style?: any
}

const Skeleton = defineComponent({
    props: {
        active: {
            type: Boolean as PropType<SkeletonProps['active']>,
        },
        loading: {
            type: Boolean as PropType<SkeletonProps['loading']>,
        },
        placeholder: {
            type: Object as PropType<SkeletonProps['placeholder']>,
        },
        width: {
            type: String as PropType<SkeletonProps['width']>,
        },
        height: {
            type: String as PropType<SkeletonProps['height']>,
        },
        style: {
            type: Object as PropType<SkeletonProps['style']>,
        }
    },
    setup (props: SkeletonProps, { slots }) {
        const classList = computed(() => ({
            'cm-skeleton': true,
            'cm-skeleton-active': props.active
        }));
        const style = computed(() => {
            const obj = {
                ...props.style
            };
            if (props.width !== undefined) {
                obj.width = props.width;
            }
            if (props.height !== undefined) {
                obj.height = props.height;
            }
            return obj;
        });

        return () => props.loading ?
            <div class={classList.value} style={style.value}>
                {props.placeholder}
            </div>
            : slots.default?.();
    },
});

Skeleton.Avatar = Avatar;
Skeleton.Image = Image;
Skeleton.Title = Title;
Skeleton.Button = Button;
Skeleton.Item = Item;
Skeleton.Paragraph = Paragraph;

export default Skeleton;
