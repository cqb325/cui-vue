import { defineComponent, ref } from "vue";
import type { InnerPopupProps } from "../inner/InnerPopup";
import { InnerPopup, innerPopupDefinedProps } from "../inner/InnerPopup";

export interface PopoverProps extends InnerPopupProps {
}

export default defineComponent({
    name: "Popover",
    props: innerPopupDefinedProps,
    emits: ['update:modelValue', 'visibleChange'],
    setup (props: PopoverProps, {slots, emit, attrs, expose}) {
        const pop = ref();
        expose({
            updatePosition () {
                pop.value?.updatePosition();
            }
        });
        return () => <InnerPopup ref={pop} {...props} {...attrs} theme={props.theme || "light"}
            onUpdate:modelValue={(val: boolean) => emit('update:modelValue', val)}
            onVisibleChange={(val: boolean) => emit('visibleChange', val)}
        >
            {slots.default?.()}
        </InnerPopup>;
    }
});
