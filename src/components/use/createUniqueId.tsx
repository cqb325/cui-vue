let counter = 0;
const randomSeed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const index = Math.floor(Math.random() * randomSeed.length);
let id = randomSeed.substring(index, index + 1);
export function createUniqueId() {
    return `${id}_${counter++}`;
}