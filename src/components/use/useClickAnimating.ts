import { Ref, ref } from "vue";

/**
 * 点击触发animating
 * @returns
 */
export function useClickAnimating (): [Ref<boolean>, () => void]{
    const animating = ref(false);
    const setAnimate = () => {
        animating.value = true;
        setTimeout(() => {
            animating.value = false;
        }, 1000);
    };
    return [animating, setAnimate];
}

