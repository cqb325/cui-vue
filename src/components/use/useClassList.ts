import { reactive } from "vue";

export function useClassList (props: any, ...customClassList: any) {
    let obj: any = reactive({});
    if (props && props.class) {
        obj[props.class] = true;
    }
    if (typeof props === 'string') {
        obj[props] = true;
    }

    if (customClassList) {
        for (let i = 0; i < customClassList.length; i++) {
            const item = customClassList[i];
            if (typeof item === 'string') {
                obj[item] = true;
            } else {
                for (let key in item) {
                    obj[key] = item[key];
                }
            }
        }
    }

    return obj;
}