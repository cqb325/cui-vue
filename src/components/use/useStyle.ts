export function useStyle (customStyle: any) {
    let obj = {
        ...customStyle,
    };
    return obj;
}