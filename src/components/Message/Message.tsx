import { PropType, computed, defineComponent, onMounted, ref } from "vue";
import usezIndex from "../use/usezIndex";
import Loading from "../inner/Loading";
import { F7CheckmarkAltCircleFill, F7ExclamationmarkTriangleFill, F7InfoCircleFill, F7XmarkCircleFill } from "cui-vue-icons/f7";
import { FeatherX } from "cui-vue-icons/feather";

function getIcon (type: string) {
    let icon = null;
    switch (type) {
    case 'info': {
        icon = <F7InfoCircleFill class="cm-message-icon" size={19}/>;
        break;
    }
    case 'success': {
        icon = <F7CheckmarkAltCircleFill class="cm-message-icon" size={19}/>;
        break;
    }
    case 'warning': {
        icon = <F7ExclamationmarkTriangleFill class="cm-message-icon" size={19}/>;
        break;
    }
    case 'error': {
        icon = <F7XmarkCircleFill class="cm-message-icon" size={19}/>;
        break;
    }
    }
    return icon;
}

const Message = defineComponent({
    name: 'Message',
    props: {
        data: {type: Object},
    },
    emits: ['close'],
    setup (props, { emit }) {
        const visible = ref(false);
        const data = props.data;
        const classList = computed(() => {
            const obj = {
                'cm-message': true,
                'cm-message-visible': visible.value,
                [`cm-message-${data.type}`]: data.type,
                'cm-message-background': data.background
            };
            if (data.class) {
                obj[data.class] = true;
            }
            return obj;
        });

        onMounted(() => {
            // 设置效果
            setTimeout(() => {
                visible.value = true;
            });
            let duration = data.duration;
            if (duration === undefined || duration === null) {
                duration = 4;
            }

            if (duration) {
                setTimeout(() => {
                    hide();
                }, duration * 1000);
            }
        });

        const hide = () => {
            // 隐藏效果
            visible.value = false;
        };

        const close = () => {
            if (!visible.value) {
                emit('close', data);
                data.onClose && data.onClose();
            }
        };

        const style = computed(() => ({...data.style, 'z-index': usezIndex()}));
        return () => <div class={classList.value} style={style.value} onTransitionend={close}>
            <div class="cm-message-inner">
                {
                    data.loading ? <Loading /> : getIcon(data.type)
                }
                <div class="cm-message-content">
                    {data.content}
                </div>
                {
                    data.closeable ? <div class="cm-message-close">
                        <FeatherX class="cm-message-close-icon" size={14} onClick={hide}/>
                    </div> : null
                }
            </div>
        </div>;
    }
});
export default Message;

export const Messages = defineComponent({
    name: 'Messages',
    props: {
        data: {
            type: Array,
            default: () => []
        },
        onClose: {
            type: Function as PropType<(...args: any[]) => any>
        }
    },
    setup (props) {
        return () => props.data && props.data.map((item: any) => {
            return <Message key={item.key} data={item} onClose={props.onClose}/>;
        });
    }
});
