import { createApp, reactive } from "vue";
import { Messages } from "./Message";
import usePortal from "../use/usePortal";
import { createUniqueId } from "../use/createUniqueId";

type MessageProps = {
    key?: string,
    duration?: number,
    type?: 'info'|'success'|'warning'|'error',
    class?: string,
    style?: any,
    content?: any,
    closeable?: boolean,
    background?: any,
    loading?: boolean,
    onClose?: (item?: any) => void
}

function Message () {
    const store = reactive({list: []} as any);
    const ele = usePortal('cm-message-portal', 'cm-messages-wrap');
    const onClose = (item: any) => {
        const items = store.list.filter((aitem: any) => {
            return aitem.key !== item.key;
        });
        store.list = [...items];
    };
    createApp(() => <Messages data={store.list} onClose={onClose}/>).mount(ele);
    return {
        close: (key: string) => {
            const item: undefined | MessageProps = store.list.find((aitem: any) => {
                return aitem.key === key;
            });
            onClose(item);

            if (item) {
                item.onClose && item.onClose();
            }
        },
        open: (config: string | MessageProps, type: 'info'|'success'|'warning'|'error') => {
            if (typeof config === 'string') {
                config = {
                    content: config
                };
            }
            if (!config.key) {
                config.key = createUniqueId();
            }
            config.type = type;

            store.list.push(config);
        },
        info (config: string | MessageProps) {
            this.open(config, 'info');
        },
        success (config: string | MessageProps) {
            this.open(config, 'success');
        },
        warning (config: string | MessageProps) {
            this.open(config, 'warning');
        },
        error (config: string | MessageProps) {
            this.open(config, 'error');
        }
    };
}

export const message = Message();
