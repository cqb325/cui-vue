import { PropType, computed, defineComponent } from "vue";
import { FeatherAlertTriangle, FeatherCheck, FeatherXCircle } from "cui-vue-icons/feather";

export interface StepProps {
    title?: any | string
    description?: any | string
    icon?: any,
    status?: 'finished'|'process'|'error'|'warning'|'wait'
    current: number,
    index: number,
}

export default defineComponent({
    name: 'Step',
    props: {
        title: {type: [Object, String] as PropType<StepProps['title']>, default: undefined},
        description: {type: [Object, String] as PropType<StepProps['description']>, default: undefined},
        icon: {type: Object as PropType<StepProps['icon']>, default: undefined},
        status: {type: String as PropType<StepProps['status']>, default: undefined},
        current: {type: Number as PropType<StepProps['current']>, default: 0},
        index: {type: Number as PropType<StepProps['index']>, default: 0},
    },
    setup (props: StepProps, {slots}) {
        const status = computed(() : string => {
            if (props.status) {
                return props.status;
            }
            let ret:string = '';
            if (props.current + 1 > props.index) {
                ret = 'finished';
            }
            if (props.current + 1 === props.index) {
                ret = 'process';
            }
            return ret || 'wait';
        });
        const done = computed(() : string => {
            let ret:string = '';
            if (props.current + 1 > props.index) {
                ret = 'done';
            }
            if (props.current + 1 === props.index) {
                ret = 'active';
            }
            return ret;
        });

        const classList = computed(() => ({
            'cm-steps-item': true,
            [`cm-steps-status-${status.value}`]: status.value,
            [`cm-steps-status-${done.value}`]: done.value,
        }));

        const inner = computed(() => {
            let ret;
            if (!props.icon) {
                if (status.value === 'finished') {
                    ret = <div class="cm-step-head-inner"><FeatherCheck /></div>;
                } else if (status.value === 'error') {
                    ret = <FeatherXCircle size={26}/>;
                } else if (status.value === 'warning') {
                    ret = <FeatherAlertTriangle size={26}/>;
                } else {
                    ret = <div class="cm-step-head-inner"><span>{props.index}</span></div>;
                }
            } else {
                ret = props.icon || slots.icon?.();
            }
            return ret;
        });

        return () => <div class={classList.value}>
            <div class="cm-step-head">
                {inner.value}
            </div>
            <div class="cm-step-main">
                <div class="cm-step-title">{props.title}</div>
                <div class="cm-step-description">{props.description}</div>
            </div>
        </div>;
    },
});
