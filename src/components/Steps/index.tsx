import { PropType, computed, defineComponent } from "vue";
import useChildren from "../use/useChildren";
import Step from "./Step";

export interface StepsProps {
    size?: 'small'|'default'
    current?: number
    dir?: 'v'|'h'
}

const Steps = defineComponent({
    name: 'Steps',
    props: {
        size: {type: String as PropType<StepsProps['size']>, default: 'default'},
        current: {type: Number, default: 0},
        dir: {type: String as PropType<StepsProps['dir']>, default: ''}
    },
    setup (props: StepsProps, {slots}) {
        const classList = computed(() => ({
            'cm-steps': true,
            [`cm-steps-${props.size}`]: props.size,
            'cm-steps-vertical': props.dir === 'v'
        }));

        const children = computed(() => {
            const childs = useChildren({
                slots,
                props: {
                    current: props.current,
                },
                sameType: Step
            });
            return childs;
        });

        return () => <div class={classList.value}>
            { children.value }
        </div>;
    }
});

Steps.Step = Step;

export default Steps;
