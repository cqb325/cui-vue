import { HTMLAttributes, PropType, computed, defineComponent } from 'vue';
import { isColor } from '../utils/assist';

type DividerDirection = 'h' | 'v';
type DividerAlign = 'left' | 'right';
type DividerTheme =
    | 'primary'
    | 'success'
    | 'warning'
    | 'error'
    | 'info'
    | 'blue'
    | 'green'
    | 'red'
    | 'yellow'
    | 'pink'
    | 'magenta'
    | 'volcano'
    | 'orange'
    | 'gold'
    | 'lime'
    | 'cyan'
    | 'geekblue'
    | 'purple'
    | string;

interface DividerProps extends HTMLAttributes {
    dir?: DividerDirection; // 方向
    align?: DividerAlign; // 文本对齐方式
    theme?: DividerTheme; // 主题颜色
    dashed?: boolean; // 是否为虚线
    margin?: number | string; // 外边距
    textColor?: string; // 文本颜色
    textMargin?: number | string; // 文本外边距
}

export default defineComponent({
    name: 'Divider',
    props: {
        dir: {
            type: String as PropType<DividerProps['dir']>, default: 'h'
        },
        align: {
            type: String as PropType<DividerProps['align']>,
        },
        theme: {
            type: String as PropType<DividerProps['theme']>,
        },
        margin: {
            type: [String, Number] as PropType<DividerProps['margin']>,
        },
        textColor: {
            type: String as PropType<DividerProps['textColor']>,
        },
        textMargin: {
            type: [String, Number] as PropType<DividerProps['textMargin']>,
        },
        dashed: {
            type: Boolean as PropType<DividerProps['dashed']>,
        }
    },
    setup (props: DividerProps, { slots }) {
        const theme = isColor(props.theme) ? '' : props.theme;
        const classList = computed(() => ({
            'cm-divider': true,
            [`cm-divider-${props.dir}`]: props.dir,
            [`cm-divider-${props.align}`]: props.align,
            [`cm-divider-${theme}`]: theme,
            'cm-divider-dashed': props.dashed
        }));
        const containerStyle = computed(() => ({
            margin: `${props.margin}${typeof props.margin === 'number' ? 'px' : ''}`,
            '--cui-divider-border-color': isColor(props.theme) ? props.theme : '',
        }));

        const textStyle = () => ({
            margin: `${props.textMargin}${typeof props.textMargin === 'number' ? 'px' : ''}`,
            color: props.textColor
        });
        return () => (
            <div class={classList.value} style={containerStyle.value}>
                {slots.default ? <span class="cm-divider-text" style={textStyle()}>{slots.default()}</span> : null}
            </div>
        );
    }
});
