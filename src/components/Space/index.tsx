import { HTMLAttributes, PropType, computed, defineComponent } from "vue";

export interface SpaceOptions extends HTMLAttributes{
    dir?: 'v' | 'h'
    wrap?: boolean
    inline?: boolean
    size?: number
    align?: 'center'|'start'|'end'|'baseline'
    justify?: 'center'|'start'|'end'
    id?: string
    title?: string
}

export default defineComponent({
    name: 'Space',
    props: {
        dir: {type: String as PropType<SpaceOptions['dir']>},
        wrap: {type: Boolean as PropType<SpaceOptions['wrap']>},
        inline: {type: Boolean as PropType<SpaceOptions['inline']>},
        size: {type: Number as PropType<SpaceOptions['size']>},
        align: {type: String as PropType<SpaceOptions['align']>},
        justify: {type: String as PropType<SpaceOptions['justify']>},
        id: {type: String as PropType<SpaceOptions['id']>},
        title: {type: String as PropType<SpaceOptions['title']>},
    },
    setup (props: SpaceOptions, {slots}) {
        const dir = computed(() => props.dir ?? 'h');
        const size = computed(() => props.size ?? 8);
        const align = computed(() => props.align ?? '');

        const classList = computed(() => ({
            'cm-space': true,
            [`cm-space-${dir.value}`]: dir.value,
            [`cm-space-align-${align.value}`]: align.value,
            [`cm-space-justify-${props.justify}`]: !!props.justify,
            'cm-space-wrap': props.wrap,
            'cm-space-inline': props.inline
        }));

        const newStyle = computed(() => ({['gap']: size.value + 'px'}));

        return ()=> <div class={classList.value} style={newStyle.value} id={props.id} title={props.title}>
            {
                slots.default && slots.default()
            }
        </div>;
    }
});
