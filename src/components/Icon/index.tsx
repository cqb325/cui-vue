import { useClassList } from "../use/useClassList";
import { useStyle } from "../use/useStyle";
import { HTMLAttributes, PropType, defineComponent } from 'vue'

interface IconProps extends HTMLAttributes{
    size?: number,
    spin?: boolean,
    name?: string,
    color?: string
}

export default defineComponent({
    name: 'Icon',
    props: {
        name: {type: String as PropType<IconProps['name']>},
        spin: {type: Boolean as PropType<IconProps['spin']>},
        size: {type: Number as PropType<IconProps['size']>},
        color: {type: String as PropType<IconProps['color']>},
    },
    emits: ['click'],
    setup(props: IconProps, { slots, emit }) {
        const classList = () => useClassList('cm-icon', `cm-icon-${props.name}`, {
            'cm-icon-spin': props.spin
        });
        let newStyle = () => useStyle({ 'font-size': (props.size || 14) + 'px', color: props.color })
        return ()=> <div onClick={(e) => emit('click', e)} class={classList()} style={newStyle()}>{slots.default}</div>;
    }
})