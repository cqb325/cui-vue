import { F7CheckmarkAltCircleFill, F7ExclamationmarkTriangleFill, F7InfoCircleFill, F7XmarkCircleFill } from "cui-vue-icons/f7";
import { computed, createVNode, defineComponent, PropType, VNode } from "vue";

export interface ResultProps {
    icon?: VNode
    status?: 'success' | 'warning' | 'error' | 'info'
    title?: string | VNode
    subTitle?: string | VNode
    extra?: VNode
    desc?: string | VNode
}

const icons: any = {
    info: F7InfoCircleFill,
    success: F7CheckmarkAltCircleFill,
    warning: F7ExclamationmarkTriangleFill,
    error: F7XmarkCircleFill
};

export default defineComponent({
    name: "Result",
    props: {
        icon: {
            type: Object as PropType<ResultProps["icon"]>,
        },
        status: {
            type: String as PropType<ResultProps["status"]>,
            default: "info",
        },
        title: {
            type: [String, Object] as PropType<ResultProps["title"]>,
        },
        subTitle: {
            type: [String, Object] as PropType<ResultProps["subTitle"]>,
        },
        extra: {
            type: Object as PropType<ResultProps["extra"]>,
        },
        desc: {
            type: [String, Object] as PropType<ResultProps["desc"]>,
        },
        class: {
            type: [String, Array, Object],
        },
    },
    setup (props) {
        const getIcon = (status: string) => {
            const IconComponent = icons[status];
            return createVNode(IconComponent);
        };

        const icon = computed(() => props.icon || getIcon(props.status));
        const classList = computed(() => {
            const baseClass = "cm-result";
            const statusClass = `cm-result-${props.status}`;
            return {
                [baseClass]: true,
                [statusClass]: !!props.status
            };
        });

        return () => <div class={classList.value}>
            {
                icon.value ? <div class="cm-result-icon">
                    {icon.value}
                </div>
                    : null
            }
            {
                props.title ? <div class="cm-result-title">
                    {props.title}
                </div>
                    : null
            }
            {
                props.subTitle ? <div class="cm-result-subtitle">
                    {props.subTitle}
                </div> : null
            }
            {
                props.extra ? <div class="cm-result-extra">
                    {props.extra}
                </div> : null
            }
            {
                props.desc ? <div class="cm-result-desc">
                    {props.desc}
                </div> : null
            }
        </div>;
    }
});
