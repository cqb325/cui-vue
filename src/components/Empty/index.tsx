import { defineComponent } from "vue";
import NO_DATA from './NoData.svg';
import Image from "../Image";

export const NO_DATA_IMAGE = NO_DATA;

export default defineComponent({
    name: "Empty",
    props: {
        text: {
            type: String,
            default: "暂无数据",
        },
        width: {
            type: Number,
        },
        height: {
            type: Number,
        }
    },
    setup (props) {
        return () => <div class="cm-empty">
            <div class="cm-empty-img">
                <Image src={NO_DATA} width={props.width} height={props.height}/>
            </div>
            <div class="cm-empty-info">{props.text}</div>
        </div>;
    }
});
