import { HTMLAttributes, computed, defineComponent } from "vue";

export interface CardProps extends HTMLAttributes {
    bordered?: boolean,
    rised?: boolean,
    title?: any,
    bodyStyle?: any,
    footer?: any
}

export default defineComponent({
    name: 'Card',
    props: ['bordered', 'rised', 'title', 'bodyStyle', 'footer'],
    setup (props: CardProps, { slots }) {
        const classList = computed(() => ({
            'cm-card': true,
            'cm-card-bordered': props.bordered,
            'cm-card-rised': props.rised,
        }));
        return () => <div class={classList.value}>
            {
                props.title || slots.title ? <div class="cm-card-head">
                    { props.title || (slots.title && slots.title())}
                </div>
                    : null
            }
            <div class="cm-card-body" style={props.bodyStyle}>
                { slots.default && slots.default() }
            </div>
            {
                props.footer || slots.footer ? <div class="cm-card-footer">{props.footer || (slots.footer && slots.footer())}</div> : null
            }
        </div>;
    },
});
