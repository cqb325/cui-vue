import { PropType, computed, defineComponent, inject } from "vue";
// type TabProps = {
//     children?: any,
//     title?: any,
//     name: string,
//     disabled?: boolean
//     closeable?: boolean,
//     icon?: any
// }

// export function Tab (props: TabProps){
//     const ctx: any = useTabsContext();



//     const isShow = () => {
//         const tab = ctx?.store.tabs.find((tab: any) => tab.name === props.name);
//         return !!tab;
//     }

//     return <Show when={isShow()}>
//         <div classList={classList()} >{props.children}</div>
//     </Show>
// }


export interface TabProps {
    title?: any,
    name: string,
    disabled?: boolean
    closeable?: boolean,
    icon?: any
}
export default defineComponent({
    name: 'Tab',
    props: {
        name: {type: String as PropType<TabProps['name']>},
        title: {type: [String, Object] as PropType<TabProps['title']>},
        disabled: {type: Boolean as PropType<TabProps['disabled']>},
        closeable: {type: Boolean as PropType<TabProps['closeable']>},
        icon: {type: Object as PropType<TabProps['icon']>}
    },
    setup (props, {slots}) {
        const ctx: any = inject('CMTabsContext', null);
        const classList = computed(() => ({
            'cm-tab-panel': true,
            'cm-tab-panel-active': props.name === ctx?.store.activeName
        }));

        if (ctx) {
            ctx.addTab({
                title: props.title,
                name: props.name,
                disabled: props.disabled,
                icon: props.icon,
                closeable: props.closeable
            } as TabProps);
        }

        return () => <div class={classList.value} >{slots.default?.()}</div>;
    },
});
