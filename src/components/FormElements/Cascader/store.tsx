import { reactive, Ref, ref, VNode, watch, watchEffect } from "vue";
import type { CascaderProps } from ".";
import { TreeCheckMod } from "../../inner/Constaint";
import formFieldRef from "../../use/formFieldRef";
export type NodeKeyType = string | number

export interface CascaderNode {
    id: NodeKeyType;
    title: string | VNode;
    icon?: string | VNode;
    children?: CascaderNode[];
    expand?: boolean;
    disabled?: boolean;
    // 动态加载
    loading?: boolean;
    checked?: boolean | 'indeterminate';
    selected?: boolean;
    _parent?: CascaderNode;
    _level?: number;
    _dragging?: boolean;
    // 可扩展的属性
    [key: string]: any;
}

export interface CascaderStoreProps {
    nodeMap: Record<NodeKeyType, CascaderNode>;
    columns: NodeKeyType[][];
}

export class CascaderStore {
    store: any;
    emit: any;
    data: CascaderNode[] = [];
    flatData: CascaderNode[] = [];
    valueField = 'value';
    titleField = 'title';
    selectedKey: Ref<NodeKeyType[]>;
    value: Ref<NodeKeyType[]>;
    props: CascaderProps;
    mode: TreeCheckMod;
    valMap: Record<NodeKeyType, NodeKeyType[]> = {};

    constructor (props: CascaderProps, emit: any) {
        this.props = props;
        this.emit = emit;
        this.valueField = props.valueField || 'value';
        this.titleField = props.titleField || 'title';
        this.mode = props.mode ?? TreeCheckMod.HALF;
        const store = reactive({
            nodeMap: {},
            columns: [],
            filteredList: []
        } as CascaderStoreProps);
        this.store = store;
        const selectedKey = ref<NodeKeyType[]>([]);
        const value = formFieldRef<NodeKeyType[]>(props, emit, []);
        this.selectedKey = selectedKey;
        this.value = value;
        this.init(props.data);
        this.valMap['__'] = this.data.map(item => item[this.valueField]);
        watch(() => props.data, () => {
            if (this.data !== props.data) {
                this.init(props.data);
                this.valMap['__'] = this.data.map(item => item[this.valueField]);
            }
            const keys = selectedKey.value.filter(key => store.nodeMap[key]);
            if (keys.length !== selectedKey.value.length) {
                selectedKey.value = keys;
            }
        });

        // 外部修改selected值进行同步
        watch(() => [selectedKey.value], () => {
            const keys = selectedKey.value;
            const columns = [this.valMap['__']];

            if (keys && keys.length) {
                keys.forEach((key: any) => {
                    if (this.valMap[key]) {
                        columns.push(this.valMap[key]);
                    } else {
                        const item = store.nodeMap[key];
                        if (item && item.children) {
                            const levelIds = item.children.map((aItem: any) => aItem[this.valueField]);
                            this.valMap[key] = levelIds;
                            columns.push(levelIds);
                        }
                    }
                });
            }
            store.columns = columns;
        }, {immediate: true});

        // 外部修改selected值进行同步
        watchEffect(() => {
            const val = value.value;
            if (props.multi) {
                this.setCheckedByMod(val);
            } else {
                selectedKey.value = val || [];
            }
        });
    }

    init (data: any[]) {
        this.data = data;
        this.flatData = this.getAllFlatNodes(this.data);
        this.store.filteredList = [];
        const map: Record<NodeKeyType, CascaderNode> = {};
        this.buildRelation(this.data, null, 0, map);
        this.store.nodeMap = map;
    }

    /**
     * 构建父子关系和层级关系
     * @param data
     * @param parent
     * @param level
     */
    buildRelation = (data: CascaderNode[], parent: any, level: number, map: Record<NodeKeyType, CascaderNode>) => {
        data.forEach((item: CascaderNode) => {
            map[item[this.valueField]] = item;
            item._parent = parent;
            item._level = level;
            if (item.children) {
                this.buildRelation(item.children, item, level + 1, map);
            }
        });
    };

    /**
     * 获取显示的树节点
     * @param nodes
     * @returns
     */
    getAllFlatNodes = (nodes: CascaderNode[]) : CascaderNode[] => {
        const list: CascaderNode[] = nodes.flatMap((item: CascaderNode) => {
            if (item.children?.length) {
                return [item, this.getAllFlatNodes(item.children)].flat();
            } else {
                return [item];
            }
        });
        return list;
    };

    getStore () {
        return this.store;
    }

    clearSelect = () => {

    };

    /**
     * 过滤
     * @param keyword
     */
    filter (keyword: string) {
        if (keyword) {
            const allChildren = this.flatData.filter(item => !item.children || item.children.length === 0);
            const lines = allChildren.map(item => {
                const arr = [];
                arr.push(item);
                let parent = item._parent;
                while (parent) {
                    arr.push(parent);
                    parent = parent._parent;
                }
                arr.reverse();
                return arr;
            });
            const filteredList = lines.filter(line => {
                return line.some(item => item[this.titleField].includes(keyword));
            });
            console.log(filteredList);

            this.store.filteredList = filteredList;
        } else {
            this.store.filteredList = [];
        }
    }

    getNode = (key: NodeKeyType) => {
        return this.store.nodeMap[key];
    };

    /**
     * 选择节点
     * @param key
     */
    selectItem = (key: NodeKeyType | CascaderNode) => {
        const node = this._getNode(key);
        if (node) {
            const vals = [];
            for (let i = 0; i < node._level!; i++) {
                vals.push(this.selectedKey.value[i]);
            }
            vals[node._level!] = node[this.valueField];
            this.selectedKey.value = vals;
        }
    };

    _getNode = (nodeId: NodeKeyType|CascaderNode) => {
        let node: CascaderNode;
        if (typeof nodeId === 'string' || typeof nodeId === 'number') {
            node = this.store.nodeMap[nodeId];
        } else {
            node = nodeId as CascaderNode;
            nodeId = node[this.valueField] as NodeKeyType;
        }
        return node;
    };

    /**
     * 更新节点选择状态
     * @param nodeId
     */
    updateNodeCheckStatus = (nodeId: CascaderNode | NodeKeyType | undefined) => {
        if (!nodeId) {
            return;
        }
        const node = this._getNode(nodeId);
        if (node) {
            const n = this.store.nodeMap[node[this.valueField]];
            n.checked = this.getNodeChecked(n);
            this.setCheckedForwardUp(node);
        }
    };

    checkNode = (nodeId: CascaderNode | NodeKeyType, checked: boolean) => {
        const node = this._getNode(nodeId);
        const n = this.store.nodeMap[node[this.valueField]];
        if (this.props.beforeChecked) {
            const result = this.props.beforeChecked(n, checked);
            if (result === false) {
                return;
            }
        }
        if (checked) {
            const addNum = {num: 0};
            this.setCheckedForwardDownNum(n, checked, addNum);
            this.setCheckedForwardUpNum(n, addNum);
            if (this.props.max && (this.value.value.length + addNum.num + 1) > this.props.max) {
                this.emit('exceed');
                return;
            }
        }
        n.checked = checked;
        this.setCheckedForwardDown(n, checked);
        n.checked = this.getNodeChecked(node);
        this.setCheckedForwardUp(n);
        const checkedKeys = this.getCheckedKeys(this.mode);
        this.value.value = checkedKeys;
        this.emit('change', checkedKeys);
    };

    setCheckedForwardDownNum = (node: CascaderNode, checked: boolean, addNum: {num: number}) => {
        if (node.children) {
            node.children.forEach(item => {
                if (item.disabled) return;
                if (!item.checked) {
                    addNum.num ++;
                    this.setCheckedForwardDownNum(item, checked, addNum);
                }
            });
        }
    };

    setCheckedForwardDown = (node: CascaderNode, checked: boolean) => {
        if (node.children) {
            node.children.forEach(item => {
                if (item.disabled) return;
                this.store.nodeMap[item[this.valueField]].checked = checked;
                this.setCheckedForwardDown(item, checked);
            });
        }
    };

    getNodeChecked = (nodeId: CascaderNode | NodeKeyType) => {
        const node = this._getNode(nodeId);
        if (!node.children || node.children.length === 0) {
            return node.checked;
        } else {
            let checked: boolean | 'indeterminate' = false;
            let checkedNum = 0;
            let indeterminateNum = 0;
            node.children.forEach(item => {
                if (item.checked === true) {
                    checkedNum ++;
                }
                if (item.checked === 'indeterminate') {
                    indeterminateNum ++;
                }
            });
            if (checkedNum === node.children.length) {
                checked = true;
            } else if (checkedNum > 0) {
                checked = 'indeterminate';
            }
            if (!checked && indeterminateNum > 0) {
                checked = 'indeterminate';
            }
            return checked;
        }
    };

    getNodeChecked2 = (nodeId: CascaderNode | NodeKeyType) => {
        const node = this._getNode(nodeId);
        if (!node.children || node.children.length === 0) {
            return node.checked;
        } else {
            let checked: boolean | 'indeterminate' = false;
            let checkedNum = 0;
            let indeterminateNum = 0;
            node.children.forEach(item => {
                if (item.checked === true) {
                    checkedNum ++;
                }
                if (item.checked === 'indeterminate') {
                    indeterminateNum ++;
                }
            });
            if (checkedNum + 1 === node.children.length) {
                checked = true;
            } else if (checkedNum > 0) {
                checked = 'indeterminate';
            }
            if (!checked && indeterminateNum > 0) {
                checked = 'indeterminate';
            }
            return checked;
        }
    };

    setCheckedForwardUpNum = (node: CascaderNode, addNum: {num: number}) => {
        const parentNode = node._parent;
        if (parentNode) {
            const checked: boolean | 'indeterminate' | undefined = this.getNodeChecked2(parentNode);
            if (parentNode.checked !== checked) {
                addNum.num ++;
                this.setCheckedForwardUpNum(parentNode, addNum);
            }
        }
    };

    setCheckedForwardUp = (node: CascaderNode) => {
        const parentNode = node._parent;
        if (parentNode) {
            const checked: boolean | 'indeterminate' | undefined = this.getNodeChecked(parentNode);
            this.store.nodeMap[parentNode[this.valueField]].checked = checked;

            this.setCheckedForwardUp(parentNode);
        }
    };

    checkAll = () => {
        for (const key in this.store.nodeMap) {
            this.store.nodeMap[key].checked = true;
        }
        const checkedKeys = this.getCheckedKeys(this.mode);
        this.value.value = checkedKeys;
        this.emit('change', checkedKeys);
    };

    uncheckAll = () => {
        for (const key in this.store.nodeMap) {
            this.store.nodeMap[key].checked = false;
        }
        const checkedKeys = this.getCheckedKeys(this.mode);
        this.value.value = checkedKeys;
        this.emit('change', checkedKeys);
    };

    loadData = async (node: CascaderNode, loadDataMethod: (node: CascaderNode) => Promise<CascaderNode[]>) => {
        try {
            const list = await loadDataMethod(node);
            if (list.length > 0) {
                node.children = list;
                list.forEach(item => {
                    this.store.nodeMap[item[this.valueField]] = item;
                });
            }
        } catch (e) {
            //
        }
        this.store.nodeMap[node[this.valueField]].loading = false;
    };

    /**
     *
     * @param mode
     * @returns
     */
    getChecked = (mode: TreeCheckMod = TreeCheckMod.HALF) :CascaderNode[] => {
        if (mode === TreeCheckMod.FULL) {
            return this.getFullChecked();
        }
        if (mode === TreeCheckMod.CHILD) {
            return this.getChildChecked();
        }
        if (mode === TreeCheckMod.HALF) {
            return this.getHalfChecked();
        }
        if (mode === TreeCheckMod.SHALLOW) {
            return this.getShallowChecked();
        }
        return [];
    };

    /**
     * 获取所有选中的节点包含父节点和子节点
     * @returns
     */
    getFullChecked = () => {
        return this.flatData.filter((node: CascaderNode) => node.checked === true);
    };

    /**
     * 选中的子节点
     * @returns
     */
    getChildChecked = () => {
        return this.flatData.filter((node: CascaderNode) => node.checked === true && (!node.children || node.children.length === 0));
    };

    /**
     * 返回全部选中子节点和部分选中的父节点
     * @returns
     */
    getHalfChecked = () => {
        return this.flatData.filter((node: CascaderNode) => node.checked === true || node.checked === 'indeterminate');
    };

    /**
     * 如果父节点下所有子节点全部选中，只返回父节点
     * @returns
     */
    getShallowChecked = () => {
        const ret: CascaderNode[] = [];
        this.flatData.forEach((node: CascaderNode) => {
            if (node.checked === true) {
                const parentChecked = (() => {
                    const parent = node._parent;
                    if (!parent) { return false; }
                    return parent.checked === true;
                })();
                if (!parentChecked) { ret.push(node); }
            }
        });
        return ret;
    };

    /**
     * 选中的节点标识
     * @param mode
     * @returns
     */
    getCheckedKeys = (mode: TreeCheckMod = TreeCheckMod.HALF) :NodeKeyType[] => {
        const nodes = this.getChecked(mode);
        return nodes.map((node: CascaderNode) => node[this.valueField]);
    };

    clearChecked = () => {
        this.flatData.forEach((node: CascaderNode) => {
            this.store.nodeMap[node[this.valueField]].checked = false;
        });
    };

    setCheckedByMod = (val: NodeKeyType[]) => {
        this.clearChecked();
        if (this.mode === TreeCheckMod.FULL) {
            this.setCheckedByFull(val);
        }
        if (this.mode === TreeCheckMod.HALF) {
            this.setCheckedByHalf(val);
        }
        if (this.mode === TreeCheckMod.CHILD) {
            this.setCheckedByChild(val);
        }
        if (this.mode === TreeCheckMod.SHALLOW) {
            this.setCheckedByShallow(val);
        }
    };

    setCheckedByFull = (val: NodeKeyType[]) => {
        val.forEach((key: NodeKeyType) => {
            const n = this.store.nodeMap[key];
            if (!n) return;
            n.checked = true;
            this.setCheckedForwardUp(n);
        });
    };

    setCheckedByHalf = (val: NodeKeyType[]) => {
        val.forEach((key: NodeKeyType) => {
            const node = this._getNode(key);
            if (!node) return;
            if (!node.children || node.children.length === 0) {
                const n = this.store.nodeMap[key];
                n.checked = true;
                this.setCheckedForwardUp(n);
            }
        });
    };

    setCheckedByChild = (val: NodeKeyType[]) => {
        val.forEach((key: NodeKeyType) => {
            const node = this._getNode(key);
            if (!node) return;
            if (!node.children || node.children.length === 0) {
                const n = this.store.nodeMap[key];
                n.checked = true;
                this.setCheckedForwardUp(n);
            }
        });
    };

    setCheckedByShallow = (val: NodeKeyType[]) => {
        val.forEach((key: NodeKeyType) => {
            const n = this.store.nodeMap[key];
            if (!n) return;
            n.checked = true;
            this.setCheckedForwardUp(n);
            this.setCheckedForwardDown(n, true);
        });
    };
}
