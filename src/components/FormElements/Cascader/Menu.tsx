import { Item } from "./Item";
import Empty from "../../Empty";
import { defineComponent } from "vue";

export const Menu = defineComponent({
    name: 'CascaderMenu',
    props: {
        data: {
            type: Array,
        },
        store: {
            type: Object,
        },
        trigger: {
            type: String,
        },
        emptyText: {
            type: String,
        }
    },
    setup (props) {
        const data = () => props.data;
        return () => <div class={{"cm-cascader-list": true, 'cm-cascader-list-empty': !data().length}}>
            {
                data().length ? data().map((item: any) => {
                    return props.store.store.nodeMap[item] && <Item trigger={props.trigger} data={props.store.store.nodeMap[item]}
                        store={props.store} />;
                })
                    : <div class="cm-cascader-empty">
                        <Empty width={100} text={props.emptyText}/>
                    </div>
            }
        </div>;
    }
});
