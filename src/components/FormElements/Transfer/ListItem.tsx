import { computed, defineComponent } from "vue";
import Checkbox from "../Checkbox";

export default defineComponent({
    name: 'ListItem',
    props: {
        data: {
            type: Object,
        },
        render: {type: Function}
    },
    emits: ['select'],
    setup (props, {emit}) {
        const text = () => {
            if (props.render) {
                return props.render(props.data);
            }
            return props.data.title;
        };
        const onSelect = () => {
            console.log(111);

            if (!props.data.disabled) {
                emit('select', props.data);
            }
        };

        const style = computed(() => ({display: props.data._hide ? 'none' : 'flex'}));

        return () => <div class="cm-transfer-list-item" onClick={onSelect} style={style.value}>
            <Checkbox modelValue={props.data._checked} onChange={onSelect} disabled={props.data.disabled}/>
            <div>{text()}</div>
        </div>;
    },
});
