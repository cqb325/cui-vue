import { PropType, defineComponent, reactive, watch, watchEffect } from "vue";
import List from "./List";
import Button from "../../Button";
import formFieldRef from "../../use/formFieldRef";
import { FeatherChevronLeft, FeatherChevronRight } from "cui-vue-icons/feather";

export interface TransferProps {
    width?: number,
    height?: number,
    data?: any[],
    modelValue?: any[],
    filter?: boolean,
    rightText?: string,
    leftText?: string,
    sourceTitle?: string,
    targetTitle?: string,
    asFormField?: boolean
    render?: (item: any) => any,
    onChange?: (value: any[]) => void,
}

export type TransferStore = {
    data: any[],
    sourceDisabled: boolean,
    targetDisabled: boolean,
    sourceIds: any[],
    targetIds: any[],
}

export default defineComponent({
    name: "Transfer",
    props: {
        width: {type: Number as PropType<TransferProps['width']>},
        height: {type: Number as PropType<TransferProps['height']>},
        data: {type: Array as PropType<TransferProps['data']>},
        modelValue: {type: Array as PropType<TransferProps['modelValue']>},
        filter: {type: Boolean as PropType<TransferProps['filter']>},
        rightText: {type: String as PropType<TransferProps['rightText']>},
        leftText: {type: String as PropType<TransferProps['leftText']>},
        sourceTitle: {type: String as PropType<TransferProps['sourceTitle']>},
        targetTitle: {type: String as PropType<TransferProps['targetTitle']>},
        asFormField: {type: Boolean as PropType<TransferProps['asFormField']>, default: true},
        render: {type: Function as PropType<TransferProps['render']>}
    },
    emits: ['change', 'update:modelValue'],
    setup (props, {emit}) {
        const value = formFieldRef(props, emit, []);
        const store = reactive({
            data: [],
            sourceDisabled: true,
            targetDisabled: true,
            sourceIds: [],
            targetIds: []
        } as TransferStore);

        const rightText = props.rightText || 'To Right';
        const leftText = props.leftText || 'To Left';

        watchEffect(() => {
            store.data = props.data || [];
        });

        const onSelect = (data: any, checked: boolean) => {
            if (!data.disabled) {
                const item = store.data.find((item) => {
                    return item.id === data.id;
                });
                item._checked = checked;
            }
        };

        const transferToTarget = () => {
            store.sourceIds.forEach((id: any) => {
                const item = store.data.find(item => {
                    return item.id === id;
                });
                item._checked = false;
            });
            let v = value.value;
            v = v.concat([...store.sourceIds]);
            store.sourceIds = [];
            value.value = [...v];
            emit('change', [...v]);
        };

        const transferToSource = () => {
            store.targetIds.forEach((id: any) => {
                const item = store.data.find(item => {
                    return item.id === id;
                });
                item._checked = false;
            });
            const v = value.value;
            store.targetIds.forEach((id: any) => {
                v.splice(v.indexOf(id), 1);
            });
            store.targetIds = [];
            value.value = [...v];
            emit('change', [...v]);
        };

        return () => <div class="cm-transfer">
            <List width={props.width} height={props.height} store={store} name="source"
                value={value.value} onSelect={onSelect} filter={props.filter} render={props.render} title={props.sourceTitle}/>
            <div class="cm-transfer-operation">
                <Button disabled={store.sourceDisabled} icon={<FeatherChevronRight/>} size="small" onClick={transferToTarget}>{rightText}</Button>
                <Button disabled={store.targetDisabled} icon={<FeatherChevronLeft/>} size="small" onClick={transferToSource}>{leftText}</Button>
            </div>
            <List width={props.width} height={props.height} store={store} name="target"
                value={value.value} onSelect={onSelect} filter={props.filter} render={props.render} title={props.targetTitle}/>
        </div>;
    },
});
