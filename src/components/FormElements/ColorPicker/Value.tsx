import { computed, defineComponent, PropType, ref, watchEffect } from "vue";
import { FeatherX } from "cui-vue-icons/feather";

export interface ColorValueProps {
    disabled?: boolean
    open?: boolean
    currentValue?: any;
    value: string | number
    name: string
}

export const Value = defineComponent({
    name: "Value",
    props: {
        open: {type: Boolean as PropType<ColorValueProps['open']>},
        disabled: {type: Boolean as PropType<ColorValueProps['disabled']>},
        currentValue: {type: Object as PropType<ColorValueProps['currentValue']>},
        value: {type: String as PropType<ColorValueProps['value']>},
        name: {type: String as PropType<ColorValueProps['name']>}
    },
    setup (props: ColorValueProps) {
        const bgColorStyle = ref<any>({});
        watchEffect(() => {
            const style = props.open ? {
                background: `rgba(${props.currentValue.rgba.r},${props.currentValue.rgba.g},${props.currentValue.rgba.b},${props.currentValue.rgba.a})`
            } : { background: props.value};
            bgColorStyle.value = style;
        });

        const classList = computed(() => ({
            'cm-color-picker-value': true,
        }));

        return () => <div class={classList.value} tab-index="0">
            {/* 文字对齐辅助 */}
            <span style={{width: '0px', "font-size": '12px', visibility: 'hidden', 'line-height': 'initial'}}>A</span>
            <input type="hidden" name={props.name} value={props.value}/>
            <div class="cm-select-color-wrap">
                {
                    bgColorStyle.value.background
                        ? <div class="cm-select-color" style={bgColorStyle.value}></div>
                        : <div class="cm-select-color cm-select-color-empty"><FeatherX /></div>
                }
            </div>
        </div>;
    }
});
