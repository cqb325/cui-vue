import { defineComponent } from "vue";
import MonthPane from "./MonthPane";

export default defineComponent({
    name: 'MonthRangePane',
    props: {
        value: [String, Array<Date>],
    },
    setup (props, context) {
        const val1 = () => props.value ? props.value[0] : '';
        const val2 = () => props.value ? props.value[1] : '';
        return () => <>
            <MonthPane name="start" value={val1()}/>
            <MonthPane name="end" value={val2()}/>
        </>;
    }
});
