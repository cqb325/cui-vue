import { defineComponent } from "vue";
import DateTimePane from "./DateTimePane";

export default defineComponent({
    name: 'DateTimeRangePane',
    props: {
        value: Array<string|Date>
    },
    setup (props, context) {
        const val1 = () => props.value && props.value[0];
        const val2 = () => props.value && props.value[1];
        return () => <>
            <DateTimePane name="start" value={val1()}/>
            <DateTimePane name="end" value={val2()}/>
        </>;
    }
});
