import { defineComponent } from "vue";
import DatePane from "./DatePane";

export default defineComponent({
    name: 'DateRangePane',
    props: {
        value: [String, Array]
    },
    setup (props) {
        const val1 = () => props.value[0];
        const val2 = () => props.value[1];
        return () => <>
            <DatePane name="start" value={val1()} />
            <DatePane name="end" value={val2()} />
        </>;
    }
});
