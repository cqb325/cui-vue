import { computed, defineComponent, inject } from "vue";
import Button from "../../Button";
import Cell from "./Cell";
import { FeatherChevronLeft } from "cui-vue-icons/feather";
import dayjs from "dayjs";

export default defineComponent({
    name: 'MonthPane',
    props: {
        value: [Date, String],
        name: String,
        onMonthChange: Function
    },
    emits: ['back'],
    setup (props, {emit}) {
        const ctx = inject('CMDatepickerContext', null);
        const store = ctx.store;
        const year = computed(() => {
            if (ctx.type === 'date' || ctx.type === 'dateRange'
                || ctx.type === 'dateTime' || ctx.type === 'dateTimeRange') {
                const index = props.name === 'end' ? 1 : 0;
                return store.currentMonth[index] && store.currentMonth[index].getFullYear && store.currentMonth[index].getFullYear();
            } else {
                return props.value?.getFullYear?.();
            }
        });

        const years = () => {
            const arr = [];
            let now = new Date().getFullYear();
            now = now - 60;
            for (let i = 0; i < 100; i++) {
                arr.push(now + i);
            }
            return arr;
        };
        const months = () => {
            return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].concat([]);
        };
        const month = computed(() => {
            if (ctx.type === 'date' || ctx.type === 'dateRange'
                || ctx.type === 'dateTime' || ctx.type === 'dateTimeRange') {
                const index = props.name === 'end' ? 1 : 0;
                return store.currentMonth[index] && store.currentMonth[index].getMonth && (store.currentMonth[index].getMonth() + 1);
            } else {
                return props.value && props.value.getMonth && (props.value.getMonth() + 1);
            }
        });

        const onSelect = (type: string, num: number) => {
            const index = props.name === 'end' ? 1 : 0;
            const d = new Date(store.currentMonth[index]);
            if (type === 'year') {
                d.setFullYear(num);
                // // 设置年份的时候月份处在禁用月份，选择未禁用的月份
                // let disabled = ctx && ctx.disabledDate && ctx.disabledDate(d);
                // while(disabled) {
                //     d.setMonth(d.getMonth() - 1);
                //     disabled = ctx && ctx.disabledDate && ctx.disabledDate(d);
                // }
            }
            if (type === 'month') {
                d.setMonth(num - 1);
            }
            if (props.onMonthChange) {
                props.onMonthChange(d, type, props.name);
                return;
            }
            store.currentMonth = props.name === 'end' ? [store.currentMonth[0], d] : [d, store.currentMonth[1]];
            // dateRange的时候不需要触发选择
            if (ctx.type !== 'dateRange' && ctx.type !== 'date') {
                ctx && ctx.onSelectDate && ctx.onSelectDate(d, props.name);
            }
        };
        const onBack = () => {
            emit('back');
        };
        return () => <div class="cm-date-picker-month">
            {
                ctx.type === 'date' || ctx.type === 'dateRange' || ctx.type === 'dateTime' || ctx.type === 'dateTimeRange'
                    ? <div class="cm-date-picker-month-header">
                        <Button theme="borderless" onClick={onBack} icon={<FeatherChevronLeft size={16}/>}>返回选择日期</Button>
                    </div>
                    : null
            }
            <div class="cm-date-picker-month-body">
                <Cell data={years()} value={year.value} day={store.currentMonth[0]} type="year" onSelect={onSelect}/>
                <Cell data={months()} value={month.value} day={store.currentMonth[0]} type="month" onSelect={onSelect}/>
            </div>
        </div>;
    }
});
