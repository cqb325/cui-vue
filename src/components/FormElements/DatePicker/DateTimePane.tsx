import DatePane from "./DatePane";
import TimePane from "../TimePicker/TimePane";
import dayjs from "dayjs";
import { defineComponent, inject, ref } from "vue";
import { FeatherCalendar, FeatherClock } from "cui-vue-icons/feather";

export default defineComponent({
    name: "DateTimePane",
    props: {
        name: String,
        value: [String, Date],
    },
    setup (props) {
        const ctx = inject('CMDatepickerContext', null);
        const tab = ref<string>('date');
        const val = () => ctx.store.currentMonth[props.name === 'end' ? 1 : 0];

        const displayDate = () => {
            return dayjs(props.value || new Date()).format('YYYY-MM-DD');
        };
        const displayTime = () => {
            return dayjs(val()).format('HH:mm:ss');
        };

        const selectTab = (atab: string) => {
            tab.value = atab;
        };

        const onSelectTime = (type: string, num: number, name: string) => {
            const v = new Date(val());

            if (type === 'hour') {
                v.setHours(num);
            }
            if (type === 'minute') {
                v.setMinutes(num);
            }
            if (type === 'second') {
                v.setSeconds(num);
            }
            ctx && ctx.onSelectTime(v, props.name);
        };
        return () => <div class="cm-date-picker-datetime">
            <div class="cm-datetime-content">
                {
                    tab.value === 'date' ? <DatePane {...props}/> : null
                }
                {
                    tab.value === 'time' ?
                        <TimePane {...props} format={ctx.format} header="选择时间" value={val()} onSelectTime={onSelectTime}/>
                        : null
                }
            </div>
            <div class="cm-datetime-switch">
                <div class={{"cm-datetime-switch-item": true, 'active': tab.value === 'date'}} onClick={selectTab.bind(null, 'date')}>
                    <FeatherCalendar size={12}/>
                    {displayDate()}
                </div>
                <div class={{"cm-datetime-switch-item": true, 'active': tab.value === 'time'}} onClick={selectTab.bind(null, 'time')}>
                    <FeatherClock size={12}/>
                    {displayTime()}
                </div>
            </div>
        </div>;
    }
});
