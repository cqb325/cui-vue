import { Value } from "../../inner/Value";
import DatePane from "./DatePane";
import MonthPane from "./MonthPane";
import MonthRangePane from "./MonthRangePane";
import DateRangePane from "./DateRangePane";
import DateTimePane from "./DateTimePane";
import DateTimeRangePane from "./DateTimeRangePane";
import dayjs from "dayjs";
import { PropType, computed, defineComponent, provide, reactive, ref, watchEffect } from "vue";
import Dropdown from "../../Dropdown";
import formFieldRef from "../../use/formFieldRef";
import { FeatherCalendar } from "cui-vue-icons/feather";

type DatepickerProps = {
    disabled?: boolean,
    theme?: 'light'|'dark',
    size?: 'small'|'large',
    clearable?: boolean,
    type?: 'date'|'dateRange'|'monthRange'|'month'|'dateTime'|'dateTimeRange',
    align?: 'bottomLeft'|'bottomRight',
    format?: string,
    modelValue?: string | Date | string[] | Date[],
    prepend?: string | any,
    header?: string | any | string[] | any[],
    footer?: string | any | string[] | any[],
    seperator?: string,
    transfer?: boolean,
    trigger?: () => any,
    disabledDate?: (day: Date) => boolean,
    onChange?: (value: any) => void,
    maxRange?: number,
    shortCuts?: (() => any) | any,
    revers?: boolean
    placeholder?: string
    // daterange的月份是否粘连
    stick?: boolean
    asFormField?: boolean
}

export type DatepickerStore = {
    currentMonth: Date[],
    range: Date[],
    hoverDate?: Date
}

export default defineComponent({
    name: 'Datepicker',
    props: {
        disabled: {type: Boolean as PropType<DatepickerProps['disabled']>},
        theme: {type: String as PropType<DatepickerProps['theme']>},
        size: {type: String as PropType<DatepickerProps['size']>},
        clearable: {type: Boolean as PropType<DatepickerProps['clearable']>},
        type: {type: String as PropType<DatepickerProps['type']>},
        align: {type: String as PropType<DatepickerProps['align']>},
        format: {type: String as PropType<DatepickerProps['format']>},
        modelValue: {type: [String, Date, Array] as PropType<DatepickerProps['modelValue']>, default: undefined},
        prepend: {type: [String, Object] as PropType<DatepickerProps['prepend']>},
        header: {type: [String, Object] as PropType<DatepickerProps['header']>},
        footer: {type: [String, Object] as PropType<DatepickerProps['footer']>},
        seperator: {type: String as PropType<DatepickerProps['seperator']>},
        disabledDate: {type: Function as PropType<DatepickerProps['disabledDate']>},
        transfer: {type: Boolean as PropType<DatepickerProps['transfer']>},
        trigger: {type: Function as PropType<DatepickerProps['trigger']>},
        onChange: {type: Function as PropType<DatepickerProps['onChange']>},
        maxRange: {type: Number as PropType<DatepickerProps['maxRange']>},
        shortCuts: {type: [Function, Object] as PropType<DatepickerProps['shortCuts']>},
        revers: {type: Boolean as PropType<DatepickerProps['revers']>},
        placeholder: {type: String as PropType<DatepickerProps['placeholder']>},
        stick: {type: Boolean as PropType<DatepickerProps['stick']>},
        asFormField: {type: Boolean as PropType<DatepickerProps['asFormField']>, default: true}
    },
    emits: ['change', 'update:modelValue'],
    setup (props, {emit}) {
        const visible = ref(false);
        const type = props.type ?? 'date';
        const value = formFieldRef(props, emit, props.modelValue ?? (type === 'dateRange' || type === 'dateTimeRange') ? [] : '');
        const v = ref<any>();
        let format = props.format ?? 'YYYY-MM-DD';
        if (type === 'month' || type === 'monthRange') {
            format = props.format ?? 'YYYY-MM';
        }
        if (type === 'dateTime' || type === 'dateTimeRange') {
            format = props.format ?? 'YYYY-MM-DD HH:mm:ss';
        }
        const now = new Date();
        const next = new Date();
        next.setMonth(next.getMonth() + 1);
        const store = reactive({
            currentMonth: [now, next],
            range: [],
            hoverDate: undefined
        } as DatepickerStore);
        const align = props.align ?? 'bottomLeft';
        const seperator = props.seperator || '~';

        watchEffect(() => {
            let val: any = value.value;
            if (val && val instanceof Array && typeof val[0] === 'function') {
                val = val[0]();
            }
            let currentMonth: Date[];
            if (val) {
                if (typeof val === 'string') {
                    if (type === 'dateRange' || type === 'monthRange' || type === 'dateTimeRange') {
                        const arr: string[] = val.split(seperator);
                        val = [
                            dayjs(arr[0]).toDate(),
                            dayjs(arr[1]).toDate(),
                        ];
                        const prev = new Date(val[0]);
                        const next = new Date(val[1]);
                        if (dayjs(prev).format('YYYY-MM') === dayjs(next).format('YYYY-MM')) {
                            next.setMonth(next.getMonth() + 1);
                        }
                        currentMonth = [prev, next];
                    } else {
                        val = dayjs(val).toDate();
                        const prev = new Date(val);
                        const next = new Date(val);
                        next.setMonth(next.getMonth() + 1);
                        currentMonth = [prev, next];
                    }
                } else if (val instanceof Date) {
                    val = dayjs(val).toDate();
                    const prev = new Date(val);
                    const next = new Date(val);
                    next.setMonth(next.getMonth() + 1);
                    currentMonth = [prev, next];
                } else {
                    let prev: Date = new Date(); let next: Date = new Date();
                    if (val instanceof Array) {
                        if (typeof val[0] === 'string') {
                            val[0] = dayjs(val[0]).toDate();
                        }
                        if (typeof val[1] === 'string') {
                            val[1] = dayjs(val[1]).toDate();
                        }

                        prev = val[0] === undefined ? new Date() : (val[0] ? new Date(val[0]) : new Date());
                        next = val[1] === undefined ? new Date() : (val[1] ? new Date(val[1]) : new Date());
                    }
                    if (type=== 'month' && val instanceof Date) {
                        prev = val;
                        next = new Date(val);
                    }

                    if (dayjs(prev).format('YYYY-MM') === dayjs(next).format('YYYY-MM')) {
                        next.setMonth(next.getMonth() + 1);
                    }
                    currentMonth = [prev, next];
                }
                if (type === 'dateRange' || type === 'dateTimeRange') {
                    store.range = val;
                }
            } else {
                currentMonth = [now, next];
            }
            // 粘连时根据第一个month往后加一个月
            if (props.stick) {
                currentMonth[1] = new Date(currentMonth[0]);
                currentMonth[1].setMonth(currentMonth[1].getMonth() + 1);
            }
            currentMonth[0].setDate(1);
            currentMonth[1].setDate(1);

            store.currentMonth = currentMonth;
            v.value = val;
        });

        const classList = computed(() => ({
            'cm-date-picker': true,
            [`cm-date-picker-${props.size}`]: props.size,
            'cm-date-picker-disabled': props.disabled,
            'cm-date-picker-clearable': !props.disabled && props.clearable && (value.value && value.value.length !== 0),
        }));

        const onClear = () => {
            value.value = '';
            if (type === 'dateRange') {
                store.range = [];
            }
            emit('change', '');
        };

        // 点击选择日期
        const onSelectDate = (day: Date, name: string) => {
            const va = new Date(day);
            if (type === 'month' || type === 'monthRange') {
                va.setDate(1);
                va.setHours(0);
                va.setMinutes(0);
                va.setSeconds(0);
                va.setMilliseconds(0);
            }
            if (type === 'dateTime' || type === 'dateTimeRange') {
                let val = v.value;
                if (type === 'dateTimeRange') {
                    val = val && val.length ? val[store.range.length === 1 ? 1 : 0] : store.currentMonth[store.range.length === 1 ? 1 : 0];
                } else {
                    val = val ? val : store.currentMonth[store.range.length === 1 ? 1 : 0];
                }
                va.setHours(val.getHours());
                va.setMinutes(val.getMinutes());
                va.setSeconds(val.getSeconds());
            }
            const now = new Date();
            let origin = v.value || (type === 'monthRange' || type === 'dateRange' || type === 'dateTimeRange' ? [now, now] : now);
            if ((type === 'dateRange' || type === 'dateTimeRange') && !origin.length) {
                origin = [];
                origin.push(now);
                origin.push(now);
            }
            let newVal;
            if (name === 'start') {
                newVal = [va, origin[1]];
            } else if (name === 'end') {
                newVal = [origin[0], va];
            } else {
                newVal = va;
            }
            if (newVal instanceof Array && newVal[0].getTime() > newVal[1].getTime()) {
                newVal.reverse();
            }
            // dateRange特殊处理
            if (type === 'dateRange' || type === 'dateTimeRange') {
                const range = store.range;
                let newRange: Date[] = [];
                // 上次已经选择
                if ((range[0] && range[1]) || (!range[0] && !range[1])) {
                    newRange = [va];
                    store.hoverDate = new Date(va);
                }
                if (range[0] && !range[1]) {
                    if (isOutRange(range[0], va)) {
                        return;
                    }
                    newRange = [range[0], va];
                    if (newRange[0].getTime() > newRange[1].getTime()) {
                        newRange.reverse();
                        // dateTImeRange时需要切换currentMonth的时间
                        const tmp = new Date();
                        copyHMS(tmp, store.currentMonth[0]);
                        copyHMS(store.currentMonth[0], store.currentMonth[1]);
                        copyHMS(store.currentMonth[1], tmp);
                        store.currentMonth = [...store.currentMonth];
                    }
                    value.value = newRange;
                    if (type === 'dateRange') {
                        visible.value = false;
                    }
                }
                store.range = newRange;
                return;
            }
            value.value = newVal;
            emit('change', newVal);
            if (type === 'date') {
                visible.value = false;
            }
        };

        const copyHMS = (target: Date, source: Date) => {
            target.setHours(source.getHours());
            target.setMinutes(source.getMinutes());
            target.setSeconds(source.getSeconds());
        };

        const onSelectTime = (time: Date, name: 'start'|'end') => {
            let val = v.value;
            let month: Date;
            if (name === 'start') {
                month = store.currentMonth[0];
                if (val && val[0]) {
                    copyHMS(val[0], time);
                    if (val[0].getTime() > val[1].getTime()) {
                        val.reverse();
                        // dateTImeRange时需要切换currentMonth的时间
                        copyHMS(store.currentMonth[0], val[0]);
                        copyHMS(store.currentMonth[1], val[1]);
                    } else {
                        copyHMS(month, time);
                    }
                    value.value = [...val];
                } else {
                    copyHMS(month, time);
                }
            } else if (name === 'end') {
                month = store.currentMonth[1];
                if (val && val[1]) {
                    copyHMS(val[1], time);
                    if (val[0].getTime() > val[1].getTime()) {
                        val.reverse();
                        // dateTImeRange时需要切换currentMonth的时间
                        copyHMS(store.currentMonth[0], val[0]);
                        copyHMS(store.currentMonth[1], val[1]);
                    } else {
                        copyHMS(month, time);
                    }
                    value.value = [...val];
                } else {
                    copyHMS(month, time);
                }
            } else {
                if (!val) {
                    val = new Date();
                }
                copyHMS(val, time);
                month = store.currentMonth[0];
                copyHMS(month, time);
                value.value = new Date(val);
            }
            store.currentMonth = [...store.currentMonth];
        };

        /**
         * 是超出maxRange
         * @param start
         * @param current
         * @returns
         */
        const isOutRange = (start: Date, current: Date) => {
            if (props.maxRange) {
                const ms = start.getTime() - current.getTime();
                const days = Math.abs(ms / 1000 / 60 / 60 / 24);
                if (days > props.maxRange - 1) {
                    return true;
                }
            }
            return false;
        };

        // 时间段选择
        const onMouseOver = (day: Date) => {
            if (store.range && store.range[0]) {
                if (isOutRange(store.range[0], day) && props.maxRange) {
                    const end = new Date(store.range[0]);
                    const delta = day.getTime() > store.range[0].getTime() ? 1 : -1;
                    end.setDate(end.getDate() + (props.maxRange - 1) * delta);
                    store.hoverDate = end;
                    return;
                }
                store.hoverDate = new Date(day);
            }
        };

        const text = () => {
            const val = v.value;
            if (val) {
                if (typeof val === 'string') {
                    return val;
                } else {
                    if (type === 'dateRange' || type === 'monthRange' || type === 'dateTimeRange') {
                        if (!val[0]) {
                            return '';
                        }
                        return [
                            dayjs(val[0]).format(format),
                            dayjs(val[1]).format(format),
                        ].join(seperator);
                    }
                    return dayjs(val).format(format);
                }
            }
            return '';
        };

        provide('CMDatepickerContext', {store, onSelectDate, onMouseOver, visible,
            disabledDate: props.disabledDate, onSelectTime, stick: props.stick, type, format});

        return () => <div class={classList.value}>
            <Dropdown v-model={visible.value} transfer={props.transfer} align={align} revers={props.revers}
                trigger="click" disabled={props.disabled} menu={<div class="cm-date-picker-wrap">
                    {
                        props.shortCuts ?
                            <div class="cm-date-picker-shortcuts">
                                {typeof props.shortCuts === 'function' ? props.shortCuts() : props.shortCuts}
                            </div>
                            : null
                    }
                    {
                        type === 'date' ?
                            <DatePane value={v.value}/>
                            : null
                    }
                    {
                        type === 'month' ? <MonthPane value={v.value}/> : null
                    }
                    {
                        type === 'monthRange' ? <MonthRangePane value={v.value}/> : null
                    }
                    {
                        type === 'dateRange' ?
                            <DateRangePane value={v.value}/>
                            : null
                    }
                    {
                        type === 'dateTime' ?
                            <DateTimePane value={v.value}/>
                            : null
                    }
                    {
                        type === 'dateTimeRange' ?
                            <DateTimeRangePane value={v.value}/>
                            : null
                    }
                </div>}>
                {
                    !props.trigger ?
                        <Value prepend={props.prepend} text={text()} onClear={onClear} clearable={props.clearable}
                            placeholder={props.placeholder} disabled={props.disabled} size={props.size} icon={<FeatherCalendar />}/>
                        : props.trigger && props.trigger()
                }
            </Dropdown>
        </div>;
    }
});
