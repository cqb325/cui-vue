import { computed, defineComponent, PropType, VNode, watchEffect } from "vue";
import Cell from "./Cell";

export default defineComponent({
    name: 'TimePane',
    props: {
        value: {
            type: [String, Object] as PropType<string | Date>
        },
        format: String,
        header: [String, Object] as PropType<string | VNode>,
        footer: [String, Object] as PropType<string | VNode>,
        name: String,
        hourStep: Number,
        minuteStep: Number,
        secondStep: Number,
    },
    emits: ['selectTime'],
    setup (props, ctx) {
        const hour = computed(() => {
            return props.value instanceof Date ? props.value.getHours() : undefined;
        });

        const minute = computed(() => {
            return props.value instanceof Date ? props.value.getMinutes() : undefined;
        });

        const second = computed(() => {
            return props.value instanceof Date ? props.value.getSeconds() : undefined;
        });

        // 检查格式是否需要显示小时、分钟、秒
        const hasHour = computed(() => props.format.includes('H'));
        const hasMinute = computed(() => props.format.includes('m'));
        const hasSecond = computed(() => props.format.includes('s'));

        return () => <div class="cm-time-picker-pane">
            {
                props.header && <div class="cm-time-picker-header">{props.header}</div>
            }
            <div class="cm-time-picker-options">
                {
                    hasHour.value &&
                        <Cell max={24} type="hour" value={hour.value} step={props.hourStep} name={props.name} onSelectTime={ctx.emit.bind(ctx, 'selectTime')}/>
                }
                {
                    hasMinute.value &&
                        <Cell max={60} type="minute" value={minute.value} step={props.minuteStep} name={props.name} onSelectTime={ctx.emit.bind(ctx, 'selectTime')}/>
                }
                {
                    hasSecond.value &&
                        <Cell max={60} type="second" value={second.value} step={props.secondStep} name={props.name} onSelectTime={ctx.emit.bind(ctx, 'selectTime')}/>
                }
            </div>
            {
                props.footer && <div class="cm-time-picker-footer">{props.footer}</div>
            }
        </div>;
    }
});
