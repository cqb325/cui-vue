import { defineComponent, PropType, VNode } from "vue";
import TimePane from "./TimePane";

export default defineComponent({
    name: "TimeRange",
    props: {
        header: Array as PropType<VNode[]>,
        footer: Array as PropType<VNode[]>,
        value: [String, Array] as PropType<string[] | Date[]>,
        format: String,
        minuteStep: Number,
        hourStep: Number,
        secondStep: Number,
    },
    setup (props, ctx) {
        const {header, footer, value, ...others} = props;
        return () => <>
            <TimePane value={props.value[0]} header={props.header[0]} footer={(props.footer && props.footer.length) && props.footer[0]} {...others} name="start"/>
            <TimePane value={props.value[1]} header={props.header[1]} footer={(props.footer && props.footer.length) && props.footer[1]} {...others} name="end"/>
        </>;
    },
});
