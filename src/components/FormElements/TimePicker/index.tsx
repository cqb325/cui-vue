import { Value } from "../../inner/Value";
import TimePane from "./TimePane";
import TimeRange from "./TimeRange";
import Dropdown from "../../Dropdown";
import dayjs from "dayjs";
import { PropType, VNode, computed, defineComponent, provide, ref, watchEffect } from "vue";
import formFieldRef from "../../use/formFieldRef";
import { FeatherClock } from "cui-vue-icons/feather";

type TimePickerProps = {
    type?: 'time'|'timeRange',
    disabled?: boolean,
    theme?: 'light'|'dark',
    size?: 'small'|'large',
    clearable?: boolean,
    align?: 'bottomLeft'|'bottomRight',
    format?: string,
    modelValue?: string | Date | string[] | Date[],
    prepend?: string | any,
    disabledTime?: (num: number, type: string) => boolean,
    minuteStep?: number,
    secondStep?: number,
    hourStep?: number,
    header?: string | any | string[] | VNode[],
    footer?: string | any | string[] | VNode[],
    seperator?: string,
    transfer?: boolean,
    trigger?: () => VNode,
    placeholder?: string,
    asFormField?: boolean,
}

export default defineComponent({
    name: 'TimePicker',
    props: {
        type: {type: String as PropType<TimePickerProps['type']>, default: 'time'},
        disabled: {type: Boolean as PropType<TimePickerProps['disabled']>, default: false},
        theme: {type: String as PropType<TimePickerProps['theme']>, default: 'light'},
        size: {type: String as PropType<TimePickerProps['size']>, default: ''},
        clearable: {type: Boolean as PropType<TimePickerProps['clearable']>, default: false},
        align: {type: String as PropType<TimePickerProps['align']>, default: 'bottomLeft'},
        format: {type: String as PropType<TimePickerProps['format']>, default: 'HH:mm:ss'},
        modelValue: {type: [String, Date, Array] as PropType<TimePickerProps['modelValue']>, default: undefined},
        prepend: {type: [String, Object] as PropType<TimePickerProps['prepend']>, default: ''},
        disabledTime: {type: Function as PropType<TimePickerProps['disabledTime']>},
        minuteStep: {type: Number as PropType<TimePickerProps['minuteStep']>},
        secondStep: {type: Number as PropType<TimePickerProps['secondStep']>},
        hourStep: {type: Number as PropType<TimePickerProps['hourStep']>},
        header: {type: [String, Array] as PropType<TimePickerProps['header']>},
        footer: {type: [String, Array] as PropType<TimePickerProps['footer']>},
        seperator: {type: String as PropType<TimePickerProps['seperator']>},
        transfer: {type: Boolean as PropType<TimePickerProps['transfer']>},
        trigger: {type: Function as PropType<TimePickerProps['trigger']>},
        placeholder: {type: String as PropType<TimePickerProps['placeholder']>},
        asFormField: {type: Boolean as PropType<TimePickerProps['asFormField']>, default: true},
    },
    emits: ['change', 'update:modelValue'],
    setup (props, {emit}) {
        const value = formFieldRef<string | Date | string[] | Date[]>(props, emit, props.modelValue ?? props.type === 'timeRange' ? [] : '');
        // 内部value，防止类似form调用的setValue后重复执行effect
        const v = ref<any>(value.value);
        const visible = ref(false);
        const align = props.align ?? 'bottomLeft';
        const format = props.format ?? 'HH:mm:ss';
        const seperator = props.seperator || '~';
        const header = props.header ?? (props.type === 'timeRange' ? ['开始时间', '结束时间'] : undefined);
        const classList = computed(() => ({
            'cm-time-picker': true,
            'cm-time-picker-disabled': props.disabled,
            [`cm-time-picker-${props.theme}`]: props.theme,
            [`cm-time-picker-${props.size}`]: props.size,
            'cm-time-picker-clearable': !props.disabled && props.clearable && (value.value !== '' && value.value.length !== 0),
            'cm-time-picker-open': open
        }));

        watchEffect(() => {
            let val = value.value;
            if (val) {
                if (typeof val === 'string') {
                    if (props.type === 'timeRange') {
                        const arr: string[] = val.split(seperator);
                        val = [
                            dayjs(dayjs().format('YYYY-MM-DD ') + arr[0]).toDate(),
                            dayjs(dayjs().format('YYYY-MM-DD ') + arr[1]).toDate(),
                        ];
                    } else {
                        val = dayjs(dayjs().format('YYYY-MM-DD ') + val).toDate();
                    }
                } else if (val instanceof Array) {
                    if (val[0] && typeof val[0] === 'string') {
                        val = [
                            dayjs(dayjs().format('YYYY-MM-DD ') + val[0]).toDate(),
                            dayjs(dayjs().format('YYYY-MM-DD ') + val[1]).toDate(),
                        ];
                    }
                }
            }

            v.value = val;
        });

        const onSelect = (type: string, num: number, name: string) => {
            const now = new Date();
            const origin: any = v.value || (props.type === 'timeRange' ? [now, now] : now);
            if (props.type === 'timeRange' && !(origin as any[]).length) {
                origin.push(now);
                origin.push(now);
            }
            let val: Date;
            if (name === 'start') {
                val = origin[0];
            } else if (name === 'end') {
                val = origin[1];
            } else {
                val = origin;
            }
            if (type === 'hour') {
                val.setHours(num);
            }
            if (type === 'minute') {
                val.setMinutes(num);
            }
            if (type === 'second') {
                val.setSeconds(num);
            }
            if (props.type === 'timeRange') {
                let newVal: Date[] = [];
                if (name === 'start') {
                    newVal = [new Date(val), origin[1]];
                }
                if (name === 'end') {
                    newVal = [origin[0], new Date(val)];
                }

                if (newVal[0].getTime() > newVal[1].getTime()) {
                    newVal = [newVal[1], newVal[0]];
                }

                value.value = newVal;
                props.onChange && props.onChange(newVal);
            } else {
                const ret = new Date(val);
                value.value = ret;
                emit('change', ret);
            }
        };

        const onClear = () => {
            value.value = '';
            emit('change', '');
        };

        const text = computed(() => {
            const val: any = v.value;
            if (val) {
                if (typeof val === 'string') {
                    return val;
                } else {
                    if (props.type === 'timeRange') {
                        if (val.length) {
                            if (typeof val[0] === 'string') {
                                return val.join(seperator);
                            }
                            return [
                                dayjs(val[0]).format(format),
                                dayjs(val[1]).format(format),
                            ].join(seperator);
                        } else {
                            return '';
                        }
                    }
                    return dayjs(val).format(format);
                }
            }
            return '';
        });

        provide('CMTimepickerContext', {onSelect, disabledTime: props.disabledTime, visible});

        return () => <div class={classList.value} x-placement={align} tab-index="1">
            <Dropdown transfer={props.transfer} align={align} v-model={visible.value} trigger="click"
                disabled={props.disabled} menu={<div class="cm-time-picker-wrap">
                    {
                        props.type === 'timeRange'
                            ? <TimeRange value={v.value} format={format} minuteStep={props.minuteStep} secondStep={props.secondStep}
                                hourStep={props.hourStep} header={header} footer={props.footer}/>
                            : <TimePane value={v.value} format={format} minuteStep={props.minuteStep} secondStep={props.secondStep}
                                hourStep={props.hourStep} header={header} footer={props.footer}/>
                    }
                </div>}>
                {
                    !props.trigger
                        ? <Value prepend={props.prepend} text={text.value} onClear={onClear} clearable={props.clearable}
                            placeholder={props.placeholder} disabled={props.disabled} size={props.size} icon={<FeatherClock />}/>
                        : props.trigger && props.trigger()
                }
            </Dropdown>
        </div>;
    }
});
