import { PropType, defineComponent } from "vue";
import formFieldRef from "../../use/formFieldRef";
import Checkbox from "../../inner/Checkbox";
import { CheckboxProps } from "../Checkbox";

export default defineComponent({
    name: 'Radio',
    props: {
        disabled: {
            type: Boolean,
        },
        type: {
            type: String as PropType<CheckboxProps['type']>,
        },
        name: {
            type: String as PropType<CheckboxProps['name']>,
        },
        label: {
            type: [String, Object] as PropType<CheckboxProps['label']>,
        },
        inner: {
            type: Boolean as PropType<CheckboxProps['inner']>,
        },
        modelValue: {
            type: [Boolean, String] as PropType<CheckboxProps['modelValue']>,
        },
        value: {
            type: [String, Number] as PropType<CheckboxProps['value']>,
        },
        asFormField: {
            type: Boolean as PropType<CheckboxProps['asFormField']>, default: true
        }
    },
    emits: ['change', 'update:modelValue'],
    setup (props, { emit }) {
        const checked = formFieldRef(props, emit);

        const onChange = (checkedVal, value) => {
            if (props.disabled) {
                return;
            }
            checked.value = checkedVal;
            emit('change', checkedVal, value);
        };
        return () => <Checkbox {...props} class="cm-radio" type={props.type ?? 'radio'} onChange={onChange} checked={checked.value}/>;
    }
});
