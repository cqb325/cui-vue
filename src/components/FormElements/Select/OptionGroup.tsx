import { defineComponent, inject, onUnmounted } from "vue";

export default defineComponent({
    name: "OptionGroup",
    props: {
        value: {type: [String, Number, Boolean]},
        label: {type: String},
    },
    setup (props, { slots }) {
        const ctx = inject('CMSelectContext', null);
        ctx.addOption({...props, group: true});
        onUnmounted(() => {
            ctx.removeOption({value: props.value});
        });
        return () => slots.default?.();
    }
});
