import { computed, defineComponent } from "vue";

type SelectOptions = {
    data: any,
    checked?: boolean,
    disabled?: boolean,
    onClick?: Function,
    style?: any,
    visible?: boolean,
}

export default defineComponent({
    name: 'EmptyOption',
    props: {
        checked: Boolean,
        data: Object,
    },
    emits: ['click'],
    setup (props, {emit}) {
        const classList = computed(() => ({
            'cm-select-option': true,
            'cm-select-option-active': props.checked,
        }));
        const value = props.data.value;
        return () => <li class={classList.value} onClick={() => emit('click', value)}>
            {props.data.label}
        </li>;
    }
});
