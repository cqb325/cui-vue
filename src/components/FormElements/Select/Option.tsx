import { PropType, defineComponent, inject, onUnmounted } from "vue";

export interface SelectOptions {
    disabled?: boolean,
    value?: any,
    style?: any,
    class?: string,
    label?: string,
    [key: string]: any
}

export default defineComponent({
    name: 'Option',
    props: {
        style: {type: Object as PropType<SelectOptions['style']>},
        class: {type: Object as PropType<SelectOptions['class']>},
        disabled: {type: Boolean as PropType<SelectOptions['disabled']>},
        value: {type: [String, Number, Boolean] as PropType<SelectOptions['value']>},
        label: {type: String as PropType<SelectOptions['label']>},
    },
    setup (props) {
        const ctx = inject('CMSelectContext', null);
        ctx.addOption({...props});
        onUnmounted(() => {
            ctx.removeOption({value: props.value});
        });
        return () => null;
    }
});
