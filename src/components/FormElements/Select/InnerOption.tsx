import { PropType, computed, defineComponent } from "vue";

export type SelectOptions = {
    data: any,
    checked?: boolean,
    disabled?: boolean,
    textField: string,
    valueField: string,
    onClick?: (value: any) => void,
    visible?: boolean,
    emptyOption: boolean,
    value?: any,
    label?: string,
    renderOption?: (item: any) => any
}

export default defineComponent({
    name: 'InnerOption',
    props: {
        data: {type: Object as PropType<SelectOptions['data']>},
        checked: {type: Boolean as PropType<SelectOptions['checked']>},
        disabled: {type: Boolean as PropType<SelectOptions['disabled']>},
        textField: {type: String as PropType<SelectOptions['textField']>},
        valueField: {type: String as PropType<SelectOptions['valueField']>},
        visible: {type: Boolean as PropType<SelectOptions['visible']>},
        value: {type: [String, Number, Boolean] as PropType<SelectOptions['value']>},
        label: {type: String as PropType<SelectOptions['label']>},
        emptyOption: {type: Boolean as PropType<SelectOptions['emptyOption']>},
        renderOption: {type: Function as PropType<SelectOptions['renderOption']>}
    },
    emits: ['click'],
    setup (props, {emit}) {
        const classList = computed(() => ({
            'cm-select-option': true,
            'cm-select-group-wrap': props.data.group,
            'cm-select-option-active': props.checked,
            'cm-select-option-disabled': props.data.disabled,
        }));
        const onClick = () => {
            if (props.disabled) {
                return;
            }
            emit('click', value, props.data);
        };
        const value = props.data[props.valueField];
        return () => props.visible ? <li class={classList.value} onClick={onClick}>
            {props.renderOption ? props.renderOption(props.data) : props.data[props.textField]}
        </li> : null;
    }
});
