import { PropType, computed, defineComponent, provide } from "vue";
import { createGutter } from "../use/createGridOffset";
export const RowContext = Symbol('RowContext');

export type GutterProps = {
    xs?: number | number[],
    sm?: number | number[],
    md?: number | number[],
    lg?: number | number[],
    xl?: number | number[],
    xxl?: number | number[]
}

export interface RowProps {
    justify?: 'start'|'center'|'end'|'space-between'|'space-around',
    align?: 'top'|'middle'|'bottom',
    gutter?: number | number[] | GutterProps,
}

export default defineComponent({
    name: 'Row',
    props: {
        justify: {
            type: String as PropType<RowProps['justify']>,
        },
        align: {
            type: String as PropType<RowProps['align']>,
        },
        gutter: {
            type: [Number, Array, Object] as PropType<RowProps['gutter']>,
        }
    },
    setup (props, { slots }) {
        const gutterClass = typeof props.gutter === 'object' ? createGutter(props.gutter) : {};
        const classList = computed(() => ({
            'cm-row': true,
            ...gutterClass,
            [`cm-row-${props.justify}`]: props.justify,
            [`cm-row-${props.align}`]: props.align
        }));

        const newStyle = computed(() => {
            const obj = {
            };
            let half = 0;
            let rowGutter = 0;
            if (typeof props.gutter === 'number') {
                half = props.gutter ? props.gutter / 2 : 0;
                rowGutter = props.gutter || 0;
            }
            if (Array.isArray(props.gutter)) {
                half = props.gutter[0] ? props.gutter[0] / 2 : 0;
                rowGutter = props.gutter[1] || 0;
            }

            if (half) {
                obj['margin-left'] = `-${half}px`;
                obj['margin-right'] = `-${half}px`;
            }
            if (rowGutter) {
                obj['row-gap'] = `${rowGutter}px`;
            }
            return obj;
        });
        provide(RowContext, props.gutter);
        return () => <div class={classList.value} style={newStyle.value}>{slots.default?.()}</div>;
    }
});
