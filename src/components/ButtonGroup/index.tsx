import { HTMLAttributes, PropType, defineComponent, provide } from 'vue';
import { type ButtonProps } from '../Button';

export const BUTTON_GROUP_CONTEXT = Symbol('button_group_context');

interface ButtonGroupProps extends HTMLAttributes {
    type?: ButtonProps['type'],
    theme?: ButtonProps['theme'],
    size?: 'small'|'default'|'large'
    disabled?: boolean
}

export default defineComponent({
    name: 'ButtonGroup',
    props: {
        type: {type: String as PropType<ButtonGroupProps['type']>},
        theme: {type: String as PropType<ButtonGroupProps['theme']>},
        size: { type: String as PropType<ButtonGroupProps['size']>},
        disabled: {
            type: Boolean as PropType<ButtonGroupProps['disabled']>,
        }
    },
    setup (props: ButtonGroupProps, {slots}) {
        provide(BUTTON_GROUP_CONTEXT, {
            type: props.type,
            theme: props.theme,
            size: props.size,
            disabled: props.disabled,
        });
        return () => (
            <div class="cm-button-group">
                {slots.default?.()}
            </div>
        );
    },
});
