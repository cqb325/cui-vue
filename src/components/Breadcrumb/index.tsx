import Item from "./Item";
import { HTMLAttributes, PropType, VueElement, defineComponent, provide } from "vue";

export const BreadcrumbContext = Symbol('BreadcrumbContext');

export interface BreadcrumbProps extends HTMLAttributes {
    separator?: string,
}

const Breadcrumb = defineComponent({
    name: 'Breadcrumb',
    props: {
        separator: {
            type: [String, VueElement] as PropType<BreadcrumbProps['separator']>
        },
    },
    setup (props: BreadcrumbProps, { slots }) {
        provide(BreadcrumbContext, {
            separator: props.separator
        });
        return () => (
            <div class="cm-breadcrumb">
                { slots.default?.() }
            </div>
        );
    }
});

Breadcrumb.Item = Item;

export default Breadcrumb;
