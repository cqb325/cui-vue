import { BreadcrumbContext } from ".";
import Space from "../Space";
import { PropType, VueElement, defineComponent, inject } from "vue";

export interface BreadcrumbItemProps {
    link?: string,
    icon?: any,
    separator?: string | VueElement
}

export default defineComponent({
    name: 'BreadcrumbItem',
    props: {
        link: {
            type: String as PropType<BreadcrumbItemProps['link']>
        },
        icon: {
            type: Object as PropType<BreadcrumbItemProps['icon']>
        },
        separator: {
            type: [String, VueElement] as PropType<BreadcrumbItemProps['separator']>
        },
    },
    setup (props, {slots}) {
        const ctx: any = inject(BreadcrumbContext);
        return () => <span class="cm-breadcrumb-wrap">
            <a class="cm-breadcrumb-item" href={props.link}>
                <Space size={4} align="center">
                    {props.icon}
                    {slots.default && slots.default()}
                </Space>
            </a>
            <span class="cm-breadcrumb-separator">{ctx.separator || props.separator || '/'}</span>
        </span>;
    },
});
