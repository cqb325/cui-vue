import { defineComponent, ref } from "vue";
import type { InnerPopupProps } from "../inner/InnerPopup";
import { InnerPopup, innerPopupDefinedProps } from "../inner/InnerPopup";

export interface TooltipProps extends Omit<InnerPopupProps, 'title'> {
}

export default defineComponent({
    name: "Tooltip",
    props: innerPopupDefinedProps,
    emits: ['update:modelValue', 'visibleChange'],
    setup (props: TooltipProps, {slots, emit, attrs, expose}) {
        const pop = ref();
        expose({
            updatePosition () {
                pop.value?.updatePosition();
            }
        });

        const align = props.align ?? 'top';
        const arrow = props.arrow ?? true;

        return () => <InnerPopup ref={pop} {...props} {...attrs} clsPrefix="cm-tooltip"
            varName="tooltip" align={align} arrow={arrow} confirm={false}
            onUpdate:modelValue={(val: boolean) => emit('update:modelValue', val)}
            onVisibleChange={(val: boolean) => emit('visibleChange', val)}
        >
            {slots.default?.()}
        </InnerPopup>;
    }
});
