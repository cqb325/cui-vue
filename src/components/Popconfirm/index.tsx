import type { InnerPopupProps } from "../inner/InnerPopup";
import { InnerPopup, innerPopupDefinedProps } from "../inner/InnerPopup";
import type { ButtonProps } from "../Button";
import { defineComponent, PropType, VNode } from "vue";

export interface PopconfirmProps extends InnerPopupProps {
    okText?: string
    cancelText?: string
    onOk?: () => void | Promise<boolean|void>
    onCancel?: () => void
    okType?: ButtonProps['type']
    cancelType?: ButtonProps['type']
    icon?: VNode
    showCancel?: boolean
}

export default defineComponent({
    name: "Popconfirm",
    props: {...innerPopupDefinedProps,
        showCancel: {type: Boolean as PropType<InnerPopupProps['showCancel']>, default: true},
    },
    emits: ['update:modelValue', 'visibleChange'],
    setup (props: PopconfirmProps, {slots, emit, attrs, expose}) {

        return () => <InnerPopup {...props} clsPrefix="cm-popconfirm" varName="popconfirm" {...attrs} theme={props.theme || "light"} confirm
            align={props.align || 'top'} title={<>
                {props.icon}
                <div class="cm-popconfirm-title-text">{props.title}</div>
            </>}
            onUpdate:modelValue={(val: boolean) => emit('update:modelValue', val)}
            onVisibleChange={(val: boolean) => emit('visibleChange', val)}
        >
            {slots.default?.()}
        </InnerPopup>;
    }
});
