import { computed, defineComponent, reactive } from "vue";

export default defineComponent({
    name: 'LoadingBar',
    props: {

    },
    setup (props, { expose }) {
        const store = reactive({
            show: false,
            status: 'success',
            percent: 0
        });
        const classList = computed(() => ({
            'cm-loading-bar': true,
            "cm-loading-bar-show": store.show
        }));
        const innerClass = computed(() => ({
            'cm-loading-bar-inner': true,
            [`cm-loading-bar-status-${store.status}`]: !!store.status
        }));

        const innerStyle = computed(() => ({
            width: `${store.percent}%`
        }));

        const update = (options: any) => {
            if (options.percent !== undefined) {
                store.percent = options.percent;
            }
            if (options.status !== undefined) {
                store.status = options.status;
            }
            if (options.show !== undefined) {
                store.show = options.show;
            }
        };

        expose({
            update
        });

        return () => <div class={classList.value} >
            <div class={innerClass.value} style={innerStyle.value}></div>
        </div>;
    }
});
