import { anchorData, codes, itemPropsData, propsData } from "./config";
import DemoCode from "../../common/code";
import Space from "@/components/Space";
import Title from "@/components/Typography/Title";
import Card from "@/components/Card";
import Breadcrumb from "@/components/Breadcrumb";
import Divider from "@/components/Divider";
import Text from "@/components/Typography/Text";
import Paragraph from "@/components/Typography/Paragraph";
import { F7House, F7LogoApple } from "cui-vue-icons/f7";

function BreadcrumbDemo () {
    return <>
        <div class='sys-ctx-main-left'>
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Breadcrumb 面包屑
                </Title>
                <Space id="breadcrumb_base" dir="v">
                    <Card bordered>
                        <Breadcrumb>
                            <Breadcrumb.Item>home</Breadcrumb.Item>
                            <Breadcrumb.Item>dashboard</Breadcrumb.Item>
                        </Breadcrumb>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            最基础的用法，通过设置link属性添加链接。
                        </Paragraph>
                        <DemoCode data={codes['breadcrumb_base']}/>
                    </Card>
                </Space>

                <Space id="breadcrumb_icon" dir="v">
                    <Card bordered>
                        <Breadcrumb>
                            <Breadcrumb.Item icon={<F7House />}>首页</Breadcrumb.Item>
                            <Breadcrumb.Item icon={<F7LogoApple />} link="#/nav/breadcrumb">面板</Breadcrumb.Item>
                            <Breadcrumb.Item>管理</Breadcrumb.Item>
                        </Breadcrumb>
                        <Divider align="left"><Text type="primary">带图标</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            可自定义每项的内容，比如带有一个图标
                        </Paragraph>
                        <DemoCode data={codes['breadcrumb_icon']}/>
                    </Card>
                </Space>


                <Space id="breadcrumb_sep" dir="v">
                    <Card bordered>
                        <Breadcrumb separator=">">
                            <Breadcrumb.Item>首页</Breadcrumb.Item>
                            <Breadcrumb.Item>面板</Breadcrumb.Item>
                            <Breadcrumb.Item>管理</Breadcrumb.Item>
                        </Breadcrumb>
                        <Divider align="left"><Text type="primary">分隔符</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            通过设置 <Text code>separator</Text> 属性来自定义分隔符，比如 <Text code>&gt;</Text> 
                        </Paragraph>
                        <DemoCode data={codes['breadcrumb_sep']}/>
                    </Card>
                </Space>


                <Space dir="v" size={24} id="comp_api">
                    <Title type="primary" heading={3}>API</Title>
                    <Space id='comp_props' dir="v">
                        <Title type="primary" heading={4}>Breadcrumb Props</Title>
                        {/* <Table columns={propsColumns} data={propsData} border size='small' /> */}
                    </Space>

                    <Space id='comp_item_props' dir="v">
                        <Title type="primary" heading={4}>Breadcrumb.Item Props</Title>
                        {/* <Table columns={propsColumns} data={itemPropsData} border size='small' /> */}
                    </Space>
                </Space>
            </Space>
        </div>

        {/* <CompAnchor data={anchorData}/> */}
    </>
}

BreadcrumbDemo.displayName = 'BreadcrumbDemo'
export default BreadcrumbDemo;