import Card from "@/components/Card";
import Divider from "@/components/Divider";
import Form from "@/components/Form";
import Input from "@/components/FormElements/Input";
import Slider from "@/components/FormElements/Slider";
import Spinner from "@/components/FormElements/Spinner";
import FormItem from "@/components/FormItem";
import Space from "@/components/Space";
import Paragraph from "@/components/Typography/Paragraph";
import Text from "@/components/Typography/Text";
import Title from "@/components/Typography/Title";
import Watermark from "@/components/Watermark";
import { reactive, ref } from "vue";

export default function WatermarkPage () {
    const form = ref({
        content: 'CUI-Vue',
        fontSize: 16,
        zIndex: 9,
        rotate: -22,
        gap: [100, 100],
        offset: [0, 0],
    });

    return <>
        <div class="sys-ctx-main-left">
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Watermark 水印
                </Title>
                <Space id="tree_base" dir="v">
                    <Card bordered>
                        <Watermark content={form.value.content} font={{fontSize: form.value.fontSize}} zIndex={form.value.zIndex} rotate={form.value.rotate} gap={form.value.gap} offset={form.value.offset}>
                            <div style={{width: '100%', height: '400px'}} />
                        </Watermark>
                        <div>
                            <Form model={form}>
                                <FormItem name="content" label="内容">
                                    <Input v-model={form.value.content}/>
                                </FormItem>
                                <FormItem name="fontSize" label="FontSize">
                                    <Slider min={0} max={100} v-model={form.value.fontSize}/>
                                </FormItem>
                                <FormItem name="zIndex" label="zIndex">
                                    <Slider min={0} max={100} v-model={form.value.zIndex}/>
                                </FormItem>
                                <FormItem name="rotate" label="Rotate">
                                    <Slider min={-180} max={180} v-model={form.value.rotate}/>
                                </FormItem>
                                <Space>
                                    <FormItem name="gap.0" label="Gap">
                                        <Spinner min={0} v-model={form.value.gap[0]}/>
                                    </FormItem>
                                    <FormItem name="gap.1">
                                        <Spinner min={0} v-model={form.value.gap[1]}/>
                                    </FormItem>
                                </Space>
                                <Space>
                                    <FormItem name="offset.0" label="Offset">
                                        <Spinner min={-10000} v-model={form.value.offset[0]} />
                                    </FormItem>
                                    <FormItem name="offset.1">
                                        <Spinner min={-10000} v-model={form.value.offset[1]}/>
                                    </FormItem>
                                </Space>
                            </Form>
                        </div>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            基础用法
                        </Paragraph>
                    </Card>
                </Space>
            </Space>
        </div>
    </>;
}

WatermarkPage.displayName = 'WatermarkPage';
