import { eventsColumns, propsColumns } from "../../common/columns";
import { anchorData, codes, eventsData, propsData } from "./config";
import { CompAnchor } from "../../common/CompAnchor";
import DemoCode from "../../common/code";
import Space from "@/components/Space";
import Title from "@/components/Typography/Title";
import Card from "@/components/Card";
import Button from "@/components/Button";
import Modal, { modal } from "@/components/Modal";
import Divider from "@/components/Divider";
import Text from "@/components/Typography/Text";
import Paragraph from "@/components/Typography/Paragraph";
import { ref } from "vue";

export default function ModalPage () {
    const visible = ref(false);
    const visible2 = ref(false);
    const visible3 = ref(false);
    const visible4 = ref(false);
    const visible5 = ref(false);
    const visible6 = ref(false);
    const visible7 = ref(false);
    const visible8 = ref(false);
    const visible9 = ref(false);
    const fullScreen = ref(false);

    return <>
        <div class='sys-ctx-main-left'>
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Modal 模态对话框
                </Title>
                <Space id="modal_base" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible.value = true;
                        }}>打开</Button>
                        <Modal title='提示' v-model={visible.value}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            <Text code>visible</Text> 为可控绑定参数
                        </Paragraph>
                        <DemoCode data={codes['modal_base']}/>
                    </Card>
                </Space>

                <Space id="modal_disabled" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible2.value = true;
                        }}>打开</Button>
                        <Modal disabled title='提示' v-model={visible2.value}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">禁用拖拽</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            使用<Text code>disabled</Text> 参数可禁用拖拽
                        </Paragraph>
                        <DemoCode data={codes['modal_disabled']}/>
                    </Card>
                </Space>


                <Space id="modal_style" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible3.value = true;
                        }}>打开</Button>
                        <Modal title='提示' v-model={visible3.value} defaultPosition={{top: '200px'}}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">自定义位置</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            使用<Text code>defaultPosition</Text> 参数可初始化位置
                        </Paragraph>
                        <DemoCode data={codes['modal_style']}/>
                    </Card>
                </Space>


                <Space id="modal_footer" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible4.value = true;
                        }}>打开</Button>
                        <Modal title='提示' v-model={visible4.value} footer={false}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">影藏底部</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            设置<Text code>footer</Text> 参数为false可隐藏底部元素
                        </Paragraph>
                        <DemoCode data={codes['modal_footer']}/>
                    </Card>
                </Space>


                <Space id="modal_loading" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible5.value = true;
                        }}>打开</Button>
                        <Modal title='提示' v-model={visible5.value} loading onOk={() => {
                            console.log('click ok');
                            setTimeout(() => {
                                visible5.value = false;
                            }, 2000);
                        }}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">加载中</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            设置<Text code>loading</Text> 参数，点击确定按钮变成加载中
                        </Paragraph>
                        <DemoCode data={codes['modal_loading']}/>
                    </Card>
                </Space>

                <Space id="modal_fullscreen" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible6.value = true;
                        }}>打开</Button>
                        <Modal title='提示' resetPostion v-model={visible6.value} fullScreen={fullScreen.value}>
                            <div><Button onClick={() => {
                                fullScreen.value = !fullScreen.value
                            }}>全屏</Button></div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">全屏</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            <Text code>fullScreen</Text> 属性为可控全屏属性
                        </Paragraph>
                        <DemoCode data={codes['modal_fullscreen']}/>
                    </Card>
                </Space>


                <Space id="modal_reset" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible7.value = true;
                        }}>打开</Button>
                        <Modal title='提示' resetPostion v-model={visible7.value} fullScreen={fullScreen.value}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">重新打开重置位置</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            设置<Text code>resetPostion</Text> 属性，拖拽后 重新打开会定位到初始化位置
                        </Paragraph>
                        <DemoCode data={codes['modal_reset']}/>
                    </Card>
                </Space>


                <Space id="modal_mask" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible8.value = true;
                        }}>打开</Button>
                        <Modal title='提示' mask={false} v-model={visible8.value} fullScreen={fullScreen.value}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">不显示遮罩</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            设置<Text code>mask</Text> 属性为false，将不会显示遮罩
                        </Paragraph>
                        <DemoCode data={codes['modal_mask']}/>
                    </Card>
                </Space>


                <Space id="modal_maskclose" dir="v">
                    <Card bordered>
                        <Button type='primary' onClick={() => {
                            visible9.value = true;
                        }}>打开</Button>
                        <Modal title='提示' maskClosable={false} v-model={visible9.value} fullScreen={fullScreen.value}>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                            <div>modal 内容</div>
                        </Modal>
                        <Divider align="left"><Text type="primary">禁用遮罩关闭</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            设置<Text code>maskClosable</Text> 属性为false，禁用遮罩点击关闭对话框功能
                        </Paragraph>
                        <DemoCode data={codes['modal_maskclose']}/>
                    </Card>
                </Space>


                <Space id="modal_instance" dir="v">
                    <Card bordered>
                        <Space dir="h">
                            <Button type='primary' onClick={() => {
                                modal.info({
                                    title: '信息',
                                    content: <div>信息</div>
                                });
                            }}>信息</Button>

                            <Button type='success' onClick={() => {
                                modal.success({
                                    title: '成功',
                                    content: <div>成功</div>
                                });
                            }}>成功</Button>

                            <Button type='warning' onClick={() => {
                                modal.warning({
                                    title: '警告',
                                    content: <div>警告</div>
                                });
                            }}>警告</Button>

                            <Button type='error' onClick={() => {
                                modal.error({
                                    title: '错误',
                                    content: <div>错误提示</div>
                                });
                            }}>错误</Button>

                            <Button type='error' onClick={() => {
                                modal.confirm({
                                    title: '提示',
                                    content: <div>确认信息</div>
                                });
                            }}>确认</Button>
                        </Space>
                        <Divider align="left"><Text type="primary">单实例使用</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            使用modal的 方法弹出对话框
                        </Paragraph>
                        <DemoCode data={codes['modal_instance']}/>
                    </Card>
                </Space>


                <Space dir="v" size={24} id="comp_api">
                    <Title type="primary" heading={3}>API</Title>
                    <Space id='comp_props' dir="v">
                        <Title type="primary" heading={4}>Modal Props</Title>
                        {/* <Table columns={propsColumns} data={propsData} border size='small' /> */}
                    </Space>

                    <Space id='comp_events' dir="v">
                        <Title type="primary" heading={4}>Events</Title>
                        {/* <Table columns={eventsColumns} data={eventsData} border size='small' /> */}
                    </Space>
                </Space>
            </Space>
        </div>
        
        <CompAnchor data={anchorData}/>
    </>
}

ModalPage.displayName = 'ModalPage';