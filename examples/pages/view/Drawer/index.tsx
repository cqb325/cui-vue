import DemoCode from "../../common/code";
import { anchorData, codes, eventsData, propsData } from "./config";
import { eventsColumns, propsColumns } from "../../common/columns";
import { CompAnchor } from "../../common/CompAnchor";
import { ref } from "vue";
import Space from "@/components/Space";
import Title from "@/components/Typography/Title";
import Card from "@/components/Card";
import Drawer from "@/components/Drawer";
import Button from "@/components/Button";
import Divider from "@/components/Divider";
import Text from "@/components/Typography/Text";
import Paragraph from "@/components/Typography/Paragraph";
import RadioGroup from "@/components/FormElements/RadioGroup";

export default function DrawerPage () {
    const visible = ref(false);
    const visible2 = ref(false);
    const visible3 = ref(false);
    const align = ref<'right'|'left'|'top'|'bottom'>('right');

    return <>
        <div class='sys-ctx-main-left'>
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Drawer 抽屉
                </Title>
                <Space id="drawer_base" dir="v">
                    <Card bordered>
                        <Drawer v-model={visible.value} title="侧边栏">
                            content
                        </Drawer>
                        <Button type="primary" onClick={() => {
                            visible.value = true;
                        }}>打开</Button>
                        <Divider align="left"><Text type="primary">基本用法</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            基本用法
                        </Paragraph>
                        <DemoCode data={codes['drawer_base']}/>
                    </Card>
                </Space>

                <Space id="drawer_align" dir="v">
                    <Card bordered>
                        <Drawer v-model={visible2.value} title="侧边栏" align={align.value}>
                            content
                        </Drawer>
                        <Space dir="h">
                            <RadioGroup stick v-model={align.value} data={[{label: 'Left', value: 'left'}, {label: 'Top', value: 'top'}, {label: 'Right', value: 'right'}, {label: 'Bottom', value: 'bottom'}]}></RadioGroup>
                            <Button type="primary" onClick={() => {
                                visible2.value = true;
                            }}>打开</Button>
                        </Space>
                        <Divider align="left"><Text type="primary">位置</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            <Text code>align</Text> 属性支持 <Text code>left</Text> <Text code>right</Text> <Text code>top</Text> <Text code>bottom</Text>
                            默认是 <Text code>right</Text>
                        </Paragraph>
                        <DemoCode data={codes['drawer_align']}/>
                    </Card>
                </Space>

                <Space id="drawer_size" dir="v">
                    <Card bordered>
                        <Drawer v-model={visible3.value} title="侧边栏" size={500}>
                            content
                        </Drawer>
                        <Button type="primary" onClick={() => {
                            visible3.value = true;
                        }}>打开</Button>
                        <Divider align="left"><Text type="primary">尺寸</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            设置<Text code>size</Text> 可以修改Drawer的尺寸
                        </Paragraph>
                        <DemoCode data={codes['drawer_size']}/>
                    </Card>
                </Space>

                <Space dir="v" size={24} id="comp_api">
                    <Title type="primary" heading={3}>API</Title>
                    <Space id='comp_props' dir="v">
                        <Title type="primary" heading={4}>Drawer Props</Title>
                        {/* <Table columns={propsColumns} data={propsData} border size='small' /> */}
                    </Space>
                    

                    <Space id='comp_events' dir="v">
                        <Title type="primary" heading={4}>Events</Title>
                        {/* <Table columns={eventsColumns} data={eventsData} border size='small' /> */}
                    </Space>
                </Space>
            </Space>
        </div>

        <CompAnchor data={anchorData}/>
    </>
}

DrawerPage.displayName = 'DrawerPage'