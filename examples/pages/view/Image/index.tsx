
import './style.less';

import img1 from './1.jpg';
import img2 from './2.jpg';
import img3 from './3.jpg';
import img4 from './4.jpg';
import img5 from './5.jpg';
import img6 from './6.jpg';
import img7 from './7.jpg';
import img8 from './8.jpg';
import img9 from './9.jpg';
import img10 from './10.jpg';
import { ref } from 'vue';
import Space from '@/components/Space';
import Title from '@/components/Typography/Title';
import Card from '@/components/Card';
import Row from '@/components/Row';
import Col from '@/components/Col';
import Image from '@/components/Image';
import Divider from '@/components/Divider';
import Text from '@/components/Typography/Text';
import Paragraph from '@/components/Typography/Paragraph';
import Button from '@/components/Button';
import Spin from '@/components/Spin';
import { FeatherImage } from 'cui-vue-icons/feather';
import ImagePreview from '@/components/ImagePreview';

export default function ImagePage () {
    const src1 = ref('');
    const src2 = ref('');
    const visible = ref(false);
    const verticalUrlList = [
        img1,
        img2,
        img3,
        img4,
        img5,
        img6,
        img7,
        img8,
        img9,
        img10,
    ];
    return <>
        <div class="sys-ctx-main-left">
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Image 图片
                </Title>
                <Space id="image_base" dir="v">
                    <Card bordered>
                        <Row gutter={20} style={{'text-align': 'center'}}>
                            <Col grid={1/5}>
                                <Image width={100} height={100} fit="contain" src={img1}/>
                                <div>contain</div>
                            </Col>
                            <Col grid={1/5}>
                                <Image width={100} height={100} fit="cover" src={img1}/>
                                <div>cover</div>
                            </Col>
                            <Col grid={1/5}>
                                <Image width={100} height={100} fit="fill" src={img1}/>
                                <div>fill</div>
                            </Col>
                            <Col grid={1/5}>
                                <Image width={100} height={100} fit="none" src={img1}/>
                                <div>none</div>
                            </Col>
                            <Col grid={1/5}>
                                <Image width={100} height={100} fit="scale-down" src={img1}/>
                                <div>scale-down</div>
                            </Col>
                        </Row>
                        <Divider align="left"><Text type="primary">基本用法</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            可通过 fit 来设置图片在容器的样式，同原生 <Text link="https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit">object-fit</Text>。
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="image_placeholder" dir="v">
                    <Card bordered>
                        <Space dir="h">
                            <Row style={{'text-align': 'center'}} gutter={20}>
                                <Col grid={0.5}>
                                    <Image width={200} height={100} fit="cover" src={src1.value}/>
                                    <div>默认</div>
                                </Col>
                                <Col grid={0.5}>
                                    <Image width={200} height={100} fit="cover" src={src1.value} placeholder={<Spin />}/>
                                    <div>自定义</div>
                                </Col>
                            </Row>
                            <Button type="primary" onClick={() => {
                                src1.value = img10;
                            }}>加载</Button>
                        </Space>
                        <Divider align="left"><Text type="primary">占位</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            通过<Text code>placeholder</Text>属性可以自定义占位样式，可通过console的Network修改网络速率进行测试
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="image_error" dir="v">
                    <Card bordered>
                        <Space dir="h">
                            <Row style={{'text-align': 'center'}} gutter={20}>
                                <Col grid={0.5}>
                                    <Image width={200} height={100} fit="cover" src={src2.value}/>
                                    <div>默认</div>
                                </Col>
                                <Col grid={0.5}>
                                    <Image width={200} height={100} fit="cover" src={src2.value} failInfo={<FeatherImage />}/>
                                    <div>自定义</div>
                                </Col>
                            </Row>
                            <Button type="primary" onClick={() => {
                                src2.value = img1;
                            }}>加载</Button>
                        </Space>
                        <Divider align="left"><Text type="primary">失败</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            通过<Text code>failInfo</Text>属性可以自定义图片加载失败样式
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="image_lazy" dir="v">
                    <Card bordered>
                        <Space dir="v">
                            <div class="demo-image-lazy-vertical">
                                {
                                    verticalUrlList.map(url => {
                                        return <Image src={url} lazy scrollContainer=".demo-image-lazy-vertical"/>;
                                    })
                                }
                            </div>
                        </Space>
                        <Divider align="left"><Text type="primary">懒加载</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            设置属性 <Text code>lazy</Text> 可以开启图片懒加载功能，当图片滚动到可视范围内才会加载。
                        </Paragraph>
                    </Card>
                </Space>

                <Space id="image_preview" dir="v">
                    <Card bordered>
                        <Space dir="h" wrap>
                            {
                                verticalUrlList.map((url, index) => {
                                    return <Image src={url} width={120} maskClosable={false} height={80} fit="contain" preview previewIndex={index} previewList={verticalUrlList}/>;
                                })
                            }
                        </Space>
                        <Divider align="left"><Text type="primary">预览</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            设置属性 <Text code>preview</Text> 可以开启图片预览模式，通过属性 <Text code>previewList</Text> 来设置图片列表，<Text code>previewIndex</Text> 属性设置打开预览时显示图片的索引。<br/>
                            预览时，可以使用 <kbd>←</kbd>、<kbd>→</kbd> 切换图片，<kbd>↑</kbd>、<kbd>↓</kbd> 缩放图片，<kbd>Space</kbd> 显示 1:1 图片，<kbd>ESC</kbd> 退出预览。
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="image_image_preview" dir="v">
                    <Card bordered>
                        <Space dir="h" wrap>
                            <Button type="primary" onClick={() => {
                                visible.value = true;
                            }}>打开</Button>
                            <ImagePreview v-model={visible.value} previewList={
                                [
                                    img1,
                                    img2,
                                    img3,
                                    img4,
                                    img5,
                                    img6,
                                    img7,
                                    img8,
                                    img9,
                                    img10,
                                ]
                            } />
                        </Space>
                        <Divider align="left"><Text type="primary">单独使用图片预览</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            图片预览组件 ImagePreview 也可以单独使用。
                        </Paragraph>
                    </Card>
                </Space>
            </Space>
        </div>
    </>;
}

ImagePage.displayName = 'Image';
