import { propsData, anchorData, codes } from "./config";
import DemoCode from "../../common/code";
import { ref } from "vue";
import Space from "@/components/Space";
import Title from "@/components/Typography/Title";
import Card from "@/components/Card";
import Spin from "@/components/Spin";
import Text from "@/components/Typography/Text";
import Divider from "@/components/Divider";
import Paragraph from "@/components/Typography/Paragraph";
import Button from "@/components/Button";

export default function SpinPage () {
    const loading = ref(true);
    return <>
        <div class='sys-ctx-main-left'>
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Spin 加载中
                </Title>
                <Space id="spin_base" dir="v">
                    <Card bordered>
                        <Space dir="v">
                            <Card title="卡片" style={{width: '300px', height: '300px', border: '1px solid #ccc', position: 'relative'}}>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <Spin type="pulse"></Spin>
                            </Card>

                            <Card title="卡片" style={{width: '300px', height: '300px', border: '1px solid #ccc', position: 'relative'}}>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <Spin type="gear"></Spin>
                            </Card>

                            <Card title="卡片" style={{width: '300px', height: '300px', border: '1px solid #ccc', position: 'relative'}}>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <Spin type="oval"></Spin>
                            </Card>

                            <Card title="卡片" style={{width: '300px', height: '300px', border: '1px solid #ccc', position: 'relative'}}>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <Spin type="dot" size="small"/>
                            </Card>
                        </Space>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            Spin的父组件需要设置position为 <Text code>relative/absolute/fixed</Text> 
                        </Paragraph>
                        <Paragraph type="secondary" spacing='extended'>
                            Spin的type支持 <Text code>pulse/gear/oval</Text> 默认为 pulse
                        </Paragraph>
                        <DemoCode data={codes['spin_base']}/>
                    </Card>
                </Space>

                <Space id="spin_control" dir="v">
                    <Card bordered>
                        <Space dir="v">
                            <Card title="卡片" style={{width: '300px', height: '300px', border: '1px solid #ccc', position: 'relative'}}>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                <div>卡片内容卡片内容</div>
                                {
                                    loading.value ? <Spin title='加载中'></Spin> : null
                                }
                            </Card>
                            <div>
                                <Button type='primary' onClick={() => {
                                    loading.value = !loading.value;
                                }}>Toggle</Button>
                            </div>
                        </Space>
                        <Divider align="left"><Text type="primary">可控</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            可以设置title属性修改文案，默认为loading...
                        </Paragraph>
                        <DemoCode data={codes['spin_control']}/>
                    </Card>
                </Space>

                <Space dir="v" size={24} id="comp_api">
                    <Title type="primary" heading={3}>API</Title>
                    <Space id='comp_props' dir="v">
                        <Title type="primary" heading={4}>Spin Props</Title>
                        {/* <Table columns={propsColumns} data={propsData} border size='small' /> */}
                    </Space>
                </Space>
            </Space>
        </div>

        {/* <CompAnchor data={anchorData}/> */}
    </>
}

SpinPage.displayName = 'SpinPage';