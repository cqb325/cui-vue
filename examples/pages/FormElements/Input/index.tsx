import Input from "@/components/FormElements/Input";
import { anchorData, codes, eventsData, propsData } from "./config";
import DemoCode from "../../common/code";
import { reactive, ref, watchEffect } from "vue";
import Space from "@/components/Space";
import Title from "@/components/Typography/Title";
import Card from "@/components/Card";
import Divider from "@/components/Divider";
import Text from "@/components/Typography/Text";
import Paragraph from "@/components/Typography/Paragraph";
import Button from "@/components/Button";
import { FeatherSearch, FeatherUser } from "cui-vue-icons/feather";

function InputDemo () {
    const count = ref(1);
    const form = reactive({
        text: '1'
    });
    watchEffect(() => {
        console.log(form.text);
    });
    return <>
        <div class="sys-ctx-main-left">
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Input 输入框
                </Title>
                <Space id="input_base" dir="v">
                    <Card bordered>
                        <Input v-model={form.text}/>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            Input的基础用法
                        </Paragraph>
                        <DemoCode data={codes['input_base']}/>
                    </Card>
                </Space>

                <Space id="input_disabled" dir="v">
                    <Card bordered>
                        <Input disabled modelValue="disabled"/>
                        <Divider align="left"><Text type="primary">禁用</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            使用 disabled 可以禁用 Input
                        </Paragraph>
                        <DemoCode data={codes['input_disabled']}/>
                    </Card>
                </Space>


                <Space id="input_placeholder" dir="v">
                    <Card bordered>
                        <Input placeholder="请输入xxx"/>
                        <Divider align="left"><Text type="primary">placeholder</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            使用 placeholder 的样例
                        </Paragraph>
                        <DemoCode data={codes['input_placeholder']}/>
                    </Card>
                </Space>


                <Space id="input_clearable" dir="v">
                    <Card bordered>
                        <Input modelValue="1" clearable/>
                        <Divider align="left"><Text type="primary">可清空</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            使用 clearable 属性， 有值时可以进行清空
                        </Paragraph>
                        <DemoCode data={codes['input_clearable']}/>
                    </Card>
                </Space>


                <Space id="input_control" dir="v">
                    <Card bordered>
                        <Space dir="h">
                            <Input v-model={count.value}/>
                            <Button onClick={() => {
                                count.value = count.value + 1;
                            }}>Add</Button>
                        </Space>
                        <Divider align="left"><Text type="primary">可控</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            <Text code>value</Text> 属性是可控属性
                        </Paragraph>
                        <DemoCode data={codes['input_control']}/>
                    </Card>
                </Space>

                <Space id="input_prefix" dir="v">
                    <Card bordered>
                        <Input name="count" prefix="￥" suffix="元"/>
                        <Divider align="left"><Text type="primary">前缀后缀</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            使用<Text code>prefix</Text> 可以给Input添加前缀， 使用<Text code>suffix</Text>属性可添加后缀
                        </Paragraph>
                        <DemoCode data={codes['input_prefix']}/>
                    </Card>
                </Space>


                <Space id="input_append" dir="v">
                    <Card bordered>
                        <Input prepend={<FeatherUser/>} append={<FeatherSearch />}/>
                        <Divider align="left"><Text type="primary">追加</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            使用<Text code>prepend</Text> 可添加前追加， 使用<Text code>append</Text>属性可添加后追加
                        </Paragraph>
                        <DemoCode data={codes['input_append']}/>
                    </Card>
                </Space>


                <Space id="input_size" dir="v">
                    <Card bordered>
                        <Space dir="h">
                            <Input clearable size="large"/><Input clearable/><Input clearable size="small"/>
                        </Space>
                        <Divider align="left"><Text type="primary">尺寸</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            <Text code>size</Text> 属性支持 <Text code>small</Text> <Text code>large</Text> 默认尺寸中
                        </Paragraph>
                        <DemoCode data={codes['input_size']}/>
                    </Card>
                </Space>

                <Space id="input_password" dir="v">
                    <Card bordered>
                        <Space dir="h">
                            <Input password type="password"/>
                            <Input type="search" onSearch={(v) => console.log(v)}/>
                        </Space>
                        <Divider align="left"><Text type="primary">尺寸</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            <Text code>size</Text> 属性支持 <Text code>small</Text> <Text code>large</Text> 默认尺寸中
                        </Paragraph>
                    </Card>
                </Space>

                <Space dir="v" size={24} id="comp_api">
                    <Title type="primary" heading={3}>API</Title>
                    <Space id="comp_props" dir="v">
                        <Title type="primary" heading={4}>Input Props</Title>
                        {/* <Table columns={propsColumns} data={propsData} border size='small' /> */}
                    </Space>
                    <Space id="comp_events" dir="v">
                        <Title type="primary" heading={4}>Input Events</Title>
                        {/* <Table columns={eventsColumns} data={eventsData} border size="small" /> */}
                    </Space>
                </Space>
            </Space>
        </div>
        {/* <CompAnchor data={anchorData}/> */}
    </>;
}

InputDemo.displayName = 'InputDemo';

export default InputDemo;
