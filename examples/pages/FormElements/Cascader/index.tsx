import Button from "@/components/Button";
import Card from "@/components/Card";
import Divider from "@/components/Divider";
import Cascader from "@/components/FormElements/Cascader";
import { TreeCheckMod } from "@/components/inner/Constaint";
import Space from "@/components/Space";
import Link from "@/components/Typography/Link";
import Paragraph from "@/components/Typography/Paragraph";
import Text from "@/components/Typography/Text";
import Title from "@/components/Typography/Title";
import { defineComponent, nextTick, ref } from "vue";

export default defineComponent({
    name: "CascaderPage",
    setup () {
        const value = ref();
        const value2 = ref(['gugong']);

        const data = [
            {
                value: 'beijing', title: '北京',
                children: [
                    {value: 'gugong', title: '故宫'},
                    {value: 'tiantan', title: '天坛'},
                    {value: 'wangfujing', title: '王府井'},
                ]
            },
            {
                value: 'jiangsu',
                title: '江苏',
                children: [
                    {
                        value: 'nanjing',
                        title: '南京',
                        children: [
                            {
                                value: 'fuzimiao',
                                title: '夫子庙',
                            }
                        ]
                    },
                    {
                        value: 'suzhou',
                        title: '苏州',
                        children: [
                            {
                                value: 'zhuozhengyuan',
                                title: '拙政园',
                                disabled: true
                            },
                            {
                                value: 'shizilin',
                                title: '狮子林',
                            }
                        ]
                    }
                ],
            }
        ];
        const data2 = JSON.parse(JSON.stringify(data));
        const data3 = JSON.parse(JSON.stringify(data));
        const data4 = JSON.parse(JSON.stringify(data));
        const data5 = JSON.parse(JSON.stringify(data));
        const data6 = JSON.parse(JSON.stringify(data));
        const data7 = JSON.parse(JSON.stringify(data));
        const data8 = [
            {
                value: 'beijing', title: '北京',
                children: [
                    {value: 'gugong', title: '故宫'},
                    {value: 'tiantan', title: '天坛'},
                    {value: 'wangfujing', title: '王府井xxxxxxxxxxx'},
                    {value: 'wangfujing2', title: '王府井2', disabled: true},
                    {value: 'wangfujing3', title: '王府井3'},
                    {value: 'wangfujing4', title: '王府井4'},
                    {value: 'wangfujing5', title: '王府井5'},
                    {value: 'wangfujing6', title: '王府井6'},
                    {value: 'wangfujing7', title: '王府井7'},
                    {value: 'wangfujing8', title: '王府井8'},
                    {value: 'wangfujing9', title: '王府井9'},
                ]
            },
            {
                value: 'jiangsu',
                title: '江苏',
                children: [
                    {
                        value: 'nanjing',
                        title: '南京',
                        children: [
                            {
                                value: 'fuzimiao',
                                title: '夫子庙',
                            }
                        ]
                    },
                    {
                        value: 'suzhou',
                        title: '苏州',
                        children: [
                            {
                                value: 'zhuozhengyuan',
                                title: '拙政园',
                                disabled: false
                            },
                            {
                                value: 'shizilin',
                                title: '狮子林',
                            }
                        ]
                    }
                ],
            }
        ];

        const data9 = JSON.parse(JSON.stringify(data8));
        const data10 = JSON.parse(JSON.stringify(data8));
        const data11 = JSON.parse(JSON.stringify(data8));
        const data12 = ref(data);
        const data18 = ref(JSON.parse(JSON.stringify(data)));
        const data13 = JSON.parse(JSON.stringify(data8));
        const data14 = JSON.parse(JSON.stringify(data8));
        const data15 = JSON.parse(JSON.stringify(data8));
        const data16 = [
            {
                value: 'beijing', title: '北京',
                checkable: false,
                children: [
                    {value: 'gugong', title: '故宫'},
                    {value: 'tiantan', title: '天坛'},
                    {value: 'wangfujing', title: '王府井xxxxxxxxxxx'},
                    {value: 'wangfujing2', title: '王府井2', disabled: true},
                    {value: 'wangfujing3', title: '王府井3'},
                    {value: 'wangfujing4', title: '王府井4'},
                    {value: 'wangfujing5', title: '王府井5'},
                    {value: 'wangfujing6', title: '王府井6'},
                    {value: 'wangfujing7', title: '王府井7'},
                    {value: 'wangfujing8', title: '王府井8'},
                    {value: 'wangfujing9', title: '王府井9'},
                ]
            },
            {
                value: 'jiangsu',
                title: '江苏',
                checkable: false,
                children: [
                    {
                        value: 'nanjing',
                        title: '南京',
                    },
                    {
                        value: 'suzhou',
                        title: '苏州',
                    }
                ],
            }
        ];
        const data17 = JSON.parse(JSON.stringify(data16));
        const value17 = ref([]);
        const value17Map = ref({});
        const onChange17 = (item: any, checked: boolean) => {
            if (checked) {
                value17Map.value[item._parent.value] = item.value;
            } else {
                delete value17Map.value[item._parent.value];
            }
            nextTick(() => {
                value17.value = Object.values(value17Map.value);
            });
            return true;
        };
        return () => (
            <>
                <div class="sys-ctx-main-left">
                    <Space dir="v" size={32}>
                        <Title heading={2}>
                            Cascader 级联选择
                        </Title>
                    </Space>

                    <Space id="cascader_base" dir="v">
                        <Card bordered>
                            <Cascader data={data}/>
                            <Cascader data={data12.value}/>
                            <Button onClick={() => {
                                data12.value = ([
                                    {
                                        value: 'beijing', title: '北京1111',
                                        children: [
                                            {value: 'gugong', title: '故宫111'},
                                            {value: 'tiantan', title: '天坛1111'},
                                            {value: 'wangfujing', title: '王府井xxxxxxxxxxx'},
                                        ]
                                    }
                                ]);
                            }}>改变数据</Button>
                            <Cascader multi data={data18.value}/>
                            <Button onClick={() => {
                                data18.value = ([
                                    {
                                        value: 'beijing', title: '北京1111',
                                        children: [
                                            {value: 'gugong', title: '故宫111'},
                                            {value: 'tiantan', title: '天坛1111'},
                                            {value: 'wangfujing', title: '王府井xxxxxxxxxxx'},
                                        ]
                                    }
                                ]);
                            }}>改变数据</Button>
                            <Cascader data={[]}/>
                            <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            基础用法
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_disabled" dir="v">
                        <Card bordered>
                            <Cascader data={data2} disabled/>
                            <Divider align="left"><Text type="primary">禁用</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            使用 disabled 禁用
                            </Paragraph>
                        </Card>
                    </Space>

                    <Space id="cascader_size" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={data3} size="small" modelValue={['jiangsu','suzhou','shizilin']}/>
                                <Cascader data={data3} modelValue={['jiangsu','suzhou','shizilin']} />
                                <Cascader data={data3} size="large" modelValue={['jiangsu','suzhou','shizilin']} />
                            </Space>
                            <Divider align="left"><Text type="primary">尺寸</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            size 支持 small 和 large
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_sep" dir="v">
                        <Card bordered>
                            <Cascader data={data4} seperator=">"/>
                            <Divider align="left"><Text type="primary">自定义分隔符</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            使用 seperator 可以自定义分隔符
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_trigger" dir="v">
                        <Card bordered>
                            <Cascader data={data5} trigger="hover" modelValue={['jiangsu','suzhou','shizilin']} />
                            <Divider align="left"><Text type="primary">触发事件</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            使用 trigger 修改展开的触发条件， 支持hover和click
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_change" dir="v">
                        <Card bordered>
                            <Cascader data={data6} changeOnSelect modelValue={['jiangsu','suzhou','shizilin']} />
                            <Divider align="left"><Text type="primary">选择及改变</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            设置 changeOnSelect 选择选项及改变值
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_control" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={data7} v-model={value.value} />
                                <Button type="primary" onClick={() => {
                                    value.value = ['jiangsu','suzhou','shizilin'];
                                }}>
                                设置值
                                </Button>
                            </Space>
                            <Divider align="left"><Text type="primary">可控</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            value 可控属性
                            </Paragraph>
                        </Card>
                    </Space>

                    <Space id="cascader_multi" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={data8} v-model={value2.value} showMore showMax={2} multi mode={TreeCheckMod.SHALLOW}/>
                            </Space>
                            <Space>
                                <Cascader data={data13} v-model={value2.value} multi mode={TreeCheckMod.CHILD} tagRender={(item) => {
                                    const labels = [item.title];
                                    while (item._parent) {
                                        item = item._parent;
                                        labels.unshift(item.title);
                                    }
                                    return labels.join(' / ');
                                }}/>
                            </Space>
                            <Divider align="left"><Text type="primary">多选</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            多选
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_filter" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={data9} filter />
                            </Space>
                            <Divider align="left"><Text type="primary">过滤</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            过滤
                            </Paragraph>
                        </Card>
                    </Space>

                    <Space id="cascader_filter2" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={data10} filter multi/>
                            </Space>
                            <Divider align="left"><Text type="primary">过滤多选</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            过滤多选
                            </Paragraph>
                        </Card>
                    </Space>

                    <Space id="cascader_max" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader mode={TreeCheckMod.CHILD} data={data11} filter max={2} onExceed={() => console.error('最多只能选择2个')} multi/>
                            </Space>
                            <Divider align="left"><Text type="primary">max</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            max
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_header_footer" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader mode={TreeCheckMod.CHILD} data={data14} filter multi
                                    header={<div style={{
                                        height: '36px',
                                        display: 'flex',
                                        padding: '0 16px',
                                        'align-items': 'center',
                                        'border-bottom': '1px solid var(--cui-color-border)'
                                    }}><Text>选择省份</Text></div>} footer={<div style={{
                                        height: '36px',
                                        display: 'flex',
                                        padding: '0 16px',
                                        'align-items': 'center',
                                        cursor: 'pointer',
                                        'border-top': '1px solid var(--cui-color-border)'
                                    }}>
                                        <Text>找不到相关选项？</Text>
                                        <Link>去新建</Link>
                                    </div>}/>
                            </Space>
                            <Divider align="left"><Text type="primary">Header Footer</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            Header Footer
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_header_footer" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={[
                                    {value: 'A', title: 'A', children: [
                                        {value: 'B', title: 'B', children: [
                                            {value: 'C', title: 'C', children: [
                                                {value: 'D', title: 'D', children: [
                                                    {value: 'E', title: 'E'},
                                                ]},
                                            ]},
                                        ]},
                                    ]},
                                ]} />
                            </Space>
                            <Divider align="left"><Text type="primary">宽度限制</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            宽度限制
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_trigger_render" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={data15} triggerRender={(text, values) => {
                                    return <Button>{text || '请选择'}</Button>;
                                }}/>
                            </Space>
                            <Divider align="left"><Text type="primary">trigger_render</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            trigger_render
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_trigger_render" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader data={data16} multi mode={TreeCheckMod.CHILD} tagRender={(item) => {
                                    const labels = [item.title];
                                    while (item._parent) {
                                        item = item._parent;
                                        labels.unshift(item.title);
                                    }
                                    return labels.join(' / ');
                                }}/>
                            </Space>
                            <Divider align="left"><Text type="primary">仅子元素可选择</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            trigger_render
                            </Paragraph>
                        </Card>
                    </Space>


                    <Space id="cascader_trigger_render" dir="v">
                        <Card bordered>
                            <Space>
                                <Cascader v-model={value17.value} beforeChecked={onChange17} data={data17} multi mode={TreeCheckMod.CHILD} tagRender={(item) => {
                                    const labels = [item.title];
                                    while (item._parent) {
                                        item = item._parent;
                                        labels.unshift(item.title);
                                    }
                                    return labels.join(' / ');
                                }}/>
                            </Space>
                            <Divider align="left"><Text type="primary">子元素单选</Text></Divider>
                            <Paragraph type="secondary" spacing="extended">
                            trigger_render
                            </Paragraph>
                        </Card>
                    </Space>

                </div>
            </>
        );
    },
});
