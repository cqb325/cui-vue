import Button from "@/components/Button";
import Card from "@/components/Card";
import Col from "@/components/Col";
import Divider from "@/components/Divider";
import TreeSelect from "@/components/FormElements/TreeSelect";
import Row from "@/components/Row";
import Space from "@/components/Space";
import { TreeCheckMod } from "@/components/Tree";
import Paragraph from "@/components/Typography/Paragraph";
import Text from "@/components/Typography/Text";
import Title from "@/components/Typography/Title";
import { F7TagFill } from "cui-vue-icons/f7";
import { ref } from "vue";

export default function TreeSelectPage () {
    const value = ref(['1_2_1']);
    const value2 = ref('1_2_1');
    const data1 = [
        {id: 'beijing', title: '北京', children: [
            {id: 'gugong', title: '故宫'},
            {id: 'tiantan', title: '天坛'},
        ]},
        {id: 'zhejiang', title: '浙江', children: [
            {id: 'xihu', title: '西湖'},
            {id: 'linyin', title: '灵隐'},
        ]},
    ];
    const data3 = [];
    for (let i = 0; i < 3; i++) {
        const c = [];
        for (let j = 0; j < 3; j++) {
            const d = [];
            let disabled = false;
            if (i === 0 && j === 0) {
                disabled = true;
            }
            for (let k = 0; k < 3; k++) {
                d.push({id: `${i + 1}_${j + 1}_${k + 1}`, title: `node_${i + 1}_${j + 1}_${k + 1}`});
            }
            c.push({id: `${i + 1}_${j + 1}`, title: `node_${i + 1}_${j + 1}`, children: d, disabled});
        }
        // patch: <div style={{
        //     display: 'flex',
        //     "justify-content": 'end'
        // }}>
        //     <Text type="success">查看</Text>
        // </div>

        data3.push({id: `${i + 1}`, title: `node_${i + 1}`, children: c});
    }

    const data2 = [];
    for (let i = 0; i < 3; i++) {
        const c = [];
        for (let j = 0; j < 3; j++) {
            const d = [];
            let disabled = false;
            if (i === 0 && j === 0) {
                disabled = true;
            }
            for (let k = 0; k < 3; k++) {
                d.push({id: `${i + 1}_${j + 1}_${k + 1}`, title: `node_${i + 1}_${j + 1}_${k + 1}`});
            }
            c.push({id: `${i + 1}_${j + 1}`, title: `node_${i + 1}_${j + 1}`, children: d, disabled});
        }
        // patch: <div style={{
        //     display: 'flex',
        //     "justify-content": 'end'
        // }}>
        //     <Text type="success">查看</Text>
        // </div>

        data2.push({id: `${i + 1}`, title: `node_${i + 1}`, children: c});
    }

    const data4 = JSON.parse(JSON.stringify(data1));
    const data5 = JSON.parse(JSON.stringify(data1));
    const data6 = JSON.parse(JSON.stringify(data1));
    const data7 = JSON.parse(JSON.stringify(data1));
    const data8 = JSON.parse(JSON.stringify(data1));
    const data9 = JSON.parse(JSON.stringify(data1));
    const data10 = JSON.parse(JSON.stringify(data1));
    const data11 = JSON.parse(JSON.stringify(data1));
    const data12 = JSON.parse(JSON.stringify(data1));
    const data13 = JSON.parse(JSON.stringify(data1));
    const data14 = JSON.parse(JSON.stringify(data1));
    const data15 = ref(JSON.parse(JSON.stringify(data1)));
    const data16 = ref(JSON.parse(JSON.stringify(data1)));

    return <>
        <div class="sys-ctx-main-left">
            <Space dir="v" size={32}>
                <Title heading={2}>
                    TreeSelect 树选择
                </Title>
                <Space id="tree_base" dir="v">
                    <Card bordered>
                        <TreeSelect data={data1}/>
                        <TreeSelect data={data15.value}/>
                        <Button onClick={() => {
                            data15.value = [
                                {id: 'beijing', title: '北京1', children: [
                                    {id: 'gugong', title: '故宫1'},
                                    {id: 'tiantan', title: '天坛1'},
                                ]},
                            ];
                        }}>设置数据</Button>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        基础用法
                        </Paragraph>
                    </Card>
                </Space>

                <Space id="tree_disabled" dir="v">
                    <Card bordered>
                        <TreeSelect data={data1} disabled/>
                        <Divider align="left"><Text type="primary">禁用</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        使用 disabled 进行禁用
                        </Paragraph>
                    </Card>
                </Space>

                <Space id="tree_size" dir="v">
                    <Card bordered>
                        <Row>
                            <Col grid={0.33}>
                                <TreeSelect data={data4} size="small"/>
                            </Col>
                            <Col grid={0.33}>
                                <TreeSelect data={data4}/>
                            </Col>
                            <Col grid={0.33}>
                                <TreeSelect data={data4} size="large"/>
                            </Col>
                        </Row>
                        <Divider align="left"><Text type="primary">尺寸</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        size 支持 small large
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="tree_clearable" dir="v">
                    <Card bordered>
                        <TreeSelect data={data5} clearable/>
                        <Divider align="left"><Text type="primary">可清空</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        clearable 可清空选择数据
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="tree_prepend" dir="v">
                    <Card bordered>
                        <TreeSelect prepend={<F7TagFill />} data={data6} clearable/>
                        <Divider align="left"><Text type="primary">前缀</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        使用 prepend 可添加前缀
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="tree_multi" dir="v">
                    <Card bordered>
                        <TreeSelect multi data={data7} clearable/>
                        <TreeSelect multi data={data16.value} clearable/>
                        <Button onClick={() => {
                            data16.value = [
                                {id: 'beijing', title: '北京1', children: [
                                    {id: 'gugong', title: '故宫1'},
                                    {id: 'tiantan', title: '天坛1'},
                                ]},
                                {id: 'zhejiang', title: '浙江1', children: [
                                    {id: 'xihu', title: '西湖1'},
                                    {id: 'linyin', title: '灵隐1'},
                                ]}
                            ];
                        }}>设置数据</Button>
                        <Divider align="left"><Text type="primary">多选</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        使用 multi 为多选模式, 多选默认级联选择
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="tree_relation" dir="v">
                    <Card bordered>
                        <TreeSelect multi data={data8} clearable checkRelation="unRelated" />
                        <Divider align="left"><Text type="primary">多选非级联</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        checkRelation 支持 unRelated (非级联) 和 related (级联) 默认 related
                        </Paragraph>
                    </Card>
                </Space>



                <Space id="tree_showMax" dir="v">
                    <Card bordered>
                        <TreeSelect multi data={data9} clearable checkRelation="unRelated" showMax={2}/>
                        <Divider align="left"><Text type="primary">显示个数</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        showMax 可以设置最多显示的个数
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="tree_valueClosable" dir="v">
                    <Card bordered>
                        <TreeSelect multi data={data10} valueClosable clearable checkRelation="unRelated" />
                        <Divider align="left"><Text type="primary">值可关闭</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        valueClosable 支持 选择的值可以进行关闭
                        </Paragraph>
                    </Card>
                </Space>

                <Space id="tree_mode" dir="v">
                    <Card bordered>
                        <Space dir="v">
                            <div>CHILD:</div>
                            <TreeSelect multi data={data11} clearable mode={TreeCheckMod.CHILD} />
                            <div>HALF:</div>
                            <TreeSelect multi data={data12} clearable mode={TreeCheckMod.HALF} />
                            <div>SHALLOW:</div>
                            <TreeSelect multi data={data13} clearable mode={TreeCheckMod.SHALLOW} />
                            <div>FULL:</div>
                            <TreeSelect multi data={data14} clearable mode={TreeCheckMod.FULL} />
                        </Space>
                        <Divider align="left"><Text type="primary">选择模式</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                        mode 支持 FULL 、 HALF、 CHILD、 SHALLOW 默认为 HALF
                        </Paragraph>
                    </Card>
                </Space>
            </Space>
        </div>
    </>;
}

TreeSelectPage.displayName = 'TreeSelect';
