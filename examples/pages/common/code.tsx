import { defineComponent, h, ref, withDirectives } from "vue";
import Space from '@/components/Space';
import Button from "@/components/Button";
import useCopy from "@/components/use/useCopy";
import Tooltip from "@/components/Tooltip";
import vHlt from "./vHlt";
import { message } from "@/components/Message";
import { FeatherCode, FeatherCopy } from "cui-vue-icons/feather";

export default defineComponent({
    name: 'DemoCode',
    props: ['data'],
    setup (props, ctx) {
        const open = ref(false);
        const classList = () => ({
            'cm-demo-code-wrap': true,
            'cm-demo-code-wrap-open': open.value,
        });

        const onCopy = async () => {
            const ret = await useCopy(props.data);
            if (ret) {
                message.success({
                    content: '复制成功!',
                    duration: 2
                });
            }
        };

        return () => (
            <Space dir="v" class="cm-demo-code">
                <Space dir="h" justify="end" size={20}>
                    <Tooltip content="拷贝" align="top">
                        <FeatherCopy size={16} onClick={onCopy}/>
                    </Tooltip>
                    <Tooltip content={open.value ? '收起代码' : '显示代码'} align="top">
                        <FeatherCode size={16} onClick={() => open.value = !open.value}/>
                    </Tooltip>
                </Space>
                <Space class={classList()} dir="v" justify="center">
                    <pre>
                        {
                            withDirectives(h('code', {class: 'language-js'}, props.data), [[vHlt]])
                        }
                    </pre>
                    <Button theme="dashed" onClick={() => open.value = false}>收 起 代 码</Button>
                </Space>
            </Space>
        );
    },
});
