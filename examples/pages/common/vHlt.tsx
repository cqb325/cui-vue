import his from 'highlight.js'

const vHlt = {
    mounted: (el) => {
        his.highlightElement(el as HTMLElement)
    }
}

export default vHlt;