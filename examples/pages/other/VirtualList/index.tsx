import Card from "@/components/Card";
import Divider from "@/components/Divider";
import Space from "@/components/Space";
import Paragraph from "@/components/Typography/Paragraph";
import Text from "@/components/Typography/Text";
import Title from "@/components/Typography/Title";
import VirtualList from "@/components/virtual-list";
import { defineComponent } from "vue";


const ListItem = defineComponent({
    name: 'ListItem',
    props: {
        item: Object,
        index: Number,
    },
    setup (props: any) {
        return () => <div
            role="listitem"
            style={{
                lineHeight: `${props.item.value}px`,
            }}
            class={{
                "ListItemOdd": props.index % 2 === 0,
                "ListItemEven": props.index % 2 === 1,
            }}
        >
            <div>{props.item.text}</div>
        </div>;
    }
});

const createArray = (count: number) => {
    return new Array(count).fill(true).map((_, index: number) => {
        return {
            value: 20 + Math.round(Math.random() * 15),
            text: `Row ${index}`
        };
    });
};

export default defineComponent({
    name: "VirtualList",
    setup () {
        const data1 = createArray(1000);
        return () => (
            <>
                <div class="sys-ctx-main-left">
                    <Space dir="v" size={32}>
                        <Title heading={2}>
                            Virtual List 虚拟列表
                        </Title>

                        <Space id="countup_base" dir="v">
                            <Card bordered>
                                <div style={{width: '300px', border: '1px solid #ccc'}}>
                                    <VirtualList height={300} items={data1} itemEstimatedSize={20}>
                                        {{
                                            default: scope => {
                                                return <ListItem item={scope.item} index={scope.index} />;
                                            }
                                        }}
                                    </VirtualList>
                                </div>
                                <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                                <Paragraph type="secondary" spacing="extended">
                                    虚拟列表的基础用法。
                                </Paragraph>
                            </Card>
                        </Space>
                    </Space>
                </div>
            </>
        );
    },
});
