import Card from "@/components/Card";
import Divider from "@/components/Divider";
import Form from "@/components/Form";
import Input from "@/components/FormElements/Input";
import FormItem from "@/components/FormItem";
import Space from "@/components/Space";
import Table from "@/components/Table";
import TableStyleLayout, { TableStyleLayoutCol, TableStyleLayoutLabel, TableStyleLayoutRow, TableStyleLayoutValue } from "@/components/TableStyleLayout";
import Paragraph from "@/components/Typography/Paragraph";
import Text from "@/components/Typography/Text";
import Title from "@/components/Typography/Title";
import { ref } from "vue";

export default function TSLPage () {
    const columns = [
        { type: 'index', title: '序号', width: 68 },
        { name: 'name', title: '字段名称' },
        { name: 'type', title: '字段类型' }
    ];

    const formData = ref({
        userName: '',
        department: ''
    });

    return <>
        <div class="sys-ctx-main-left">
            <Space dir="v" size={32}>
                <Title heading={2}>
                    TableStyleLayout 类表格布局
                </Title>
                <Space id="split_base" dir="v">
                    <Card bordered>
                        <TableStyleLayout labelWidth={100}>
                            <TableStyleLayoutRow>
                                <TableStyleLayoutLabel>用户名</TableStyleLayoutLabel>
                                <TableStyleLayoutValue>xxxxx</TableStyleLayoutValue>
                                <TableStyleLayoutLabel>部门</TableStyleLayoutLabel>
                                <TableStyleLayoutValue>xxxxx</TableStyleLayoutValue>
                                <TableStyleLayoutLabel>联系方式</TableStyleLayoutLabel>
                                <TableStyleLayoutValue>xxxxx</TableStyleLayoutValue>
                            </TableStyleLayoutRow>
                            <TableStyleLayoutRow>
                                <TableStyleLayoutCol flex={0.333}>
                                    <TableStyleLayoutLabel>职位</TableStyleLayoutLabel>
                                    <TableStyleLayoutValue>xxxxx</TableStyleLayoutValue>
                                </TableStyleLayoutCol>
                                <TableStyleLayoutCol flex={2 / 3}>
                                    <TableStyleLayoutLabel>职责</TableStyleLayoutLabel>
                                    <TableStyleLayoutValue>xxxxx</TableStyleLayoutValue>
                                </TableStyleLayoutCol>
                            </TableStyleLayoutRow>
                            <TableStyleLayoutRow>
                                <TableStyleLayoutLabel verticalAlign="start">字段信息</TableStyleLayoutLabel>
                                <TableStyleLayoutValue>
                                    <Table columns={columns} data={[]} border size="small"/>
                                </TableStyleLayoutValue>
                            </TableStyleLayoutRow>
                        </TableStyleLayout>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            基础用法。
                        </Paragraph>
                    </Card>
                </Space>


                <Space id="split_form" dir="v">
                    <Card bordered>
                        <Form model={formData.value} errorTransfer errorAlign="bottomLeft" onChange={(name, v) => {
                            console.log(name, v);
                        }}>
                            <TableStyleLayout labelWidth={100}>
                                <TableStyleLayoutRow>
                                    <TableStyleLayoutLabel required>用户名</TableStyleLayoutLabel>
                                    <TableStyleLayoutValue>
                                        <FormItem name="userName" rules={[{ required: true, message: '请输入用户名' }]}>
                                            <Input v-model={formData.value.userName} placeholder="用户名"/>
                                        </FormItem>
                                    </TableStyleLayoutValue>
                                    <TableStyleLayoutLabel required>部门</TableStyleLayoutLabel>
                                    <TableStyleLayoutValue>
                                        <FormItem name="department" rules={[{ required: true, message: '请输入部门' }]}>
                                            <Input v-model={formData.value.department} placeholder="部门"/>
                                        </FormItem>
                                    </TableStyleLayoutValue>
                                    <TableStyleLayoutLabel>联系方式</TableStyleLayoutLabel>
                                    <TableStyleLayoutValue>xxxxx</TableStyleLayoutValue>
                                </TableStyleLayoutRow>
                            </TableStyleLayout>
                        </Form>
                        <Divider align="left"><Text type="primary">表单用法</Text></Divider>
                        <Paragraph type="secondary" spacing="extended">
                            基础用法。
                        </Paragraph>
                    </Card>
                </Space>
            </Space>
        </div>
    </>;
}

TSLPage.displayName = 'TSLPage';
