import { anchorData, codes, propsData, slotsData } from "./config"
import DemoCode from "../../common/code";
import Space from "@/components/Space";
import Title from "@/components/Typography/Title";
import Card from "@/components/Card";
import Split from "@/components/Split";
import Divider from "@/components/Divider";
import Text from "@/components/Typography/Text";
import Paragraph from "@/components/Typography/Paragraph";

export default function SplitPage() {
    return <>
        <div class='sys-ctx-main-left'>
            <Space dir="v" size={32}>
                <Title heading={2}>
                    Split 面板分割
                </Title>
                <Space id="split_base" dir="v">
                    <Card bordered>
                        <div style={{height: '300px', border: '1px solid #ccc'}}>
                            <Split split="300px" max={500}>
                                {{
                                    prev: () => <div>LEFT</div>,
                                    next: () => <div>RIGHT</div>
                                }}
                            </Split>
                        </div>
                        <Divider align="left"><Text type="primary">基础用法</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            左右分割用法。
                        </Paragraph>
                        <DemoCode data={codes['split_base']}/>
                    </Card>
                </Space>


                <Space id="split_h" dir="v">
                    <Card bordered>
                        <div style={{height: '300px', border: '1px solid #ccc'}}>
                            <Split dir='h' split={0.3}>
                                {{
                                    prev: () => <div>TOP</div>,
                                    next: () => <div>BOTTOM</div>
                                }}
                            </Split>
                        </div>
                        <Divider align="left"><Text type="primary">上下分割</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            上下分割用法。
                        </Paragraph>
                        <DemoCode data={codes['split_h']}/>
                    </Card>
                </Space>


                <Space id="split_insert" dir="v">
                    <Card bordered>
                        <div style={{height: '300px', border: '1px solid #ccc'}}>
                            <Split split={0.5}>
                                {{
                                    prev: () => <Split dir='h' split={0.3}>
                                        {{
                                            prev: () => <div>TOP</div>,
                                            next: () => <div>BOTTOM</div>
                                        }}
                                    </Split>,
                                    next: () => <div>Right</div>
                                }}
                            </Split>
                        </div>
                        <Divider align="left"><Text type="primary">嵌套</Text></Divider>
                        <Paragraph type="secondary" spacing='extended'>
                            嵌套使用。
                        </Paragraph>
                        <DemoCode data={codes['split_insert']}/>
                    </Card>
                </Space>


                <Space dir="v" size={24} id="comp_api">
                    <Title type="primary" heading={3}>API</Title>
                    <Space id='comp_props' dir="v">
                        <Title type="primary" heading={4}>Split Props</Title>
                        {/* <Table columns={propsColumns} data={propsData} border size='small' /> */}
                    </Space>

                    <Space id='comp_slots' dir="v">
                        <Title type="primary" heading={4}>Split slots</Title>
                        {/* <Table columns={slotsColumns} data={slotsData} border size='small' /> */}
                    </Space>
                </Space>
            </Space>
        </div>
        
        {/* <CompAnchor data={anchorData}/> */}
    </>
}

SplitPage.displayName = 'SplitPage';