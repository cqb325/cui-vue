import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import { loadingBar } from '../src/components/LoadingBar';
import { menuData } from './menuData';
import 'highlight.js/styles/xcode.css';

const router = createRouter({
    routes: menuData,
    history: createWebHashHistory()
});

router.beforeEach(async (to, from) => {
    loadingBar.start();
});

router.afterEach(async (to, from) => {
    loadingBar.finish();
});
import './index.less';
import '@/theme/theme.less';
import 'cui-vue-icons/style.css';
import App from './App';

createApp(App).use(router).mount('#app');
