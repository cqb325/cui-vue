import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import path from 'node:path';

export default defineConfig({
  plugins: [vue(), vueJsx()],
  base: './',
  build: {
    minify: false,
    target: 'esnext',
    outDir: './dist',
    rollupOptions: {
        input: {
            index: path.resolve('src/components/index.tsx'),
        },
        context: 'globalThis',
        preserveEntrySignatures: 'strict',
        external: ['vue', 'countup.js', 'tinycolor2', 'async-validator', 'cui-vue-icons/feather', 'cui-vue-icons/f7', 'dayjs'],
        output: [{
            dir: './es',
            preserveModules: true,
            format: 'es',
            entryFileNames: '[name].js',
            chunkFileNames: '[name].js',
            assetFileNames: '[name].[ext]',
        },
        {
            dir: './dist',
            format: 'es',
            exports: 'named',
            sourcemap: false,
            entryFileNames: 'cui.min.esm.js',
            chunkFileNames: '[name].js',
            assetFileNames: '[name].[ext]',
            inlineDynamicImports: false,
            manualChunks: undefined,
        }]
    }
  },
  resolve: {
    conditions: ['development', 'browser'],
    dedupe: ['vue'],
    alias: {
      // 'solid-vue-router': path.resolve('./src/solid-router'),
      '@': path.resolve('./src')
    }
  },
  server: {
    port: 5174,
    host: '0.0.0.0',
    proxy: {
        '/test': {
            target: 'http://localhost:18080/'
        },
    }
  },
});
