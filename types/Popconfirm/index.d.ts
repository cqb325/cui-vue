import type { InnerPopupProps } from "../inner/InnerPopup";
import type { ButtonProps } from "../Button";
import { PropType, VNode } from "vue";
export interface PopconfirmProps extends InnerPopupProps {
    okText?: string;
    cancelText?: string;
    onOk?: () => void | Promise<boolean | void>;
    onCancel?: () => void;
    okType?: ButtonProps['type'];
    cancelType?: ButtonProps['type'];
    icon?: VNode;
    showCancel?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    showCancel: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
    };
    trigger: {
        type: PropType<"click" | "focus" | "hover">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    arrow: {
        type: PropType<boolean>;
        default: any;
    };
    hideDelay: {
        type: PropType<number>;
    };
    content: {
        type: PropType<string | number | JSX.Element>;
    };
    contentStyle: {
        type: PropType<string | import("vue").CSSProperties>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    theme: {
        type: PropType<string>;
    };
    confirm: {
        type: PropType<boolean>;
        default: any;
    };
    okText: {
        type: PropType<any>;
    };
    okType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    title: {
        type: PropType<string | number | JSX.Element>;
    };
    cancelText: {
        type: PropType<any>;
    };
    cancelType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    offset: {
        type: PropType<number>;
    };
    clsPrefix: {
        type: PropType<string>;
    };
    varName: {
        type: PropType<string>;
    };
    arrowPointAtCenter: {
        type: PropType<boolean>;
    };
    onOk: {
        type: PropType<() => void | Promise<boolean | void>>;
    };
    onCancel: {
        type: PropType<() => void>;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("update:modelValue" | "visibleChange")[], "update:modelValue" | "visibleChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    showCancel: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
    };
    trigger: {
        type: PropType<"click" | "focus" | "hover">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    arrow: {
        type: PropType<boolean>;
        default: any;
    };
    hideDelay: {
        type: PropType<number>;
    };
    content: {
        type: PropType<string | number | JSX.Element>;
    };
    contentStyle: {
        type: PropType<string | import("vue").CSSProperties>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    theme: {
        type: PropType<string>;
    };
    confirm: {
        type: PropType<boolean>;
        default: any;
    };
    okText: {
        type: PropType<any>;
    };
    okType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    title: {
        type: PropType<string | number | JSX.Element>;
    };
    cancelText: {
        type: PropType<any>;
    };
    cancelType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    offset: {
        type: PropType<number>;
    };
    clsPrefix: {
        type: PropType<string>;
    };
    varName: {
        type: PropType<string>;
    };
    arrowPointAtCenter: {
        type: PropType<boolean>;
    };
    onOk: {
        type: PropType<() => void | Promise<boolean | void>>;
    };
    onCancel: {
        type: PropType<() => void>;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}>> & {
    onVisibleChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    confirm: boolean;
    arrow: boolean;
    showCancel: boolean;
}, {}>;
export default _default;
