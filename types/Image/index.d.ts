import { PropType, VNode } from "vue";
export interface ImageProps {
    failInfo?: string | VNode;
    preview?: boolean;
    previewTip?: string | VNode;
    previewList?: string[];
    fit?: 'fill' | 'contain' | 'cover' | 'none' | 'scale-down';
    alt?: string;
    src?: string;
    lazy?: boolean;
    referrerPolicy?: string;
    scrollContainer?: string | HTMLElement;
    placeholder?: string | VNode;
    width?: number | string;
    height?: number | string;
    infinite?: boolean;
    maskClosable?: boolean;
    previewIndex?: number;
}
declare const _default: import("vue").DefineComponent<{
    failInfo: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    preview: {
        type: PropType<boolean>;
    };
    previewTip: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    previewList: {
        type: PropType<string[]>;
    };
    fit: {
        type: PropType<"fill" | "none" | "contain" | "cover" | "scale-down">;
    };
    alt: {
        type: PropType<string>;
    };
    src: {
        type: PropType<string>;
    };
    lazy: {
        type: PropType<boolean>;
    };
    referrerPolicy: {
        type: PropType<string>;
    };
    scrollContainer: {
        type: PropType<string | HTMLElement>;
    };
    placeholder: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    width: {
        type: PropType<string | number>;
    };
    height: {
        type: PropType<string | number>;
    };
    infinite: {
        type: PropType<boolean>;
    };
    maskClosable: {
        type: PropType<boolean>;
    };
    previewIndex: {
        type: PropType<number>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("error" | "switch" | "close" | "load")[], "error" | "switch" | "close" | "load", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    failInfo: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    preview: {
        type: PropType<boolean>;
    };
    previewTip: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    previewList: {
        type: PropType<string[]>;
    };
    fit: {
        type: PropType<"fill" | "none" | "contain" | "cover" | "scale-down">;
    };
    alt: {
        type: PropType<string>;
    };
    src: {
        type: PropType<string>;
    };
    lazy: {
        type: PropType<boolean>;
    };
    referrerPolicy: {
        type: PropType<string>;
    };
    scrollContainer: {
        type: PropType<string | HTMLElement>;
    };
    placeholder: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    width: {
        type: PropType<string | number>;
    };
    height: {
        type: PropType<string | number>;
    };
    infinite: {
        type: PropType<boolean>;
    };
    maskClosable: {
        type: PropType<boolean>;
    };
    previewIndex: {
        type: PropType<number>;
    };
}>> & {
    onError?: (...args: any[]) => any;
    onLoad?: (...args: any[]) => any;
    onClose?: (...args: any[]) => any;
    onSwitch?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
