import { PropType } from "vue";
export declare const RowContext: unique symbol;
export type GutterProps = {
    xs?: number | number[];
    sm?: number | number[];
    md?: number | number[];
    lg?: number | number[];
    xl?: number | number[];
    xxl?: number | number[];
};
export interface RowProps {
    justify?: 'start' | 'center' | 'end' | 'space-between' | 'space-around';
    align?: 'top' | 'middle' | 'bottom';
    gutter?: number | number[] | GutterProps;
}
declare const _default: import("vue").DefineComponent<{
    justify: {
        type: PropType<"center" | "end" | "start" | "space-around" | "space-between">;
    };
    align: {
        type: PropType<"top" | "bottom" | "middle">;
    };
    gutter: {
        type: PropType<number | number[] | GutterProps>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    justify: {
        type: PropType<"center" | "end" | "start" | "space-around" | "space-between">;
    };
    align: {
        type: PropType<"top" | "bottom" | "middle">;
    };
    gutter: {
        type: PropType<number | number[] | GutterProps>;
    };
}>>, {}, {}>;
export default _default;
