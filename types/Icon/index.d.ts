import { PropType } from 'vue';
declare const _default: import("vue").DefineComponent<{
    name: {
        type: PropType<string>;
    };
    spin: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<number>;
    };
    color: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: PropType<string>;
    };
    spin: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<number>;
    };
    color: {
        type: PropType<string>;
    };
}>> & {
    onClick?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
