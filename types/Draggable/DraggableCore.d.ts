import { PropType } from 'vue';
export interface DraggableCoreProps {
    handle?: string;
    grid?: number[];
}
declare const _default: import("vue").DefineComponent<{
    onMouseDown: {
        type: FunctionConstructor;
    };
    onStart: {
        type: FunctionConstructor;
    };
    onStop: {
        type: FunctionConstructor;
    };
    onDrag: {
        type: FunctionConstructor;
    };
    allowAnyClick: {
        type: BooleanConstructor;
    };
    disabled: {
        type: BooleanConstructor;
    };
    handle: {
        type: PropType<string>;
    };
    cancel: {
        type: StringConstructor;
    };
    scale: {
        type: NumberConstructor;
    };
    offsetParent: {
        type: ObjectConstructor;
    };
    grid: {
        type: PropType<number[]>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    onMouseDown: {
        type: FunctionConstructor;
    };
    onStart: {
        type: FunctionConstructor;
    };
    onStop: {
        type: FunctionConstructor;
    };
    onDrag: {
        type: FunctionConstructor;
    };
    allowAnyClick: {
        type: BooleanConstructor;
    };
    disabled: {
        type: BooleanConstructor;
    };
    handle: {
        type: PropType<string>;
    };
    cancel: {
        type: StringConstructor;
    };
    scale: {
        type: NumberConstructor;
    };
    offsetParent: {
        type: ObjectConstructor;
    };
    grid: {
        type: PropType<number[]>;
    };
}>>, {
    disabled: boolean;
    allowAnyClick: boolean;
}, {}>;
export default _default;
