import { DraggableData } from './utils';
import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    defaultPosition: {
        type: PropType<any>;
    };
    position: {
        type: PropType<any>;
    };
    scale: {
        type: PropType<number>;
    };
    bounds: {
        type: PropType<string>;
    };
    axis: {
        type: PropType<"both" | "x" | "y">;
    };
    positionOffset: {
        type: PropType<any>;
    };
    grid: {
        type: PropType<number[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    handle: {
        type: PropType<string | HTMLElement>;
    };
    onStart: {
        type: PropType<(e: any, data: DraggableData) => boolean>;
    };
    onDrag: {
        type: PropType<(e: any, data: DraggableData) => boolean>;
    };
    onStop: {
        type: PropType<(e: any, data: DraggableData) => boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    defaultPosition: {
        type: PropType<any>;
    };
    position: {
        type: PropType<any>;
    };
    scale: {
        type: PropType<number>;
    };
    bounds: {
        type: PropType<string>;
    };
    axis: {
        type: PropType<"both" | "x" | "y">;
    };
    positionOffset: {
        type: PropType<any>;
    };
    grid: {
        type: PropType<number[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    handle: {
        type: PropType<string | HTMLElement>;
    };
    onStart: {
        type: PropType<(e: any, data: DraggableData) => boolean>;
    };
    onDrag: {
        type: PropType<(e: any, data: DraggableData) => boolean>;
    };
    onStop: {
        type: PropType<(e: any, data: DraggableData) => boolean>;
    };
}>>, {}, {}>;
export default _default;
