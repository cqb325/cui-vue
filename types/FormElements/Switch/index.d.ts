import { PropType, VNode } from "vue";
export interface SwitchProps {
    size?: 'small' | 'default' | 'large';
    disabled?: boolean;
    name?: string;
    labels?: any[];
    values?: any[];
    round?: boolean;
    icon?: VNode | VNode[];
    colors?: string[];
    onBeforeChange?: (currentStatus: boolean) => Promise<boolean>;
    loading?: boolean;
    modelValue?: boolean;
    asFormField?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    labels: {
        type: PropType<any[]>;
    };
    values: {
        type: PropType<any[]>;
    };
    round: {
        type: PropType<boolean>;
        default: boolean;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }> | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>[]>;
    };
    colors: {
        type: PropType<string[]>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    onBeforeChange: {
        type: PropType<(currentStatus: boolean) => Promise<boolean>>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    labels: {
        type: PropType<any[]>;
    };
    values: {
        type: PropType<any[]>;
    };
    round: {
        type: PropType<boolean>;
        default: boolean;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }> | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>[]>;
    };
    colors: {
        type: PropType<string[]>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    onBeforeChange: {
        type: PropType<(currentStatus: boolean) => Promise<boolean>>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    round: boolean;
    asFormField: boolean;
}, {}>;
export default _default;
