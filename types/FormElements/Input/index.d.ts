import { PropType } from 'vue';
export interface InputProps {
    name?: string;
    disabled?: boolean;
    size?: 'small' | 'default' | 'large';
    type?: string;
    append?: any;
    prepend?: any;
    prefix?: any;
    suffix?: any;
    suffixStyle?: any;
    prefixStyle?: any;
    clearable?: boolean;
    placeholder?: string;
    autocomplete?: string;
    trigger?: string;
    modelValue?: string | number;
    password?: boolean;
    wordCount?: boolean;
    maxLength?: number;
    autoHeight?: boolean;
    asFormField?: boolean;
    rows?: number;
    enterButton?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    name: {
        type: StringConstructor;
    };
    disabled: {
        type: BooleanConstructor;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    type: {
        type: StringConstructor;
    };
    append: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    prepend: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    prefix: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    suffix: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    suffixStyle: {
        type: ObjectConstructor;
    };
    prefixStyle: {
        type: ObjectConstructor;
    };
    clearable: {
        type: BooleanConstructor;
    };
    placeholder: {
        type: StringConstructor;
    };
    autocomplete: {
        type: StringConstructor;
    };
    trigger: {
        type: StringConstructor;
    };
    modelValue: {
        type: (StringConstructor | NumberConstructor)[];
    };
    password: {
        type: BooleanConstructor;
    };
    wordCount: {
        type: BooleanConstructor;
    };
    maxLength: {
        type: NumberConstructor;
    };
    autoHeight: {
        type: BooleanConstructor;
    };
    asFormField: {
        type: BooleanConstructor;
        default: boolean;
    };
    rows: {
        type: NumberConstructor;
    };
    enterButton: {
        type: BooleanConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("search" | "input" | "clear" | "blur" | "change" | "compositionend" | "compositionstart" | "focus" | "enter" | "update:modelValue" | "keyUp" | "keyDown")[], "search" | "input" | "clear" | "blur" | "change" | "compositionend" | "compositionstart" | "focus" | "enter" | "update:modelValue" | "keyUp" | "keyDown", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: StringConstructor;
    };
    disabled: {
        type: BooleanConstructor;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    type: {
        type: StringConstructor;
    };
    append: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    prepend: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    prefix: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    suffix: {
        type: (ObjectConstructor | StringConstructor)[];
    };
    suffixStyle: {
        type: ObjectConstructor;
    };
    prefixStyle: {
        type: ObjectConstructor;
    };
    clearable: {
        type: BooleanConstructor;
    };
    placeholder: {
        type: StringConstructor;
    };
    autocomplete: {
        type: StringConstructor;
    };
    trigger: {
        type: StringConstructor;
    };
    modelValue: {
        type: (StringConstructor | NumberConstructor)[];
    };
    password: {
        type: BooleanConstructor;
    };
    wordCount: {
        type: BooleanConstructor;
    };
    maxLength: {
        type: NumberConstructor;
    };
    autoHeight: {
        type: BooleanConstructor;
    };
    asFormField: {
        type: BooleanConstructor;
        default: boolean;
    };
    rows: {
        type: NumberConstructor;
    };
    enterButton: {
        type: BooleanConstructor;
    };
}>> & {
    onEnter?: (...args: any[]) => any;
    onCompositionend?: (...args: any[]) => any;
    onCompositionstart?: (...args: any[]) => any;
    onFocus?: (...args: any[]) => any;
    onBlur?: (...args: any[]) => any;
    onChange?: (...args: any[]) => any;
    onInput?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onSearch?: (...args: any[]) => any;
    onClear?: (...args: any[]) => any;
    onKeyUp?: (...args: any[]) => any;
    onKeyDown?: (...args: any[]) => any;
}, {
    disabled: boolean;
    password: boolean;
    clearable: boolean;
    wordCount: boolean;
    autoHeight: boolean;
    asFormField: boolean;
    enterButton: boolean;
}, {}>;
export default _default;
