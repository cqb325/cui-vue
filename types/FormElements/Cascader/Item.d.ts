export declare const Item: import("vue").DefineComponent<{
    data: {
        type: ObjectConstructor;
    };
    store: {
        type: ObjectConstructor;
    };
    trigger: {
        type: StringConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: ObjectConstructor;
    };
    store: {
        type: ObjectConstructor;
    };
    trigger: {
        type: StringConstructor;
    };
}>>, {}, {}>;
