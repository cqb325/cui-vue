export declare const Menu: import("vue").DefineComponent<{
    data: {
        type: ArrayConstructor;
    };
    store: {
        type: ObjectConstructor;
    };
    trigger: {
        type: StringConstructor;
    };
    emptyText: {
        type: StringConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: ArrayConstructor;
    };
    store: {
        type: ObjectConstructor;
    };
    trigger: {
        type: StringConstructor;
    };
    emptyText: {
        type: StringConstructor;
    };
}>>, {}, {}>;
