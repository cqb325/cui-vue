import { PropType } from "vue";
import { CascaderNode, CascaderStore } from "./store";
export interface OptionProps {
    data: CascaderNode[];
    seperator: string;
    store: CascaderStore;
    filter?: boolean;
}
export declare const Option: import("vue").DefineComponent<{
    data: {
        type: PropType<CascaderNode[]>;
    };
    seperator: {
        type: PropType<string>;
    };
    store: {
        type: PropType<CascaderStore>;
    };
    filter: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<CascaderNode[]>;
    };
    seperator: {
        type: PropType<string>;
    };
    store: {
        type: PropType<CascaderStore>;
    };
    filter: {
        type: PropType<boolean>;
    };
}>>, {}, {}>;
