import type { TagGroupProps } from "../../TagGroup";
import { PropType, VNode } from "vue";
import { TreeCheckMod } from "../../inner/Constaint";
export interface CascaderProps {
    disabled?: boolean;
    clearable?: boolean;
    size?: 'small' | 'large';
    prepend?: string | VNode;
    modelValue?: string | string[] | number[];
    valueField?: string;
    titleField?: string;
    mode?: TreeCheckMod;
    showMax?: TagGroupProps['max'];
    max?: number;
    showMore?: boolean;
    filter?: boolean;
    emptyText?: string;
    seperator?: string;
    transfer?: boolean;
    header?: VNode;
    footer?: VNode;
    triggerRender?: (labels: any, values: any) => VNode;
    align?: 'bottomLeft' | 'bottomRight';
    revers?: boolean;
    data: any[];
    trigger?: 'click' | 'hover';
    multi?: boolean;
    changeOnSelect?: boolean;
    placeholder?: string;
    beforeChecked?: (item: any, checked: boolean) => boolean;
    tagRender?: (item: any) => string | number | VNode;
    loadData?: (item: any) => Promise<any>;
    asFormField?: boolean;
}
export declare const CascaderContextKey: unique symbol;
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: PropType<boolean>;
    };
    clearable: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    prepend: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    modelValue: {
        type: PropType<string | string[] | number[]>;
    };
    valueField: {
        type: PropType<string>;
    };
    titleField: {
        type: PropType<string>;
    };
    mode: {
        type: PropType<TreeCheckMod>;
    };
    showMax: {
        type: PropType<number | "auto">;
    };
    max: {
        type: PropType<number>;
    };
    showMore: {
        type: PropType<boolean>;
    };
    filter: {
        type: PropType<boolean>;
    };
    emptyText: {
        type: PropType<string>;
    };
    seperator: {
        type: PropType<string>;
    };
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
    header: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    footer: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    triggerRender: {
        type: PropType<(labels: any, values: any) => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
    };
    revers: {
        type: PropType<boolean>;
        default: boolean;
    };
    data: {
        type: PropType<any[]>;
    };
    trigger: {
        type: PropType<"click" | "hover">;
    };
    multi: {
        type: PropType<boolean>;
    };
    tagRender: {
        type: PropType<(item: any) => string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    changeOnSelect: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    beforeChecked: {
        type: PropType<(item: any, checked: boolean) => boolean>;
    };
    loadData: {
        type: PropType<(item: any) => Promise<any>>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("select" | "change" | "update:modelValue" | "exceed")[], "select" | "change" | "update:modelValue" | "exceed", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: PropType<boolean>;
    };
    clearable: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    prepend: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    modelValue: {
        type: PropType<string | string[] | number[]>;
    };
    valueField: {
        type: PropType<string>;
    };
    titleField: {
        type: PropType<string>;
    };
    mode: {
        type: PropType<TreeCheckMod>;
    };
    showMax: {
        type: PropType<number | "auto">;
    };
    max: {
        type: PropType<number>;
    };
    showMore: {
        type: PropType<boolean>;
    };
    filter: {
        type: PropType<boolean>;
    };
    emptyText: {
        type: PropType<string>;
    };
    seperator: {
        type: PropType<string>;
    };
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
    header: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    footer: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    triggerRender: {
        type: PropType<(labels: any, values: any) => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
    };
    revers: {
        type: PropType<boolean>;
        default: boolean;
    };
    data: {
        type: PropType<any[]>;
    };
    trigger: {
        type: PropType<"click" | "hover">;
    };
    multi: {
        type: PropType<boolean>;
    };
    tagRender: {
        type: PropType<(item: any) => string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    changeOnSelect: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    beforeChecked: {
        type: PropType<(item: any, checked: boolean) => boolean>;
    };
    loadData: {
        type: PropType<(item: any) => Promise<any>>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    onSelect?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onExceed?: (...args: any[]) => any;
}, {
    revers: boolean;
    transfer: boolean;
    asFormField: boolean;
}, {}>;
export default _default;
