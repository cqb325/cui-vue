export declare const Alpha: import("vue").DefineComponent<{
    value: {
        type: ObjectConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "change"[], "change", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    value: {
        type: ObjectConstructor;
    };
}>> & {
    onChange?: (...args: any[]) => any;
}, {}, {}>;
