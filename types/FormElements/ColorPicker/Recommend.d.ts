import { PropType } from "vue";
export declare const Recommend: import("vue").DefineComponent<{
    colors: {
        type: PropType<string[]>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "change"[], "change", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    colors: {
        type: PropType<string[]>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
}, {}, {}>;
