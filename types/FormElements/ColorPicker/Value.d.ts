import { PropType } from "vue";
export interface ColorValueProps {
    disabled?: boolean;
    open?: boolean;
    currentValue?: any;
    value: string | number;
    name: string;
}
export declare const Value: import("vue").DefineComponent<{
    open: {
        type: PropType<boolean>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    currentValue: {
        type: PropType<any>;
    };
    value: {
        type: PropType<string | number>;
    };
    name: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    open: {
        type: PropType<boolean>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    currentValue: {
        type: PropType<any>;
    };
    value: {
        type: PropType<string | number>;
    };
    name: {
        type: PropType<string>;
    };
}>>, {}, {}>;
