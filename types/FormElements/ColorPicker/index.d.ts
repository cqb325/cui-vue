import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    name: {
        type: PropType<string>;
        default: string;
    };
    modelValue: {
        type: PropType<string>;
        default: string;
    };
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
    inline: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
        default: string;
    };
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    alpha: {
        type: PropType<boolean>;
        default: boolean;
    };
    size: {
        type: PropType<"small" | "large">;
        default: string;
    };
    recommend: {
        type: PropType<boolean>;
        default: boolean;
    };
    colors: {
        type: PropType<string[]>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: PropType<string>;
        default: string;
    };
    modelValue: {
        type: PropType<string>;
        default: string;
    };
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
    inline: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
        default: string;
    };
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    alpha: {
        type: PropType<boolean>;
        default: boolean;
    };
    size: {
        type: PropType<"small" | "large">;
        default: string;
    };
    recommend: {
        type: PropType<boolean>;
        default: boolean;
    };
    colors: {
        type: PropType<string[]>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    inline: boolean;
    alpha: boolean;
    name: string;
    disabled: boolean;
    size: "small" | "large";
    align: "bottomLeft" | "bottomRight";
    modelValue: string;
    transfer: boolean;
    asFormField: boolean;
    recommend: boolean;
}, {}>;
export default _default;
