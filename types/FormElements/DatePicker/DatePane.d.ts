export declare function clearHms(date: Date): Date;
declare const _default: import("vue").DefineComponent<{
    name: StringConstructor;
    value: (ObjectConstructor | StringConstructor)[];
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: StringConstructor;
    value: (ObjectConstructor | StringConstructor)[];
}>>, {}, {}>;
export default _default;
