declare const _default: import("vue").DefineComponent<{
    value: (StringConstructor | ArrayConstructor)[];
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    value: (StringConstructor | ArrayConstructor)[];
}>>, {}, {}>;
export default _default;
