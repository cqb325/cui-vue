import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    type: {
        type: PropType<"month" | "year">;
        default: any;
    };
    value: {
        type: (StringConstructor | NumberConstructor)[];
        default: any;
    };
    data: {
        type: ObjectConstructor;
        default: any;
    };
    day: {
        type: DateConstructor;
        default: any;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "select"[], "select", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: PropType<"month" | "year">;
        default: any;
    };
    value: {
        type: (StringConstructor | NumberConstructor)[];
        default: any;
    };
    data: {
        type: ObjectConstructor;
        default: any;
    };
    day: {
        type: DateConstructor;
        default: any;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
}, {
    type: "month" | "year";
    data: Record<string, any>;
    value: string | number;
    day: Date;
}, {}>;
export default _default;
