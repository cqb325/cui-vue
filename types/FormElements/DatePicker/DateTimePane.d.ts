declare const _default: import("vue").DefineComponent<{
    name: StringConstructor;
    value: (StringConstructor | DateConstructor)[];
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: StringConstructor;
    value: (StringConstructor | DateConstructor)[];
}>>, {}, {}>;
export default _default;
