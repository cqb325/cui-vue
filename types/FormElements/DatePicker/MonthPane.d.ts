declare const _default: import("vue").DefineComponent<{
    value: (StringConstructor | DateConstructor)[];
    name: StringConstructor;
    onMonthChange: FunctionConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "back"[], "back", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    value: (StringConstructor | DateConstructor)[];
    name: StringConstructor;
    onMonthChange: FunctionConstructor;
}>> & {
    onBack?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
