import { PropType } from "vue";
export type DatepickerStore = {
    currentMonth: Date[];
    range: Date[];
    hoverDate?: Date;
};
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: PropType<boolean>;
    };
    theme: {
        type: PropType<"dark" | "light">;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    clearable: {
        type: PropType<boolean>;
    };
    type: {
        type: PropType<"month" | "date" | "dateRange" | "dateTimeRange" | "dateTime" | "monthRange">;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
    };
    format: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<string | string[] | Date | Date[]>;
        default: any;
    };
    prepend: {
        type: PropType<any>;
    };
    header: {
        type: PropType<any>;
    };
    footer: {
        type: PropType<any>;
    };
    seperator: {
        type: PropType<string>;
    };
    disabledDate: {
        type: PropType<(day: Date) => boolean>;
    };
    transfer: {
        type: PropType<boolean>;
    };
    trigger: {
        type: PropType<() => any>;
    };
    onChange: {
        type: PropType<(value: any) => void>;
    };
    maxRange: {
        type: PropType<number>;
    };
    shortCuts: {
        type: PropType<any>;
    };
    revers: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    stick: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: PropType<boolean>;
    };
    theme: {
        type: PropType<"dark" | "light">;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    clearable: {
        type: PropType<boolean>;
    };
    type: {
        type: PropType<"month" | "date" | "dateRange" | "dateTimeRange" | "dateTime" | "monthRange">;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
    };
    format: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<string | string[] | Date | Date[]>;
        default: any;
    };
    prepend: {
        type: PropType<any>;
    };
    header: {
        type: PropType<any>;
    };
    footer: {
        type: PropType<any>;
    };
    seperator: {
        type: PropType<string>;
    };
    disabledDate: {
        type: PropType<(day: Date) => boolean>;
    };
    transfer: {
        type: PropType<boolean>;
    };
    trigger: {
        type: PropType<() => any>;
    };
    onChange: {
        type: PropType<(value: any) => void>;
    };
    maxRange: {
        type: PropType<number>;
    };
    shortCuts: {
        type: PropType<any>;
    };
    revers: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    stick: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    modelValue: string | string[] | Date | Date[];
    asFormField: boolean;
}, {}>;
export default _default;
