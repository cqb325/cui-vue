declare const _default: import("vue").DefineComponent<{
    day: DateConstructor;
    type: StringConstructor;
    value: (ObjectConstructor | StringConstructor)[];
    month: DateConstructor;
    range: {
        (arrayLength: number): Date[];
        (...items: Date[]): Date[];
        new (arrayLength: number): Date[];
        new (...items: Date[]): Date[];
        isArray(arg: any): arg is any[];
        readonly prototype: any[];
        from<T>(arrayLike: ArrayLike<T>): T[];
        from<T_1, U>(arrayLike: ArrayLike<T_1>, mapfn: (v: T_1, k: number) => U, thisArg?: any): U[];
        from<T_2>(iterable: Iterable<T_2> | ArrayLike<T_2>): T_2[];
        from<T_3, U_1>(iterable: Iterable<T_3> | ArrayLike<T_3>, mapfn: (v: T_3, k: number) => U_1, thisArg?: any): U_1[];
        of<T_4>(...items: T_4[]): T_4[];
        readonly [Symbol.species]: ArrayConstructor;
    };
    hoverDate: DateConstructor;
    name: StringConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    day: DateConstructor;
    type: StringConstructor;
    value: (ObjectConstructor | StringConstructor)[];
    month: DateConstructor;
    range: {
        (arrayLength: number): Date[];
        (...items: Date[]): Date[];
        new (arrayLength: number): Date[];
        new (...items: Date[]): Date[];
        isArray(arg: any): arg is any[];
        readonly prototype: any[];
        from<T>(arrayLike: ArrayLike<T>): T[];
        from<T_1, U>(arrayLike: ArrayLike<T_1>, mapfn: (v: T_1, k: number) => U, thisArg?: any): U[];
        from<T_2>(iterable: Iterable<T_2> | ArrayLike<T_2>): T_2[];
        from<T_3, U_1>(iterable: Iterable<T_3> | ArrayLike<T_3>, mapfn: (v: T_3, k: number) => U_1, thisArg?: any): U_1[];
        of<T_4>(...items: T_4[]): T_4[];
        readonly [Symbol.species]: ArrayConstructor;
    };
    hoverDate: DateConstructor;
    name: StringConstructor;
}>>, {}, {}>;
export default _default;
