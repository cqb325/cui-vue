import { PropType, VNode } from "vue";
declare const _default: import("vue").DefineComponent<{
    type: {
        type: PropType<"time" | "timeRange">;
        default: string;
    };
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    theme: {
        type: PropType<"dark" | "light">;
        default: string;
    };
    size: {
        type: PropType<"small" | "large">;
        default: string;
    };
    clearable: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
        default: string;
    };
    format: {
        type: PropType<string>;
        default: string;
    };
    modelValue: {
        type: PropType<string | string[] | Date | Date[]>;
        default: any;
    };
    prepend: {
        type: PropType<any>;
        default: string;
    };
    disabledTime: {
        type: PropType<(num: number, type: string) => boolean>;
    };
    minuteStep: {
        type: PropType<number>;
    };
    secondStep: {
        type: PropType<number>;
    };
    hourStep: {
        type: PropType<number>;
    };
    header: {
        type: PropType<any>;
    };
    footer: {
        type: PropType<any>;
    };
    seperator: {
        type: PropType<string>;
    };
    transfer: {
        type: PropType<boolean>;
    };
    trigger: {
        type: PropType<() => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    placeholder: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: PropType<"time" | "timeRange">;
        default: string;
    };
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    theme: {
        type: PropType<"dark" | "light">;
        default: string;
    };
    size: {
        type: PropType<"small" | "large">;
        default: string;
    };
    clearable: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
        default: string;
    };
    format: {
        type: PropType<string>;
        default: string;
    };
    modelValue: {
        type: PropType<string | string[] | Date | Date[]>;
        default: any;
    };
    prepend: {
        type: PropType<any>;
        default: string;
    };
    disabledTime: {
        type: PropType<(num: number, type: string) => boolean>;
    };
    minuteStep: {
        type: PropType<number>;
    };
    secondStep: {
        type: PropType<number>;
    };
    hourStep: {
        type: PropType<number>;
    };
    header: {
        type: PropType<any>;
    };
    footer: {
        type: PropType<any>;
    };
    seperator: {
        type: PropType<string>;
    };
    transfer: {
        type: PropType<boolean>;
    };
    trigger: {
        type: PropType<() => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    placeholder: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    type: "time" | "timeRange";
    prepend: any;
    disabled: boolean;
    size: "small" | "large";
    align: "bottomLeft" | "bottomRight";
    format: string;
    theme: "dark" | "light";
    modelValue: string | string[] | Date | Date[];
    clearable: boolean;
    asFormField: boolean;
}, {}>;
export default _default;
