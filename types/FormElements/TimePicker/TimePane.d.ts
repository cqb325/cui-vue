import { PropType, VNode } from "vue";
declare const _default: import("vue").DefineComponent<{
    value: {
        type: PropType<string | Date>;
    };
    format: StringConstructor;
    header: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>>;
    footer: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>>;
    name: StringConstructor;
    hourStep: NumberConstructor;
    minuteStep: NumberConstructor;
    secondStep: NumberConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "selectTime"[], "selectTime", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    value: {
        type: PropType<string | Date>;
    };
    format: StringConstructor;
    header: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>>;
    footer: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>>;
    name: StringConstructor;
    hourStep: NumberConstructor;
    minuteStep: NumberConstructor;
    secondStep: NumberConstructor;
}>> & {
    onSelectTime?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
