declare const _default: import("vue").DefineComponent<{
    max: {
        type: NumberConstructor;
    };
    step: {
        type: NumberConstructor;
    };
    type: {
        type: StringConstructor;
    };
    name: {
        type: StringConstructor;
    };
    value: {
        type: (ObjectConstructor | StringConstructor | NumberConstructor)[];
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "selectTime"[], "selectTime", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    max: {
        type: NumberConstructor;
    };
    step: {
        type: NumberConstructor;
    };
    type: {
        type: StringConstructor;
    };
    name: {
        type: StringConstructor;
    };
    value: {
        type: (ObjectConstructor | StringConstructor | NumberConstructor)[];
    };
}>> & {
    onSelectTime?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
