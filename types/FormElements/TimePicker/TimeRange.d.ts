import { PropType, VNode } from "vue";
declare const _default: import("vue").DefineComponent<{
    header: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>[]>;
    footer: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>[]>;
    value: PropType<string[] | Date[]>;
    format: StringConstructor;
    minuteStep: NumberConstructor;
    hourStep: NumberConstructor;
    secondStep: NumberConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    header: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>[]>;
    footer: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>[]>;
    value: PropType<string[] | Date[]>;
    format: StringConstructor;
    minuteStep: NumberConstructor;
    hourStep: NumberConstructor;
    secondStep: NumberConstructor;
}>>, {}, {}>;
export default _default;
