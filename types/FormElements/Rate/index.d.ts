import { PropType } from "vue";
export interface RateProps {
    disabled?: boolean;
    icon: any;
    modelValue?: number;
    count?: number;
    allowHalf?: boolean;
    onChange?: (val: number) => void;
    name?: string;
    asFormField?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
    modelValue: {
        type: PropType<number>;
    };
    count: {
        type: PropType<number>;
    };
    allowHalf: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
    modelValue: {
        type: PropType<number>;
    };
    count: {
        type: PropType<number>;
    };
    allowHalf: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    asFormField: boolean;
}, {}>;
export default _default;
