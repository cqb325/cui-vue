import { PropType } from "vue";
export interface RateItemProps {
    icon?: any;
    index: number;
    allowHalf?: boolean;
    current: number;
}
declare const _default: import("vue").DefineComponent<{
    icon: {
        type: PropType<any>;
    };
    index: {
        type: PropType<number>;
    };
    allowHalf: {
        type: PropType<boolean>;
    };
    current: {
        type: PropType<number>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("mouseEnter" | "mouseEnterHalf" | "clickHalfStar" | "clickStar")[], "mouseEnter" | "mouseEnterHalf" | "clickHalfStar" | "clickStar", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    icon: {
        type: PropType<any>;
    };
    index: {
        type: PropType<number>;
    };
    allowHalf: {
        type: PropType<boolean>;
    };
    current: {
        type: PropType<number>;
    };
}>> & {
    onMouseEnter?: (...args: any[]) => any;
    onMouseEnterHalf?: (...args: any[]) => any;
    onClickHalfStar?: (...args: any[]) => any;
    onClickStar?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
