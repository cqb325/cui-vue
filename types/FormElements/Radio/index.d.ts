import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: BooleanConstructor;
    };
    type: {
        type: PropType<"checkbox" | "radio">;
    };
    name: {
        type: PropType<string>;
    };
    label: {
        type: PropType<any>;
    };
    inner: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<any>;
    };
    value: {
        type: PropType<any>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: BooleanConstructor;
    };
    type: {
        type: PropType<"checkbox" | "radio">;
    };
    name: {
        type: PropType<string>;
    };
    label: {
        type: PropType<any>;
    };
    inner: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<any>;
    };
    value: {
        type: PropType<any>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    disabled: boolean;
    asFormField: boolean;
}, {}>;
export default _default;
