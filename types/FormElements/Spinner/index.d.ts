import { PropType } from "vue";
export interface SpinnerProps {
    size?: 'small' | 'default' | 'large';
    name?: string;
    modelValue?: number;
    max?: number;
    min?: number;
    step?: number;
    loop?: boolean;
    disabled?: boolean;
    placeholder?: string;
    asFormField?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    name: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
    };
    min: {
        type: PropType<number>;
    };
    step: {
        type: PropType<number>;
    };
    loop: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("sub" | "change" | "plus" | "update:modelValue")[], "sub" | "change" | "plus" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    name: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
    };
    min: {
        type: PropType<number>;
    };
    step: {
        type: PropType<number>;
    };
    loop: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onSub?: (...args: any[]) => any;
    onPlus?: (...args: any[]) => any;
}, {
    asFormField: boolean;
}, {}>;
export default _default;
