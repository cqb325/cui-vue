import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    block: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    value: {
        type: PropType<any[]>;
    };
    modelValue: {
        type: PropType<any[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    data: {
        type: PropType<any>;
    };
    textField: {
        type: PropType<string>;
    };
    valueField: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    block: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    value: {
        type: PropType<any[]>;
    };
    modelValue: {
        type: PropType<any[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    data: {
        type: PropType<any>;
    };
    textField: {
        type: PropType<string>;
    };
    valueField: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    asFormField: boolean;
}, {}>;
export default _default;
