import { PropType } from "vue";
export interface RadioGroupProps {
    block?: boolean;
    name?: string;
    value?: any;
    modelValue?: any;
    disabled?: boolean;
    onChange?: (v: any) => void;
    data?: any;
    type?: 'radio' | 'checkbox';
    textField?: string;
    valueField?: string;
    stick?: boolean;
    asFormField?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    block: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    value: {
        type: PropType<any>;
    };
    modelValue: {
        type: PropType<any>;
        default: any;
    };
    disabled: {
        type: BooleanConstructor;
    };
    data: {
        type: PropType<any>;
    };
    type: {
        type: PropType<"checkbox" | "radio">;
    };
    textField: {
        type: PropType<string>;
    };
    valueField: {
        type: PropType<string>;
    };
    stick: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    block: {
        type: PropType<boolean>;
    };
    name: {
        type: PropType<string>;
    };
    value: {
        type: PropType<any>;
    };
    modelValue: {
        type: PropType<any>;
        default: any;
    };
    disabled: {
        type: BooleanConstructor;
    };
    data: {
        type: PropType<any>;
    };
    type: {
        type: PropType<"checkbox" | "radio">;
    };
    textField: {
        type: PropType<string>;
    };
    valueField: {
        type: PropType<string>;
    };
    stick: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    disabled: boolean;
    modelValue: any;
    asFormField: boolean;
}, {}>;
export default _default;
