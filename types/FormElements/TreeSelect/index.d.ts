import { NodeKeyType, TreeCheckMod, TreeNode, TreeProps } from "../../Tree";
import { PropType } from "vue";
export interface TreeSelectProps extends TreeProps {
    data: TreeNode[];
    transfer?: boolean;
    align?: 'bottomLeft' | 'bottomRight';
    disabled?: boolean;
    clearable?: boolean;
    prepend?: any;
    mode?: TreeCheckMod;
    size?: 'small' | 'large';
    showMax?: number;
    valueClosable?: boolean;
    placeholder?: string;
    showMore?: boolean;
    multi?: boolean;
    asFormField?: boolean;
    onChange?: (value: NodeKeyType | NodeKeyType[]) => void;
}
declare const _default: import("vue").DefineComponent<{
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    clearable: {
        type: PropType<boolean>;
    };
    prepend: {
        type: PropType<any>;
    };
    mode: {
        type: PropType<TreeCheckMod>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    showMax: {
        type: PropType<number>;
    };
    valueClosable: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    showMore: {
        type: PropType<boolean>;
    };
    multi: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    emptyText: {
        type: PropType<string>;
        default: string;
    };
    data: {
        type: PropType<TreeNode[]>;
        default: () => any[];
    };
    checkable: {
        type: PropType<boolean>;
        default: boolean;
    };
    checkRelation: {
        type: PropType<"related" | "unRelated">;
        default: string;
    };
    directory: {
        type: PropType<boolean>;
        default: boolean;
    };
    contextMenu: {
        type: PropType<import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    draggable: {
        type: PropType<boolean>;
        default: boolean;
    };
    loadData: {
        type: PropType<(node: TreeNode | NodeKeyType) => Promise<any>>;
    };
    beforeDropMethod: {
        type: PropType<(node: TreeNode, dragNode: TreeNode, hoverPart: import("../../Tree").dragHoverPartEnum) => Promise<boolean>>;
    };
    beforeExpand: {
        type: PropType<(node: TreeNode, expand: boolean) => Promise<boolean>>;
    };
    onNodeDragStart: {
        type: PropType<(e: any, node: TreeNode) => void>;
    };
    onNodeDragEnter: {
        type: PropType<(e: any, node: any, hoverPart: import("../../Tree").dragHoverPartEnum) => void>;
    };
    onNodeDragOver: {
        type: PropType<(e: any, node: any, hoverPart: import("../../Tree").dragHoverPartEnum) => void>;
    };
    onNodeDragLeave: {
        type: PropType<(e: any, node: any, hoverPart: import("../../Tree").dragHoverPartEnum) => void>;
    };
    onSelectMenu: {
        type: PropType<(name: string) => void>;
    };
    onChange: {
        type: PropType<(value: NodeKeyType[]) => void>;
    };
    onContextMenu: {
        type: PropType<(data: TreeNode) => void>;
    };
    modelValue: {
        type: PropType<NodeKeyType[]>;
    };
    keyField: {
        type: PropType<string>;
    };
    titleField: {
        type: PropType<string>;
    };
    selectedClass: {
        type: PropType<string>;
    };
    draggingClass: {
        type: PropType<string>;
    };
    customIcon: {
        type: PropType<(node: TreeNode) => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    arrowIcon: {
        type: PropType<() => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue" | "nodeSelect" | "nodeExpand" | "nodeCollapse" | "nodeChecked" | "nodeDrop")[], "change" | "update:modelValue" | "nodeSelect" | "nodeExpand" | "nodeCollapse" | "nodeChecked" | "nodeDrop", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    clearable: {
        type: PropType<boolean>;
    };
    prepend: {
        type: PropType<any>;
    };
    mode: {
        type: PropType<TreeCheckMod>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    showMax: {
        type: PropType<number>;
    };
    valueClosable: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    showMore: {
        type: PropType<boolean>;
    };
    multi: {
        type: PropType<boolean>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    emptyText: {
        type: PropType<string>;
        default: string;
    };
    data: {
        type: PropType<TreeNode[]>;
        default: () => any[];
    };
    checkable: {
        type: PropType<boolean>;
        default: boolean;
    };
    checkRelation: {
        type: PropType<"related" | "unRelated">;
        default: string;
    };
    directory: {
        type: PropType<boolean>;
        default: boolean;
    };
    contextMenu: {
        type: PropType<import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    draggable: {
        type: PropType<boolean>;
        default: boolean;
    };
    loadData: {
        type: PropType<(node: TreeNode | NodeKeyType) => Promise<any>>;
    };
    beforeDropMethod: {
        type: PropType<(node: TreeNode, dragNode: TreeNode, hoverPart: import("../../Tree").dragHoverPartEnum) => Promise<boolean>>;
    };
    beforeExpand: {
        type: PropType<(node: TreeNode, expand: boolean) => Promise<boolean>>;
    };
    onNodeDragStart: {
        type: PropType<(e: any, node: TreeNode) => void>;
    };
    onNodeDragEnter: {
        type: PropType<(e: any, node: any, hoverPart: import("../../Tree").dragHoverPartEnum) => void>;
    };
    onNodeDragOver: {
        type: PropType<(e: any, node: any, hoverPart: import("../../Tree").dragHoverPartEnum) => void>;
    };
    onNodeDragLeave: {
        type: PropType<(e: any, node: any, hoverPart: import("../../Tree").dragHoverPartEnum) => void>;
    };
    onSelectMenu: {
        type: PropType<(name: string) => void>;
    };
    onChange: {
        type: PropType<(value: NodeKeyType[]) => void>;
    };
    onContextMenu: {
        type: PropType<(data: TreeNode) => void>;
    };
    modelValue: {
        type: PropType<NodeKeyType[]>;
    };
    keyField: {
        type: PropType<string>;
    };
    titleField: {
        type: PropType<string>;
    };
    selectedClass: {
        type: PropType<string>;
    };
    draggingClass: {
        type: PropType<string>;
    };
    customIcon: {
        type: PropType<(node: TreeNode) => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    arrowIcon: {
        type: PropType<() => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onNodeSelect?: (...args: any[]) => any;
    onNodeExpand?: (...args: any[]) => any;
    onNodeCollapse?: (...args: any[]) => any;
    onNodeChecked?: (...args: any[]) => any;
    onNodeDrop?: (...args: any[]) => any;
}, {
    data: TreeNode[];
    directory: boolean;
    draggable: boolean;
    transfer: boolean;
    asFormField: boolean;
    emptyText: string;
    checkable: boolean;
    checkRelation: "related" | "unRelated";
}, {}>;
export default _default;
