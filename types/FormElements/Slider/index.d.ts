import { PropType } from "vue";
export interface SliderProps {
    range?: boolean;
    min?: number;
    max?: number;
    step?: number;
    modelValue?: number | number[];
    disabled?: boolean;
    tipFormatter?: (value: any) => any;
    marks?: any;
    asFormField?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    range: {
        type: PropType<boolean>;
    };
    min: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
    };
    step: {
        type: PropType<number>;
    };
    modelValue: {
        type: PropType<number | number[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    tipFormatter: {
        type: PropType<(value: any) => any>;
    };
    marks: {
        type: PropType<any>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    range: {
        type: PropType<boolean>;
    };
    min: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
    };
    step: {
        type: PropType<number>;
    };
    modelValue: {
        type: PropType<number | number[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    tipFormatter: {
        type: PropType<(value: any) => any>;
    };
    marks: {
        type: PropType<any>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    asFormField: boolean;
}, {}>;
export default _default;
