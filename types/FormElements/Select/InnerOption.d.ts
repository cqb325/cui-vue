import { PropType } from "vue";
export type SelectOptions = {
    data: any;
    checked?: boolean;
    disabled?: boolean;
    textField: string;
    valueField: string;
    onClick?: (value: any) => void;
    visible?: boolean;
    emptyOption: boolean;
    value?: any;
    label?: string;
    renderOption?: (item: any) => any;
};
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<any>;
    };
    checked: {
        type: PropType<boolean>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    textField: {
        type: PropType<string>;
    };
    valueField: {
        type: PropType<string>;
    };
    visible: {
        type: PropType<boolean>;
    };
    value: {
        type: PropType<any>;
    };
    label: {
        type: PropType<string>;
    };
    emptyOption: {
        type: PropType<boolean>;
    };
    renderOption: {
        type: PropType<(item: any) => any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<any>;
    };
    checked: {
        type: PropType<boolean>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    textField: {
        type: PropType<string>;
    };
    valueField: {
        type: PropType<string>;
    };
    visible: {
        type: PropType<boolean>;
    };
    value: {
        type: PropType<any>;
    };
    label: {
        type: PropType<string>;
    };
    emptyOption: {
        type: PropType<boolean>;
    };
    renderOption: {
        type: PropType<(item: any) => any>;
    };
}>> & {
    onClick?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
