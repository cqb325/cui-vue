declare const _default: import("vue").DefineComponent<{
    value: {
        type: (StringConstructor | BooleanConstructor | NumberConstructor)[];
    };
    label: {
        type: StringConstructor;
    };
}, () => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
    [key: string]: any;
}>[], unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    value: {
        type: (StringConstructor | BooleanConstructor | NumberConstructor)[];
    };
    label: {
        type: StringConstructor;
    };
}>>, {}, {}>;
export default _default;
