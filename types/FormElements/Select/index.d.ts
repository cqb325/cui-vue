import { PropType, VNode } from 'vue';
export { default as Option } from './Option';
export { default as OptionGroup } from './OptionGroup';
declare const _default: import("vue").DefineComponent<{
    name: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<any>;
        default: any;
    };
    disabled: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    clearable: {
        type: PropType<boolean>;
    };
    multi: {
        type: PropType<boolean>;
    };
    prefix: {
        type: PropType<any>;
    };
    placeholder: {
        type: PropType<string>;
    };
    data: {
        type: PropType<any[]>;
    };
    textField: {
        type: PropType<string>;
        default: string;
    };
    valueField: {
        type: PropType<string>;
        default: string;
    };
    filter: {
        type: PropType<boolean>;
    };
    renderOption: {
        type: PropType<(data: any) => any>;
    };
    renderSelectedItem: {
        type: PropType<(data: any) => any>;
    };
    emptyOption: {
        type: PropType<any>;
    };
    showMax: {
        type: PropType<number | "auto">;
    };
    max: {
        type: PropType<number>;
    };
    status: {
        type: PropType<"error" | "warning">;
    };
    footer: {
        type: PropType<any>;
    };
    header: {
        type: PropType<any>;
    };
    triggerRender: {
        type: PropType<(text: string | any[] | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>) => any>;
    };
    valueClosable: {
        type: PropType<boolean>;
    };
    transfer: {
        type: PropType<boolean>;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
        default: string;
    };
    showMore: {
        type: PropType<boolean>;
    };
    maxHeight: {
        type: PropType<number>;
    };
    loading: {
        type: PropType<boolean>;
    };
    remoteMethod: {
        type: PropType<(queryStr: any) => void>;
    };
    debounceTime: {
        type: PropType<number>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    defaultLabel: {
        type: PropType<string | string[]>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue" | "exceed")[], "change" | "update:modelValue" | "exceed", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: PropType<string>;
    };
    modelValue: {
        type: PropType<any>;
        default: any;
    };
    disabled: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    clearable: {
        type: PropType<boolean>;
    };
    multi: {
        type: PropType<boolean>;
    };
    prefix: {
        type: PropType<any>;
    };
    placeholder: {
        type: PropType<string>;
    };
    data: {
        type: PropType<any[]>;
    };
    textField: {
        type: PropType<string>;
        default: string;
    };
    valueField: {
        type: PropType<string>;
        default: string;
    };
    filter: {
        type: PropType<boolean>;
    };
    renderOption: {
        type: PropType<(data: any) => any>;
    };
    renderSelectedItem: {
        type: PropType<(data: any) => any>;
    };
    emptyOption: {
        type: PropType<any>;
    };
    showMax: {
        type: PropType<number | "auto">;
    };
    max: {
        type: PropType<number>;
    };
    status: {
        type: PropType<"error" | "warning">;
    };
    footer: {
        type: PropType<any>;
    };
    header: {
        type: PropType<any>;
    };
    triggerRender: {
        type: PropType<(text: string | any[] | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>) => any>;
    };
    valueClosable: {
        type: PropType<boolean>;
    };
    transfer: {
        type: PropType<boolean>;
    };
    align: {
        type: PropType<"bottomLeft" | "bottomRight">;
        default: string;
    };
    showMore: {
        type: PropType<boolean>;
    };
    maxHeight: {
        type: PropType<number>;
    };
    loading: {
        type: PropType<boolean>;
    };
    remoteMethod: {
        type: PropType<(queryStr: any) => void>;
    };
    debounceTime: {
        type: PropType<number>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    defaultLabel: {
        type: PropType<string | string[]>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onExceed?: (...args: any[]) => any;
}, {
    align: "bottomLeft" | "bottomRight";
    modelValue: any;
    asFormField: boolean;
    valueField: string;
    textField: string;
}, {}>;
export default _default;
