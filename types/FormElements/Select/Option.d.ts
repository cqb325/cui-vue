import { PropType } from "vue";
export interface SelectOptions {
    disabled?: boolean;
    value?: any;
    style?: any;
    class?: string;
    label?: string;
    [key: string]: any;
}
declare const _default: import("vue").DefineComponent<{
    style: {
        type: PropType<any>;
    };
    class: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    value: {
        type: PropType<any>;
    };
    label: {
        type: PropType<string>;
    };
}, () => any, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    style: {
        type: PropType<any>;
    };
    class: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    value: {
        type: PropType<any>;
    };
    label: {
        type: PropType<string>;
    };
}>>, {}, {}>;
export default _default;
