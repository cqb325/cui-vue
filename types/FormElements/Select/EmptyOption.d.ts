declare const _default: import("vue").DefineComponent<{
    checked: BooleanConstructor;
    data: ObjectConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    checked: BooleanConstructor;
    data: ObjectConstructor;
}>> & {
    onClick?: (...args: any[]) => any;
}, {
    checked: boolean;
}, {}>;
export default _default;
