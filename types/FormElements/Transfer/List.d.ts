import { PropType } from "vue";
export type TransferListProps = {
    width?: number;
    height?: number;
    store?: any;
    name?: string;
    value?: any[];
    title?: string;
    render?: (item: any) => any;
    filter?: boolean;
};
declare const _default: import("vue").DefineComponent<{
    width: {
        type: PropType<number>;
    };
    height: {
        type: PropType<number>;
    };
    store: {
        type: PropType<any>;
    };
    name: {
        type: PropType<string>;
    };
    value: {
        type: PropType<any[]>;
    };
    title: {
        type: PropType<string>;
    };
    render: {
        type: PropType<(item: any) => any>;
    };
    filter: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "select"[], "select", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    width: {
        type: PropType<number>;
    };
    height: {
        type: PropType<number>;
    };
    store: {
        type: PropType<any>;
    };
    name: {
        type: PropType<string>;
    };
    value: {
        type: PropType<any[]>;
    };
    title: {
        type: PropType<string>;
    };
    render: {
        type: PropType<(item: any) => any>;
    };
    filter: {
        type: PropType<boolean>;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
