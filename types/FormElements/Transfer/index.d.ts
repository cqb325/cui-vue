import { PropType } from "vue";
export interface TransferProps {
    width?: number;
    height?: number;
    data?: any[];
    modelValue?: any[];
    filter?: boolean;
    rightText?: string;
    leftText?: string;
    sourceTitle?: string;
    targetTitle?: string;
    asFormField?: boolean;
    render?: (item: any) => any;
    onChange?: (value: any[]) => void;
}
export type TransferStore = {
    data: any[];
    sourceDisabled: boolean;
    targetDisabled: boolean;
    sourceIds: any[];
    targetIds: any[];
};
declare const _default: import("vue").DefineComponent<{
    width: {
        type: PropType<number>;
    };
    height: {
        type: PropType<number>;
    };
    data: {
        type: PropType<any[]>;
    };
    modelValue: {
        type: PropType<any[]>;
    };
    filter: {
        type: PropType<boolean>;
    };
    rightText: {
        type: PropType<string>;
    };
    leftText: {
        type: PropType<string>;
    };
    sourceTitle: {
        type: PropType<string>;
    };
    targetTitle: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    render: {
        type: PropType<(item: any) => any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    width: {
        type: PropType<number>;
    };
    height: {
        type: PropType<number>;
    };
    data: {
        type: PropType<any[]>;
    };
    modelValue: {
        type: PropType<any[]>;
    };
    filter: {
        type: PropType<boolean>;
    };
    rightText: {
        type: PropType<string>;
    };
    leftText: {
        type: PropType<string>;
    };
    sourceTitle: {
        type: PropType<string>;
    };
    targetTitle: {
        type: PropType<string>;
    };
    asFormField: {
        type: PropType<boolean>;
        default: boolean;
    };
    render: {
        type: PropType<(item: any) => any>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    asFormField: boolean;
}, {}>;
export default _default;
