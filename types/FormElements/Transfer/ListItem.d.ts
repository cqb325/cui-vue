declare const _default: import("vue").DefineComponent<{
    data: {
        type: ObjectConstructor;
    };
    render: {
        type: FunctionConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "select"[], "select", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: ObjectConstructor;
    };
    render: {
        type: FunctionConstructor;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
