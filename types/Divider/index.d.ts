import { PropType } from 'vue';
type DividerDirection = 'h' | 'v';
type DividerAlign = 'left' | 'right';
declare const _default: import("vue").DefineComponent<{
    dir: {
        type: PropType<DividerDirection>;
        default: string;
    };
    align: {
        type: PropType<DividerAlign>;
    };
    theme: {
        type: PropType<string>;
    };
    margin: {
        type: PropType<string | number>;
    };
    textColor: {
        type: PropType<string>;
    };
    textMargin: {
        type: PropType<string | number>;
    };
    dashed: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    dir: {
        type: PropType<DividerDirection>;
        default: string;
    };
    align: {
        type: PropType<DividerAlign>;
    };
    theme: {
        type: PropType<string>;
    };
    margin: {
        type: PropType<string | number>;
    };
    textColor: {
        type: PropType<string>;
    };
    textMargin: {
        type: PropType<string | number>;
    };
    dashed: {
        type: PropType<boolean>;
    };
}>>, {
    dir: DividerDirection;
}, {}>;
export default _default;
