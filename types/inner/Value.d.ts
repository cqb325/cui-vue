import type { TagGroupProps } from "../TagGroup";
import { PropType, Ref, VNode } from "vue";
export interface ValueProps {
    prepend?: any;
    text?: string | VNode | Array<any>;
    clearable?: boolean;
    icon?: VNode;
    disabled?: boolean;
    size?: 'small' | 'large';
    multi?: boolean;
    showMax?: TagGroupProps['max'];
    onlyInput?: boolean;
    placeholder?: string;
    valueClosable?: boolean;
    filter?: boolean;
    query?: Ref<any>;
    showMore?: boolean;
    tagRender?: (item: any) => string | number | VNode;
}
export declare const Value: import("vue").DefineComponent<{
    prepend: {
        type: PropType<any>;
    };
    text: {
        type: PropType<string | any[] | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    clearable: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    multi: {
        type: PropType<boolean>;
    };
    showMax: {
        type: PropType<number | "auto">;
    };
    onlyInput: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    valueClosable: {
        type: PropType<boolean>;
    };
    filter: {
        type: PropType<boolean>;
    };
    query: {
        type: PropType<Ref<any>>;
    };
    showMore: {
        type: PropType<boolean>;
    };
    tagRender: {
        type: PropType<(item: any) => string | number | VNode>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("input" | "clear" | "close" | "deleteLastValue")[], "input" | "clear" | "close" | "deleteLastValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    prepend: {
        type: PropType<any>;
    };
    text: {
        type: PropType<string | any[] | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    clearable: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    multi: {
        type: PropType<boolean>;
    };
    showMax: {
        type: PropType<number | "auto">;
    };
    onlyInput: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<string>;
    };
    valueClosable: {
        type: PropType<boolean>;
    };
    filter: {
        type: PropType<boolean>;
    };
    query: {
        type: PropType<Ref<any>>;
    };
    showMore: {
        type: PropType<boolean>;
    };
    tagRender: {
        type: PropType<(item: any) => string | number | VNode>;
    };
}>> & {
    onInput?: (...args: any[]) => any;
    onClose?: (...args: any[]) => any;
    onClear?: (...args: any[]) => any;
    onDeleteLastValue?: (...args: any[]) => any;
}, {}, {}>;
