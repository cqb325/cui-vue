import { PropType } from "vue";
export interface CollapaseProps {
    open?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    open: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("end" | "open")[], "end" | "open", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    open: {
        type: PropType<boolean>;
    };
}>> & {
    onEnd?: (...args: any[]) => any;
    onOpen?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
