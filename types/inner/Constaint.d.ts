export declare enum TreeCheckMod {
    FULL = "FULL",
    HALF = "HALF",
    CHILD = "CHILD",
    SHALLOW = "SHALLOW"
}
