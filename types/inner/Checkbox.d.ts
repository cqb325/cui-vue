import { PropType } from "vue";
export interface InnerCheckboxProps {
    checked?: any;
    disabled?: boolean;
    type?: 'radio' | 'checkbox';
    name?: string;
    label?: any;
    value?: any;
}
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: BooleanConstructor;
    };
    type: {
        type: PropType<"checkbox" | "radio">;
    };
    name: {
        type: PropType<string>;
    };
    label: {
        type: PropType<any>;
    };
    checked: {
        type: PropType<any>;
    };
    value: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: BooleanConstructor;
    };
    type: {
        type: PropType<"checkbox" | "radio">;
    };
    name: {
        type: PropType<string>;
    };
    label: {
        type: PropType<any>;
    };
    checked: {
        type: PropType<any>;
    };
    value: {
        type: PropType<any>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    disabled: boolean;
}, {}>;
export default _default;
