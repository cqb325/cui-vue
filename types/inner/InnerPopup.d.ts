import type { ButtonProps } from "../Button";
import { CSSProperties, HTMLAttributes, PropType, VNode } from "vue";
export interface InnerPopupProps extends Omit<HTMLAttributes, 'title'> {
    align?: 'top' | 'bottom' | 'left' | 'right' | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight' | 'leftTop' | 'leftBottom' | 'rightTop' | 'rightBottom';
    trigger?: 'hover' | 'click' | 'focus';
    disabled?: boolean;
    arrow?: boolean;
    hideDelay?: number;
    onVisibleChange?: (open: boolean) => void;
    content?: string | number | JSX.Element;
    contentStyle?: CSSProperties | string;
    modelValue?: boolean;
    theme?: string | 'light' | 'primary' | 'success' | 'info' | 'warning' | 'error' | 'blue' | 'green' | 'red' | 'yellow' | 'pink' | 'magenta' | 'volcano' | 'orange' | 'gold' | 'lime' | 'cyan' | 'geekblue' | 'purple';
    confirm?: boolean;
    okText?: any;
    okType?: ButtonProps['type'];
    title?: string | number | JSX.Element;
    cancelText?: any;
    cancelType?: ButtonProps['type'];
    offset?: number;
    clsPrefix?: string;
    varName?: string;
    onOk?: () => void | Promise<boolean | void>;
    onCancel?: () => void;
    arrowPointAtCenter?: boolean;
    showCancel?: boolean;
    icon?: VNode;
}
export declare const innerPopupDefinedProps: {
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
    };
    trigger: {
        type: PropType<"click" | "focus" | "hover">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    arrow: {
        type: PropType<boolean>;
        default: any;
    };
    hideDelay: {
        type: PropType<number>;
    };
    content: {
        type: PropType<string | number | JSX.Element>;
    };
    contentStyle: {
        type: PropType<string | CSSProperties>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    theme: {
        type: PropType<string>;
    };
    confirm: {
        type: PropType<boolean>;
        default: any;
    };
    okText: {
        type: PropType<any>;
    };
    okType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    title: {
        type: PropType<string | number | JSX.Element>;
    };
    cancelText: {
        type: PropType<any>;
    };
    cancelType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    offset: {
        type: PropType<number>;
    };
    clsPrefix: {
        type: PropType<string>;
    };
    varName: {
        type: PropType<string>;
    };
    arrowPointAtCenter: {
        type: PropType<boolean>;
    };
    showCancel: {
        type: PropType<boolean>;
    };
    onOk: {
        type: PropType<() => void | Promise<boolean | void>>;
    };
    onCancel: {
        type: PropType<() => void>;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
};
export declare const InnerPopup: import("vue").DefineComponent<{
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
    };
    trigger: {
        type: PropType<"click" | "focus" | "hover">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    arrow: {
        type: PropType<boolean>;
        default: any;
    };
    hideDelay: {
        type: PropType<number>;
    };
    content: {
        type: PropType<string | number | JSX.Element>;
    };
    contentStyle: {
        type: PropType<string | CSSProperties>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    theme: {
        type: PropType<string>;
    };
    confirm: {
        type: PropType<boolean>;
        default: any;
    };
    okText: {
        type: PropType<any>;
    };
    okType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    title: {
        type: PropType<string | number | JSX.Element>;
    };
    cancelText: {
        type: PropType<any>;
    };
    cancelType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    offset: {
        type: PropType<number>;
    };
    clsPrefix: {
        type: PropType<string>;
    };
    varName: {
        type: PropType<string>;
    };
    arrowPointAtCenter: {
        type: PropType<boolean>;
    };
    showCancel: {
        type: PropType<boolean>;
    };
    onOk: {
        type: PropType<() => void | Promise<boolean | void>>;
    };
    onCancel: {
        type: PropType<() => void>;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("update:modelValue" | "visibleChange")[], "update:modelValue" | "visibleChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
    };
    trigger: {
        type: PropType<"click" | "focus" | "hover">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    arrow: {
        type: PropType<boolean>;
        default: any;
    };
    hideDelay: {
        type: PropType<number>;
    };
    content: {
        type: PropType<string | number | JSX.Element>;
    };
    contentStyle: {
        type: PropType<string | CSSProperties>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    theme: {
        type: PropType<string>;
    };
    confirm: {
        type: PropType<boolean>;
        default: any;
    };
    okText: {
        type: PropType<any>;
    };
    okType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    title: {
        type: PropType<string | number | JSX.Element>;
    };
    cancelText: {
        type: PropType<any>;
    };
    cancelType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    offset: {
        type: PropType<number>;
    };
    clsPrefix: {
        type: PropType<string>;
    };
    varName: {
        type: PropType<string>;
    };
    arrowPointAtCenter: {
        type: PropType<boolean>;
    };
    showCancel: {
        type: PropType<boolean>;
    };
    onOk: {
        type: PropType<() => void | Promise<boolean | void>>;
    };
    onCancel: {
        type: PropType<() => void>;
    };
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}>> & {
    onVisibleChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    confirm: boolean;
    arrow: boolean;
}, {}>;
