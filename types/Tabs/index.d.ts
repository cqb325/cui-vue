import { PropType } from "vue";
export * from './Tab';
export interface TabsProps {
    card?: boolean;
    activeName?: string;
    extra?: any;
    onTabClick?: Function;
    onRemove?: Function;
    duration?: number;
}
declare const _default: import("vue").DefineComponent<{
    card: {
        type: PropType<boolean>;
    };
    activeName: {
        type: PropType<string>;
    };
    extra: {
        type: PropType<any>;
    };
    duration: {
        type: PropType<number>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("remove" | "tabClick")[], "remove" | "tabClick", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    card: {
        type: PropType<boolean>;
    };
    activeName: {
        type: PropType<string>;
    };
    extra: {
        type: PropType<any>;
    };
    duration: {
        type: PropType<number>;
    };
}>> & {
    onRemove?: (...args: any[]) => any;
    onTabClick?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
