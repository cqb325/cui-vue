import { PropType } from "vue";
export interface TabProps {
    title?: any;
    name: string;
    disabled?: boolean;
    closeable?: boolean;
    icon?: any;
}
declare const _default: import("vue").DefineComponent<{
    name: {
        type: PropType<string>;
    };
    title: {
        type: PropType<any>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    closeable: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: PropType<string>;
    };
    title: {
        type: PropType<any>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    closeable: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
}>>, {}, {}>;
export default _default;
