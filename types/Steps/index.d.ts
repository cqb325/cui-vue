import { PropType } from "vue";
export interface StepsProps {
    size?: 'small' | 'default';
    current?: number;
    dir?: 'v' | 'h';
}
declare const Steps: import("vue").DefineComponent<{
    size: {
        type: PropType<"small" | "default">;
        default: string;
    };
    current: {
        type: NumberConstructor;
        default: number;
    };
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    size: {
        type: PropType<"small" | "default">;
        default: string;
    };
    current: {
        type: NumberConstructor;
        default: number;
    };
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
}>>, {
    dir: "v" | "h";
    size: "small" | "default";
    current: number;
}, {}>;
export default Steps;
