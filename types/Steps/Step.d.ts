import { PropType } from "vue";
export interface StepProps {
    title?: any | string;
    description?: any | string;
    icon?: any;
    status?: 'finished' | 'process' | 'error' | 'warning' | 'wait';
    current: number;
    index: number;
}
declare const _default: import("vue").DefineComponent<{
    title: {
        type: PropType<any>;
        default: any;
    };
    description: {
        type: PropType<any>;
        default: any;
    };
    icon: {
        type: PropType<any>;
        default: any;
    };
    status: {
        type: PropType<"error" | "finished" | "wait" | "warning" | "process">;
        default: any;
    };
    current: {
        type: PropType<number>;
        default: number;
    };
    index: {
        type: PropType<number>;
        default: number;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    title: {
        type: PropType<any>;
        default: any;
    };
    description: {
        type: PropType<any>;
        default: any;
    };
    icon: {
        type: PropType<any>;
        default: any;
    };
    status: {
        type: PropType<"error" | "finished" | "wait" | "warning" | "process">;
        default: any;
    };
    current: {
        type: PropType<number>;
        default: number;
    };
    index: {
        type: PropType<number>;
        default: number;
    };
}>>, {
    index: number;
    title: any;
    description: any;
    status: "error" | "finished" | "wait" | "warning" | "process";
    icon: any;
    current: number;
}, {}>;
export default _default;
