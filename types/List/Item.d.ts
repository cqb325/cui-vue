export interface ListItemProps {
    id: string | number;
    data?: any;
    actions?: any;
    avatar?: any;
    content?: any;
    title?: any;
    desc?: any;
    activeKey?: any;
}
declare const _default: import("vue").DefineComponent<Readonly<{
    data?: any;
    id?: any;
    content?: any;
    title?: any;
    desc?: any;
    activeKey?: any;
    avatar?: any;
    actions?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    data?: any;
    id?: any;
    content?: any;
    title?: any;
    desc?: any;
    activeKey?: any;
    avatar?: any;
    actions?: any;
}>>>, {
    readonly data?: any;
    readonly id?: any;
    readonly content?: any;
    readonly title?: any;
    readonly desc?: any;
    readonly activeKey?: any;
    readonly avatar?: any;
    readonly actions?: any;
}, {}>;
export default _default;
