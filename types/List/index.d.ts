import { PropType, Ref } from "vue";
export declare const ListContext: unique symbol;
export interface ListProps {
    border?: boolean;
    size?: 'small' | 'large';
    head?: any;
    foot?: any;
    selectable?: boolean;
    activeKey: string | number;
}
export type ListContextProps = {
    selectable?: boolean;
    activeKey: Ref<string | number>;
    setActiveKey?: (key: string | number) => void;
    onSelect?: (item: any) => void;
};
declare const List: import("vue").DefineComponent<{
    border: {
        type: BooleanConstructor;
    };
    size: {
        type: () => 'small' | 'large';
    };
    head: {
        type: PropType<any>;
    };
    foot: {
        type: PropType<any>;
    };
    activeKey: {
        type: PropType<string | number>;
    };
    selectable: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("select" | "update:activeKey")[], "select" | "update:activeKey", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    border: {
        type: BooleanConstructor;
    };
    size: {
        type: () => 'small' | 'large';
    };
    head: {
        type: PropType<any>;
    };
    foot: {
        type: PropType<any>;
    };
    activeKey: {
        type: PropType<string | number>;
    };
    selectable: {
        type: PropType<boolean>;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
    "onUpdate:activeKey"?: (...args: any[]) => any;
}, {
    border: boolean;
}, {}>;
export default List;
