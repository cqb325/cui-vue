import { PropType } from "vue";
export interface ImagePreviewProps {
    failInfo?: string;
    previewList: string[];
    infinite?: boolean;
    modelValue?: boolean;
    maskClosable?: boolean;
    initIndex?: number;
}
declare const _default: import("vue").DefineComponent<{
    failInfo: {
        type: PropType<string>;
    };
    previewList: {
        type: PropType<string[]>;
    };
    infinite: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    maskClosable: {
        type: PropType<boolean>;
        default: boolean;
    };
    initIndex: {
        type: PropType<number>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("switch" | "close" | "update:modelValue")[], "switch" | "close" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    failInfo: {
        type: PropType<string>;
    };
    previewList: {
        type: PropType<string[]>;
    };
    infinite: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    maskClosable: {
        type: PropType<boolean>;
        default: boolean;
    };
    initIndex: {
        type: PropType<number>;
    };
}>> & {
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onClose?: (...args: any[]) => any;
    onSwitch?: (...args: any[]) => any;
}, {
    maskClosable: boolean;
}, {}>;
export default _default;
export declare function downloadFile(url: string, name?: string): Promise<void>;
