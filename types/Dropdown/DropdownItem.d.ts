import { PropType } from "vue";
export interface DropdownItemProps {
    disabled?: boolean;
    name?: string;
    divided?: boolean;
    icon?: JSX.Element;
    arrow?: boolean;
    data?: any;
    theme?: string | 'primary' | 'secondary' | 'tertiary' | 'success' | 'warning' | 'error' | 'info' | 'light';
    selected?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    divided: {
        type: PropType<boolean>;
        default: boolean;
    };
    name: {
        type: PropType<string>;
        default: string;
    };
    icon: {
        type: PropType<JSX.Element>;
        default: any;
    };
    arrow: {
        type: PropType<boolean>;
        default: boolean;
    };
    data: {
        type: PropType<any>;
        default: any;
    };
    theme: {
        type: PropType<string>;
        default: string;
    };
    selected: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    divided: {
        type: PropType<boolean>;
        default: boolean;
    };
    name: {
        type: PropType<string>;
        default: string;
    };
    icon: {
        type: PropType<JSX.Element>;
        default: any;
    };
    arrow: {
        type: PropType<boolean>;
        default: boolean;
    };
    data: {
        type: PropType<any>;
        default: any;
    };
    theme: {
        type: PropType<string>;
        default: string;
    };
    selected: {
        type: PropType<boolean>;
        default: boolean;
    };
}>>, {
    data: any;
    name: string;
    disabled: boolean;
    icon: JSX.Element;
    selected: boolean;
    theme: string;
    arrow: boolean;
    divided: boolean;
}, {}>;
export default _default;
