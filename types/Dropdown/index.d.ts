import { PropType } from "vue";
import { DropdownItemProps } from "./DropdownItem";
export { default as DropdownMenu } from './DropdownMenu';
export { default as DropdownItem } from './DropdownItem';
export interface DropdownPosition {
    x: number;
    y: number;
}
export interface DropdownNode extends DropdownItemProps {
    title: string;
    children?: DropdownNode[];
    [key: string]: any;
}
export interface DropdownProps {
    trigger?: 'hover' | 'click' | 'contextMenu' | 'custom';
    align?: 'bottom' | 'bottomLeft' | 'bottomRight' | 'right' | 'rightBottom' | 'left' | 'leftBottom' | 'top' | 'topLeft' | 'topRight' | 'rightTop' | 'leftTop';
    menu?: JSX.Element;
    modelValue?: boolean;
    theme?: string | 'dark' | 'light' | 'primary' | 'success' | 'warning' | 'error' | 'info' | 'blue' | 'green' | 'red' | 'yellow' | 'pink' | 'magenta' | 'volcano' | 'orange' | 'gold' | 'lime' | 'cyan' | 'geekblue' | 'purple';
    data?: DropdownNode[];
    disabled?: boolean;
    revers?: boolean;
    handler?: string;
    fixWidth?: boolean;
    gradient?: string[];
    color?: string;
    arrow?: boolean;
    offset?: number;
    position?: DropdownPosition;
    onBeforeDrop?: (visible: boolean) => boolean;
    transfer?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    trigger: {
        type: PropType<"custom" | "click" | "hover" | "contextMenu">;
        default: string;
    };
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    theme: {
        type: PropType<string>;
    };
    menu: {
        type: PropType<JSX.Element>;
    };
    modelValue: {
        type: PropType<boolean>;
        default: boolean;
    };
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    revers: {
        type: PropType<boolean>;
        default: boolean;
    };
    handler: {
        type: PropType<string>;
        default: any;
    };
    fixWidth: {
        type: PropType<boolean>;
        default: boolean;
    };
    gradient: {
        type: PropType<string[]>;
        default: () => any[];
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    arrow: {
        type: PropType<boolean>;
        default: boolean;
    };
    offset: {
        type: PropType<number>;
        default: number;
    };
    position: {
        type: PropType<DropdownPosition>;
        default: any;
    };
    onBeforeDrop: {
        type: PropType<(visible: boolean) => boolean>;
    };
    data: {
        type: PropType<DropdownNode[]>;
        default: any;
    };
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("select" | "update:modelValue" | "visibleChange" | "mouseClick")[], "select" | "update:modelValue" | "visibleChange" | "mouseClick", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    trigger: {
        type: PropType<"custom" | "click" | "hover" | "contextMenu">;
        default: string;
    };
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    theme: {
        type: PropType<string>;
    };
    menu: {
        type: PropType<JSX.Element>;
    };
    modelValue: {
        type: PropType<boolean>;
        default: boolean;
    };
    disabled: {
        type: PropType<boolean>;
        default: boolean;
    };
    revers: {
        type: PropType<boolean>;
        default: boolean;
    };
    handler: {
        type: PropType<string>;
        default: any;
    };
    fixWidth: {
        type: PropType<boolean>;
        default: boolean;
    };
    gradient: {
        type: PropType<string[]>;
        default: () => any[];
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    arrow: {
        type: PropType<boolean>;
        default: boolean;
    };
    offset: {
        type: PropType<number>;
        default: number;
    };
    position: {
        type: PropType<DropdownPosition>;
        default: any;
    };
    onBeforeDrop: {
        type: PropType<(visible: boolean) => boolean>;
    };
    data: {
        type: PropType<DropdownNode[]>;
        default: any;
    };
    transfer: {
        type: PropType<boolean>;
        default: boolean;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
    onVisibleChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onMouseClick?: (...args: any[]) => any;
}, {
    data: DropdownNode[];
    offset: number;
    color: string;
    position: DropdownPosition;
    disabled: boolean;
    handler: string;
    align: "left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom";
    trigger: "custom" | "click" | "hover" | "contextMenu";
    arrow: boolean;
    modelValue: boolean;
    fixWidth: boolean;
    revers: boolean;
    gradient: string[];
    transfer: boolean;
}, {}>;
export default _default;
