import { PropType } from "vue";
export interface AccordionItemProps {
    name?: string;
    title?: any;
    icon?: any;
}
declare const _default: import("vue").DefineComponent<{
    name: {
        type: PropType<string>;
    };
    title: {
        type: PropType<any>;
    };
    icon: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: PropType<string>;
    };
    title: {
        type: PropType<any>;
    };
    icon: {
        type: PropType<any>;
    };
}>>, {}, {}>;
export default _default;
