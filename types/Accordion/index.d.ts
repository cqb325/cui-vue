import { PropType } from 'vue';
export interface AccordionProps {
    multi?: boolean;
    activeKey?: any;
    flex?: boolean;
}
declare const Accordion: import("vue").DefineComponent<{
    multi: {
        type: PropType<boolean>;
    };
    activeKey: {
        type: PropType<any>;
    };
    flex: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "select"[], "select", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    multi: {
        type: PropType<boolean>;
    };
    activeKey: {
        type: PropType<any>;
    };
    flex: {
        type: PropType<boolean>;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
}, {}, {}>;
export default Accordion;
