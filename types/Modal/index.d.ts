import { PropType } from "vue";
import { ButtonProps } from "../Button";
type Position = {
    top?: string;
    bottom?: string;
    left?: string;
    right?: string;
};
type ModalProps = {
    bounds?: string;
    disabled?: boolean;
    style?: any;
    class?: any;
    headerStyle?: any;
    bodyStyle?: any;
    title?: any;
    content?: any;
    footer?: boolean;
    footerAlign?: 'start' | 'center' | 'end';
    footerReverse?: boolean;
    loading?: boolean;
    onOk?: () => boolean | Promise<boolean | void> | undefined | void;
    onCancel?: () => void;
    onClosed?: () => void;
    onClickClose?: () => void;
    okText?: any;
    okButtonType?: ButtonProps['type'];
    cancleButtonType?: ButtonProps['type'];
    cancleText?: any;
    modelValue?: boolean;
    defaultPosition?: Position;
    mask?: boolean;
    maskClosable?: boolean;
    resetPostion?: boolean;
    hasCloseIcon?: boolean;
    fullScreen?: boolean;
    destroyOnClose?: boolean;
};
declare const Modal: import("vue").DefineComponent<{
    bounds: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    style: {
        type: PropType<any>;
    };
    class: {
        type: PropType<any>;
    };
    headerStyle: {
        type: PropType<any>;
    };
    bodyStyle: {
        type: PropType<any>;
    };
    title: {
        type: PropType<any>;
    };
    content: {
        type: PropType<any>;
    };
    footer: {
        type: PropType<boolean>;
        default: boolean;
    };
    footerAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    footerReverse: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    okText: {
        type: PropType<any>;
    };
    okButtonType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    cancleButtonType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    cancleText: {
        type: PropType<any>;
    };
    defaultPosition: {
        type: PropType<Position>;
    };
    mask: {
        type: PropType<boolean>;
        default: boolean;
    };
    maskClosable: {
        type: PropType<boolean>;
        default: boolean;
    };
    resetPostion: {
        type: PropType<boolean>;
        default: boolean;
    };
    hasCloseIcon: {
        type: PropType<boolean>;
    };
    fullScreen: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    destroyOnClose: {
        type: PropType<boolean>;
    };
    onOk: {
        type: PropType<() => boolean | Promise<boolean | void> | undefined | void>;
    };
    onCancel: {
        type: PropType<() => void>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("closed" | "update:modelValue" | "clickClose")[], "closed" | "update:modelValue" | "clickClose", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    bounds: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    style: {
        type: PropType<any>;
    };
    class: {
        type: PropType<any>;
    };
    headerStyle: {
        type: PropType<any>;
    };
    bodyStyle: {
        type: PropType<any>;
    };
    title: {
        type: PropType<any>;
    };
    content: {
        type: PropType<any>;
    };
    footer: {
        type: PropType<boolean>;
        default: boolean;
    };
    footerAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    footerReverse: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    okText: {
        type: PropType<any>;
    };
    okButtonType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    cancleButtonType: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    cancleText: {
        type: PropType<any>;
    };
    defaultPosition: {
        type: PropType<Position>;
    };
    mask: {
        type: PropType<boolean>;
        default: boolean;
    };
    maskClosable: {
        type: PropType<boolean>;
        default: boolean;
    };
    resetPostion: {
        type: PropType<boolean>;
        default: boolean;
    };
    hasCloseIcon: {
        type: PropType<boolean>;
    };
    fullScreen: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
    };
    destroyOnClose: {
        type: PropType<boolean>;
    };
    onOk: {
        type: PropType<() => boolean | Promise<boolean | void> | undefined | void>;
    };
    onCancel: {
        type: PropType<() => void>;
    };
}>> & {
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onClosed?: (...args: any[]) => any;
    onClickClose?: (...args: any[]) => any;
}, {
    mask: boolean;
    footer: boolean;
    maskClosable: boolean;
    resetPostion: boolean;
}, {}>;
export default Modal;
export interface ModalConfig extends ModalProps {
    content?: any;
    status?: 'success' | 'info' | 'warning' | 'error' | 'confirm';
}
export declare const modal: {
    open(config: ModalConfig): void;
    success(config: ModalConfig): any;
    info(config: ModalConfig): any;
    warning(config: ModalConfig): any;
    error(config: ModalConfig): any;
    confirm(config: ModalConfig): any;
    remove(): void;
};
