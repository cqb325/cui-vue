import { HTMLAttributes, PropType } from "vue";
export interface AvatarProps extends HTMLAttributes {
    size?: number | 'small' | 'large';
    icon?: JSX.Element;
    src?: string;
    shape?: string;
    title?: any;
    hoverMask?: any;
}
declare const _default: import("vue").DefineComponent<{
    size: {
        type: PropType<number | "small" | "large">;
    };
    icon: {
        type: PropType<JSX.Element>;
    };
    src: {
        type: PropType<string>;
    };
    shape: {
        type: PropType<string>;
        default: string;
    };
    title: {
        type: PropType<any>;
    };
    hoverMask: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("click" | "mouseEnter" | "mouseLeave")[], "click" | "mouseEnter" | "mouseLeave", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    size: {
        type: PropType<number | "small" | "large">;
    };
    icon: {
        type: PropType<JSX.Element>;
    };
    src: {
        type: PropType<string>;
    };
    shape: {
        type: PropType<string>;
        default: string;
    };
    title: {
        type: PropType<any>;
    };
    hoverMask: {
        type: PropType<any>;
    };
}>> & {
    onClick?: (...args: any[]) => any;
    onMouseEnter?: (...args: any[]) => any;
    onMouseLeave?: (...args: any[]) => any;
}, {
    shape: string;
}, {}>;
export default _default;
