export interface WatermarkFontType {
    color?: string;
    fontSize?: number | string;
    fontWeight?: 'normal' | 'light' | 'weight' | number;
    fontStyle?: 'none' | 'normal' | 'italic' | 'oblique';
    fontFamily?: string;
}
export interface WatermarkProps {
    zIndex?: number;
    rotate?: number;
    width?: number;
    height?: number;
    image?: string;
    content: string | string[];
    font?: WatermarkFontType;
    gap?: number[];
    offset?: number[];
}
declare const _default: import("vue").DefineComponent<Readonly<{
    offset?: any;
    height?: any;
    width?: any;
    content?: any;
    font?: any;
    gap?: any;
    rotate?: any;
    zIndex?: any;
    image?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    offset?: any;
    height?: any;
    width?: any;
    content?: any;
    font?: any;
    gap?: any;
    rotate?: any;
    zIndex?: any;
    image?: any;
}>>>, {
    readonly offset?: any;
    readonly height?: any;
    readonly width?: any;
    readonly content?: any;
    readonly font?: any;
    readonly gap?: any;
    readonly rotate?: any;
    readonly zIndex?: any;
    readonly image?: any;
}, {}>;
export default _default;
