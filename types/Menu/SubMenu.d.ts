import { PropType } from "vue";
export interface SubMenuProps {
    name?: string;
    align?: "bottom" | "right" | "bottomLeft" | "bottomRight" | "rightTop" | "left" | "leftTop";
    title?: any;
    padding?: number;
    parent?: any;
    icon?: any;
}
declare const _default: import("vue").DefineComponent<{
    name: {
        type: PropType<string>;
    };
    align: {
        type: PropType<"left" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "rightTop" | "leftTop">;
    };
    title: {
        type: PropType<any>;
    };
    padding: {
        type: PropType<number>;
    };
    parent: {
        type: PropType<any>;
    };
    icon: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: PropType<string>;
    };
    align: {
        type: PropType<"left" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "rightTop" | "leftTop">;
    };
    title: {
        type: PropType<any>;
    };
    padding: {
        type: PropType<number>;
    };
    parent: {
        type: PropType<any>;
    };
    icon: {
        type: PropType<any>;
    };
}>>, {}, {}>;
export default _default;
