import { HTMLAttributes, PropType } from "vue";
export interface MenuItemProps extends HTMLAttributes {
    name?: string;
    disabled?: boolean;
    isSubmenuTitle?: boolean;
    data?: any;
    cert?: boolean;
    icon?: any;
}
declare const _default: import("vue").DefineComponent<{
    name: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    isSubmenuTitle: {
        type: PropType<boolean>;
    };
    data: {
        type: PropType<any>;
    };
    cert: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "select"[], "select", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: PropType<string>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    isSubmenuTitle: {
        type: PropType<boolean>;
    };
    data: {
        type: PropType<any>;
    };
    cert: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
