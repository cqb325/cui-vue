import { HTMLAttributes, PropType } from "vue";
export { default as MenuItem } from "./MenuItem";
export { default as SubMenu } from "./SubMenu";
export { default as MenuGroup } from "./MenuGroup";
export interface MenuProps extends HTMLAttributes {
    accordion?: boolean;
    theme?: 'light' | 'dark';
    dir?: 'v' | 'h';
    min?: boolean;
    activeName?: any;
    openKeys?: string[];
}
export declare const MenuContext: unique symbol;
export declare const MenuTreeContext: unique symbol;
declare const _default: import("vue").DefineComponent<{
    accordion: {
        type: PropType<boolean>;
        default: boolean;
    };
    theme: {
        type: PropType<"dark" | "light">;
        default: string;
    };
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
    min: {
        type: PropType<boolean>;
        default: boolean;
    };
    activeName: {
        type: PropType<any>;
    };
    openKeys: {
        type: PropType<string[]>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("select" | "update:activeName")[], "select" | "update:activeName", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    accordion: {
        type: PropType<boolean>;
        default: boolean;
    };
    theme: {
        type: PropType<"dark" | "light">;
        default: string;
    };
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
    min: {
        type: PropType<boolean>;
        default: boolean;
    };
    activeName: {
        type: PropType<any>;
    };
    openKeys: {
        type: PropType<string[]>;
    };
}>> & {
    onSelect?: (...args: any[]) => any;
    "onUpdate:activeName"?: (...args: any[]) => any;
}, {
    min: boolean;
    dir: "v" | "h";
    theme: "dark" | "light";
    accordion: boolean;
}, {}>;
export default _default;
