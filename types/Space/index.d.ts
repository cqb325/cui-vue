import { HTMLAttributes, PropType } from "vue";
export interface SpaceOptions extends HTMLAttributes {
    dir?: 'v' | 'h';
    wrap?: boolean;
    inline?: boolean;
    size?: number;
    align?: 'center' | 'start' | 'end' | 'baseline';
    justify?: 'center' | 'start' | 'end';
    id?: string;
    title?: string;
}
declare const _default: import("vue").DefineComponent<{
    dir: {
        type: PropType<"v" | "h">;
    };
    wrap: {
        type: PropType<boolean>;
    };
    inline: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<number>;
    };
    align: {
        type: PropType<"center" | "end" | "start" | "baseline">;
    };
    justify: {
        type: PropType<"center" | "end" | "start">;
    };
    id: {
        type: PropType<string>;
    };
    title: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    dir: {
        type: PropType<"v" | "h">;
    };
    wrap: {
        type: PropType<boolean>;
    };
    inline: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<number>;
    };
    align: {
        type: PropType<"center" | "end" | "start" | "baseline">;
    };
    justify: {
        type: PropType<"center" | "end" | "start">;
    };
    id: {
        type: PropType<string>;
    };
    title: {
        type: PropType<string>;
    };
}>>, {}, {}>;
export default _default;
