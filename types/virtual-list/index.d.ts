import { PropType } from "vue";
export { default as VirtualListCore } from './core';
export interface CustomComponentProps {
    component: any;
    props: any;
}
export interface VirtualListProps {
    height?: number;
    maxHeight?: number;
    itemEstimatedSize: number;
    overscan?: number;
    items: any[];
    onScroll?: (scrollTop: number) => void;
    itemComponent: CustomComponentProps;
    ref?: any;
    displayDelay?: number;
}
export interface MeasuredData {
    size: number;
    offset: number;
}
export interface IMeasuredDataMap {
    [key: number]: MeasuredData;
}
declare const _default: import("vue").DefineComponent<{
    height: {
        type: PropType<number>;
    };
    maxHeight: {
        type: PropType<number>;
    };
    itemEstimatedSize: {
        type: PropType<number>;
    };
    overscan: {
        type: PropType<number>;
    };
    items: {
        type: PropType<any[]>;
    };
    itemComponent: {
        type: PropType<CustomComponentProps>;
    };
    displayDelay: {
        type: PropType<number>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "scroll"[], "scroll", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    height: {
        type: PropType<number>;
    };
    maxHeight: {
        type: PropType<number>;
    };
    itemEstimatedSize: {
        type: PropType<number>;
    };
    overscan: {
        type: PropType<number>;
    };
    items: {
        type: PropType<any[]>;
    };
    itemComponent: {
        type: PropType<CustomComponentProps>;
    };
    displayDelay: {
        type: PropType<number>;
    };
}>> & {
    onScroll?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
