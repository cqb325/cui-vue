import { PropType } from "vue";
import type { CustomComponentProps } from ".";
export interface VirtualListCoreProps {
    height?: number;
    maxHeight?: number;
    itemEstimatedSize: number;
    overscan?: number;
    items: any[];
    itemComponent: CustomComponentProps;
    scrollElement: HTMLDivElement;
    contentElement: HTMLDivElement;
    bodyElement: HTMLDivElement;
    displayDelay?: number;
}
export interface MeasuredData {
    size: number;
    offset: number;
}
export interface IMeasuredDataMap {
    [key: number]: MeasuredData;
}
declare const _default: import("vue").DefineComponent<{
    height: {
        type: PropType<number>;
    };
    maxHeight: {
        type: PropType<number>;
    };
    itemEstimatedSize: {
        type: PropType<number>;
    };
    overscan: {
        type: PropType<number>;
    };
    items: {
        type: PropType<any[]>;
    };
    itemComponent: {
        type: PropType<CustomComponentProps>;
    };
    displayDelay: {
        type: PropType<number>;
    };
    scrollElement: {
        type: PropType<HTMLDivElement>;
    };
    contentElement: {
        type: PropType<HTMLDivElement>;
    };
    bodyElement: {
        type: PropType<HTMLDivElement>;
    };
}, () => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
    [key: string]: any;
}>[][], unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "scroll"[], "scroll", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    height: {
        type: PropType<number>;
    };
    maxHeight: {
        type: PropType<number>;
    };
    itemEstimatedSize: {
        type: PropType<number>;
    };
    overscan: {
        type: PropType<number>;
    };
    items: {
        type: PropType<any[]>;
    };
    itemComponent: {
        type: PropType<CustomComponentProps>;
    };
    displayDelay: {
        type: PropType<number>;
    };
    scrollElement: {
        type: PropType<HTMLDivElement>;
    };
    contentElement: {
        type: PropType<HTMLDivElement>;
    };
    bodyElement: {
        type: PropType<HTMLDivElement>;
    };
}>> & {
    onScroll?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
