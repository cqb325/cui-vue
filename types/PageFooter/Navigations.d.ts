export declare const FooterNavigations: import("vue").DefineComponent<{}, () => JSX.Element, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{}>>, {}, {}>;
export declare const FooterNavigation: import("vue").DefineComponent<{
    head: {
        type: (ObjectConstructor | StringConstructor)[];
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    head: {
        type: (ObjectConstructor | StringConstructor)[];
    };
}>>, {}, {}>;
export interface FooterNavigationLink {
    icon?: any;
    link: string;
    style?: any;
}
