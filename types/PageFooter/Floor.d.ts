import { PropType } from "vue";
export interface FloorProps {
    center?: boolean;
    padding?: string;
    color?: string;
    dividerTop?: boolean;
    dividerBottom?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    center: {
        type: PropType<boolean>;
        default: boolean;
    };
    padding: {
        type: PropType<string>;
        default: any;
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    dividerTop: {
        type: PropType<boolean>;
        default: any;
    };
    dividerBottom: {
        type: PropType<boolean>;
        default: any;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    center: {
        type: PropType<boolean>;
        default: boolean;
    };
    padding: {
        type: PropType<string>;
        default: any;
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    dividerTop: {
        type: PropType<boolean>;
        default: any;
    };
    dividerBottom: {
        type: PropType<boolean>;
        default: any;
    };
}>>, {
    center: boolean;
    color: string;
    padding: string;
    dividerTop: boolean;
    dividerBottom: boolean;
}, {}>;
export default _default;
