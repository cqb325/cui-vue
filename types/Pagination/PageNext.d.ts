declare const _default: import("vue").DefineComponent<{
    currentIndex: NumberConstructor;
    disabled: BooleanConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    currentIndex: NumberConstructor;
    disabled: BooleanConstructor;
}>> & {
    onClick?: (...args: any[]) => any;
}, {
    disabled: boolean;
}, {}>;
export default _default;
