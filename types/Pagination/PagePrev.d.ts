declare const _default: import("vue").DefineComponent<{
    currentIndex: NumberConstructor;
    current: NumberConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    currentIndex: NumberConstructor;
    current: NumberConstructor;
}>> & {
    onClick?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
