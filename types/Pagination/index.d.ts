import { PropType } from "vue";
export interface PaginationProps {
    shape?: 'normal' | 'circle';
    size?: 'small' | 'large';
    current: number;
    total: number;
    pageSize: number;
    innerNear?: number;
    displayedPages?: number;
    startEndShowNum?: number;
    showNums?: boolean;
    mini?: boolean;
    showTotal?: boolean;
    showPage?: boolean;
    showJumper?: boolean;
    pages?: any[];
}
declare const _default: import("vue").DefineComponent<{
    shape: {
        type: PropType<"normal" | "circle">;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    current: {
        type: PropType<number>;
    };
    total: {
        type: PropType<number>;
    };
    pageSize: {
        type: PropType<number>;
    };
    innerNear: {
        type: PropType<number>;
    };
    displayedPages: {
        type: PropType<number>;
    };
    startEndShowNum: {
        type: PropType<number>;
    };
    showNums: {
        type: PropType<boolean>;
        default: boolean;
    };
    mini: {
        type: PropType<boolean>;
    };
    showTotal: {
        type: PropType<boolean>;
        default: boolean;
    };
    showPage: {
        type: PropType<boolean>;
        default: boolean;
    };
    showJumper: {
        type: PropType<boolean>;
        default: boolean;
    };
    pages: {
        type: PropType<any[]>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "changePageSize")[], "change" | "changePageSize", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    shape: {
        type: PropType<"normal" | "circle">;
    };
    size: {
        type: PropType<"small" | "large">;
    };
    current: {
        type: PropType<number>;
    };
    total: {
        type: PropType<number>;
    };
    pageSize: {
        type: PropType<number>;
    };
    innerNear: {
        type: PropType<number>;
    };
    displayedPages: {
        type: PropType<number>;
    };
    startEndShowNum: {
        type: PropType<number>;
    };
    showNums: {
        type: PropType<boolean>;
        default: boolean;
    };
    mini: {
        type: PropType<boolean>;
    };
    showTotal: {
        type: PropType<boolean>;
        default: boolean;
    };
    showPage: {
        type: PropType<boolean>;
        default: boolean;
    };
    showJumper: {
        type: PropType<boolean>;
        default: boolean;
    };
    pages: {
        type: PropType<any[]>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    onChangePageSize?: (...args: any[]) => any;
}, {
    showNums: boolean;
    showTotal: boolean;
    showPage: boolean;
    showJumper: boolean;
}, {}>;
export default _default;
