declare const _default: import("vue").DefineComponent<{
    active: BooleanConstructor;
    currentIndex: (StringConstructor | NumberConstructor)[];
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    active: BooleanConstructor;
    currentIndex: (StringConstructor | NumberConstructor)[];
}>> & {
    onClick?: (...args: any[]) => any;
}, {
    active: boolean;
}, {}>;
export default _default;
