import { PropType } from 'vue';
declare const _default: import("vue").DefineComponent<{
    prefix: {
        type: PropType<string | object>;
    };
    suffix: {
        type: PropType<string | object>;
    };
    value: {
        type: PropType<string | number | Date>;
        required: true;
    };
    format: {
        type: PropType<string>;
        default: string;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "end"[], "end", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    prefix: {
        type: PropType<string | object>;
    };
    suffix: {
        type: PropType<string | object>;
    };
    value: {
        type: PropType<string | number | Date>;
        required: true;
    };
    format: {
        type: PropType<string>;
        default: string;
    };
}>> & {
    onEnd?: (...args: any[]) => any;
}, {
    format: string;
}, {}>;
export default _default;
