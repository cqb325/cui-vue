import { type HTMLAttributes, type PropType } from "vue";
export interface ButtonProps extends Omit<HTMLAttributes, 'type'> {
    type?: 'primary' | 'secondary' | 'tertiary' | 'success' | 'error' | 'danger' | 'warning' | 'default';
    theme?: 'light' | 'solid' | 'borderless' | 'outline' | 'dashed';
    block?: boolean;
    size?: 'small' | 'default' | 'large';
    active?: boolean;
    shape?: 'square' | 'round' | 'circle';
    disabled?: boolean;
    loading?: boolean;
    icon?: JSX.Element;
    iconAlign?: 'left' | 'right';
}
declare const _default: import("vue").DefineComponent<{
    type: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    theme: {
        type: PropType<"outline" | "dashed" | "solid" | "light" | "borderless">;
    };
    shape: {
        type: PropType<"square" | "round" | "circle">;
    };
    disabled: {
        type: PropType<boolean>;
        default: any;
    };
    block: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    active: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<JSX.Element>;
    };
    iconAlign: {
        type: PropType<"left" | "right">;
        default: string;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "click"[], "click", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    theme: {
        type: PropType<"outline" | "dashed" | "solid" | "light" | "borderless">;
    };
    shape: {
        type: PropType<"square" | "round" | "circle">;
    };
    disabled: {
        type: PropType<boolean>;
        default: any;
    };
    block: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    active: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<JSX.Element>;
    };
    iconAlign: {
        type: PropType<"left" | "right">;
        default: string;
    };
}>> & {
    onClick?: (...args: any[]) => any;
}, {
    disabled: boolean;
    iconAlign: "left" | "right";
}, {}>;
export default _default;
