/**
 * Spin 类
 * @class Spin
 * @constructor
 */
export interface SpinProps {
    type?: 'pulse' | 'oval' | 'gear' | 'dot';
    title?: any;
    size?: number | 'small' | 'large';
}
declare const _default: import("vue").DefineComponent<{
    type: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: (ObjectConstructor | StringConstructor | NumberConstructor)[];
        default: string;
    };
    size: {
        type: (StringConstructor | NumberConstructor)[];
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: StringConstructor;
        default: string;
    };
    title: {
        type: (ObjectConstructor | StringConstructor | NumberConstructor)[];
        default: string;
    };
    size: {
        type: (StringConstructor | NumberConstructor)[];
    };
}>>, {
    type: string;
    title: string | number | Record<string, any>;
}, {}>;
export default _default;
