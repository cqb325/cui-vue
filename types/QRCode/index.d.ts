import { PropType } from "vue";
export interface QRCodeProps {
    level?: string;
    value: string;
    size?: number;
    style?: any;
    color?: string;
    bgColor?: string;
    includeMargin?: boolean;
    marginSize?: number;
    icon?: string;
    imageSettings?: ImageSettings;
    title?: string;
}
type ImageSettings = {
    src: string;
    height?: number;
    width?: number;
    excavate: boolean;
    x?: number;
    y?: number;
};
export declare const QRCodeCanvas: import("vue").DefineComponent<{
    level: {
        type: PropType<string>;
        default: any;
    };
    bgColor: {
        type: PropType<string>;
        default: any;
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    value: {
        type: PropType<string>;
        required: boolean;
    };
    style: {
        type: PropType<any>;
    };
    size: {
        type: PropType<number>;
        default: number;
    };
    includeMargin: {
        type: PropType<boolean>;
    };
    marginSize: {
        type: PropType<number>;
    };
    icon: {
        type: PropType<string>;
    };
    imageSettings: {
        type: PropType<ImageSettings>;
    };
    title: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    level: {
        type: PropType<string>;
        default: any;
    };
    bgColor: {
        type: PropType<string>;
        default: any;
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    value: {
        type: PropType<string>;
        required: boolean;
    };
    style: {
        type: PropType<any>;
    };
    size: {
        type: PropType<number>;
        default: number;
    };
    includeMargin: {
        type: PropType<boolean>;
    };
    marginSize: {
        type: PropType<number>;
    };
    icon: {
        type: PropType<string>;
    };
    imageSettings: {
        type: PropType<ImageSettings>;
    };
    title: {
        type: PropType<string>;
    };
}>>, {
    color: string;
    size: number;
    level: string;
    bgColor: string;
}, {}>;
declare const _default: import("vue").DefineComponent<{
    level: {
        type: PropType<string>;
        default: any;
    };
    bgColor: {
        type: PropType<string>;
        default: any;
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    value: {
        type: PropType<string>;
        required: boolean;
    };
    style: {
        type: PropType<any>;
    };
    size: {
        type: PropType<number>;
        default: number;
    };
    includeMargin: {
        type: PropType<boolean>;
    };
    marginSize: {
        type: PropType<number>;
    };
    icon: {
        type: PropType<string>;
    };
    imageSettings: {
        type: PropType<ImageSettings>;
    };
    title: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    level: {
        type: PropType<string>;
        default: any;
    };
    bgColor: {
        type: PropType<string>;
        default: any;
    };
    color: {
        type: PropType<string>;
        default: any;
    };
    value: {
        type: PropType<string>;
        required: boolean;
    };
    style: {
        type: PropType<any>;
    };
    size: {
        type: PropType<number>;
        default: number;
    };
    includeMargin: {
        type: PropType<boolean>;
    };
    marginSize: {
        type: PropType<number>;
    };
    icon: {
        type: PropType<string>;
    };
    imageSettings: {
        type: PropType<ImageSettings>;
    };
    title: {
        type: PropType<string>;
    };
}>>, {
    color: string;
    size: number;
    level: string;
    bgColor: string;
}, {}>;
export default _default;
