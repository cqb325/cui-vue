export declare const Notices: import("vue").DefineComponent<{
    data: ObjectConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "close"[], "close", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: ObjectConstructor;
}>> & {
    onClose?: (...args: any[]) => any;
}, {}, {}>;
