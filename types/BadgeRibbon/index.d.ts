import { PropType } from "vue";
export interface BadgeRibbonProps {
    text?: any;
    status?: 'success' | 'error' | 'warning' | 'primary' | 'info';
    align?: 'start' | 'end';
    color?: string | 'blue' | 'green' | 'red' | 'yellow' | 'pink' | 'magenta' | 'volcano' | 'orange' | 'gold' | 'lime' | 'cyan' | 'geekblue' | 'purple';
}
declare const _default: import("vue").DefineComponent<{
    text: {
        type: PropType<any>;
    };
    status: {
        type: PropType<"error" | "success" | "primary" | "warning" | "info">;
    };
    align: {
        type: PropType<"end" | "start">;
    };
    color: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    text: {
        type: PropType<any>;
    };
    status: {
        type: PropType<"error" | "success" | "primary" | "warning" | "info">;
    };
    align: {
        type: PropType<"end" | "start">;
    };
    color: {
        type: PropType<string>;
    };
}>>, {}, {}>;
export default _default;
