import { PropType, VNode } from "vue";
type DrawerAlign = 'right' | 'left' | 'top' | 'bottom';
export interface DrawerProps {
    align?: DrawerAlign;
    size?: number;
    title?: string | VNode;
    maskCloseable?: boolean;
    hasClose?: boolean;
    escClose?: boolean;
    modelValue?: boolean;
    onClose?: () => void;
    onShow?: () => void;
    destroyOnClose?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    align: {
        type: PropType<DrawerAlign>;
        default: string;
    };
    size: {
        type: PropType<number>;
        default: number;
    };
    title: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    maskCloseable: {
        type: PropType<boolean>;
        default: boolean;
    };
    hasClose: {
        type: PropType<boolean>;
        default: boolean;
    };
    escClose: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
        default: boolean;
    };
    destroyOnClose: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("show" | "close" | "update:modelValue")[], "show" | "close" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    align: {
        type: PropType<DrawerAlign>;
        default: string;
    };
    size: {
        type: PropType<number>;
        default: number;
    };
    title: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    maskCloseable: {
        type: PropType<boolean>;
        default: boolean;
    };
    hasClose: {
        type: PropType<boolean>;
        default: boolean;
    };
    escClose: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
        default: boolean;
    };
    destroyOnClose: {
        type: PropType<boolean>;
    };
}>> & {
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onShow?: (...args: any[]) => any;
    onClose?: (...args: any[]) => any;
}, {
    size: number;
    align: DrawerAlign;
    modelValue: boolean;
    maskCloseable: boolean;
    hasClose: boolean;
}, {}>;
export default _default;
