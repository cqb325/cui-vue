import { PropType } from "vue";
export interface StrokeProps {
    percent: number;
    color: string;
}
declare const _default: import("vue").DefineComponent<{
    hidePercent: {
        type: PropType<boolean>;
    };
    status: {
        type: PropType<"normal" | "error" | "active" | "success">;
    };
    value: {
        type: PropType<number>;
    };
    strokeWidth: {
        type: PropType<number>;
    };
    textInside: {
        type: PropType<boolean>;
    };
    infoRender: {
        type: PropType<(status: any, value: any) => any>;
    };
    strokeColor: {
        type: PropType<string | string[] | StrokeProps[]>;
    };
    type: {
        type: PropType<"circle" | "line">;
    };
    radius: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    hidePercent: {
        type: PropType<boolean>;
    };
    status: {
        type: PropType<"normal" | "error" | "active" | "success">;
    };
    value: {
        type: PropType<number>;
    };
    strokeWidth: {
        type: PropType<number>;
    };
    textInside: {
        type: PropType<boolean>;
    };
    infoRender: {
        type: PropType<(status: any, value: any) => any>;
    };
    strokeColor: {
        type: PropType<string | string[] | StrokeProps[]>;
    };
    type: {
        type: PropType<"circle" | "line">;
    };
    radius: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
    };
}>>, {}, {}>;
export default _default;
