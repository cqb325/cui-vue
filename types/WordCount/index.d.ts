import { PropType } from "vue";
export interface WordCountProps {
    total: number;
    value: string;
    prefix?: any;
    prefixOverflow?: any;
    suffix?: any;
    suffixOverflow?: any;
    circle?: boolean;
    overflow?: boolean;
    radius?: number;
}
declare const _default: import("vue").DefineComponent<{
    total: {
        type: PropType<number>;
        default: number;
    };
    value: {
        type: PropType<string>;
        default: string;
    };
    prefix: {
        type: PropType<any>;
        default: string;
    };
    prefixOverflow: {
        type: PropType<any>;
        default: string;
    };
    suffix: {
        type: PropType<any>;
        default: string;
    };
    suffixOverflow: {
        type: PropType<any>;
        default: string;
    };
    circle: {
        type: PropType<boolean>;
        default: boolean;
    };
    overflow: {
        type: PropType<boolean>;
        default: boolean;
    };
    radius: {
        type: PropType<number>;
        default: number;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    total: {
        type: PropType<number>;
        default: number;
    };
    value: {
        type: PropType<string>;
        default: string;
    };
    prefix: {
        type: PropType<any>;
        default: string;
    };
    prefixOverflow: {
        type: PropType<any>;
        default: string;
    };
    suffix: {
        type: PropType<any>;
        default: string;
    };
    suffixOverflow: {
        type: PropType<any>;
        default: string;
    };
    circle: {
        type: PropType<boolean>;
        default: boolean;
    };
    overflow: {
        type: PropType<boolean>;
        default: boolean;
    };
    radius: {
        type: PropType<number>;
        default: number;
    };
}>>, {
    total: number;
    overflow: boolean;
    value: string;
    circle: boolean;
    prefix: any;
    radius: number;
    suffix: any;
    prefixOverflow: any;
    suffixOverflow: any;
}, {}>;
export default _default;
