import { PropType } from 'vue';
import { InnerPopupProps } from '../inner/InnerPopup';
export interface FormItemContextProps {
    name?: string;
}
export interface FormItemProps {
    inline?: boolean;
    name?: string;
    labelStyle?: object;
    label?: string;
    labelAlign?: 'start' | 'end' | 'center';
    rules?: {
        [key: string]: any;
    };
    errorTransfer?: boolean;
    errorAlign?: InnerPopupProps['align'];
}
export declare const FormItemContextKey: unique symbol;
declare const _default: import("vue").DefineComponent<{
    inline: {
        type: BooleanConstructor;
        default: any;
    };
    name: {
        type: StringConstructor;
    };
    labelStyle: {
        type: ObjectConstructor;
    };
    label: {
        type: StringConstructor;
    };
    labelAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    rules: {
        type: PropType<{
            [key: string]: any;
        }>;
    };
    errorTransfer: {
        type: BooleanConstructor;
        default: any;
    };
    errorAlign: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: any;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    inline: {
        type: BooleanConstructor;
        default: any;
    };
    name: {
        type: StringConstructor;
    };
    labelStyle: {
        type: ObjectConstructor;
    };
    label: {
        type: StringConstructor;
    };
    labelAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    rules: {
        type: PropType<{
            [key: string]: any;
        }>;
    };
    errorTransfer: {
        type: BooleanConstructor;
        default: any;
    };
    errorAlign: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: any;
    };
}>>, {
    inline: boolean;
    errorAlign: "left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom";
    errorTransfer: boolean;
}, {}>;
export default _default;
