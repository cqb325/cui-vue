declare const _default: import("vue").DefineComponent<Readonly<{
    type?: any;
    size?: any;
    spacing?: any;
    copyable?: any;
    copyText?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "copy"[], "copy", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    type?: any;
    size?: any;
    spacing?: any;
    copyable?: any;
    copyText?: any;
}>>> & {
    onCopy?: (...args: any[]) => any;
}, {
    readonly type?: any;
    readonly size?: any;
    readonly spacing?: any;
    readonly copyable?: any;
    readonly copyText?: any;
}, {}>;
export default _default;
