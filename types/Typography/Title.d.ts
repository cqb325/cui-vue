declare const _default: import("vue").DefineComponent<Readonly<{
    type?: any;
    inline?: any;
    prefix?: any;
    gradient?: any;
    prefixWidth?: any;
    prefixGap?: any;
    prefixColor?: any;
    heading?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    type?: any;
    inline?: any;
    prefix?: any;
    gradient?: any;
    prefixWidth?: any;
    prefixGap?: any;
    prefixColor?: any;
    heading?: any;
}>>>, {
    readonly type?: any;
    readonly inline?: any;
    readonly prefix?: any;
    readonly gradient?: any;
    readonly prefixWidth?: any;
    readonly prefixGap?: any;
    readonly prefixColor?: any;
    readonly heading?: any;
}, {}>;
export default _default;
