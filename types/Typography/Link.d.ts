import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    type: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "warning">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    link: {
        type: PropType<string>;
    };
    icon: {
        type: PropType<any>;
    };
    mark: {
        type: PropType<boolean>;
    };
    code: {
        type: PropType<boolean>;
    };
    underline: {
        type: PropType<boolean>;
    };
    deleted: {
        type: PropType<boolean>;
    };
    strong: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    gradient: {
        type: PropType<string[]>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "warning">;
    };
    disabled: {
        type: PropType<boolean>;
    };
    link: {
        type: PropType<string>;
    };
    icon: {
        type: PropType<any>;
    };
    mark: {
        type: PropType<boolean>;
    };
    code: {
        type: PropType<boolean>;
    };
    underline: {
        type: PropType<boolean>;
    };
    deleted: {
        type: PropType<boolean>;
    };
    strong: {
        type: PropType<boolean>;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    gradient: {
        type: PropType<string[]>;
    };
}>>, {}, {}>;
export default _default;
