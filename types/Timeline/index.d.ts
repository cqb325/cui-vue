export interface TimelineProps {
    mode?: 'left' | 'right' | 'alternate' | 'center';
}
declare const Timeline: import("vue").DefineComponent<{
    mode: {
        type: () => 'left' | 'right' | 'alternate' | 'center';
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    mode: {
        type: () => 'left' | 'right' | 'alternate' | 'center';
    };
}>>, {}, {}>;
export default Timeline;
