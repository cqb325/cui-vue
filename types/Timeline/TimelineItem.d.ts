import { PropType } from "vue";
export interface TimelineItemProps {
    color?: 'blue' | 'green' | 'red' | 'yellow';
    fill?: boolean;
    icon?: any;
    class?: string;
    time?: string;
}
declare const _default: import("vue").DefineComponent<{
    color: {
        type: PropType<"blue" | "green" | "red" | "yellow">;
        default: string;
    };
    class: {
        type: PropType<string>;
    };
    fill: {
        type: PropType<boolean>;
        default: any;
    };
    icon: {
        type: PropType<any>;
    };
    time: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    color: {
        type: PropType<"blue" | "green" | "red" | "yellow">;
        default: string;
    };
    class: {
        type: PropType<string>;
    };
    fill: {
        type: PropType<boolean>;
        default: any;
    };
    icon: {
        type: PropType<any>;
    };
    time: {
        type: PropType<string>;
    };
}>>, {
    fill: boolean;
    color: "blue" | "green" | "red" | "yellow";
}, {}>;
export default _default;
