import { PropType } from "vue";
export interface SplitProps {
    dir?: 'v' | 'h';
    split?: number | string;
    min?: number;
    max?: number;
}
declare const _default: import("vue").DefineComponent<{
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
    split: {
        type: PropType<string | number>;
    };
    min: {
        type: NumberConstructor;
        default: number;
    };
    max: {
        type: NumberConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
    split: {
        type: PropType<string | number>;
    };
    min: {
        type: NumberConstructor;
        default: number;
    };
    max: {
        type: NumberConstructor;
    };
}>>, {
    min: number;
    dir: "v" | "h";
}, {}>;
export default _default;
