import { PropType, VNode } from "vue";
export interface ResultProps {
    icon?: VNode;
    status?: 'success' | 'warning' | 'error' | 'info';
    title?: string | VNode;
    subTitle?: string | VNode;
    extra?: VNode;
    desc?: string | VNode;
}
declare const _default: import("vue").DefineComponent<{
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    status: {
        type: PropType<"error" | "success" | "warning" | "info">;
        default: string;
    };
    title: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    subTitle: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    extra: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    desc: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    class: {
        type: (ObjectConstructor | StringConstructor | ArrayConstructor)[];
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    icon: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    status: {
        type: PropType<"error" | "success" | "warning" | "info">;
        default: string;
    };
    title: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    subTitle: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    extra: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    desc: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    class: {
        type: (ObjectConstructor | StringConstructor | ArrayConstructor)[];
    };
}>>, {
    status: "error" | "success" | "warning" | "info";
}, {}>;
export default _default;
