import { PropType } from "vue";
export interface SideBySideProps {
    left?: any;
    right?: any;
}
declare const _default: import("vue").DefineComponent<{
    left: {
        type: PropType<any>;
        default: any;
    };
    right: {
        type: PropType<any>;
        default: any;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    left: {
        type: PropType<any>;
        default: any;
    };
    right: {
        type: PropType<any>;
        default: any;
    };
}>>, {
    left: any;
    right: any;
}, {}>;
export default _default;
