import { PropType } from "vue";
export interface BadgeProps {
    count?: number;
    dot?: boolean;
    overflowCount?: number;
    text?: string;
    status?: 'success' | 'error' | 'processing' | 'warning' | 'default';
    color?: string | 'blue' | 'green' | 'red' | 'yellow' | 'pink' | 'magenta' | 'volcano' | 'orange' | 'gold' | 'lime' | 'cyan' | 'geekblue' | 'purple';
    type?: 'primary' | 'success' | 'normal' | 'info' | 'error' | 'warning';
}
declare const _default: import("vue").DefineComponent<{
    count: {
        type: PropType<number>;
        default: any;
    };
    dot: {
        type: PropType<boolean>;
    };
    overflowCount: {
        type: PropType<number>;
    };
    text: {
        type: PropType<string>;
        default: any;
    };
    status: {
        type: PropType<"error" | "default" | "success" | "warning" | "processing">;
    };
    color: {
        type: PropType<string>;
    };
    type: {
        type: PropType<"normal" | "error" | "success" | "primary" | "warning" | "info">;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    count: {
        type: PropType<number>;
        default: any;
    };
    dot: {
        type: PropType<boolean>;
    };
    overflowCount: {
        type: PropType<number>;
    };
    text: {
        type: PropType<string>;
        default: any;
    };
    status: {
        type: PropType<"error" | "default" | "success" | "warning" | "processing">;
    };
    color: {
        type: PropType<string>;
    };
    type: {
        type: PropType<"normal" | "error" | "success" | "primary" | "warning" | "info">;
    };
}>>, {
    text: string;
    count: number;
}, {}>;
export default _default;
