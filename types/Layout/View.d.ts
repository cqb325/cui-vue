import { HTMLAttributes } from "vue";
export interface ViewProps extends HTMLAttributes {
    size?: string;
}
export declare const View: import("vue").DefineComponent<Readonly<{
    size?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    size?: any;
}>>>, {
    readonly size?: any;
}, {}>;
export declare const HView: import("vue").DefineComponent<Readonly<{
    size?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    size?: any;
}>>>, {
    readonly size?: any;
}, {}>;
export declare const VView: import("vue").DefineComponent<Readonly<{
    size?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    size?: any;
}>>>, {
    readonly size?: any;
}, {}>;
export declare const FixedView: import("vue").DefineComponent<Readonly<{
    size?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    size?: any;
}>>>, {
    readonly size?: any;
}, {}>;
