import { HTMLAttributes } from "vue";
export interface CenterProps extends HTMLAttributes {
    width?: number;
    height?: number;
}
declare const _default: import("vue").DefineComponent<Readonly<{
    height?: any;
    width?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    height?: any;
    width?: any;
}>>>, {
    readonly height?: any;
    readonly width?: any;
}, {}>;
export default _default;
