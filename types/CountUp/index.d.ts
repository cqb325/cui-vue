import { PropType } from "vue";
/**
 *
 * @param props
 * @returns
 */
declare const _default: import("vue").DefineComponent<{
    value: {
        type: PropType<string | number>;
        default: number;
    };
    start: {
        type: PropType<number>;
        default: number;
    };
    duration: {
        type: PropType<number>;
        default: number;
    };
    decimal: {
        type: PropType<number>;
        default: number;
    };
    useGrouping: {
        type: PropType<boolean>;
        default: boolean;
    };
    useEasing: {
        type: PropType<boolean>;
        default: boolean;
    };
    separator: {
        type: PropType<string>;
        default: string;
    };
    formattingFn: {
        type: PropType<(n: number) => string>;
    };
    prefix: {
        type: PropType<string>;
        default: string;
    };
    suffix: {
        type: PropType<string>;
        default: string;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "end"[], "end", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    value: {
        type: PropType<string | number>;
        default: number;
    };
    start: {
        type: PropType<number>;
        default: number;
    };
    duration: {
        type: PropType<number>;
        default: number;
    };
    decimal: {
        type: PropType<number>;
        default: number;
    };
    useGrouping: {
        type: PropType<boolean>;
        default: boolean;
    };
    useEasing: {
        type: PropType<boolean>;
        default: boolean;
    };
    separator: {
        type: PropType<string>;
        default: string;
    };
    formattingFn: {
        type: PropType<(n: number) => string>;
    };
    prefix: {
        type: PropType<string>;
        default: string;
    };
    suffix: {
        type: PropType<string>;
        default: string;
    };
}>> & {
    onEnd?: (...args: any[]) => any;
}, {
    start: number;
    value: string | number;
    prefix: string;
    decimal: number;
    separator: string;
    duration: number;
    suffix: string;
    useGrouping: boolean;
    useEasing: boolean;
}, {}>;
export default _default;
