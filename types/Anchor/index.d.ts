import { PropType } from "vue";
export interface AnchorProps {
    container?: string | HTMLElement;
    scrollContainer?: string | HTMLElement;
    scrollOffset?: number;
    offsetTop?: number;
    bounds?: number;
    showInk?: boolean;
    mode?: 'hash' | 'history';
}
declare const Anchor: import("vue").DefineComponent<{
    container: {
        type: PropType<string | HTMLElement>;
        default: any;
    };
    scrollContainer: {
        type: PropType<string | HTMLElement>;
        default: any;
    };
    scrollOffset: {
        type: PropType<number>;
        default: any;
    };
    offsetTop: {
        type: PropType<number>;
        default: any;
    };
    bounds: {
        type: PropType<number>;
        default: any;
    };
    showInk: {
        type: PropType<boolean>;
        default: any;
    };
    mode: {
        type: PropType<"history" | "hash">;
        default: any;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    container: {
        type: PropType<string | HTMLElement>;
        default: any;
    };
    scrollContainer: {
        type: PropType<string | HTMLElement>;
        default: any;
    };
    scrollOffset: {
        type: PropType<number>;
        default: any;
    };
    offsetTop: {
        type: PropType<number>;
        default: any;
    };
    bounds: {
        type: PropType<number>;
        default: any;
    };
    showInk: {
        type: PropType<boolean>;
        default: any;
    };
    mode: {
        type: PropType<"history" | "hash">;
        default: any;
    };
}>>, {
    container: string | HTMLElement;
    offsetTop: number;
    mode: "history" | "hash";
    scrollContainer: string | HTMLElement;
    scrollOffset: number;
    bounds: number;
    showInk: boolean;
}, {}>;
export default Anchor;
