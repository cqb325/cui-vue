import { PropType } from "vue";
export interface AnchorLinkProps {
    href: string;
    title: any;
}
declare const _default: import("vue").DefineComponent<{
    href: {
        type: PropType<string>;
        default: any;
    };
    title: {
        type: PropType<any>;
        default: any;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    href: {
        type: PropType<string>;
        default: any;
    };
    title: {
        type: PropType<any>;
        default: any;
    };
}>>, {
    title: any;
    href: string;
}, {}>;
export default _default;
