import type { TreeCheckMod, TreeNode } from './store';
import { PropType, VNode } from 'vue';
export * from './store';
export type NodeKeyType = string | number;
export interface TreeContextProps {
    onOpenClose: (node: any) => void;
    onNodeSelect: (node: any) => void;
    store: any;
    draggable?: boolean;
    checkable: boolean;
    directory?: boolean;
    contextMenu?: VNode;
    selectedClass?: string;
    dragHoverClass?: string;
    draggingClass?: string;
    onContextMenu?: (data: any) => void;
    onDragStart?: (e: any, node: any) => void;
    onDragEnter?: (e: any, node: any, hoverPart: dragHoverPartEnum) => void;
    onDragOver?: (e: any, node: any, hoverPart: dragHoverPartEnum) => void;
    onDragLeave?: (e: any, node: any, hoverPart: dragHoverPartEnum) => void;
    onDrop: (e: any, targetKey: NodeKeyType, dropPosition: dragHoverPartEnum) => void;
    onNodeCheck: (node: NodeKeyType | TreeNode, checked: boolean) => void;
    customIcon?: (node: TreeNode) => VNode;
    arrowIcon?: () => VNode;
}
export interface TreeInstanceProps {
    prepend: (parentKey: NodeKeyType, nodeId: NodeKeyType | TreeNode) => void;
    append: (parentKey: NodeKeyType, nodeId: NodeKeyType | TreeNode) => void;
    insertBefore: (targetKey: NodeKeyType, nodeId: NodeKeyType | TreeNode) => void;
    insertAfter: (targetKey: NodeKeyType, nodeId: NodeKeyType | TreeNode) => void;
    getNode: (nodeId: NodeKeyType) => TreeNode;
    remove: (nodeId: NodeKeyType | TreeNode) => void;
    filter: (keyword: string, filterMethod: any) => void;
    expandAll: () => void;
    collapseAll: () => void;
    expandNode: (nodeId: TreeNode | NodeKeyType, expand: boolean) => Promise<void>;
    scrollTo: (nodeId: TreeNode | NodeKeyType, position: 'top' | 'center' | 'bottom') => void;
    rename: (nodeId: TreeNode | NodeKeyType, title: string) => void;
    checkNode: (node: TreeNode | NodeKeyType, checked: boolean) => void;
    checkAll: () => void;
    uncheckAll: () => void;
    loadData: (nodeId: TreeNode | NodeKeyType, loadDataMethod: (node: TreeNode) => Promise<TreeNode[]>) => void;
    selectNode: (nodeId: NodeKeyType | TreeNode, silence?: boolean) => void;
    getChecked: (mode: TreeCheckMod) => TreeNode[];
    getCheckedKeys: (mode: TreeCheckMod) => NodeKeyType[];
}
export interface TreeProps {
    emptyText?: string;
    data: TreeNode[];
    checkable?: boolean;
    checkRelation?: 'related' | 'unRelated';
    directory?: boolean;
    contextMenu?: VNode;
    onContextMenu?: (data: TreeNode) => void;
    onNodeCheck?: (node: NodeKeyType | TreeNode, checked: boolean) => void;
    onNodeSelect?: (node: TreeNode) => void;
    onOpenClose?: (node: NodeKeyType | TreeNode) => void;
    ref?: any;
    draggable?: boolean;
    style?: any;
    class?: string;
    classList?: any;
    loadData?: (node: NodeKeyType | TreeNode) => Promise<any>;
    beforeDropMethod?: (node: TreeNode, dragNode: TreeNode, hoverPart: dragHoverPartEnum) => Promise<boolean>;
    beforeExpand?: (node: TreeNode, expand: boolean) => Promise<boolean>;
    onNodeDrop?: (e: any, node: TreeNode, dragNode: TreeNode, hoverPart: dragHoverPartEnum) => void;
    onNodeDragStart?: (e: any, node: TreeNode) => void;
    onNodeDragEnter?: (e: any, node: any, hoverPart: dragHoverPartEnum) => void;
    onNodeDragOver?: (e: any, node: any, hoverPart: dragHoverPartEnum) => void;
    onNodeDragLeave?: (e: any, node: any, hoverPart: dragHoverPartEnum) => void;
    onSelectMenu?: (name: string) => void;
    onNodeExpand?: (node: TreeNode) => void;
    onNodeCollapse?: (node: TreeNode) => void;
    onChange?: (value: NodeKeyType[]) => void;
    selected?: NodeKeyType;
    modelValue?: NodeKeyType[];
    keyField?: string;
    titleField?: string;
    selectedClass?: string;
    dragHoverClass?: string;
    draggingClass?: string;
    customIcon?: (node: TreeNode) => VNode;
    arrowIcon?: () => VNode;
    mode?: TreeCheckMod;
}
export declare enum dragHoverPartEnum {
    before = "before",
    body = "body",
    after = "after"
}
export declare const TreeContextKey: unique symbol;
declare const _default: import("vue").DefineComponent<{
    emptyText: {
        type: PropType<string>;
        default: string;
    };
    data: {
        type: PropType<TreeNode[]>;
        default: () => any[];
    };
    checkable: {
        type: PropType<boolean>;
        default: boolean;
    };
    checkRelation: {
        type: PropType<"related" | "unRelated">;
        default: string;
    };
    directory: {
        type: PropType<boolean>;
        default: boolean;
    };
    contextMenu: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    draggable: {
        type: PropType<boolean>;
        default: boolean;
    };
    loadData: {
        type: PropType<(node: TreeNode | NodeKeyType) => Promise<any>>;
    };
    beforeDropMethod: {
        type: PropType<(node: TreeNode, dragNode: TreeNode, hoverPart: dragHoverPartEnum) => Promise<boolean>>;
    };
    beforeExpand: {
        type: PropType<(node: TreeNode, expand: boolean) => Promise<boolean>>;
    };
    onNodeDragStart: {
        type: PropType<(e: any, node: TreeNode) => void>;
    };
    onNodeDragEnter: {
        type: PropType<(e: any, node: any, hoverPart: dragHoverPartEnum) => void>;
    };
    onNodeDragOver: {
        type: PropType<(e: any, node: any, hoverPart: dragHoverPartEnum) => void>;
    };
    onNodeDragLeave: {
        type: PropType<(e: any, node: any, hoverPart: dragHoverPartEnum) => void>;
    };
    onSelectMenu: {
        type: PropType<(name: string) => void>;
    };
    onChange: {
        type: PropType<(value: NodeKeyType[]) => void>;
    };
    onContextMenu: {
        type: PropType<(data: TreeNode) => void>;
    };
    selected: {
        type: PropType<NodeKeyType>;
    };
    modelValue: {
        type: PropType<NodeKeyType[]>;
    };
    keyField: {
        type: PropType<string>;
    };
    titleField: {
        type: PropType<string>;
    };
    selectedClass: {
        type: PropType<string>;
    };
    draggingClass: {
        type: PropType<string>;
    };
    customIcon: {
        type: PropType<(node: TreeNode) => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    arrowIcon: {
        type: PropType<() => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    mode: {
        type: PropType<TreeCheckMod>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue" | "update:selected" | "nodeSelect" | "nodeExpand" | "nodeCollapse" | "nodeChecked" | "nodeDrop")[], "change" | "update:modelValue" | "update:selected" | "nodeSelect" | "nodeExpand" | "nodeCollapse" | "nodeChecked" | "nodeDrop", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    emptyText: {
        type: PropType<string>;
        default: string;
    };
    data: {
        type: PropType<TreeNode[]>;
        default: () => any[];
    };
    checkable: {
        type: PropType<boolean>;
        default: boolean;
    };
    checkRelation: {
        type: PropType<"related" | "unRelated">;
        default: string;
    };
    directory: {
        type: PropType<boolean>;
        default: boolean;
    };
    contextMenu: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    draggable: {
        type: PropType<boolean>;
        default: boolean;
    };
    loadData: {
        type: PropType<(node: TreeNode | NodeKeyType) => Promise<any>>;
    };
    beforeDropMethod: {
        type: PropType<(node: TreeNode, dragNode: TreeNode, hoverPart: dragHoverPartEnum) => Promise<boolean>>;
    };
    beforeExpand: {
        type: PropType<(node: TreeNode, expand: boolean) => Promise<boolean>>;
    };
    onNodeDragStart: {
        type: PropType<(e: any, node: TreeNode) => void>;
    };
    onNodeDragEnter: {
        type: PropType<(e: any, node: any, hoverPart: dragHoverPartEnum) => void>;
    };
    onNodeDragOver: {
        type: PropType<(e: any, node: any, hoverPart: dragHoverPartEnum) => void>;
    };
    onNodeDragLeave: {
        type: PropType<(e: any, node: any, hoverPart: dragHoverPartEnum) => void>;
    };
    onSelectMenu: {
        type: PropType<(name: string) => void>;
    };
    onChange: {
        type: PropType<(value: NodeKeyType[]) => void>;
    };
    onContextMenu: {
        type: PropType<(data: TreeNode) => void>;
    };
    selected: {
        type: PropType<NodeKeyType>;
    };
    modelValue: {
        type: PropType<NodeKeyType[]>;
    };
    keyField: {
        type: PropType<string>;
    };
    titleField: {
        type: PropType<string>;
    };
    selectedClass: {
        type: PropType<string>;
    };
    draggingClass: {
        type: PropType<string>;
    };
    customIcon: {
        type: PropType<(node: TreeNode) => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    arrowIcon: {
        type: PropType<() => VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    mode: {
        type: PropType<TreeCheckMod>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
    "onUpdate:selected"?: (...args: any[]) => any;
    onNodeSelect?: (...args: any[]) => any;
    onNodeExpand?: (...args: any[]) => any;
    onNodeCollapse?: (...args: any[]) => any;
    onNodeChecked?: (...args: any[]) => any;
    onNodeDrop?: (...args: any[]) => any;
}, {
    data: TreeNode[];
    directory: boolean;
    draggable: boolean;
    emptyText: string;
    checkable: boolean;
    checkRelation: "related" | "unRelated";
}, {}>;
export default _default;
