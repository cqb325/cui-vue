import type { TreeNode } from "./store";
import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<TreeNode>;
        required: true;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<TreeNode>;
        required: true;
    };
}>>, {}, {}>;
export default _default;
