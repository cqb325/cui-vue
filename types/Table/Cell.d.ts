import { PropType } from "vue";
import { ColumnProps } from ".";
declare const _default: import("vue").DefineComponent<{
    column: {
        type: PropType<ColumnProps>;
    };
    colIndex: {
        type: NumberConstructor;
    };
    type: {
        type: StringConstructor;
    };
    showFixedLeft: {
        type: BooleanConstructor;
    };
    showFixedRight: {
        type: BooleanConstructor;
    };
    placeholder: {
        type: BooleanConstructor;
    };
    data: {
        type: ObjectConstructor;
    };
    index: {
        type: NumberConstructor;
    };
    checkedAll: {
        type: (StringConstructor | BooleanConstructor)[];
    };
    colSpan: {
        type: NumberConstructor;
    };
    rowSpan: {
        type: NumberConstructor;
    };
    summary: {
        type: BooleanConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    column: {
        type: PropType<ColumnProps>;
    };
    colIndex: {
        type: NumberConstructor;
    };
    type: {
        type: StringConstructor;
    };
    showFixedLeft: {
        type: BooleanConstructor;
    };
    showFixedRight: {
        type: BooleanConstructor;
    };
    placeholder: {
        type: BooleanConstructor;
    };
    data: {
        type: ObjectConstructor;
    };
    index: {
        type: NumberConstructor;
    };
    checkedAll: {
        type: (StringConstructor | BooleanConstructor)[];
    };
    colSpan: {
        type: NumberConstructor;
    };
    rowSpan: {
        type: NumberConstructor;
    };
    summary: {
        type: BooleanConstructor;
    };
}>>, {
    summary: boolean;
    placeholder: boolean;
    showFixedLeft: boolean;
    showFixedRight: boolean;
}, {}>;
export default _default;
