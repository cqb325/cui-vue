import { TableStore } from '.';
import { PropType } from "vue";
export declare const Row: import("vue").DefineComponent<{
    store: {
        type: PropType<TableStore>;
    };
    data: {
        type: PropType<any>;
    };
    index: {
        type: PropType<number>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    store: {
        type: PropType<TableStore>;
    };
    data: {
        type: PropType<any>;
    };
    index: {
        type: PropType<number>;
    };
}>>, {}, {}>;
export declare const EmprtyRow: import("vue").DefineComponent<{
    store: ObjectConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    store: ObjectConstructor;
}>>, {}, {}>;
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<TableStore>;
    };
    height: {
        type: PropType<number>;
    };
    virtual: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "scroll"[], "scroll", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<TableStore>;
    };
    height: {
        type: PropType<number>;
    };
    virtual: {
        type: PropType<boolean>;
    };
}>> & {
    onScroll?: (...args: any[]) => any;
}, {}, {}>;
export default _default;
