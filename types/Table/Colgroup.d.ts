import { PropType } from 'vue';
import { TableStore } from '.';
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<TableStore>;
        required: true;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<TableStore>;
        required: true;
    };
}>>, {}, {}>;
export default _default;
