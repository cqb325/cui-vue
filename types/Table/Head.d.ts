import { TableStore } from '.';
import { PropType } from "vue";
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<TableStore>;
    };
    sticky: {
        type: PropType<boolean>;
    };
    onInitColumnWidth: {
        type: PropType<(idx: number, width: number) => void>;
    };
    onResizeHeader: {
        type: PropType<(width: number, height: number) => void>;
    };
    virtual: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<TableStore>;
    };
    sticky: {
        type: PropType<boolean>;
    };
    onInitColumnWidth: {
        type: PropType<(idx: number, width: number) => void>;
    };
    onResizeHeader: {
        type: PropType<(width: number, height: number) => void>;
    };
    virtual: {
        type: PropType<boolean>;
    };
}>>, {}, {}>;
export default _default;
