import { PropType, VNode } from "vue";
import { PopoverProps } from "../Popover";
export type TableStore = {
    columns: ColumnProps[];
    columnsRows: any[];
    data: any[];
    showFixedLeft: boolean;
    showFixedRight: boolean;
    checkedAll: boolean | string;
    resizing: boolean;
    headerSize: any;
    summarySize: any;
    headerLeft: number;
    x: number;
    posX: number;
    startX: number;
    resizeId?: string;
};
export type ColumnProps = {
    name?: string;
    title?: string | VNode;
    render?: (data: {
        value: any;
        column: any;
        record: any;
        index: number;
    }) => any;
    type?: 'index' | 'date' | 'datetime' | 'enum' | 'checkbox' | 'expand';
    width?: string;
    minWidth?: number;
    maxWidth?: number;
    _width?: number;
    resize?: boolean;
    sort?: boolean | 'custom';
    sortMethod?(a: any, b: any): number;
    sortType?: 'asc' | 'desc' | '';
    fixed?: 'left' | 'right';
    tree?: boolean;
    ellipsis?: boolean;
    tooltip?: boolean;
    tooltipAlign?: PopoverProps['align'];
    tooltipTheme?: PopoverProps['color'];
    tooltipMaxWidth?: number;
    tooltipStyle?: any;
    fixedLeftLast?: boolean;
    fixedRightFirst?: boolean;
    children?: ColumnProps[];
    _colspan?: number;
    _rowspan?: number;
    _parent?: ColumnProps;
    _level?: number;
    id: string;
    _index: number;
    _: string;
    enum?: {
        [key: string]: string;
    };
};
export declare const TableContextKey: unique symbol;
declare const _default: import("vue").DefineComponent<{
    columns: {
        type: PropType<any[]>;
    };
    data: {
        type: PropType<any[]>;
    };
    rowKey: {
        type: PropType<string>;
        default: string;
    };
    height: {
        type: NumberConstructor;
        default: any;
    };
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    stripe: {
        type: BooleanConstructor;
        default: boolean;
    };
    highlight: {
        type: PropType<boolean>;
    };
    showHeader: {
        type: PropType<boolean>;
        default: boolean;
    };
    selectedRowKeys: {
        type: PropType<KeyType[]>;
        default: () => any[];
    };
    size: {
        type: PropType<"small">;
    };
    spanMethod: {
        type: PropType<(data: any, column: any, index: number, columnIndex: number) => any>;
    };
    showSummary: {
        type: PropType<boolean>;
    };
    summaryMethod: {
        type: PropType<(columns: ColumnProps[], data: any[]) => any>;
    };
    loading: {
        type: PropType<boolean>;
    };
    loadingText: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    title: {
        type: PropType<string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    footer: {
        type: PropType<string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    empty: {
        type: PropType<string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    virtual: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("sort" | "checkedAll" | "rowSelect" | "rowChecked" | "update:selectedRowKeys")[], "sort" | "checkedAll" | "rowSelect" | "rowChecked" | "update:selectedRowKeys", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    columns: {
        type: PropType<any[]>;
    };
    data: {
        type: PropType<any[]>;
    };
    rowKey: {
        type: PropType<string>;
        default: string;
    };
    height: {
        type: NumberConstructor;
        default: any;
    };
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    stripe: {
        type: BooleanConstructor;
        default: boolean;
    };
    highlight: {
        type: PropType<boolean>;
    };
    showHeader: {
        type: PropType<boolean>;
        default: boolean;
    };
    selectedRowKeys: {
        type: PropType<KeyType[]>;
        default: () => any[];
    };
    size: {
        type: PropType<"small">;
    };
    spanMethod: {
        type: PropType<(data: any, column: any, index: number, columnIndex: number) => any>;
    };
    showSummary: {
        type: PropType<boolean>;
    };
    summaryMethod: {
        type: PropType<(columns: ColumnProps[], data: any[]) => any>;
    };
    loading: {
        type: PropType<boolean>;
    };
    loadingText: {
        type: PropType<string | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    title: {
        type: PropType<string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    footer: {
        type: PropType<string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    empty: {
        type: PropType<string | number | VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
    virtual: {
        type: PropType<boolean>;
    };
}>> & {
    onSort?: (...args: any[]) => any;
    onCheckedAll?: (...args: any[]) => any;
    onRowSelect?: (...args: any[]) => any;
    onRowChecked?: (...args: any[]) => any;
    "onUpdate:selectedRowKeys"?: (...args: any[]) => any;
}, {
    height: number;
    border: boolean;
    rowKey: string;
    showHeader: boolean;
    selectedRowKeys: KeyType[];
    stripe: boolean;
}, {}>;
export default _default;
