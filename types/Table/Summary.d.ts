import type { ColumnProps, TableStore } from ".";
export interface SummaryProps {
    data: TableStore;
    summaryMethod?: (columns: ColumnProps[], data: any[]) => any;
    onResizeSummary: (width: number, height: number) => void;
}
declare const _default: import("vue").DefineComponent<Readonly<{
    data?: any;
    summaryMethod?: any;
    onResizeSummary?: any;
}>, JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    data?: any;
    summaryMethod?: any;
    onResizeSummary?: any;
}>>>, {
    readonly data?: any;
    readonly summaryMethod?: any;
    readonly onResizeSummary?: any;
}, {}>;
export default _default;
