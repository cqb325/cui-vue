import { PropType } from 'vue';
export interface ColResponsiveProps {
    grid?: number;
    offset?: number;
}
export interface ColProps {
    grid?: number;
    push?: number;
    pull?: number;
    offset?: number;
    flex?: string;
    fixWidth?: boolean;
    xs?: number | ColResponsiveProps;
    sm?: number | ColResponsiveProps;
    md?: number | ColResponsiveProps;
    lg?: number | ColResponsiveProps;
    xl?: number | ColResponsiveProps;
    xxl?: number | ColResponsiveProps;
}
declare const _default: import("vue").DefineComponent<{
    grid: {
        type: NumberConstructor;
    };
    push: {
        type: NumberConstructor;
    };
    pull: {
        type: NumberConstructor;
    };
    offset: {
        type: NumberConstructor;
    };
    flex: {
        type: StringConstructor;
    };
    fixWidth: {
        type: BooleanConstructor;
    };
    xs: {
        type: PropType<number | ColResponsiveProps>;
    };
    sm: {
        type: PropType<number | ColResponsiveProps>;
    };
    md: {
        type: PropType<number | ColResponsiveProps>;
    };
    lg: {
        type: PropType<number | ColResponsiveProps>;
    };
    xl: {
        type: PropType<number | ColResponsiveProps>;
    };
    xxl: {
        type: PropType<number | ColResponsiveProps>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    grid: {
        type: NumberConstructor;
    };
    push: {
        type: NumberConstructor;
    };
    pull: {
        type: NumberConstructor;
    };
    offset: {
        type: NumberConstructor;
    };
    flex: {
        type: StringConstructor;
    };
    fixWidth: {
        type: BooleanConstructor;
    };
    xs: {
        type: PropType<number | ColResponsiveProps>;
    };
    sm: {
        type: PropType<number | ColResponsiveProps>;
    };
    md: {
        type: PropType<number | ColResponsiveProps>;
    };
    lg: {
        type: PropType<number | ColResponsiveProps>;
    };
    xl: {
        type: PropType<number | ColResponsiveProps>;
    };
    xxl: {
        type: PropType<number | ColResponsiveProps>;
    };
}>>, {
    fixWidth: boolean;
}, {}>;
export default _default;
