import { PropType } from 'vue';
export declare const BUTTON_GROUP_CONTEXT: unique symbol;
declare const _default: import("vue").DefineComponent<{
    type: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    theme: {
        type: PropType<"outline" | "dashed" | "solid" | "light" | "borderless">;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    disabled: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    theme: {
        type: PropType<"outline" | "dashed" | "solid" | "light" | "borderless">;
    };
    size: {
        type: PropType<"small" | "default" | "large">;
    };
    disabled: {
        type: PropType<boolean>;
    };
}>>, {}, {}>;
export default _default;
