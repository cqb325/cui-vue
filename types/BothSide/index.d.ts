declare function BothSide(props: any, { slots }: {
    slots: any;
}): JSX.Element;
declare namespace BothSide {
    var displayName: string;
}
export default BothSide;
