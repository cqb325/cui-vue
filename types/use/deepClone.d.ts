export declare function deepCopy(data: any, hash?: WeakMap<WeakKey, any>): any;
