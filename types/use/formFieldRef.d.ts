export default function formFieldRef<T>(props: any, emit: any, defaultValue?: T): import("vue").Ref<import("vue").UnwrapRef<T>>;
