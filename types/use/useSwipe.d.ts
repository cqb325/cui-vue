export interface Position {
    x: number;
    y: number;
}
export interface UseSwipeOptions {
    /**
     * @default 50
     */
    threshold?: number;
    /**
     * Callback on swipe start.
     */
    onSwipeStart?: (e: PointerEvent) => void;
    /**
     * Callback on swipe move.
     */
    onSwipe?: (e: PointerEvent) => void;
    /**
     * Callback on swipe end.
     */
    onSwipeEnd?: (e: PointerEvent, direction: 'left' | 'right' | 'up' | 'down' | 'none', duration?: number) => void;
    /**
     * Pointer types to listen to.
     *
     * @default ['mouse', 'touch', 'pen']
     */
    pointerTypes?: any[];
}
export declare function useSwipe(target: any, options: UseSwipeOptions): {
    isSwiping: import("vue").Ref<boolean>;
    direction: import("vue").ComputedRef<"none" | "left" | "right" | "up" | "down">;
    posStart: {
        x: number;
        y: number;
    };
    posEnd: {
        x: number;
        y: number;
    };
    distanceX: import("vue").ComputedRef<number>;
    distanceY: import("vue").ComputedRef<number>;
    duration: import("vue").Ref<number>;
    stop: () => void;
};
