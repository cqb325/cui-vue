import type { Slots, VNode } from 'vue';
interface UseChildrenOptions<T> {
    slots: Slots;
    props: Record<string, any>;
    sameType?: T;
    transform?: (props: Record<string, any>, index: number) => Record<string, any>;
}
declare function useChildren<T extends abstract new (...args: any) => any>({ slots, props, sameType, transform }: UseChildrenOptions<T>): VNode<import("vue").RendererNode, import("vue").RendererElement, {
    [key: string]: any;
}>[];
export default useChildren;
