export default function usePortal(id: string, className: string): HTMLElement;
