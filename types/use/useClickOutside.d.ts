export default function useClickOutside(ref?: any, callback?: Function, onMouseDown?: Function): () => void;
