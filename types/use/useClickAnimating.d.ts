import { Ref } from "vue";
/**
 * 点击触发animating
 * @returns
 */
export declare function useClickAnimating(): [Ref<boolean>, () => void];
