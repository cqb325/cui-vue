import { PropType } from "vue";
export interface BannerProps {
    type: 'warning' | 'info' | 'success' | 'error';
    bordered?: boolean;
    icon?: any;
    closeIcon?: any;
    title?: any;
    fullMode?: boolean;
    modelValue?: boolean;
}
declare const _default: import("vue").DefineComponent<{
    type: {
        type: PropType<"error" | "success" | "warning" | "info">;
    };
    bordered: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
    closeIcon: {
        type: PropType<any>;
    };
    title: {
        type: PropType<any>;
    };
    fullMode: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
        default: boolean;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: PropType<"error" | "success" | "warning" | "info">;
    };
    bordered: {
        type: PropType<boolean>;
    };
    icon: {
        type: PropType<any>;
    };
    closeIcon: {
        type: PropType<any>;
    };
    title: {
        type: PropType<any>;
    };
    fullMode: {
        type: PropType<boolean>;
    };
    modelValue: {
        type: PropType<boolean>;
        default: boolean;
    };
}>>, {
    modelValue: boolean;
}, {}>;
export default _default;
