import { PropType } from "vue";
export interface TableStyleLayoutLabelProps {
    verticalAlign?: 'start' | 'center' | 'end';
    width?: number;
    required?: boolean;
}
export declare const TableStyleLayoutLabel: import("vue").DefineComponent<{
    verticalAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    width: {
        type: PropType<number>;
    };
    required: {
        type: PropType<boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    verticalAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    width: {
        type: PropType<number>;
    };
    required: {
        type: PropType<boolean>;
    };
}>>, {}, {}>;
