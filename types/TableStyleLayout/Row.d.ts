export declare const TableStyleLayoutRow: import("vue").DefineComponent<{
    class: StringConstructor;
    classList: FunctionConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    class: StringConstructor;
    classList: FunctionConstructor;
}>>, {}, {}>;
