import { PropType } from "vue";
export interface TableStyleLayoutValueProps {
    verticalAlign?: 'start' | 'center' | 'end';
    column?: boolean;
    row?: boolean;
    justify?: 'flex-start' | 'flex-end' | 'center';
}
export declare const TableStyleLayoutValue: import("vue").DefineComponent<{
    verticalAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    column: {
        type: PropType<boolean>;
    };
    row: {
        type: PropType<boolean>;
    };
    justify: {
        type: PropType<"center" | "flex-end" | "flex-start">;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    verticalAlign: {
        type: PropType<"center" | "end" | "start">;
    };
    column: {
        type: PropType<boolean>;
    };
    row: {
        type: PropType<boolean>;
    };
    justify: {
        type: PropType<"center" | "flex-end" | "flex-start">;
    };
}>>, {}, {}>;
