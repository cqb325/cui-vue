import { PropType } from "vue";
export interface TableStyleLayoutColProps {
    flex?: number;
}
export declare const TableStyleLayoutCol: import("vue").DefineComponent<{
    flex: {
        type: PropType<number>;
        default: any;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    flex: {
        type: PropType<number>;
        default: any;
    };
}>>, {
    flex: number;
}, {}>;
