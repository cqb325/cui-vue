import { PropType } from 'vue';
export * from './Row';
export * from './Col';
export * from './Label';
export * from './Value';
export interface TableStyleLayoutProps {
    labelWidth?: number;
}
export interface TableStyleLayoutContextProps {
    labelWidth: number;
}
export declare const TableStyleLayoutContextKey: unique symbol;
declare const _default: import("vue").DefineComponent<{
    labelWidth: {
        type: PropType<number>;
        default: number;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    labelWidth: {
        type: PropType<number>;
        default: number;
    };
}>>, {
    labelWidth: number;
}, {}>;
export default _default;
