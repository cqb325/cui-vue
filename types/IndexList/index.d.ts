import { PropType } from "vue";
export interface IndexListProps {
    data: any[];
    selectable?: boolean;
    promote?: boolean;
    border?: boolean;
    modelValue?: any[];
    renderItem?(item: any, active: boolean): any;
}
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<any[]>;
    };
    selectable: {
        type: BooleanConstructor;
        default: boolean;
    };
    promote: {
        type: BooleanConstructor;
        default: boolean;
    };
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    modelValue: {
        type: PropType<any[]>;
        default: () => any[];
    };
    renderItem: {
        type: PropType<(item: any, active: boolean) => any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "change"[], "change", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<any[]>;
    };
    selectable: {
        type: BooleanConstructor;
        default: boolean;
    };
    promote: {
        type: BooleanConstructor;
        default: boolean;
    };
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    modelValue: {
        type: PropType<any[]>;
        default: () => any[];
    };
    renderItem: {
        type: PropType<(item: any, active: boolean) => any>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
}, {
    border: boolean;
    modelValue: any[];
    selectable: boolean;
    promote: boolean;
}, {}>;
export default _default;
