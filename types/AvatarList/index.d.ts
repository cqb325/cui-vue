import { HTMLAttributes, PropType } from "vue";
export interface AvatarListProps extends HTMLAttributes {
    size?: 'small' | 'large';
    align?: 'top' | 'bottom' | 'left' | 'right' | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight' | 'leftTop' | 'leftBottom' | 'rightTop' | 'rightBottom';
    gutter?: number;
    max?: number;
    excessStyle?: any;
}
declare const _default: import("vue").DefineComponent<{
    size: {
        type: PropType<"small" | "large">;
    };
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    gutter: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
        default: number;
    };
    excessStyle: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    size: {
        type: PropType<"small" | "large">;
    };
    align: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    gutter: {
        type: PropType<number>;
    };
    max: {
        type: PropType<number>;
        default: number;
    };
    excessStyle: {
        type: PropType<any>;
    };
}>>, {
    max: number;
    align: "left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom";
}, {}>;
export default _default;
