declare const _default: import("vue").DefineComponent<{
    data: {
        type: ObjectConstructor;
    };
    index: {
        type: NumberConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: ObjectConstructor;
    };
    index: {
        type: NumberConstructor;
    };
}>>, {}, {}>;
export default _default;
