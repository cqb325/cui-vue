import { PropType } from "vue";
export interface CarouselProps {
    height?: number;
    arrow?: boolean;
    autoPlay?: boolean;
    duration?: number;
    effect?: 'fade' | 'slide' | 'card';
    dotType?: 'dot' | 'line' | 'columnar';
    dotAlign?: 'left' | 'right' | 'top' | 'bottom';
    dotColor?: string;
    dotActiveColor?: string;
    activeIndex?: number;
    itemsPerView?: number | 'auto';
    gutter?: number;
    draggable?: boolean;
    dir?: 'h' | 'v';
    onChange?: (v: any) => void;
}
export declare const CarouselContextKey: unique symbol;
declare const Carousel: import("vue").DefineComponent<{
    height: {
        type: PropType<number>;
    };
    arrow: {
        type: PropType<boolean>;
        default: boolean;
    };
    autoPlay: {
        type: PropType<boolean>;
        default: boolean;
    };
    duration: {
        type: PropType<number>;
        default: number;
    };
    effect: {
        type: PropType<"card" | "fade" | "slide">;
        default: string;
    };
    dotType: {
        type: PropType<"line" | "dot" | "columnar">;
        default: string;
    };
    dotAlign: {
        type: PropType<"left" | "top" | "bottom" | "right">;
    };
    dotColor: {
        type: PropType<string>;
        default: string;
    };
    dotActiveColor: {
        type: PropType<string>;
        default: string;
    };
    activeIndex: {
        type: PropType<number>;
    };
    itemsPerView: {
        type: PropType<number | "auto">;
        default: number;
    };
    gutter: {
        type: PropType<number>;
        default: number;
    };
    draggable: {
        type: PropType<boolean>;
    };
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:activeIndex")[], "change" | "update:activeIndex", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    height: {
        type: PropType<number>;
    };
    arrow: {
        type: PropType<boolean>;
        default: boolean;
    };
    autoPlay: {
        type: PropType<boolean>;
        default: boolean;
    };
    duration: {
        type: PropType<number>;
        default: number;
    };
    effect: {
        type: PropType<"card" | "fade" | "slide">;
        default: string;
    };
    dotType: {
        type: PropType<"line" | "dot" | "columnar">;
        default: string;
    };
    dotAlign: {
        type: PropType<"left" | "top" | "bottom" | "right">;
    };
    dotColor: {
        type: PropType<string>;
        default: string;
    };
    dotActiveColor: {
        type: PropType<string>;
        default: string;
    };
    activeIndex: {
        type: PropType<number>;
    };
    itemsPerView: {
        type: PropType<number | "auto">;
        default: number;
    };
    gutter: {
        type: PropType<number>;
        default: number;
    };
    draggable: {
        type: PropType<boolean>;
    };
    dir: {
        type: PropType<"v" | "h">;
        default: string;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:activeIndex"?: (...args: any[]) => any;
}, {
    dir: "v" | "h";
    duration: number;
    arrow: boolean;
    gutter: number;
    autoPlay: boolean;
    effect: "card" | "fade" | "slide";
    dotType: "line" | "dot" | "columnar";
    dotColor: string;
    dotActiveColor: string;
    itemsPerView: number | "auto";
}, {}>;
export default Carousel;
