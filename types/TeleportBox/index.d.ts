import { PropType } from "vue";
export interface TeleportBoxItem {
    value: any;
    label: any;
    disabled?: boolean;
    checked?: boolean;
    children?: TeleportBoxItem[];
    [key: string]: any;
}
export interface TeleportBoxProps {
    data?: TeleportBoxItem[];
    modelValue?: any[];
    defaultValue?: any[];
    disabled?: boolean;
    virtual?: boolean;
    onChange?: (value: any[]) => void;
    renderSourceItem?: (item: TeleportBoxItem, onChange: (checked: boolean) => void) => JSX.Element;
    renderSelectedItem?: (item: TeleportBoxItem, onRemove: () => void) => JSX.Element;
    filter?: (item: TeleportBoxItem, keyword: string) => boolean;
}
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<TeleportBoxItem[]>;
        default: () => any[];
    };
    modelValue: {
        type: PropType<any[]>;
        default: () => any[];
    };
    defaultValue: {
        type: PropType<any[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    virtual: {
        type: PropType<boolean>;
    };
    renderSourceItem: {
        type: PropType<(item: TeleportBoxItem, onChange: (checked: boolean) => void) => JSX.Element>;
    };
    renderSelectedItem: {
        type: PropType<(item: TeleportBoxItem, onRemove: () => void) => JSX.Element>;
    };
    filter: {
        type: PropType<(item: TeleportBoxItem, keyword: string) => boolean>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "update:modelValue")[], "change" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<TeleportBoxItem[]>;
        default: () => any[];
    };
    modelValue: {
        type: PropType<any[]>;
        default: () => any[];
    };
    defaultValue: {
        type: PropType<any[]>;
    };
    disabled: {
        type: PropType<boolean>;
    };
    virtual: {
        type: PropType<boolean>;
    };
    renderSourceItem: {
        type: PropType<(item: TeleportBoxItem, onChange: (checked: boolean) => void) => JSX.Element>;
    };
    renderSelectedItem: {
        type: PropType<(item: TeleportBoxItem, onRemove: () => void) => JSX.Element>;
    };
    filter: {
        type: PropType<(item: TeleportBoxItem, keyword: string) => boolean>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    data: TeleportBoxItem[];
    modelValue: any[];
}, {}>;
export default _default;
