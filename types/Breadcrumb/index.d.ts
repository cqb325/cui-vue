import { HTMLAttributes, PropType } from "vue";
export declare const BreadcrumbContext: unique symbol;
export interface BreadcrumbProps extends HTMLAttributes {
    separator?: string;
}
declare const Breadcrumb: import("vue").DefineComponent<{
    separator: {
        type: PropType<string>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    separator: {
        type: PropType<string>;
    };
}>>, {}, {}>;
export default Breadcrumb;
