import { PropType, VueElement } from "vue";
export interface BreadcrumbItemProps {
    link?: string;
    icon?: any;
    separator?: string | VueElement;
}
declare const _default: import("vue").DefineComponent<{
    link: {
        type: PropType<string>;
    };
    icon: {
        type: PropType<any>;
    };
    separator: {
        type: PropType<string | VueElement>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    link: {
        type: PropType<string>;
    };
    icon: {
        type: PropType<any>;
    };
    separator: {
        type: PropType<string | VueElement>;
    };
}>>, {}, {}>;
export default _default;
