import type { InnerPopupProps } from "../inner/InnerPopup";
export interface TooltipProps extends Omit<InnerPopupProps, 'title'> {
}
declare const _default: import("vue").DefineComponent<{
    align: {
        type: import("vue").PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
    };
    trigger: {
        type: import("vue").PropType<"click" | "focus" | "hover">;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
    };
    arrow: {
        type: import("vue").PropType<boolean>;
        default: any;
    };
    hideDelay: {
        type: import("vue").PropType<number>;
    };
    content: {
        type: import("vue").PropType<string | number | JSX.Element>;
    };
    contentStyle: {
        type: import("vue").PropType<string | import("vue").CSSProperties>;
    };
    modelValue: {
        type: import("vue").PropType<boolean>;
    };
    theme: {
        type: import("vue").PropType<string>;
    };
    confirm: {
        type: import("vue").PropType<boolean>;
        default: any;
    };
    okText: {
        type: import("vue").PropType<any>;
    };
    okType: {
        type: import("vue").PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    title: {
        type: import("vue").PropType<string | number | JSX.Element>;
    };
    cancelText: {
        type: import("vue").PropType<any>;
    };
    cancelType: {
        type: import("vue").PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    offset: {
        type: import("vue").PropType<number>;
    };
    clsPrefix: {
        type: import("vue").PropType<string>;
    };
    varName: {
        type: import("vue").PropType<string>;
    };
    arrowPointAtCenter: {
        type: import("vue").PropType<boolean>;
    };
    showCancel: {
        type: import("vue").PropType<boolean>;
    };
    onOk: {
        type: import("vue").PropType<() => void | Promise<boolean | void>>;
    };
    onCancel: {
        type: import("vue").PropType<() => void>;
    };
    icon: {
        type: import("vue").PropType<import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("update:modelValue" | "visibleChange")[], "update:modelValue" | "visibleChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    align: {
        type: import("vue").PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
    };
    trigger: {
        type: import("vue").PropType<"click" | "focus" | "hover">;
    };
    disabled: {
        type: import("vue").PropType<boolean>;
    };
    arrow: {
        type: import("vue").PropType<boolean>;
        default: any;
    };
    hideDelay: {
        type: import("vue").PropType<number>;
    };
    content: {
        type: import("vue").PropType<string | number | JSX.Element>;
    };
    contentStyle: {
        type: import("vue").PropType<string | import("vue").CSSProperties>;
    };
    modelValue: {
        type: import("vue").PropType<boolean>;
    };
    theme: {
        type: import("vue").PropType<string>;
    };
    confirm: {
        type: import("vue").PropType<boolean>;
        default: any;
    };
    okText: {
        type: import("vue").PropType<any>;
    };
    okType: {
        type: import("vue").PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    title: {
        type: import("vue").PropType<string | number | JSX.Element>;
    };
    cancelText: {
        type: import("vue").PropType<any>;
    };
    cancelType: {
        type: import("vue").PropType<"error" | "default" | "success" | "primary" | "secondary" | "tertiary" | "danger" | "warning">;
    };
    offset: {
        type: import("vue").PropType<number>;
    };
    clsPrefix: {
        type: import("vue").PropType<string>;
    };
    varName: {
        type: import("vue").PropType<string>;
    };
    arrowPointAtCenter: {
        type: import("vue").PropType<boolean>;
    };
    showCancel: {
        type: import("vue").PropType<boolean>;
    };
    onOk: {
        type: import("vue").PropType<() => void | Promise<boolean | void>>;
    };
    onCancel: {
        type: import("vue").PropType<() => void>;
    };
    icon: {
        type: import("vue").PropType<import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}>> & {
    onVisibleChange?: (...args: any[]) => any;
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    confirm: boolean;
    arrow: boolean;
}, {}>;
export default _default;
