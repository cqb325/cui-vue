import { PropType, Ref } from "vue";
import { InnerPopupProps } from "../inner/InnerPopup";
export type FormContextOptions = {
    labelWidth?: Ref<number>;
    inline?: boolean;
    model: any;
    onChange: (name: string, v: any) => void;
    setCheck: (name: any, checkFn: any) => void;
    setClearValid: (name: any, fn: any) => void;
    validation: any;
    errorTransfer?: boolean;
    errorAlign?: InnerPopupProps['align'];
    rules?: Ref<{
        [key: string]: any;
    }>;
};
export interface FormProps {
    labelWidth?: number;
    model?: any;
    inline?: boolean;
    onBeforeSubmit?: () => boolean | Promise<boolean>;
    autocomplete?: string;
    validation?: any;
    errorTransfer?: boolean;
    errorAlign?: InnerPopupProps['align'];
    rules?: {
        [key: string]: any;
    };
}
export declare const FormContextKey: unique symbol;
declare const _default: import("vue").DefineComponent<{
    labelWidth: {
        type: NumberConstructor;
    };
    inline: {
        type: BooleanConstructor;
    };
    model: {
        type: ObjectConstructor;
    };
    autocomplete: {
        type: StringConstructor;
    };
    validation: {
        type: ObjectConstructor;
    };
    errorTransfer: {
        type: BooleanConstructor;
        default: boolean;
    };
    errorAlign: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    onBeforeSubmit: {
        type: PropType<() => boolean | Promise<boolean>>;
    };
    rules: {
        type: PropType<{
            [key: string]: any;
        }>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("change" | "beforeSubmit")[], "change" | "beforeSubmit", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    labelWidth: {
        type: NumberConstructor;
    };
    inline: {
        type: BooleanConstructor;
    };
    model: {
        type: ObjectConstructor;
    };
    autocomplete: {
        type: StringConstructor;
    };
    validation: {
        type: ObjectConstructor;
    };
    errorTransfer: {
        type: BooleanConstructor;
        default: boolean;
    };
    errorAlign: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    onBeforeSubmit: {
        type: PropType<() => boolean | Promise<boolean>>;
    };
    rules: {
        type: PropType<{
            [key: string]: any;
        }>;
    };
}>> & {
    onChange?: (...args: any[]) => any;
    onBeforeSubmit?: (...args: any[]) => any;
}, {
    inline: boolean;
    errorAlign: "left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom";
    errorTransfer: boolean;
}, {}>;
export default _default;
