export declare function findComponentUpward(wantFind: any, _context: any): any;
/**
 * 划到顶部
 * @param el
 * @param from
 * @param to
 * @param duration
 * @param endCallback
 */
export declare function scrollTop(el: any, from: number, to: number, duration?: number, endCallback?: Function): void;
/**
 * 获取安全的随机数
 */
export declare function getRandomIntInclusive(min: number, max: number): number;
export declare function getRandomNumber(): number;
export declare const uuidv4: () => string;
/**
 * 字符串是否为颜色值
 * @param strColor
 * @returns
 */
export declare function isColor(strColor: string | undefined): boolean;
