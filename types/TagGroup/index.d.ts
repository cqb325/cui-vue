import { PopoverProps } from "../Popover";
import { PropType } from "vue";
export interface TagConfig {
    id: string | number;
    title: string;
    theme?: 'primary' | 'danger' | 'warning' | 'success' | 'info' | 'magenta' | 'red' | 'volcano' | 'orange' | 'gold' | 'yellow' | 'lime' | 'green' | 'cyan' | 'blue' | 'geekblue' | 'purple';
    avatar?: any;
    [key: string]: any;
}
export interface TagGroupProps {
    data: TagConfig[];
    closable?: boolean;
    max?: number | 'auto';
    showMore?: boolean;
    moreCloseable?: boolean;
    tooltipAlign?: PopoverProps['align'];
    tooltipTheme?: PopoverProps['color'];
    tooltipTrigger?: PopoverProps['trigger'];
    tooltipStyle?: PopoverProps['style'];
    size?: 'small' | 'large' | 'xlarge';
    extra?: any;
    onClose?(item: TagConfig, e: any): void;
}
declare const _default: import("vue").DefineComponent<{
    data: {
        type: PropType<TagConfig[]>;
        default: any[];
    };
    closable: {
        type: PropType<boolean>;
        default: boolean;
    };
    max: {
        type: PropType<number | "auto">;
    };
    showMore: {
        type: PropType<boolean>;
        default: boolean;
    };
    moreCloseable: {
        type: PropType<boolean>;
    };
    tooltipAlign: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    tooltipTheme: {
        type: PropType<string>;
        default: string;
    };
    tooltipTrigger: {
        type: PropType<"click" | "focus" | "hover">;
        default: string;
    };
    tooltipStyle: {
        type: PropType<import("vue").StyleValue>;
    };
    size: {
        type: PropType<"small" | "large" | "xlarge">;
        default: string;
    };
    extra: {
        type: PropType<any>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "close"[], "close", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: PropType<TagConfig[]>;
        default: any[];
    };
    closable: {
        type: PropType<boolean>;
        default: boolean;
    };
    max: {
        type: PropType<number | "auto">;
    };
    showMore: {
        type: PropType<boolean>;
        default: boolean;
    };
    moreCloseable: {
        type: PropType<boolean>;
    };
    tooltipAlign: {
        type: PropType<"left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom">;
        default: string;
    };
    tooltipTheme: {
        type: PropType<string>;
        default: string;
    };
    tooltipTrigger: {
        type: PropType<"click" | "focus" | "hover">;
        default: string;
    };
    tooltipStyle: {
        type: PropType<import("vue").StyleValue>;
    };
    size: {
        type: PropType<"small" | "large" | "xlarge">;
        default: string;
    };
    extra: {
        type: PropType<any>;
    };
}>> & {
    onClose?: (...args: any[]) => any;
}, {
    data: TagConfig[];
    size: "small" | "large" | "xlarge";
    closable: boolean;
    showMore: boolean;
    tooltipAlign: "left" | "top" | "bottom" | "right" | "bottomLeft" | "bottomRight" | "topLeft" | "topRight" | "rightTop" | "rightBottom" | "leftTop" | "leftBottom";
    tooltipTheme: string;
    tooltipTrigger: "click" | "focus" | "hover";
}, {}>;
export default _default;
