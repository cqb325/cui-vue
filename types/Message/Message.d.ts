import { PropType } from "vue";
declare const Message: import("vue").DefineComponent<{
    data: {
        type: ObjectConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "close"[], "close", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: ObjectConstructor;
    };
}>> & {
    onClose?: (...args: any[]) => any;
}, {}, {}>;
export default Message;
export declare const Messages: import("vue").DefineComponent<{
    data: {
        type: ArrayConstructor;
        default: () => any[];
    };
    onClose: {
        type: PropType<(...args: any[]) => any>;
    };
}, () => JSX.Element[], unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    data: {
        type: ArrayConstructor;
        default: () => any[];
    };
    onClose: {
        type: PropType<(...args: any[]) => any>;
    };
}>>, {
    data: unknown[];
}, {}>;
