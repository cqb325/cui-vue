import { PropType, VNode } from "vue";
declare const _default: import("vue").DefineComponent<{
    type: {
        type: PropType<"fail" | "empty" | "404" | "403" | "500" | "deny">;
        required: true;
    };
    typeImage: {
        type: PropType<any>;
    };
    desc: {
        type: PropType<string>;
    };
    showDesc: {
        type: PropType<boolean>;
        default: boolean;
    };
    width: {
        type: PropType<number>;
    };
    height: {
        type: PropType<number>;
    };
    action: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: PropType<"fail" | "empty" | "404" | "403" | "500" | "deny">;
        required: true;
    };
    typeImage: {
        type: PropType<any>;
    };
    desc: {
        type: PropType<string>;
    };
    showDesc: {
        type: PropType<boolean>;
        default: boolean;
    };
    width: {
        type: PropType<number>;
    };
    height: {
        type: PropType<number>;
    };
    action: {
        type: PropType<VNode<import("vue").RendererNode, import("vue").RendererElement, {
            [key: string]: any;
        }>>;
    };
}>>, {
    showDesc: boolean;
}, {}>;
export default _default;
