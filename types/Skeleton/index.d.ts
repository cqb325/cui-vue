import { PropType } from "vue";
export interface SkeletonProps {
    active?: boolean;
    loading?: boolean;
    placeholder?: any;
    width?: string;
    height?: string;
    style?: any;
}
declare const Skeleton: import("vue").DefineComponent<{
    active: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<any>;
    };
    width: {
        type: PropType<string>;
    };
    height: {
        type: PropType<string>;
    };
    style: {
        type: PropType<any>;
    };
}, () => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
    [key: string]: any;
}>[] | JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    active: {
        type: PropType<boolean>;
    };
    loading: {
        type: PropType<boolean>;
    };
    placeholder: {
        type: PropType<any>;
    };
    width: {
        type: PropType<string>;
    };
    height: {
        type: PropType<string>;
    };
    style: {
        type: PropType<any>;
    };
}>>, {}, {}>;
export default Skeleton;
