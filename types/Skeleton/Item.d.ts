import { PropType } from "vue";
export type BasicProps = {
    type?: string;
    width?: string | string[];
    height?: string;
    inline?: boolean;
};
export type AvatarProps = {
    size?: 'extra-small' | 'small' | 'medium' | 'large' | 'extra-large' | number;
    shape?: 'circle' | 'square';
};
export type GenericProps = BasicProps & AvatarProps;
export declare const Avatar: (props: GenericProps) => JSX.Element;
export declare const Image: (props: GenericProps) => JSX.Element;
export declare const Title: (props: GenericProps) => JSX.Element;
export declare const Button: (props: GenericProps) => JSX.Element;
export declare const Item: (props: GenericProps) => JSX.Element;
export interface ParagraphProps extends BasicProps {
    rows?: number;
}
export declare const Paragraph: import("vue").DefineComponent<{
    type: {
        type: StringConstructor;
    };
    width: {
        type: PropType<string | string[]>;
    };
    height: {
        type: StringConstructor;
    };
    inline: {
        type: BooleanConstructor;
    };
    size: {
        type: PropType<number | "small" | "medium" | "large" | "extra-small" | "extra-large">;
    };
    shape: {
        type: PropType<"square" | "circle">;
    };
    rows: {
        type: NumberConstructor;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: StringConstructor;
    };
    width: {
        type: PropType<string | string[]>;
    };
    height: {
        type: StringConstructor;
    };
    inline: {
        type: BooleanConstructor;
    };
    size: {
        type: PropType<number | "small" | "medium" | "large" | "extra-small" | "extra-large">;
    };
    shape: {
        type: PropType<"square" | "circle">;
    };
    rows: {
        type: NumberConstructor;
    };
}>>, {
    inline: boolean;
}, {}>;
