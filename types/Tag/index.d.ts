import { HTMLAttributes } from 'vue';
export interface TagProps extends HTMLAttributes {
    theme?: 'primary' | 'danger' | 'warning' | 'success' | 'info' | 'magenta' | 'red' | 'volcano' | 'orange' | 'gold' | 'yellow' | 'lime' | 'green' | 'cyan' | 'blue' | 'geekblue' | 'purple';
    value?: any;
    circle?: boolean;
    size?: 'small' | 'large' | 'xlarge';
    avatar?: any;
    onBeforeClose?: (e: any) => boolean;
    closable?: boolean;
    modelValue?: boolean;
    border?: boolean;
}
declare const _default: import("vue").DefineComponent<Readonly<{
    border?: any;
    value?: any;
    circle?: any;
    size?: any;
    theme?: any;
    modelValue?: any;
    onClose?: any;
    avatar?: any;
    onBeforeClose?: any;
    closable?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("close" | "update:modelValue")[], "close" | "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    border?: any;
    value?: any;
    circle?: any;
    size?: any;
    theme?: any;
    modelValue?: any;
    onClose?: any;
    avatar?: any;
    onBeforeClose?: any;
    closable?: any;
}>>> & {
    "onUpdate:modelValue"?: (...args: any[]) => any;
    onClose?: (...args: any[]) => any;
}, {
    readonly border?: any;
    readonly value?: any;
    readonly circle?: any;
    readonly size?: any;
    readonly theme?: any;
    readonly modelValue?: any;
    readonly onClose?: any;
    readonly avatar?: any;
    readonly onBeforeClose?: any;
    readonly closable?: any;
}, {}>;
export default _default;
