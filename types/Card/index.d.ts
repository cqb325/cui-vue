import { HTMLAttributes } from "vue";
export interface CardProps extends HTMLAttributes {
    bordered?: boolean;
    rised?: boolean;
    title?: any;
    bodyStyle?: any;
    footer?: any;
}
declare const _default: import("vue").DefineComponent<Readonly<{
    footer?: any;
    title?: any;
    bordered?: any;
    rised?: any;
    bodyStyle?: any;
}>, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    footer?: any;
    title?: any;
    bordered?: any;
    rised?: any;
    bodyStyle?: any;
}>>>, {
    readonly footer?: any;
    readonly title?: any;
    readonly bordered?: any;
    readonly rised?: any;
    readonly bodyStyle?: any;
}, {}>;
export default _default;
