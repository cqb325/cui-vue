import { defineComponent, ref, computed, watch, watchEffect, createVNode } from 'vue';
import useTransition from '../use/useTransition.js';
import { FeatherX } from 'cui-vue-icons/feather';

const index = /* @__PURE__ */ defineComponent({
  name: 'Drawer',
  props: {
    align: {
      type: String,
      default: 'right'
    },
    size: {
      type: Number,
      default: 256
    },
    title: {
      type: [String, Object]
    },
    maskCloseable: {
      type: Boolean,
      default: true
    },
    hasClose: {
      type: Boolean,
      default: true
    },
    escClose: {
      type: Boolean
    },
    modelValue: {
      type: Boolean,
      default: false
    },
    destroyOnClose: {
      type: Boolean
    }
  },
  emits: ['close', 'show', 'update:modelValue'],
  setup(props, {
    emit,
    slots
  }) {
    const visible = ref(props.modelValue); // 控制抽屉显示/隐藏
    const destroyed = ref(props.destroyOnClose && !visible.value); // 是否已销毁内容
    const boxRef = ref(null); // 抽屉容器引用
    const targetRef = ref(null); // 抽屉内容引用
    let originBodyOverflow; // 保存原始的 body overflow 值

    // 计算抽屉样式
    const drawerStyle = computed(() => {
      const sizeValue = `${props.size}px`;
      return {
        [props.align === 'left' || props.align === 'right' ? 'width' : 'height']: sizeValue
      };
    });

    // 计算类名
    const classList = computed(() => ({
      'cm-drawer': true,
      [`cm-drawer-${props.align}`]: true
    }));

    // 初始化过渡效果
    const transition = useTransition({
      el: () => boxRef.value,
      target: () => targetRef.value,
      startClass: 'cm-drawer-visible',
      activeClass: 'cm-drawer-open',
      onLeave: () => {
        emit('close');
        document.body.style.overflow = originBodyOverflow;
        if (props.destroyOnClose) {
          destroyed.value = true;
        }
      },
      onEnter: () => {
        originBodyOverflow = document.body.style.overflow;
        document.body.style.overflow = 'hidden';
      }
    });
    watch(() => props.modelValue, val => {
      visible.value = val;
    });

    // 监听 visible 变化
    watchEffect(() => {
      const v = visible.value;
      if (v) {
        destroyed.value = false;
        transition.enter();
        emit('show');
      } else {
        transition.leave();
      }
    });

    // 点击遮罩关闭
    const onMaskClick = () => {
      if (props.maskCloseable) {
        onClose();
      }
    };

    // 关闭抽屉
    const onClose = () => {
      visible.value = false;
      emit('update:modelValue', false);
    };

    // 监听 ESC 按键
    const onKeyUp = e => {
      if (props.escClose && e.code === 'Escape') {
        onClose();
      }
    };
    return () => createVNode("div", {
      "class": classList.value,
      "ref": boxRef,
      "tab-index": 1,
      "onKeyup": onKeyUp
    }, [createVNode("div", {
      "class": "cm-drawer-mask",
      "onClick": onMaskClick
    }, null), createVNode("div", {
      "class": "cm-drawer-wrap",
      "style": drawerStyle.value,
      "ref": targetRef
    }, [props.title && createVNode("div", {
      "class": "cm-drawer-title"
    }, [props.title]), props.hasClose && createVNode(FeatherX, {
      "class": "cm-drawer-close",
      "onClick": onClose
    }, null), createVNode("div", {
      "class": "cm-drawer-body"
    }, [!destroyed.value && slots.default?.()])])]);
  }
});

export { index as default };
