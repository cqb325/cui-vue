import { defineComponent, inject, ref, watch, watchEffect, createVNode, provide } from 'vue';
import MenuItem from './MenuItem.js';
import Dropdown from '../Dropdown/index.js';
import { MenuContext, MenuTreeContext } from './index.js';

const SubMenu = /* @__PURE__ */ defineComponent({
  name: 'SubMenu',
  props: {
    name: {
      type: String
    },
    align: {
      type: String
    },
    title: {
      type: String
    },
    padding: {
      type: Number
    },
    parent: {
      type: Object
    },
    icon: {
      type: Object
    }
  },
  setup(props, {
    slots,
    expose
  }) {
    if (!props.name) {
      console.warn("SubMenu need name prop");
    }
    const ctx = inject(MenuContext, {});
    const treeCtx = inject(MenuTreeContext, {});
    const isOpened = ref(false);
    const parentPadding = treeCtx.parent ? treeCtx.parent.padding : 0;
    const padding = ref(parentPadding + 16);
    const listEle = ref();
    const classList = ref({});
    watch(() => isOpened.value, isOpen => {
      if (!listEle.value) {
        return;
      }
      listEle.value.style.transition = 'none';
      listEle.value.style.height = 'auto';
      const oh = listEle.value.offsetHeight;
      listEle.value.style.transition = '';
      if (isOpen) {
        listEle.value.style.height = '0px';
        setTimeout(() => {
          listEle.value.style.height = oh + 'px';
        });
        setTimeout(() => {
          listEle.value.style.height = 'auto';
        }, 250);
      } else {
        listEle.value.style.height = oh + 'px';
        setTimeout(() => {
          listEle.value.style.height = '0px';
        });
      }
    });
    watchEffect(() => {
      const openKeys = ctx.openKeys.value;
      isOpened.value = openKeys[props.name];
      classList.value = {
        'cm-menu-submenu': true,
        'cm-menu-submenu-open': isOpened.value
      };
    });
    const onSelect = () => {
      ctx?.setOpen(props.name);
    };
    const align = props.align || (ctx?.dir === 'h' ? 'bottom' : 'rightTop');
    const createNode = () => {
      const item = {
        name: props.name,
        parent: null,
        children: [],
        padding: padding.value
      };
      if (ctx && props.name) {
        ctx.treeMap[props.name] = item;
        if (!treeCtx.parentName) {
          ctx?.tree.push(item);
        } else {
          const parent = ctx.treeMap[treeCtx.parentName];
          if (parent) {
            item.parent = parent;
            parent.children.push(item);
          }
        }
      }
      provide(MenuTreeContext, {
        parentName: props.name,
        parent: ctx.treeMap[props.name]
      });
    };
    createNode();
    watchEffect(() => {
      ctx.treeMap[props.name].padding = ctx.store.min || ctx?.dir === 'h' ? 0 : padding.value;
    });
    return () => ctx.store.min || ctx?.dir === 'h' ? createVNode("li", {
      "class": classList.value
    }, [createVNode(Dropdown, {
      "transfer": false,
      "align": align,
      "theme": ctx?.theme,
      "revers": false,
      "menu": createVNode("ul", {
        "class": "cm-menu-submenu-list",
        "ref": listEle,
        "x-name": props.name
      }, [slots.default?.()])
    }, {
      default: () => [createVNode(MenuItem, {
        "icon": props.icon,
        "cert": true,
        "isSubmenuTitle": true,
        "onSelect": onSelect
      }, {
        default: () => [props.title]
      })]
    })]) : createVNode("li", {
      "class": classList.value
    }, [createVNode(MenuItem, {
      "cert": true,
      "icon": props.icon,
      "isSubmenuTitle": true,
      "onSelect": onSelect
    }, {
      default: () => [props.title]
    }), createVNode("ul", {
      "class": "cm-menu-submenu-list",
      "ref": listEle,
      "x-name": props.name
    }, [slots.default?.()])]);
  }
});

export { SubMenu as default };
