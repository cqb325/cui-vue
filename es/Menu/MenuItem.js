import { defineComponent, inject, ref, computed, watch, createVNode } from 'vue';
import Popover from '../Popover/index.js';
import { FeatherChevronDown } from 'cui-vue-icons/feather';
import { MenuContext, MenuTreeContext } from './index.js';

const MenuItem = /* @__PURE__ */ defineComponent({
  name: 'MenuItem',
  props: {
    name: {
      type: String
    },
    disabled: {
      type: Boolean
    },
    isSubmenuTitle: {
      type: Boolean
    },
    data: {
      type: Object
    },
    cert: {
      type: Boolean
    },
    icon: {
      type: Object
    }
  },
  emits: ['select'],
  setup(props, {
    slots,
    emit
  }) {
    if (!props.isSubmenuTitle && !props.name) {
      console.warn("MenuItem need name prop");
    }
    const ctx = inject(MenuContext, {});
    const treeCtx = inject(MenuTreeContext, {});
    const parentPadding = treeCtx.parent?.padding ?? 0;
    const padding = ref(ctx?.dir === 'h' ? 16 : props.isSubmenuTitle ? parentPadding : parentPadding + 16);
    const isRootItem = ref(false);
    const classList = computed(() => ({
      'cm-menu-item': true,
      'cm-menu-item-disabled': props.disabled,
      'cm-menu-item-active': !props.isSubmenuTitle && props.name && ctx?.store.activeName === props.name
    }));
    watch(() => ctx.store.min, () => {
      isRootItem.value = ctx.store.min && !props.isSubmenuTitle && !treeCtx.parent;
      padding.value = props.isSubmenuTitle ? treeCtx.parent?.padding : treeCtx.parent?.padding + 16;
      // 根item元素
      if (!props.isSubmenuTitle) {
        if (!treeCtx.parent) {
          padding.value = 16;
        }
      } else {
        // submenu 或则 menugroup
        if (!treeCtx.parent.parent) {
          padding.value = 16;
        }
        if (ctx.dir === 'h') {
          padding.value = 16;
        }
      }
    }, {
      immediate: true
    });
    const onSelect = () => {
      if (props.isSubmenuTitle && !ctx.store.min) {
        emit('select');
      } else {
        ctx?.onSelect(props.name, props.data);
      }
    };
    const createNode = () => {
      const item = {
        name: props.name,
        parent: null,
        children: []
      };
      if (ctx && props.name) {
        ctx.treeMap[props.name] = item;
        if (!treeCtx.parentName) {
          ctx?.tree.push(item);
        } else {
          const parent = ctx.treeMap[treeCtx.parentName];
          if (parent) {
            item.parent = parent;
            parent.children.push(item);
          }
        }
      }
    };
    if (!props.isSubmenuTitle) {
      createNode();
    }
    return () => isRootItem.value ? createVNode(Popover, {
      "align": "right",
      "arrow": true,
      "theme": ctx.theme,
      "content": createVNode("div", {
        "class": "cm-menu-item-text"
      }, [slots.default && slots.default()])
    }, {
      default: () => [createVNode("li", {
        "class": classList.value,
        "onClick": onSelect
      }, [createVNode("div", {
        "class": "cm-menu-item-icon"
      }, [props.icon])])]
    }) : createVNode("li", {
      "class": classList.value,
      "onClick": onSelect,
      "style": {
        paddingLeft: padding.value + 'px'
      }
    }, [createVNode("div", {
      "class": "cm-menu-item-icon"
    }, [props.icon]), createVNode("div", {
      "class": "cm-menu-item-text"
    }, [slots.default && slots.default()]), props.cert ? createVNode("div", {
      "class": "cm-menu-item-cert"
    }, [createVNode(FeatherChevronDown, {
      "size": 14
    }, null)]) : null]);
  }
});

export { MenuItem as default };
