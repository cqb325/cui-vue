import { defineComponent, inject, ref, watchEffect, createVNode, provide } from 'vue';
import MenuItem from './MenuItem.js';
import { MenuContext, MenuTreeContext } from './index.js';

const MenuGroup = /* @__PURE__ */ defineComponent({
  name: 'MenuGroup',
  props: {
    name: {
      type: String
    },
    title: {
      type: String
    },
    icon: {
      type: Object
    }
  },
  setup(props, {
    slots,
    expose
  }) {
    if (!props.name) {
      console.warn("MenuGroup need name prop");
    }
    const ctx = inject(MenuContext, {});
    const treeCtx = inject(MenuTreeContext, {});
    const parentPadding = treeCtx.parent ? ctx.dir === 'h' ? 0 : treeCtx.parent.padding : 0;
    const padding = ref(parentPadding + 16);
    ref();
    const createNode = () => {
      const item = {
        name: props.name,
        parent: null,
        children: [],
        padding: padding.value
      };
      if (ctx && props.name) {
        ctx.treeMap[props.name] = item;
        if (!treeCtx.parentName) {
          ctx?.tree.push(item);
        } else {
          const parent = ctx.treeMap[treeCtx.parentName];
          if (parent) {
            item.parent = parent;
            parent.children.push(item);
          }
        }
      }
      provide(MenuTreeContext, {
        parentName: props.name,
        parent: ctx.treeMap[props.name]
      });
    };
    createNode();
    watchEffect(() => {
      ctx.treeMap[props.name].padding = ctx.store.min ? 16 : padding.value;
    });
    return () => createVNode("li", {
      "class": "cm-menu-group"
    }, [createVNode(MenuItem, {
      "isSubmenuTitle": true,
      "icon": props.icon
    }, {
      default: () => [props.title]
    }), createVNode("ul", {
      "class": "cm-menu-group-list",
      "x-name": props.name
    }, [slots.default?.()])]);
  }
});

export { MenuGroup as default };
