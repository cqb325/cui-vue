import { defineComponent, ref, createVNode, mergeProps } from 'vue';
import VirtualListCore from './core.js';

const CONTAINER_CLASSNAME = `cm-virtual-list`;
let globalContainerStylesheet;
const insertGlobalStylesheet = () => {
  if (!globalContainerStylesheet) {
    globalContainerStylesheet = document.createElement('style');
    globalContainerStylesheet.type = 'text/css';
    globalContainerStylesheet.textContent = `
        .${CONTAINER_CLASSNAME} {
            position: relative !important;
            flex-shrink: 0 !important;
            width: 100%;
            height: 100%;
            overflow: auto;
        }
        .${CONTAINER_CLASSNAME} > * {
            width: 100%;
            will-change: transform !important;
            box-sizing: border-box !important;
            contain: layout !important;
        }
      `;
    document.head.appendChild(globalContainerStylesheet);
  }
};
const VirtualList = /* @__PURE__ */ defineComponent({
  name: 'VirtualList',
  props: {
    height: {
      type: Number
    },
    maxHeight: {
      type: Number
    },
    itemEstimatedSize: {
      type: Number
    },
    overscan: {
      type: Number
    },
    items: {
      type: Array
    },
    itemComponent: {
      type: Object
    },
    displayDelay: {
      type: Number
    }
  },
  emits: ['scroll'],
  setup(props, {
    slots
  }) {
    insertGlobalStylesheet();
    const scrollElement = ref();
    const contentElement = ref();
    const bodyElement = ref();
    const core = ref();
    return () => createVNode("div", {
      "class": CONTAINER_CLASSNAME,
      "ref": scrollElement
    }, [createVNode("div", {
      "ref": contentElement
    }, [createVNode("div", {
      "ref": bodyElement
    }, [scrollElement.value ? createVNode(VirtualListCore, mergeProps({
      "ref": core,
      "scrollElement": scrollElement.value,
      "contentElement": contentElement.value,
      "bodyElement": bodyElement.value
    }, props), {
      default: (...args) => {
        return slots.default?.(...args);
      }
    }) : null])])]);
  }
});

export { VirtualListCore, VirtualList as default };
