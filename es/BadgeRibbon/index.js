import { defineComponent, computed, createVNode } from 'vue';
import { isColor } from '../utils/assist.js';

const index = /* @__PURE__ */ defineComponent({
  name: 'BadgeRibbon',
  props: {
    text: {
      type: [String, Number, Object]
    },
    status: {
      type: String
    },
    align: {
      type: String
    },
    color: {
      type: String
    }
  },
  setup(props, {
    slots
  }) {
    const align = props.align ?? 'end';
    const classList = computed(() => ({
      'cm-badge-ribbon': true,
      [`cm-badge-ribbon-status-${props.status}`]: !!props.status,
      [`cm-badge-ribbon-${align}`]: !!align,
      [`cm-badge-ribbon-${props.color}`]: !!props.color && !isColor(props.color)
    }));
    const style = computed(() => ({
      '--cui-background-color': isColor(props.color) ? props.color : ''
    }));
    return () => createVNode("div", {
      "class": classList.value,
      "style": style.value
    }, [slots.default?.(), createVNode("div", {
      "class": "cm-badge-ribbon-inner"
    }, [createVNode("span", {
      "class": "cm-badge-ribbon-text"
    }, [props.text]), createVNode("div", {
      "class": "cm-badge-ribbon-corner"
    }, null)])]);
  }
});

export { index as default };
