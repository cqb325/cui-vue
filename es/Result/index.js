import { defineComponent, computed, createVNode } from 'vue';
import { F7InfoCircleFill, F7CheckmarkAltCircleFill, F7ExclamationmarkTriangleFill, F7XmarkCircleFill } from 'cui-vue-icons/f7';

const icons = {
  info: F7InfoCircleFill,
  success: F7CheckmarkAltCircleFill,
  warning: F7ExclamationmarkTriangleFill,
  error: F7XmarkCircleFill
};
const index = /* @__PURE__ */ defineComponent({
  name: "Result",
  props: {
    icon: {
      type: Object
    },
    status: {
      type: String,
      default: "info"
    },
    title: {
      type: [String, Object]
    },
    subTitle: {
      type: [String, Object]
    },
    extra: {
      type: Object
    },
    desc: {
      type: [String, Object]
    },
    class: {
      type: [String, Array, Object]
    }
  },
  setup(props) {
    const getIcon = status => {
      const IconComponent = icons[status];
      return createVNode(IconComponent);
    };
    const icon = computed(() => props.icon || getIcon(props.status));
    const classList = computed(() => {
      const baseClass = "cm-result";
      const statusClass = `cm-result-${props.status}`;
      return {
        [baseClass]: true,
        [statusClass]: !!props.status
      };
    });
    return () => createVNode("div", {
      "class": classList.value
    }, [icon.value ? createVNode("div", {
      "class": "cm-result-icon"
    }, [icon.value]) : null, props.title ? createVNode("div", {
      "class": "cm-result-title"
    }, [props.title]) : null, props.subTitle ? createVNode("div", {
      "class": "cm-result-subtitle"
    }, [props.subTitle]) : null, props.extra ? createVNode("div", {
      "class": "cm-result-extra"
    }, [props.extra]) : null, props.desc ? createVNode("div", {
      "class": "cm-result-desc"
    }, [props.desc]) : null]);
  }
});

export { index as default };
