import { defineComponent, computed, createVNode, mergeProps } from 'vue';

const View = /* @__PURE__ */ defineComponent({
  name: "View",
  props: ['size'],
  setup(props, {
    slots
  }) {
    const newStyle = computed(() => ({
      flex: `0 1 ${props.size}`
    }));
    return () => createVNode("div", {
      "class": "cm-view",
      "style": newStyle.value
    }, [slots.default && slots.default()]);
  }
});
const HView = /* @__PURE__ */ defineComponent({
  name: "HView",
  props: ['size'],
  setup(props, {
    slots
  }) {
    return () => createVNode(View, mergeProps({
      "class": "cm-h-view"
    }, props), {
      default: () => [slots.default && slots.default()]
    });
  }
});
const VView = /* @__PURE__ */ defineComponent({
  name: "VView",
  props: ['size'],
  setup(props, {
    slots
  }) {
    return () => createVNode(View, mergeProps({
      "class": "cm-v-view"
    }, props), {
      default: () => [slots.default && slots.default()]
    });
  }
});
const FixedView = /* @__PURE__ */ defineComponent({
  name: "FixedView",
  props: ['size'],
  setup(props, {
    slots
  }) {
    return () => createVNode(View, mergeProps({
      "class": "cm-fixed-view"
    }, props), {
      default: () => [slots.default && slots.default()]
    });
  }
});

export { FixedView, HView, VView, View };
