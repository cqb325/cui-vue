import { defineComponent, computed, createVNode } from 'vue';

const Center = /* @__PURE__ */ defineComponent({
  name: "Center",
  props: ['width', 'height'],
  setup(props, {
    slots
  }) {
    const newStyle = computed(() => ({
      width: props.width + 'px',
      height: props.height + 'px'
    }));
    return () => createVNode("div", {
      "class": "cm-view-center",
      "style": newStyle.value
    }, [slots.default && slots.default()]);
  }
});

export { Center as default };
