import { defineComponent, computed, ref, watch, createVNode } from 'vue';
import { FeatherX } from 'cui-vue-icons/feather';

const Tag = /* @__PURE__ */ defineComponent({
  name: 'Tag',
  props: ['theme', 'value', 'circle', 'size', 'avatar', 'onBeforeClose', 'onClose', 'closable', 'modelValue', 'border'],
  emits: ['update:modelValue', 'close'],
  setup(props, {
    emit,
    slots
  }) {
    const value = () => props.value ?? '';
    const classList = computed(() => ({
      'cm-tag': true,
      [`cm-tag-${props.theme}`]: props.theme,
      'cm-tag-has-badge': value() !== '',
      'cm-tag-border': props.border,
      'cm-tag-circle': !value() && props.circle,
      [`cm-tag-${props.size}`]: props.size,
      'cm-tag-has-avatar': props.avatar
    }));
    const visible = ref(props.modelValue ?? true);
    watch(() => props.modelValue, val => {
      visible.value = val ?? true;
    });
    const _onClose = e => {
      if (props.onBeforeClose) {
        const ret = props.onBeforeClose(e);
        if (ret) {
          doClose(e);
        }
      } else {
        doClose(e);
      }
    };
    const doClose = e => {
      visible.value = false;
      emit('update:modelValue', false);
      emit('close', e);
    };
    return () => visible.value ? createVNode("div", {
      "class": classList.value
    }, [createVNode("div", {
      "class": "cm-tag-content"
    }, [props.avatar, createVNode("div", {
      "class": "cm-tag-text"
    }, [slots.default?.()]), props.closable ? createVNode(FeatherX, {
      "class": "cm-tag-close",
      "size": 12,
      "onClick": _onClose
    }, null) : null]), value() !== '' ? createVNode("span", {
      "class": "cm-tag-badge"
    }, [createVNode("span", {
      "class": "cm-tag-badge-text"
    }, [value()])]) : null]) : null;
  }
});

export { Tag as default };
