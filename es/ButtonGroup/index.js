import { defineComponent, provide, createVNode } from 'vue';

const BUTTON_GROUP_CONTEXT = Symbol('button_group_context');
const index = /* @__PURE__ */ defineComponent({
  name: 'ButtonGroup',
  props: {
    type: {
      type: String
    },
    theme: {
      type: String
    },
    size: {
      type: String
    },
    disabled: {
      type: Boolean
    }
  },
  setup(props, {
    slots
  }) {
    provide(BUTTON_GROUP_CONTEXT, {
      type: props.type,
      theme: props.theme,
      size: props.size,
      disabled: props.disabled
    });
    return () => createVNode("div", {
      "class": "cm-button-group"
    }, [slots.default?.()]);
  }
});

export { BUTTON_GROUP_CONTEXT, index as default };
