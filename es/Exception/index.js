import { defineComponent, computed, createVNode } from 'vue';
import { typeImages } from './typeImages.js';
import Image from '../Image/index.js';
import Text from '../Typography/Text.js';

const index = /* @__PURE__ */ defineComponent({
  name: 'Exception',
  props: {
    type: {
      type: String,
      required: true
    },
    typeImage: {
      type: Object
    },
    desc: {
      type: String
    },
    showDesc: {
      type: Boolean,
      default: true
    },
    width: {
      type: Number
    },
    height: {
      type: Number
    },
    action: {
      type: Object
    }
  },
  setup(props) {
    const classList = computed(() => ({
      'cm-exception': true,
      [`cm-exception-${props.type}`]: !!props.type
    }));
    return () => createVNode("div", {
      "class": classList.value
    }, [createVNode("div", {
      "class": "cm-exception-img"
    }, [props.typeImage ? createVNode(Image, {
      "src": props.typeImage,
      "width": props.width,
      "height": props.height
    }, null) : createVNode(Image, {
      "src": typeImages(props.type),
      "width": props.width,
      "height": props.height
    }, null)]), createVNode("div", {
      "class": "cm-exception-info"
    }, [props.showDesc ? createVNode("div", {
      "class": "cm-exception-desc"
    }, [props.type === '403' ? createVNode(Text, {
      "size": "large"
    }, {
      default: () => [props.desc ?? '抱歉，你无权访问该页面']
    }) : null, props.type === '404' ? createVNode(Text, {
      "size": "large"
    }, {
      default: () => [props.desc ?? '抱歉，你访问的页面不存在']
    }) : null, props.type === '500' ? createVNode(Text, {
      "size": "large"
    }, {
      default: () => [props.desc ?? '抱歉，服务器出错了']
    }) : null, props.type === 'empty' ? createVNode(Text, {
      "size": "large"
    }, {
      default: () => [props.desc ?? '暂无数据']
    }) : null, props.type === 'fail' ? createVNode(Text, {
      "size": "large"
    }, {
      default: () => [props.desc ?? '授权失败']
    }) : null, props.type === 'deny' ? createVNode(Text, {
      "size": "large"
    }, {
      default: () => [props.desc ?? '拒绝访问']
    }) : null]) : null, props.action ? createVNode("div", {
      "class": "cm-exception-action"
    }, [props.action]) : null])]);
  }
});

export { index as default };
