import { createVNode } from 'vue';

function BothSide(props, {
  slots
}) {
  return createVNode("div", {
    "class": "cm-both-side"
  }, [slots.default && slots.default()]);
}
BothSide.displayName = 'BothSide';

export { BothSide as default };
