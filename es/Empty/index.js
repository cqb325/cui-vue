import { defineComponent, createVNode } from 'vue';
import NO_DATA from './NoData.svg.js';
import Image from '../Image/index.js';

const Empty = /* @__PURE__ */ defineComponent({
  name: "Empty",
  props: {
    text: {
      type: String,
      default: "暂无数据"
    },
    width: {
      type: Number
    },
    height: {
      type: Number
    }
  },
  setup(props) {
    return () => createVNode("div", {
      "class": "cm-empty"
    }, [createVNode("div", {
      "class": "cm-empty-img"
    }, [createVNode(Image, {
      "src": NO_DATA,
      "width": props.width,
      "height": props.height
    }, null)]), createVNode("div", {
      "class": "cm-empty-info"
    }, [props.text])]);
  }
});

export { Empty as default };
