import { defineComponent, computed, createVNode } from 'vue';

const index = /* @__PURE__ */ defineComponent({
  name: 'Card',
  props: ['bordered', 'rised', 'title', 'bodyStyle', 'footer'],
  setup(props, {
    slots
  }) {
    const classList = computed(() => ({
      'cm-card': true,
      'cm-card-bordered': props.bordered,
      'cm-card-rised': props.rised
    }));
    return () => createVNode("div", {
      "class": classList.value
    }, [props.title || slots.title ? createVNode("div", {
      "class": "cm-card-head"
    }, [props.title || slots.title && slots.title()]) : null, createVNode("div", {
      "class": "cm-card-body",
      "style": props.bodyStyle
    }, [slots.default && slots.default()]), props.footer || slots.footer ? createVNode("div", {
      "class": "cm-card-footer"
    }, [props.footer || slots.footer && slots.footer()]) : null]);
  }
});

export { index as default };
