import { defineComponent, createVNode, ref, computed, onMounted } from 'vue';
import usezIndex from '../use/usezIndex.js';
import Loading from '../inner/Loading.js';
import { F7XmarkCircleFill, F7ExclamationmarkTriangleFill, F7CheckmarkAltCircleFill, F7InfoCircleFill } from 'cui-vue-icons/f7';
import { FeatherX } from 'cui-vue-icons/feather';

function getIcon(type) {
  let icon = null;
  switch (type) {
    case 'info':
      {
        icon = createVNode(F7InfoCircleFill, {
          "class": "cm-message-icon",
          "size": 19
        }, null);
        break;
      }
    case 'success':
      {
        icon = createVNode(F7CheckmarkAltCircleFill, {
          "class": "cm-message-icon",
          "size": 19
        }, null);
        break;
      }
    case 'warning':
      {
        icon = createVNode(F7ExclamationmarkTriangleFill, {
          "class": "cm-message-icon",
          "size": 19
        }, null);
        break;
      }
    case 'error':
      {
        icon = createVNode(F7XmarkCircleFill, {
          "class": "cm-message-icon",
          "size": 19
        }, null);
        break;
      }
  }
  return icon;
}
const Message = /* @__PURE__ */ defineComponent({
  name: 'Message',
  props: {
    data: {
      type: Object
    }
  },
  emits: ['close'],
  setup(props, {
    emit
  }) {
    const visible = ref(false);
    const data = props.data;
    const classList = computed(() => {
      const obj = {
        'cm-message': true,
        'cm-message-visible': visible.value,
        [`cm-message-${data.type}`]: data.type,
        'cm-message-background': data.background
      };
      if (data.class) {
        obj[data.class] = true;
      }
      return obj;
    });
    onMounted(() => {
      // 设置效果
      setTimeout(() => {
        visible.value = true;
      });
      let duration = data.duration;
      if (duration === undefined || duration === null) {
        duration = 4;
      }
      if (duration) {
        setTimeout(() => {
          hide();
        }, duration * 1000);
      }
    });
    const hide = () => {
      // 隐藏效果
      visible.value = false;
    };
    const close = () => {
      if (!visible.value) {
        emit('close', data);
        data.onClose && data.onClose();
      }
    };
    const style = computed(() => ({
      ...data.style,
      'z-index': usezIndex()
    }));
    return () => createVNode("div", {
      "class": classList.value,
      "style": style.value,
      "onTransitionend": close
    }, [createVNode("div", {
      "class": "cm-message-inner"
    }, [data.loading ? createVNode(Loading, null, null) : getIcon(data.type), createVNode("div", {
      "class": "cm-message-content"
    }, [data.content]), data.closeable ? createVNode("div", {
      "class": "cm-message-close"
    }, [createVNode(FeatherX, {
      "class": "cm-message-close-icon",
      "size": 14,
      "onClick": hide
    }, null)]) : null])]);
  }
});
const Messages = /* @__PURE__ */ defineComponent({
  name: 'Messages',
  props: {
    data: {
      type: Array,
      default: () => []
    },
    onClose: {
      type: Function
    }
  },
  setup(props) {
    return () => props.data && props.data.map(item => {
      return createVNode(Message, {
        "key": item.key,
        "data": item,
        "onClose": props.onClose
      }, null);
    });
  }
});

export { Messages, Message as default };
