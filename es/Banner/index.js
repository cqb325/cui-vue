import { defineComponent, VueElement, ref, computed, createVNode } from 'vue';
import { F7InfoCircleFill, F7XmarkCircleFill, F7ExclamationmarkTriangleFill, F7CheckmarkAltCircleFill } from 'cui-vue-icons/f7';
import { FeatherX } from 'cui-vue-icons/feather';

const index = /* @__PURE__ */ defineComponent({
  name: 'Banner',
  props: {
    type: {
      type: String
    },
    bordered: {
      type: Boolean
    },
    icon: {
      type: VueElement
    },
    closeIcon: {
      type: VueElement
    },
    title: {
      type: [String, Object]
    },
    fullMode: {
      type: Boolean
    },
    modelValue: {
      type: Boolean,
      default: true
    }
  },
  setup(props, {
    emit,
    slots
  }) {
    const visible = ref(props.modelValue);
    const classList = computed(() => ({
      'cm-banner': true,
      [`cm-banner-${props.type}`]: props.type,
      [`cm-banner-bordered`]: props.bordered,
      [`cm-banner-full`]: props.fullMode ?? true,
      [`cm-banner-not-full`]: props.fullMode === false
    }));
    const getIconByType = () => {
      let icon = null;
      switch (props.type) {
        case 'info':
          {
            icon = createVNode(F7InfoCircleFill, null, null);
            break;
          }
        case 'success':
          {
            icon = createVNode(F7CheckmarkAltCircleFill, null, null);
            break;
          }
        case 'warning':
          {
            icon = createVNode(F7ExclamationmarkTriangleFill, null, null);
            break;
          }
        case 'error':
          {
            icon = createVNode(F7XmarkCircleFill, null, null);
            break;
          }
        default:
          {
            icon = createVNode(F7InfoCircleFill, null, null);
          }
      }
      return icon;
    };
    const onClickClose = () => {
      visible.value = false;
      emit('update:modelValue', false);
    };
    const icon = props.icon === null ? null : props.icon ?? getIconByType();
    return () => visible.value ? createVNode("div", {
      "class": classList.value
    }, [createVNode("div", {
      "class": "cm-banner-body"
    }, [createVNode("div", {
      "class": "cm-banner-content"
    }, [icon ? createVNode("div", {
      "class": "cm-banner-icon"
    }, [icon]) : null, createVNode("div", {
      "class": "cm-banner-content-body"
    }, [props.title ? createVNode("div", {
      "class": "cm-banner-title"
    }, [props.title]) : null, slots.default ? createVNode("div", {
      "class": "cm-banner-desc"
    }, [slots.default()]) : null])]), props.closeIcon !== null ? createVNode("span", {
      "class": "cm-banner-close",
      "onClick": onClickClose
    }, [props.closeIcon ?? createVNode(FeatherX, null, null)]) : null]), slots.extra ? createVNode("div", {
      "class": "cm-banner-extra"
    }, [slots.extra()]) : null]) : null;
  }
});

export { index as default };
