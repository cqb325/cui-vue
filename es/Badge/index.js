import { defineComponent, computed, createVNode, Fragment } from 'vue';

function isColor(strColor) {
  if (strColor && (strColor.startsWith('#') || strColor.startsWith('rgb') || strColor.startsWith('hsl'))) {
    const s = new Option().style;
    s.color = strColor;
    return s.color.startsWith('rgb');
  }
  return false;
}
const index = /* @__PURE__ */ defineComponent({
  name: 'Badge',
  props: {
    count: {
      type: Number,
      default: undefined
    },
    dot: {
      type: Boolean
    },
    overflowCount: {
      type: Number
    },
    text: {
      type: String,
      default: undefined
    },
    status: {
      type: String
    },
    color: {
      type: String
    },
    type: {
      type: String
    }
  },
  setup(props, {
    slots
  }) {
    const overflowCount = props.overflowCount ?? 99;
    const classList = computed(() => ({
      'cm-badge': true,
      'cm-badge-status': props.status
    }));
    const showCount = computed(() => {
      return props.count && props.count > overflowCount ? Math.min(overflowCount, props.count) + '+' : props.count;
    });
    const statusClass = computed(() => ({
      'cm-badge-status-dot': true,
      [`cm-badge-status-${props.status}`]: !!props.status,
      [`cm-badge-status-${props.color}`]: !!props.color && props.color.indexOf('#') === -1
    }));
    const statusStyle = computed(() => ({
      'background-color': isColor(props.color) ? props.color : ''
    }));
    const countClass = computed(() => ({
      'cm-badge-count': true,
      [`cm-badge-count-${props.type}`]: !!props.type
    }));
    return () => createVNode("span", {
      "class": classList.value
    }, [slots.default?.(), !props.status && !props.color ? createVNode(Fragment, null, [props.count !== undefined || props.text !== undefined ? createVNode("sup", {
      "class": countClass.value
    }, [showCount.value, props.text]) : null, props.dot ? createVNode("sup", {
      "class": "cm-badge-dot"
    }, null) : null]) : createVNode(Fragment, null, [createVNode("span", {
      "class": statusClass.value,
      "style": statusStyle.value
    }, null), createVNode("span", {
      "class": "cm-badge-status-text"
    }, [props.text])])]);
  }
});

export { index as default };
