import { defineComponent, computed, ref, watch, provide, createVNode } from 'vue';
import Item from './Item.js';

const ListContext = Symbol('ListContext');
const List = /* @__PURE__ */ defineComponent({
  name: 'List',
  props: {
    border: {
      type: Boolean
    },
    size: {
      type: String
    },
    head: {
      type: [String, Object]
    },
    foot: {
      type: [String, Object]
    },
    activeKey: {
      type: [String, Number]
    },
    selectable: {
      type: Boolean
    }
  },
  emits: ['update:activeKey', 'select'],
  setup(props, {
    slots,
    emit
  }) {
    const classList = computed(() => ({
      'cm-list': true,
      'cm-list-border': props.border,
      'cm-list-selectable': props.selectable,
      [`cm-list-${props.size}`]: props.size
    }));
    const activeKey = ref(props.activeKey);
    const setActiveKey = key => {
      activeKey.value = key;
      emit('update:activeKey', key);
    };
    watch(() => props.activeKey, newVal => {
      activeKey.value = newVal;
    });
    provide(ListContext, {
      activeKey,
      selectable: props.selectable,
      setActiveKey,
      onSelect: data => {
        emit('select', data);
      }
    });
    return () => createVNode("div", {
      "class": classList.value
    }, [props.head || slots.head ? createVNode("div", {
      "class": "cm-list-head"
    }, [props.head || slots.head?.()]) : null, slots.default?.(), props.foot || slots.foot ? createVNode("div", {
      "class": "cm-list-foot"
    }, [props.foot || slots.foot?.()]) : null]);
  }
});
List.Item = Item;

export { ListContext, List as default };
