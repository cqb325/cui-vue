import { defineComponent, inject, computed, createVNode } from 'vue';
import { ListContext } from './index.js';

const Item = /* @__PURE__ */ defineComponent({
  name: 'ListItem',
  props: ['id', 'data', 'actions', 'avatar', 'content', 'title', 'desc', 'activeKey'],
  setup(props, {
    slots,
    emit
  }) {
    const ctx = inject(ListContext);
    const setActiveKey = ctx.setActiveKey;
    const classList = computed(() => ({
      'cm-list-item': true,
      'cm-list-item-active': ctx.activeKey.value === props.id
    }));
    const onClick = () => {
      if (ctx?.selectable) {
        setActiveKey && setActiveKey(props.id);
        ctx.onSelect?.(props.data);
      }
    };
    return () => createVNode("div", {
      "class": classList.value,
      "onClick": onClick
    }, [createVNode("div", {
      "class": "cm-list-item-main"
    }, [createVNode("div", {
      "class": "cm-list-item-meta"
    }, [props.avatar ? createVNode("div", {
      "class": "cm-list-item-avatar"
    }, [props.avatar]) : null, props.title || props.desc ? createVNode("div", {
      "class": "cm-list-item-body"
    }, [createVNode("div", {
      "class": "cm-list-item-title"
    }, [props.title]), createVNode("div", {
      "class": "cm-list-item-desc"
    }, [props.desc])]) : null]), createVNode("div", {
      "class": "cm-list-item-content"
    }, [slots.default && slots.default()])]), props.actions ? createVNode("ul", {
      "class": "cm-list-item-addon"
    }, [props.actions]) : null]);
  }
});

export { Item as default };
