import { defineComponent, ref, computed, onMounted, createVNode, onBeforeMount } from 'vue';

const Avatar = /* @__PURE__ */ defineComponent({
  name: 'Avatar',
  props: {
    size: {
      type: [Number, String]
    },
    icon: {
      type: Object
    },
    src: {
      type: String
    },
    shape: {
      type: String,
      default: 'circle'
    },
    title: {
      type: [String, Number, Object]
    },
    hoverMask: {
      type: [String, Object]
    }
  },
  emits: ['click', 'mouseEnter', 'mouseLeave'],
  setup(props, {
    emit,
    slots
  }) {
    const hover = ref(false);
    const classList = computed(() => ({
      'cm-avatar': true,
      [`cm-avatar-${props.size}`]: props.size,
      'cm-avatar-icon': props.icon,
      'cm-avatar-img': props.src,
      'cm-avatar-square': props.shape === 'square'
    }));
    const onMutate = () => {
      string.style.Transform = '';
      string.style.webkitTransform = '';
      string.style.mozTransform = '';
      const wrapW = wrap.clientWidth;
      const strRect = string.getBoundingClientRect();
      const strW = strRect.width;
      const strH = 21;
      const theta = Math.acos(strH / wrapW);
      const w = Math.sin(theta) * wrapW;
      const ratio = strW > wrapW ? w / strW : 1;
      string.style.Transform = `scale(${ratio})`;
      string.style.webkitTransform = `scale(${ratio})`;
      string.style.mozTransform = `scale(${ratio})`;
    };
    let string;
    let wrap;
    let observer;
    onMounted(() => {
      if (wrap && string) {
        onMutate();
        if (MutationObserver) {
          observer = new MutationObserver(onMutate);
          observer.observe(string, {
            subtree: true,
            childList: true,
            characterData: true
          });
        }
        onBeforeMount(() => {
          if (observer) {
            observer?.disconnect();
            observer = null;
          }
        });
      }
    });
    const style = computed(() => {
      const obj = {};
      if (typeof props.size === 'number') {
        obj.width = props.size + 'px';
        obj.height = props.size + 'px';
      }
      return obj;
    });
    const onMouseEnter = e => {
      hover.value = true;
      emit('mouseEnter', e);
    };
    const onMouseLeave = e => {
      hover.value = false;
      emit('mouseLeave', e);
    };
    return () => createVNode("span", {
      "class": classList.value,
      "style": style.value,
      "onClick": e => {
        emit('click', e);
      },
      "ref": el => {
        wrap = el;
      },
      "onMouseenter": onMouseEnter,
      "onMouseleave": onMouseLeave
    }, [hover.value ? createVNode("div", {
      "class": "cm-avatar-hover"
    }, [props.hoverMask || slots.hoverMask && slots.hoverMask()]) : null, props.src ? createVNode("img", {
      "class": "cm-avatar-img",
      "src": props.src,
      "alt": ""
    }, null) : props.icon ? props.icon : createVNode("span", {
      "class": "cm-avatar-string",
      "ref": el => {
        string = el;
      }
    }, [slots.default && slots.default()])]);
  }
});

export { Avatar as default };
