import { defineComponent, computed, createVNode } from 'vue';
import { isColor } from '../utils/assist.js';

const index = /* @__PURE__ */ defineComponent({
  name: 'Divider',
  props: {
    dir: {
      type: String,
      default: 'h'
    },
    align: {
      type: String
    },
    theme: {
      type: String
    },
    margin: {
      type: [String, Number]
    },
    textColor: {
      type: String
    },
    textMargin: {
      type: [String, Number]
    },
    dashed: {
      type: Boolean
    }
  },
  setup(props, {
    slots
  }) {
    const theme = isColor(props.theme) ? '' : props.theme;
    const classList = computed(() => ({
      'cm-divider': true,
      [`cm-divider-${props.dir}`]: props.dir,
      [`cm-divider-${props.align}`]: props.align,
      [`cm-divider-${theme}`]: theme,
      'cm-divider-dashed': props.dashed
    }));
    const containerStyle = computed(() => ({
      margin: `${props.margin}${typeof props.margin === 'number' ? 'px' : ''}`,
      '--cui-divider-border-color': isColor(props.theme) ? props.theme : ''
    }));
    const textStyle = () => ({
      margin: `${props.textMargin}${typeof props.textMargin === 'number' ? 'px' : ''}`,
      color: props.textColor
    });
    return () => createVNode("div", {
      "class": classList.value,
      "style": containerStyle.value
    }, [slots.default ? createVNode("span", {
      "class": "cm-divider-text",
      "style": textStyle()
    }, [slots.default()]) : null]);
  }
});

export { index as default };
