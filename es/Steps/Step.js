import { defineComponent, computed, createVNode } from 'vue';
import { FeatherCheck, FeatherXCircle, FeatherAlertTriangle } from 'cui-vue-icons/feather';

const Step = /* @__PURE__ */ defineComponent({
  name: 'Step',
  props: {
    title: {
      type: [Object, String],
      default: undefined
    },
    description: {
      type: [Object, String],
      default: undefined
    },
    icon: {
      type: Object,
      default: undefined
    },
    status: {
      type: String,
      default: undefined
    },
    current: {
      type: Number,
      default: 0
    },
    index: {
      type: Number,
      default: 0
    }
  },
  setup(props, {
    slots
  }) {
    const status = computed(() => {
      if (props.status) {
        return props.status;
      }
      let ret = '';
      if (props.current + 1 > props.index) {
        ret = 'finished';
      }
      if (props.current + 1 === props.index) {
        ret = 'process';
      }
      return ret || 'wait';
    });
    const done = computed(() => {
      let ret = '';
      if (props.current + 1 > props.index) {
        ret = 'done';
      }
      if (props.current + 1 === props.index) {
        ret = 'active';
      }
      return ret;
    });
    const classList = computed(() => ({
      'cm-steps-item': true,
      [`cm-steps-status-${status.value}`]: status.value,
      [`cm-steps-status-${done.value}`]: done.value
    }));
    const inner = computed(() => {
      let ret;
      if (!props.icon) {
        if (status.value === 'finished') {
          ret = createVNode("div", {
            "class": "cm-step-head-inner"
          }, [createVNode(FeatherCheck, null, null)]);
        } else if (status.value === 'error') {
          ret = createVNode(FeatherXCircle, {
            "size": 26
          }, null);
        } else if (status.value === 'warning') {
          ret = createVNode(FeatherAlertTriangle, {
            "size": 26
          }, null);
        } else {
          ret = createVNode("div", {
            "class": "cm-step-head-inner"
          }, [createVNode("span", null, [props.index])]);
        }
      } else {
        ret = props.icon || slots.icon?.();
      }
      return ret;
    });
    return () => createVNode("div", {
      "class": classList.value
    }, [createVNode("div", {
      "class": "cm-step-head"
    }, [inner.value]), createVNode("div", {
      "class": "cm-step-main"
    }, [createVNode("div", {
      "class": "cm-step-title"
    }, [props.title]), createVNode("div", {
      "class": "cm-step-description"
    }, [props.description])])]);
  }
});

export { Step as default };
