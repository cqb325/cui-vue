import { defineComponent, computed, createVNode } from 'vue';
import useChildren from '../use/useChildren.js';
import Step from './Step.js';

const Steps = /* @__PURE__ */ defineComponent({
  name: 'Steps',
  props: {
    size: {
      type: String,
      default: 'default'
    },
    current: {
      type: Number,
      default: 0
    },
    dir: {
      type: String,
      default: ''
    }
  },
  setup(props, {
    slots
  }) {
    const classList = computed(() => ({
      'cm-steps': true,
      [`cm-steps-${props.size}`]: props.size,
      'cm-steps-vertical': props.dir === 'v'
    }));
    const children = computed(() => {
      const childs = useChildren({
        slots,
        props: {
          current: props.current
        },
        sameType: Step
      });
      return childs;
    });
    return () => createVNode("div", {
      "class": classList.value
    }, [children.value]);
  }
});
Steps.Step = Step;

export { Steps as default };
