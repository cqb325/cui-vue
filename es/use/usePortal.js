function usePortal(id, className) {
  function createRootElement(id) {
    const rootContainer = document.createElement('div');
    rootContainer.setAttribute('id', id);
    return rootContainer;
  }
  function addRootElement(rootElem) {
    document.body.appendChild(rootElem);
  }
  const existingParent = document.querySelector(`#${id}`);
  // Parent is either a new root or the existing dom element
  const parentElem = existingParent || createRootElement(id);
  if (!existingParent) {
    addRootElement(parentElem);
  }
  parentElem.classList.add(className);
  return parentElem;
}

export { usePortal as default };
