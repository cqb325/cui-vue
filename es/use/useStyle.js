function useStyle(customStyle) {
  let obj = {
    ...customStyle
  };
  return obj;
}

export { useStyle };
