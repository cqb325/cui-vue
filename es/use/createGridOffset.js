const gridPre = "cm-col";
const offsetPre = "cm-col-offset";
const rowPre = "cm-row";
const gapPre = "cm-row-gap";
const RESPONSIVE = {
  'xs': '576px',
  'sm': '576px',
  'md': '768px',
  'lg': '992px',
  'xl': '1200px',
  'xxl': '1600px'
};
const GRIDS = new Set();
const OFFSETS = new Set();
const GUTTERS = new Set();
const GAPS = new Set();
const defaultResponsive = 'lg';
let isCreated = false;
function initStyleElements() {
  for (const type in RESPONSIVE) {
    const id = 'cm-grid-' + type;
    if (document.getElementById(id)) continue;
    const style = document.createElement('style');
    style.id = id;
    document.head.appendChild(style);
  }
  isCreated = true;
}
function createStyle(text, responsive) {
  if (!isCreated) {
    initStyleElements();
  }
  document.getElementById('cm-grid-' + responsive).innerHTML += text;
  // const style = document.createElement('style');
  // style.innerHTML = text;
  // document.head.appendChild(style);
}
function generateGrid(width, key, responsive) {
  GRIDS.add(key);
  const minWidth = RESPONSIVE[responsive];
  const text = responsive === 'xs' ? `.${gridPre}-${key}{width: ${width}%}` : `@media (min-width: ${minWidth}) { .${gridPre}-${key}{width: ${width}%} }`;
  createStyle(text, responsive);
}
function generateOffset(offset, key, responsive) {
  OFFSETS.add(key);
  const minWidth = RESPONSIVE[responsive];
  const text = responsive === 'xs' ? `.${offsetPre}-${key}{margin-left: ${offset}%}` : `@media (min-width: ${minWidth}) { .${offsetPre}-${key}{margin-left: ${offset}%} }`;
  createStyle(text, responsive);
}
function createRowGutterStyle(gutter, key, responsive) {
  GUTTERS.add(key);
  const minWidth = RESPONSIVE[responsive];
  const text = responsive === 'xs' ? `.${rowPre}-${key}{margin-left: -${parseFloat(gutter) / 2}px; margin-right: -${parseFloat(gutter) / 2}px}
        .${rowPre}-${key} .${gridPre}{padding-left: ${parseFloat(gutter) / 2}px; padding-right: ${parseFloat(gutter) / 2}px}` : `@media (min-width: ${minWidth}) {
            .${rowPre}-${key}{margin-left: -${parseFloat(gutter) / 2}px; margin-right: -${parseFloat(gutter) / 2}px}
            .${rowPre}-${key} .${gridPre}{padding-left: ${parseFloat(gutter) / 2}px; padding-right: ${parseFloat(gutter) / 2}px}
        }`;
  createStyle(text, responsive);
}
function createGapStyle(gap, key, responsive) {
  GAPS.add(key);
  const minWidth = RESPONSIVE[responsive];
  const text = responsive === 'xs' ? `.${gapPre}-${key}{row-gap: ${gap}px;}` : `@media (min-width: ${minWidth}) {
            .${gapPre}-${key}{row-gap: ${gap}px;}
        }`;
  createStyle(text, responsive);
}
function generate(width, type, responsive) {
  let w = (width * 100).toFixed(4);
  w = w.substring(0, w.length - 1);
  responsive = responsive ?? defaultResponsive;
  const key = responsive + '-' + w.replace('.', '-');
  if (type === 'grid') {
    if (!GRIDS.has(key)) {
      generateGrid(w, key, responsive);
    }
    return `${gridPre}-${key}`;
  } else {
    if (!OFFSETS.has(key)) {
      generateOffset(w, key, responsive);
    }
    return `${offsetPre}-${key}`;
  }
}
function generateGutter(gutter, type, responsive) {
  responsive = responsive ?? defaultResponsive;
  const gu = typeof gutter === 'number' ? gutter.toFixed(2) : gutter[0].toFixed(2);
  const guValue = typeof gutter === 'number' ? gutter : gutter[0];
  const gap = typeof gutter === 'number' ? gutter.toFixed(2) : gutter[1].toFixed(2);
  const gapValue = typeof gutter === 'number' ? gutter : gutter[1];
  if (guValue || gapValue) {
    const classList = [];
    if (guValue) {
      const key = responsive + '-' + gu.replace('.', '-');
      if (!GUTTERS.has(key)) {
        createRowGutterStyle(gu, key, responsive);
      }
      classList.push(`${rowPre}-${key}`);
    }
    if (gapValue) {
      const gapKey = responsive + '-' + gap.replace('.', '-');
      if (!GAPS.has(gapKey)) {
        createGapStyle(gap, gapKey, responsive);
      }
      classList.push(`${gapPre}-${gapKey}`);
    }
    return classList;
  }
}

/**
 * 创建响应式样式,返回class
 * @param width
 * @param responsive
 * @returns
 */
function createGrid(width, responsive) {
  if (!width) {
    return '';
  }
  const gridClass = generate(width, 'grid', responsive);
  return gridClass;
}
function createOffset(offset, responsive) {
  if (!offset) {
    return '';
  }
  const gridClass = generate(offset, 'offset', responsive);
  return gridClass;
}
function createGutter(gutter) {
  if (!gutter) {
    return '';
  }
  const gutterClass = {};
  if (!Array.isArray(gutter) && typeof gutter === 'object') {
    for (const key in RESPONSIVE) {
      const gu = gutter[key]; // 16|[16,16]
      if (gu) {
        const gutterRowClass = generateGutter(gu, 'gutter', key);
        if (gutterRowClass) {
          gutterRowClass.forEach(className => {
            gutterClass[className] = true;
          });
        }
      }
    }
  }
  return gutterClass;
}

export { createGrid, createGutter, createOffset };
