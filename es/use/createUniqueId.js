let counter = 0;
const randomSeed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const index = Math.floor(Math.random() * randomSeed.length);
let id = randomSeed.substring(index, index + 1);
function createUniqueId() {
  return `${id}_${counter++}`;
}

export { createUniqueId };
