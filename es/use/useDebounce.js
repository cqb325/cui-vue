function useDebounce(func, delay) {
  let timer = null;
  return function (...rest) {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      timer = null;
      func?.(...rest);
    }, delay);
  };
}

export { useDebounce };
