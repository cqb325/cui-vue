import { ref } from 'vue';

function useClickAnimating() {
  const animating = ref(false);
  const setAnimate = () => {
    animating.value = true;
    setTimeout(() => {
      animating.value = false;
    }, 1e3);
  };
  return [animating, setAnimate];
}

export { useClickAnimating };
