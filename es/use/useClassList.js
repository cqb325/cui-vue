import { reactive } from 'vue';

function useClassList(props, ...customClassList) {
  let obj = reactive({});
  if (props && props.class) {
    obj[props.class] = true;
  }
  if (typeof props === "string") {
    obj[props] = true;
  }
  if (customClassList) {
    for (let i = 0; i < customClassList.length; i++) {
      const item = customClassList[i];
      if (typeof item === "string") {
        obj[item] = true;
      } else {
        for (let key in item) {
          obj[key] = item[key];
        }
      }
    }
  }
  return obj;
}

export { useClassList };
