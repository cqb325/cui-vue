import { defineComponent, computed, createVNode } from 'vue';
import { Avatar, Image, Title, Button, Item, Paragraph } from './Item.js';

const Skeleton = /* @__PURE__ */ defineComponent({
  props: {
    active: {
      type: Boolean
    },
    loading: {
      type: Boolean
    },
    placeholder: {
      type: Object
    },
    width: {
      type: String
    },
    height: {
      type: String
    },
    style: {
      type: Object
    }
  },
  setup(props, {
    slots
  }) {
    const classList = computed(() => ({
      'cm-skeleton': true,
      'cm-skeleton-active': props.active
    }));
    const style = computed(() => {
      const obj = {
        ...props.style
      };
      if (props.width !== undefined) {
        obj.width = props.width;
      }
      if (props.height !== undefined) {
        obj.height = props.height;
      }
      return obj;
    });
    return () => props.loading ? createVNode("div", {
      "class": classList.value,
      "style": style.value
    }, [props.placeholder]) : slots.default?.();
  }
});
Skeleton.Avatar = Avatar;
Skeleton.Image = Image;
Skeleton.Title = Title;
Skeleton.Button = Button;
Skeleton.Item = Item;
Skeleton.Paragraph = Paragraph;

export { Skeleton as default };
