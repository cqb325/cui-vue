import { defineComponent, computed, createVNode, mergeProps } from 'vue';

const GenericCompProps = {
  type: {
    type: String
  },
  width: {
    type: [String, Array]
  },
  height: {
    type: String
  },
  inline: {
    type: Boolean
  },
  size: {
    type: String
  },
  shape: {
    type: String
  },
  rows: {
    type: Number
  }
};
const Generic = /* @__PURE__ */ defineComponent({
  props: GenericCompProps,
  setup(props) {
    const size = props.size ?? 'medium';
    const shape = props.shape ?? 'circle';
    const classList = computed(() => ({
      'cm-skeleton-item': true,
      [`cm-skeleton-${props.type}`]: props.type,
      [`cm-skeleton-${props.type}-${size}`]: size,
      [`cm-skeleton-${props.type}-${shape}`]: shape,
      [`cm-skeleton-inline`]: props.inline
    }));
    const style = computed(() => ({
      width: typeof props.size === 'number' ? props.size + 'px' : props.width,
      height: typeof props.size === 'number' ? props.size + 'px' : props.height
    }));
    return () => createVNode("div", {
      "class": classList.value,
      "style": style.value
    }, null);
  }
});
const generator = type => props => createVNode(Generic, mergeProps({
  "type": type
}, props), null);
const Avatar = generator('avatar');
const Image = generator('image');
const Title = generator('title');
const Button = generator('button');
const Item = generator('item');
const Paragraph = /* @__PURE__ */ defineComponent({
  props: GenericCompProps,
  setup(props) {
    const rows = props.rows ?? 4;
    const arr = new Array(rows).fill(1);
    const outStyle = computed(() => ({
      width: props.width
    }));
    return () => createVNode("ul", {
      "class": "cm-skeleton-paragraph",
      "style": outStyle.value
    }, [arr.map((item, index) => {
      const style = {};
      if (props.width && props.width instanceof Array) {
        style.width = props.width[index];
      }
      return createVNode("li", {
        "style": style
      }, null);
    })]);
  }
});

export { Avatar, Button, Image, Item, Paragraph, Title };
