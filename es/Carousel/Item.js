import { defineComponent, inject } from 'vue';
import { CarouselContextKey } from './index.js';
import { createUniqueId } from '../use/createUniqueId.js';

const CarouselItem = /* @__PURE__ */ defineComponent({
  name: 'InnerCarouselItem',
  props: null,
  setup(props, {
    slots
  }) {
    const ctx = inject(CarouselContextKey);
    const id = createUniqueId();
    ctx?.addItem({
      id,
      ...props,
      content: slots.default
    });
    return () => null;
  }
});

export { CarouselItem as default };
