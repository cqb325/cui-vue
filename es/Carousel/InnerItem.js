import { defineComponent, inject, ref, watchEffect, onMounted, onBeforeUnmount, watch, createVNode } from 'vue';
import { CarouselContextKey } from './index.js';
import useTransition from '../use/useTransition.js';
import { useSwipe } from '../use/useSwipe.js';

const InnerItem = /* @__PURE__ */ defineComponent({
  name: 'InnerCarouselItem',
  props: {
    data: {
      type: Object
    },
    index: {
      type: Number
    }
  },
  setup(props, {
    slots
  }) {
    const ctx = inject(CarouselContextKey);
    const item = ref();
    let transition;
    const style = ref({});
    watchEffect(() => {
      const activeKey = ctx?.store.activeKey;
      const prevKey = ctx?.store.prevKey;
      const nextKey = ctx?.store.nextKey;
      const obj = {
        "width": typeof ctx?.itemsPerView === 'number' ? `${1 / ctx?.itemsPerView * 100}%` : ''
      };
      if (ctx?.effect.value === 'card') {
        if (activeKey === props.data.id) {
          Object.assign(obj, {
            width: '60%',
            opacity: 1,
            'z-index': 1,
            transform: 'translateX(-50%) translateZ(0)'
          });
        } else if (prevKey === props.data.id) {
          Object.assign(obj, {
            width: '60%',
            opacity: 0.4,
            transform: 'translateX(-100%) translateZ(-200px)'
          });
        } else if (nextKey === props.data.id) {
          Object.assign(obj, {
            width: '60%',
            opacity: 0.4,
            transform: 'translateX(0%) translateZ(-200px)'
          });
        } else {
          Object.assign(obj, {
            width: '50%',
            opacity: 0,
            transform: 'translateX(-50%) translateZ(-400px)'
          });
        }
      }
      return style.value = obj;
    });
    onMounted(() => {
      if (ctx?.draggable.value) {
        const swipe = useSwipe(item.value, {
          threshold: 10,
          onSwipe: () => {
            ctx?.onSwipe(swipe);
          },
          onSwipeEnd: (e, direction, duration) => {
            ctx?.onSwipeEnd(direction, duration);
          },
          onSwipeStart: () => {
            ctx?.onSwipeStart(swipe);
          }
        });
        onBeforeUnmount(() => {
          swipe.stop();
        });
      }
      transition = useTransition({
        el: () => item.value,
        startClass: 'cm-carousel-item-fade-start',
        activeClass: 'cm-carousel-item-fade-active'
      });
      watch(() => [ctx?.store.activeKey, ctx?.store.unActiveKey], () => {
        const activeKey = ctx?.store.activeKey;
        const unActiveKey = ctx?.store.unActiveKey;
        if (ctx?.effect.value === 'fade') {
          if (activeKey === props.data.id) {
            transition?.enter();
          }
          if (unActiveKey === props.data.id) {
            transition?.leave();
          }
        }
      }, {
        immediate: true
      });
    });
    return () => createVNode("div", {
      "class": "cm-carousel-item",
      "ref": item,
      "style": style.value
    }, [props.data.content?.()]);
  }
});

export { InnerItem as default };
