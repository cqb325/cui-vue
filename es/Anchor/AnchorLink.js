import { defineComponent, inject, createVNode } from 'vue';

const AnchorLink = /* @__PURE__ */ defineComponent({
  name: "AnchorLink",
  props: {
    href: {
      type: String,
      default: undefined
    },
    title: {
      type: [String, Object],
      default: undefined
    }
  },
  setup(props, {
    slots
  }) {
    const ctx = inject('CMAnchorContext', null);
    const gotoAnchor = e => {
      ctx?.gotoAnchor(props.href, e);
    };
    ctx.addLink({
      href: props.href,
      title: props.title
    });
    return () => createVNode("div", {
      "class": "cm-anchor-link"
    }, [createVNode("a", {
      "class": "cm-anchor-link-title",
      "href": props.href,
      "data-scroll-offset": ctx?.scrollOffset || 0,
      "data-href": props.href,
      "onClick": gotoAnchor,
      "title": props.title
    }, [props.title]), slots.default?.()]);
  }
});

export { AnchorLink as default };
