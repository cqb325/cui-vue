import { defineComponent, createVNode, createTextVNode } from 'vue';

const Checkbox = /* @__PURE__ */ defineComponent({
  name: 'InnerCheckbox',
  props: {
    disabled: {
      type: Boolean
    },
    type: {
      type: String
    },
    name: {
      type: String
    },
    label: {
      type: [String, Object]
    },
    checked: {
      type: [String, Boolean]
    },
    value: {
      type: [String, Number]
    }
  },
  emits: ['change', 'update:modelValue'],
  setup(props, {
    emit
  }) {
    const classList = () => ({
      'cm-checkbox': true,
      'cm-checkbox-checked': !!props.checked,
      'cm-checkbox-indeterminate': props.checked === 'indeterminate',
      disabled: props.disabled
    });

    // watch(() => props.checked, () => {
    //     console.log(props.checked);
    //     // checked.value = props.checked;
    // });

    const onClick = e => {
      e.preventDefault && e.preventDefault();
      e.stopPropagation && e.stopPropagation();
      if (props.disabled) {
        return;
      }
      if (props.type == 'radio' && props.checked) {
        return;
      }
      let flag = props.checked;
      if (flag === 'indeterminate') {
        flag = true;
      } else {
        flag = !flag;
      }
      emit('change', flag, props.value);
    };
    return () => createVNode("div", {
      "class": classList(),
      "onClick": onClick
    }, [createVNode("span", {
      "style": {
        width: '0px',
        "font-size": '12px',
        visibility: 'hidden'
      }
    }, [createTextVNode("A")]), createVNode("input", {
      "type": props.type,
      "name": props.name,
      "value": props.value,
      "style": {
        display: 'none'
      },
      "onChange": () => {}
    }, null), createVNode("span", {
      "style": {
        position: 'relative'
      },
      "class": "cm-checkbox-outter"
    }, [createVNode("span", {
      "class": "cm-checkbox-inner"
    }, null)]), props.label ? createVNode("label", null, [props.label]) : null]);
  }
});

export { Checkbox as default };
