import { defineComponent, onMounted, watchEffect, createVNode } from 'vue';

const Collapase = /* @__PURE__ */ defineComponent({
  name: 'Collapase',
  props: {
    open: {
      type: Boolean
    }
  },
  emits: ['open', 'end'],
  setup(props, {
    emit,
    slots,
    expose
  }) {
    let dom;
    function transitionEventEnd() {
      if (props.open) {
        dom ? dom.style.height = 'auto' : false;
      }
      emit('end', props.open);
    }
    onMounted(() => {
      watchEffect(() => {
        const open = props.open;
        if (!dom) {
          return;
        }
        if (open) {
          dom.style.height = 'auto';
          const h = dom.getBoundingClientRect().height;
          props.onOpen && props.onOpen(h);
          dom.style.height = '0px';
          dom.classList.add('cm-collapase-open');
          setTimeout(() => {
            dom.style.height = `${h}px`;
          }, 0);
        } else {
          const h = dom.getBoundingClientRect().height;
          dom.classList.add('animation');
          dom.classList.remove('cm-collapase-open');
          dom.style.height = `${h}px`;
          setTimeout(() => {
            dom.style.height = '0px';
          }, 0);
        }
      });
    });
    expose({
      getHeight() {
        const orignHeight = dom.style.height;
        dom.style.transition = 'none';
        dom.style.height = 'auto';
        const oh = dom.offsetHeight;
        dom.style.transition = '';
        dom.style.height = orignHeight;
        return oh;
      }
    });
    return () => createVNode("div", {
      "class": "cm-collapase",
      "onTransitionend": transitionEventEnd,
      "ref": el => dom = el
    }, [slots.default?.()]);
  }
});

export { Collapase as default };
