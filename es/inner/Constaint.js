var TreeCheckMod = /* @__PURE__ */ ((TreeCheckMod2) => {
  TreeCheckMod2["FULL"] = "FULL";
  TreeCheckMod2["HALF"] = "HALF";
  TreeCheckMod2["CHILD"] = "CHILD";
  TreeCheckMod2["SHALLOW"] = "SHALLOW";
  return TreeCheckMod2;
})(TreeCheckMod || {});

export { TreeCheckMod };
