import { defineComponent, VueElement, inject, createVNode } from 'vue';
import { BreadcrumbContext } from './index.js';
import Space from '../Space/index.js';

const Item = /* @__PURE__ */ defineComponent({
  name: 'BreadcrumbItem',
  props: {
    link: {
      type: String
    },
    icon: {
      type: Object
    },
    separator: {
      type: [String, VueElement]
    }
  },
  setup(props, {
    slots
  }) {
    const ctx = inject(BreadcrumbContext);
    return () => createVNode("span", {
      "class": "cm-breadcrumb-wrap"
    }, [createVNode("a", {
      "class": "cm-breadcrumb-item",
      "href": props.link
    }, [createVNode(Space, {
      "size": 4,
      "align": "center"
    }, {
      default: () => [props.icon, slots.default && slots.default()]
    })]), createVNode("span", {
      "class": "cm-breadcrumb-separator"
    }, [ctx.separator || props.separator || '/'])]);
  }
});

export { Item as default };
