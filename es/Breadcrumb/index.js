import { defineComponent, VueElement, provide, createVNode } from 'vue';
import Item from './Item.js';

const BreadcrumbContext = Symbol('BreadcrumbContext');
const Breadcrumb = /* @__PURE__ */ defineComponent({
  name: 'Breadcrumb',
  props: {
    separator: {
      type: [String, VueElement]
    }
  },
  setup(props, {
    slots
  }) {
    provide(BreadcrumbContext, {
      separator: props.separator
    });
    return () => createVNode("div", {
      "class": "cm-breadcrumb"
    }, [slots.default?.()]);
  }
});
Breadcrumb.Item = Item;

export { BreadcrumbContext, Breadcrumb as default };
