import { defineComponent, createVNode } from 'vue';

const FooterNavigation = /* @__PURE__ */ defineComponent({
  name: "FooterNavigation",
  props: {
    head: {
      type: [String, Object]
    }
  },
  setup(props, {
    slots
  }) {
    return () => createVNode("div", {
      "class": "cm-page-footer-navigation"
    }, [createVNode("dl", null, [createVNode("dt", null, [props.head || slots.head?.()]), slots.default?.()])]);
  }
});
const Link = /* @__PURE__ */ defineComponent({
  name: "Link",
  props: {
    icon: {
      type: Object
    },
    link: {
      type: String,
      required: true
    },
    style: {
      type: Object
    }
  },
  setup(props, {
    slots
  }) {
    return () => createVNode("dd", {
      "class": "cm-page-footer-navigation-link"
    }, [createVNode("a", {
      "href": props.link,
      "target": "_blank",
      "style": props.style
    }, [props.icon, slots.default?.()])]);
  }
});
FooterNavigation.Link = Link;

export { FooterNavigation };
