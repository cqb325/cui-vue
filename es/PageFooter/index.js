import { defineComponent, createVNode } from 'vue';
export { FooterNavigation } from './Navigations.js';

const index = /* @__PURE__ */ defineComponent({
  name: 'PageFooter',
  setup(props, {
    slots
  }) {
    return () => createVNode("div", {
      "class": "cm-page-footer"
    }, [slots.default?.()]);
  }
});

export { index as default };
