import { defineComponent, createVNode } from 'vue';
import { useClassList } from '../use/useClassList.js';
import { useStyle } from '../use/useStyle.js';

const Icon = /* @__PURE__ */ defineComponent({
  name: 'Icon',
  props: {
    name: {
      type: String
    },
    spin: {
      type: Boolean
    },
    size: {
      type: Number
    },
    color: {
      type: String
    }
  },
  emits: ['click'],
  setup(props, {
    slots,
    emit
  }) {
    const classList = () => useClassList('cm-icon', `cm-icon-${props.name}`, {
      'cm-icon-spin': props.spin
    });
    let newStyle = () => useStyle({
      'font-size': (props.size || 14) + 'px',
      color: props.color
    });
    return () => createVNode("div", {
      "onClick": e => emit('click', e),
      "class": classList(),
      "style": newStyle()
    }, [slots.default]);
  }
});

export { Icon as default };
