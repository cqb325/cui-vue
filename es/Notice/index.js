import { reactive, createApp, createVNode } from 'vue';
import usePortal from '../use/usePortal.js';
import { Notices } from './Notices.js';
import { createUniqueId } from '../use/createUniqueId.js';
import usezIndex from '../use/usezIndex.js';

/**
 * Notice
 * @returns
 */
function Notice() {
  const store = reactive({
    topLeft: [],
    topRight: [],
    bottomLeft: [],
    bottomRight: []
  });
  const onClose = (key, dock) => {
    const arr = store[dock].filter(item => {
      return item.key !== key;
    });
    store[dock] = arr;
  };
  const ele = usePortal('cm-notice-portal', 'cm-notices-wrap');
  createApp(() => createVNode(Notices, {
    "data": store,
    "onClose": onClose
  }, null)).mount(ele);
  return {
    open(config) {
      if (!config.dock) {
        config.dock = 'topRight';
      }
      if (config.key === undefined) {
        config.key = createUniqueId();
      }
      if (config.duration === undefined) {
        config.duration = 4.5;
      }
      store[config.dock].push(config);
      ele.style.zIndex = usezIndex() + '';
    },
    info(config) {
      config.theme = 'info';
      this.open(config);
    },
    success(config) {
      config.theme = 'success';
      this.open(config);
    },
    warning(config) {
      config.theme = 'warning';
      this.open(config);
    },
    error(config) {
      config.theme = 'error';
      this.open(config);
    },
    help(config) {
      config.theme = 'help';
      this.open(config);
    }
  };
}
const notice = Notice();

export { notice };
