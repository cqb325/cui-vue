function findComponentUpward(wantFind, _context) {
  const parent = _context.parent;
  if (parent && parent.type.name && parent.type.name === wantFind) {
    return parent;
  } else {
    if (parent.parent) {
      return findComponentUpward(wantFind, parent);
    }
  }
  return null;
}
function scrollTop(el, from = 0, to, duration = 500, endCallback) {
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback) {
      return window.setTimeout(callback, 1e3 / 60);
    };
  }
  const difference = Math.abs(from - to);
  const step = Math.ceil(difference / duration * 50);
  function scroll(start, end, step2) {
    if (start === end) {
      endCallback && endCallback();
      return;
    }
    let d = start + step2 > end ? end : start + step2;
    if (start > end) {
      d = start - step2 < end ? end : start - step2;
    }
    if (el === window) {
      window.scrollTo(d, d);
    } else {
      el.scrollTop = d;
    }
    window.requestAnimationFrame(() => scroll(d, end, step2));
  }
  scroll(from, to, step);
}
function getRandomIntInclusive(min, max) {
  const randomBuffer = new Uint32Array(1);
  window.crypto.getRandomValues(randomBuffer);
  const randomNumber = randomBuffer[0] / (4294967295 + 1);
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(randomNumber * (max - min + 1)) + min;
}
function getRandomNumber() {
  const randomBuffer = new Uint32Array(1);
  window.crypto.getRandomValues(randomBuffer);
  return randomBuffer[0] / (4294967295 + 1);
}
const uuidv4 = () => "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => (c === "x" ? 15 & getRandomNumber() * 16 : 3 & getRandomNumber() * 16 | 8).toString(16)).replace(/-/g, "");
function isColor(strColor) {
  if (strColor && (strColor.startsWith("#") || strColor.startsWith("rgb") || strColor.startsWith("hsl"))) {
    const s = new Option().style;
    s.color = strColor;
    return s.color.startsWith("rgb");
  }
  return false;
}

export { findComponentUpward, getRandomIntInclusive, getRandomNumber, isColor, scrollTop, uuidv4 };
