import { defineComponent, createVNode, mergeProps, Fragment } from 'vue';
import { innerPopupDefinedProps, InnerPopup } from '../inner/InnerPopup.js';

const index = /* @__PURE__ */ defineComponent({
  name: "Popconfirm",
  props: {
    ...innerPopupDefinedProps,
    showCancel: {
      type: Boolean,
      default: true
    }
  },
  emits: ['update:modelValue', 'visibleChange'],
  setup(props, {
    slots,
    emit,
    attrs,
    expose
  }) {
    return () => createVNode(InnerPopup, mergeProps(props, {
      "clsPrefix": "cm-popconfirm",
      "varName": "popconfirm"
    }, attrs, {
      "theme": props.theme || "light",
      "confirm": true,
      "align": props.align || 'top',
      "title": createVNode(Fragment, null, [props.icon, createVNode("div", {
        "class": "cm-popconfirm-title-text"
      }, [props.title])]),
      "onUpdate:modelValue": val => emit('update:modelValue', val),
      "onVisibleChange": val => emit('visibleChange', val)
    }), {
      default: () => [slots.default?.()]
    });
  }
});

export { index as default };
