import { defineComponent, computed, provide, createVNode } from 'vue';
import { createGutter } from '../use/createGridOffset.js';

const RowContext = Symbol('RowContext');
const index = /* @__PURE__ */ defineComponent({
  name: 'Row',
  props: {
    justify: {
      type: String
    },
    align: {
      type: String
    },
    gutter: {
      type: [Number, Array, Object]
    }
  },
  setup(props, {
    slots
  }) {
    const gutterClass = typeof props.gutter === 'object' ? createGutter(props.gutter) : {};
    const classList = computed(() => ({
      'cm-row': true,
      ...gutterClass,
      [`cm-row-${props.justify}`]: props.justify,
      [`cm-row-${props.align}`]: props.align
    }));
    const newStyle = computed(() => {
      const obj = {};
      let half = 0;
      let rowGutter = 0;
      if (typeof props.gutter === 'number') {
        half = props.gutter ? props.gutter / 2 : 0;
        rowGutter = props.gutter || 0;
      }
      if (Array.isArray(props.gutter)) {
        half = props.gutter[0] ? props.gutter[0] / 2 : 0;
        rowGutter = props.gutter[1] || 0;
      }
      if (half) {
        obj['margin-left'] = `-${half}px`;
        obj['margin-right'] = `-${half}px`;
      }
      if (rowGutter) {
        obj['row-gap'] = `${rowGutter}px`;
      }
      return obj;
    });
    provide(RowContext, props.gutter);
    return () => createVNode("div", {
      "class": classList.value,
      "style": newStyle.value
    }, [slots.default?.()]);
  }
});

export { RowContext, index as default };
