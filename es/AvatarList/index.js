import { defineComponent, computed, createVNode, createTextVNode, isVNode } from 'vue';
import Tooltip from '../Tooltip/index.js';
import Avatar from '../Avatar/index.js';
import useChildren from '../use/useChildren.js';

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !isVNode(s);
}
const index = /* @__PURE__ */ defineComponent({
  name: 'AvatarList',
  props: {
    size: {
      type: String
    },
    align: {
      type: String,
      default: 'top'
    },
    gutter: {
      type: Number
    },
    max: {
      type: Number,
      default: Number.MAX_VALUE
    },
    excessStyle: {
      type: Object
    }
  },
  setup(props, {
    slots
  }) {
    const classList = computed(() => ({
      'cm-avatar-list': true,
      [`cm-avatar-list-${props.size}`]: props.size
    }));
    const avatarsLength = computed(() => slots.default && slots.default().length);
    const gutter = computed(() => (props.gutter ?? (props.size === 'small' ? -8 : -12)) + 'px');
    const children = () => {
      const childs = useChildren({
        slots,
        props: {
          size: props.size
        },
        sameType: Avatar
      });
      if (childs) {
        const ret = [];
        childs.forEach((child, index) => {
          if (index < props.max) {
            ret.push(createVNode("div", {
              "key": index,
              "class": "cm-avatar-list-item",
              "style": {
                'margin-left': index > 0 ? gutter.value : 0
              }
            }, [createVNode(Tooltip, {
              "align": props.align,
              "content": child.props.title
            }, _isSlot(child) ? child : {
              default: () => [child]
            })]));
          }
        });
        return ret;
      }
      return null;
    };
    return () => createVNode("div", {
      "class": classList.value
    }, [children(), avatarsLength.value > props.max ? createVNode("div", {
      "class": "cm-avatar-list-item"
    }, [createVNode(Avatar, {
      "size": props.size,
      "style": props.excessStyle
    }, {
      default: () => [createTextVNode("+"), avatarsLength.value - props.max]
    })]) : null]);
  }
});

export { index as default };
