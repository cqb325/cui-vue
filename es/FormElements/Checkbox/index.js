import { defineComponent, createVNode, mergeProps } from 'vue';
import Checkbox$1 from '../../inner/Checkbox.js';
import formFieldRef from '../../use/formFieldRef.js';

const Checkbox = /* @__PURE__ */ defineComponent({
  name: 'Checkbox',
  props: {
    disabled: {
      type: Boolean
    },
    type: {
      type: String
    },
    name: {
      type: String
    },
    label: {
      type: [String, Object]
    },
    inner: {
      type: Boolean
    },
    modelValue: {
      type: [Boolean, String]
    },
    value: {
      type: [String, Number]
    },
    asFormField: {
      type: Boolean,
      default: true
    }
  },
  emits: ['change', 'update:modelValue'],
  setup(props, {
    emit
  }) {
    const checked = formFieldRef(props, emit);
    const onChange = (checkedVal, value) => {
      if (props.disabled) {
        return;
      }
      checked.value = checkedVal;
      emit('change', checkedVal, value);
    };
    return () => createVNode(Checkbox$1, mergeProps(props, {
      "onChange": onChange,
      "checked": checked.value
    }), null);
  }
});

export { Checkbox as default };
