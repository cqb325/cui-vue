import { defineComponent, createVNode, mergeProps } from 'vue';
import formFieldRef from '../../use/formFieldRef.js';
import Checkbox from '../../inner/Checkbox.js';

const Radio = /* @__PURE__ */ defineComponent({
  name: 'Radio',
  props: {
    disabled: {
      type: Boolean
    },
    type: {
      type: String
    },
    name: {
      type: String
    },
    label: {
      type: [String, Object]
    },
    inner: {
      type: Boolean
    },
    modelValue: {
      type: [Boolean, String]
    },
    value: {
      type: [String, Number]
    },
    asFormField: {
      type: Boolean,
      default: true
    }
  },
  emits: ['change', 'update:modelValue'],
  setup(props, {
    emit
  }) {
    const checked = formFieldRef(props, emit);
    const onChange = (checkedVal, value) => {
      if (props.disabled) {
        return;
      }
      checked.value = checkedVal;
      emit('change', checkedVal, value);
    };
    return () => createVNode(Checkbox, mergeProps(props, {
      "class": "cm-radio",
      "type": props.type ?? 'radio',
      "onChange": onChange,
      "checked": checked.value
    }), null);
  }
});

export { Radio as default };
