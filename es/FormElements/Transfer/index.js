import { defineComponent, reactive, watchEffect, createVNode, isVNode } from 'vue';
import List from './List.js';
import Button from '../../Button/index.js';
import formFieldRef from '../../use/formFieldRef.js';
import { FeatherChevronRight, FeatherChevronLeft } from 'cui-vue-icons/feather';

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !isVNode(s);
}
const index = /* @__PURE__ */ defineComponent({
  name: "Transfer",
  props: {
    width: {
      type: Number
    },
    height: {
      type: Number
    },
    data: {
      type: Array
    },
    modelValue: {
      type: Array
    },
    filter: {
      type: Boolean
    },
    rightText: {
      type: String
    },
    leftText: {
      type: String
    },
    sourceTitle: {
      type: String
    },
    targetTitle: {
      type: String
    },
    asFormField: {
      type: Boolean,
      default: true
    },
    render: {
      type: Function
    }
  },
  emits: ['change', 'update:modelValue'],
  setup(props, {
    emit
  }) {
    const value = formFieldRef(props, emit, []);
    const store = reactive({
      data: [],
      sourceDisabled: true,
      targetDisabled: true,
      sourceIds: [],
      targetIds: []
    });
    const rightText = props.rightText || 'To Right';
    const leftText = props.leftText || 'To Left';
    watchEffect(() => {
      store.data = props.data || [];
    });
    const onSelect = (data, checked) => {
      if (!data.disabled) {
        const item = store.data.find(item => {
          return item.id === data.id;
        });
        item._checked = checked;
      }
    };
    const transferToTarget = () => {
      store.sourceIds.forEach(id => {
        const item = store.data.find(item => {
          return item.id === id;
        });
        item._checked = false;
      });
      let v = value.value;
      v = v.concat([...store.sourceIds]);
      store.sourceIds = [];
      value.value = [...v];
      emit('change', [...v]);
    };
    const transferToSource = () => {
      store.targetIds.forEach(id => {
        const item = store.data.find(item => {
          return item.id === id;
        });
        item._checked = false;
      });
      const v = value.value;
      store.targetIds.forEach(id => {
        v.splice(v.indexOf(id), 1);
      });
      store.targetIds = [];
      value.value = [...v];
      emit('change', [...v]);
    };
    return () => createVNode("div", {
      "class": "cm-transfer"
    }, [createVNode(List, {
      "width": props.width,
      "height": props.height,
      "store": store,
      "name": "source",
      "value": value.value,
      "onSelect": onSelect,
      "filter": props.filter,
      "render": props.render,
      "title": props.sourceTitle
    }, null), createVNode("div", {
      "class": "cm-transfer-operation"
    }, [createVNode(Button, {
      "disabled": store.sourceDisabled,
      "icon": createVNode(FeatherChevronRight, null, null),
      "size": "small",
      "onClick": transferToTarget
    }, _isSlot(rightText) ? rightText : {
      default: () => [rightText]
    }), createVNode(Button, {
      "disabled": store.targetDisabled,
      "icon": createVNode(FeatherChevronLeft, null, null),
      "size": "small",
      "onClick": transferToSource
    }, _isSlot(leftText) ? leftText : {
      default: () => [leftText]
    })]), createVNode(List, {
      "width": props.width,
      "height": props.height,
      "store": store,
      "name": "target",
      "value": value.value,
      "onSelect": onSelect,
      "filter": props.filter,
      "render": props.render,
      "title": props.targetTitle
    }, null)]);
  }
});

export { index as default };
