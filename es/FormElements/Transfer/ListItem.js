import { defineComponent, computed, createVNode } from 'vue';
import Checkbox from '../Checkbox/index.js';

const ListItem = /* @__PURE__ */ defineComponent({
  name: 'ListItem',
  props: {
    data: {
      type: Object
    },
    render: {
      type: Function
    }
  },
  emits: ['select'],
  setup(props, {
    emit
  }) {
    const text = () => {
      if (props.render) {
        return props.render(props.data);
      }
      return props.data.title;
    };
    const onSelect = () => {
      console.log(111);
      if (!props.data.disabled) {
        emit('select', props.data);
      }
    };
    const style = computed(() => ({
      display: props.data._hide ? 'none' : 'flex'
    }));
    return () => createVNode("div", {
      "class": "cm-transfer-list-item",
      "onClick": onSelect,
      "style": style.value
    }, [createVNode(Checkbox, {
      "modelValue": props.data._checked,
      "onChange": onSelect,
      "disabled": props.data.disabled
    }, null), createVNode("div", null, [text()])]);
  }
});

export { ListItem as default };
