import { defineComponent, watchEffect, createVNode } from 'vue';
import BothSide from '../../BothSide/index.js';
import Checkbox from '../Checkbox/index.js';
import Input from '../Input/index.js';
import ListItem from './ListItem.js';
import { FeatherSearch } from 'cui-vue-icons/feather';

const List = /* @__PURE__ */ defineComponent({
  name: 'TransferList',
  props: {
    width: {
      type: Number
    },
    height: {
      type: Number
    },
    store: {
      type: Object
    },
    name: {
      type: String
    },
    value: {
      type: Array
    },
    title: {
      type: String
    },
    render: {
      type: Function
    },
    filter: {
      type: Boolean
    }
  },
  emits: ['select'],
  setup(props, {
    emit
  }) {
    const style = () => ({
      width: props.width ? `${props.width}px` : '',
      height: props.height ? `${props.height}px` : ''
    });
    const title = props.title ?? (props.name === 'source' ? '源列表' : '目标列表');
    const data = () => {
      const v = props.value || [];
      const map = {};
      v.forEach(vv => {
        map[vv] = true;
      });
      return props.store.data.filter(item => {
        if (props.name === 'source') {
          return !map[item.id];
        } else {
          return map[item.id];
        }
      });
    };
    const validLength = () => {
      let length = 0;
      data().forEach(item => {
        if (!item.disabled) {
          length++;
        }
      });
      return length;
    };
    const onSelect = data => {
      emit('select', data, !data._checked);
      if (data._checked) {
        props.store[`${props.name}Ids`] = [...props.store[`${props.name}Ids`], data.id];
      } else {
        const arr = props.store[`${props.name}Ids`];
        arr.splice(arr.indexOf(data.id), 1);
        props.store[`${props.name}Ids`] = arr;
      }
    };
    const isCheckedAll = () => {
      const arr = props.store[`${props.name}Ids`];
      if (arr.length > 0) {
        if (validLength() === arr.length) {
          return true;
        } else {
          return 'indeterminate';
        }
      } else {
        return false;
      }
    };
    const onCheckedAll = checked => {
      const ids = [];
      const items = data();
      items.forEach(item => {
        emit('select', item, checked);
      });
      items.forEach(item => {
        if (item._checked) {
          ids.push(item.id);
        }
      });
      props.store[`${props.name}Ids`] = ids;
    };
    watchEffect(() => {
      const arr = props.store[`${props.name}Ids`];
      if (arr.length) {
        props.store[`${props.name}Disabled`] = false;
      } else {
        props.store[`${props.name}Disabled`] = true;
      }
    });

    // 过滤
    const onFilter = v => {
      const arr = data();
      arr.forEach(item => {
        const text = () => {
          if (props.render) {
            return props.render(item);
          }
          return item.title;
        };
        const findAItem = props.store.data.find(aitem => {
          return aitem.id === item.id;
        });
        if (findAItem) {
          findAItem._hide = !text().includes(v);
        }
      });
    };
    const count = () => data().length;
    const countInfo = () => {
      const arr = props.store[`${props.name}Ids`];
      return arr.length ? arr.length + '/' + count() : count();
    };
    return () => createVNode("div", {
      "class": "cm-transfer-list",
      "style": style()
    }, [createVNode("div", {
      "class": "cm-transfer-list-header"
    }, [createVNode(BothSide, null, {
      default: () => [createVNode("div", null, [createVNode(Checkbox, {
        "modelValue": isCheckedAll(),
        "onChange": onCheckedAll
      }, null), createVNode("span", null, [title])]), createVNode("div", {
        "class": ""
      }, [countInfo()])]
    })]), createVNode("div", {
      "class": "cm-transfer-list-body"
    }, [props.filter ? createVNode("div", {
      "class": "cm-transfer-filter-wrap"
    }, [createVNode(Input, {
      "append": createVNode(FeatherSearch, null, null),
      "size": "small",
      "onInput": onFilter
    }, null)]) : null, createVNode("div", {
      "class": "cm-transfer-list-content"
    }, [data().map(item => {
      return createVNode(ListItem, {
        "data": item,
        "onSelect": onSelect,
        "render": props.render
      }, null);
    })])])]);
  }
});

export { List as default };
