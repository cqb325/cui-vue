import { defineComponent, createVNode, Fragment } from 'vue';

const Recommend = /* @__PURE__ */ defineComponent({
  name: 'Recommend',
  props: {
    colors: {
      type: Array
    }
  },
  emits: ['change'],
  setup(props, {
    emit
  }) {
    const colors = props.colors ?? ['#2d8cf0', '#19be6b', '#ff9900', '#ed4014', '#00b5ff', '#19c919', '#f9e31c', '#ea1a1a', '#9b1dea', '#00c2b1', '#ac7a33', '#1d35ea', '#8bc34a', '#f16b62', '#ea4ca3', '#0d94aa', '#febd79', '#5d4037', '#00bcd4', '#f06292', '#cddc39', '#607d8b', '#000000', '#ffffff'];
    const onClick = color => {
      emit('change', {
        hex: color,
        source: 'hex'
      });
    };
    return () => createVNode("div", {
      "class": "cm-color-picker-recommend"
    }, [createVNode("div", {
      "class": "cm-color-picker-recommend-container"
    }, [colors.map((color, index) => {
      return createVNode(Fragment, null, [createVNode("div", {
        "class": "cm-color-picker-recommend-color",
        "onClick": () => onClick(color)
      }, [createVNode("div", {
        "style": {
          background: color
        }
      }, null)]), (index + 1) % 12 === 0 ? createVNode("br", null, null) : null]);
    })])]);
  }
});

export { Recommend };
