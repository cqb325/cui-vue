import { defineComponent, onUnmounted, createVNode } from 'vue';
import { clamp } from './utils.js';

const Saturation = /* @__PURE__ */ defineComponent({
  name: 'Saturation',
  props: {
    value: {
      type: Object
    }
  },
  emits: ['change'],
  setup(props, {
    emit
  }) {
    let container;
    const handleMouseDown = e => {
      if (typeof e.button === 'number' && e.button !== 0) return false;
      handleChange(e);
      document.addEventListener('mousemove', handleChange, false);
      document.addEventListener('mouseup', onDragEnd, false);
    };
    const onDragEnd = e => {
      handleChange(e);
      document.removeEventListener('mousemove', handleChange);
      document.removeEventListener('mouseup', onDragEnd);
    };
    onUnmounted(() => {
      document.removeEventListener('mousemove', handleChange);
      document.removeEventListener('mouseup', onDragEnd);
    });
    const handleChange = e => {
      e.preventDefault();
      e.stopPropagation();
      const {
        clientWidth,
        clientHeight
      } = container;
      const xOffset = container.getBoundingClientRect().left + window.screenX;
      const yOffset = container.getBoundingClientRect().top + window.screenY;
      const left = clamp(e.clientX - xOffset, 0, clientWidth);
      const top = clamp(e.clientY - yOffset, 0, clientHeight);
      const saturation = left / clientWidth;
      const bright = clamp(1 - top / clientHeight, 0, 1);
      emit('change', {
        h: props.value.hsv.h,
        s: saturation,
        v: bright,
        a: props.value.hsv.a,
        source: 'hsva'
      });
    };
    const bgColorStyle = () => ({
      background: `hsl(${props.value.hsv.h}, 100%, 50%)`
    });
    const pointerStyle = () => ({
      top: `${-(props.value.hsv.v * 100) + 1 + 100}%`,
      left: `${props.value.hsv.s * 100}%`
    });
    return () => createVNode("div", {
      "class": "cm-saturation",
      "style": bgColorStyle(),
      "onMousedown": handleMouseDown,
      "ref": el => container = el
    }, [createVNode("div", {
      "class": "cm-saturation-white"
    }, null), createVNode("div", {
      "class": "cm-saturation-black"
    }, null), createVNode("div", {
      "class": "cm-saturation-pointer",
      "style": pointerStyle()
    }, [createVNode("div", {
      "class": "cm-saturation-circle"
    }, null)])]);
  }
});

export { Saturation };
