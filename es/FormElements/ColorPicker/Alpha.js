import { defineComponent, ref, onUnmounted, watchEffect, createVNode } from 'vue';
import { toRGBAString } from './utils.js';

const Alpha = /* @__PURE__ */ defineComponent({
  name: 'Alpha',
  props: {
    value: {
      type: Object
    }
  },
  emits: ['change'],
  setup(props, {
    emit
  }) {
    const left = ref(props.value.hsl.a * 100);
    const gradientStyle = () => {
      const {
        r,
        g,
        b
      } = props.value.rgba;
      const start = toRGBAString({
        r,
        g,
        b,
        a: 0
      });
      const finish = toRGBAString({
        r,
        g,
        b,
        a: 1
      });
      return {
        background: `linear-gradient(to right, ${start} 0%, ${finish} 100%)`
      };
    };
    let container;
    const handleMouseDown = e => {
      if (typeof e.button === 'number' && e.button !== 0) return false;
      handleChange(e);
      document.addEventListener('mousemove', handleChange, false);
      document.addEventListener('mouseup', onDragEnd, false);
    };
    const onDragEnd = e => {
      handleChange(e);
      document.removeEventListener('mousemove', handleChange);
      document.removeEventListener('mouseup', onDragEnd);
    };
    onUnmounted(() => {
      document.removeEventListener('mousemove', handleChange);
      document.removeEventListener('mouseup', onDragEnd);
    });
    const handleChange = e => {
      e.preventDefault();
      e.stopPropagation();
      const {
        clientWidth
      } = container;
      const xOffset = container.getBoundingClientRect().left + window.screenX;
      const left = e.clientX - xOffset;
      if (left < 0) {
        change(0);
        return;
      }
      if (left > clientWidth) {
        change(1);
        return;
      }
      change(Math.round(left * 100 / clientWidth) / 100);
    };
    const change = newAlpha => {
      left.value = newAlpha * 100;
      const {
        h,
        s,
        l,
        a
      } = props.value.hsl;
      if (a !== newAlpha) {
        emit('change', {
          h,
          s,
          l,
          a: newAlpha,
          source: 'rgba'
        });
      }
    };
    watchEffect(() => {
      left.value = props.value.hsl.a * 100;
    });
    return () => createVNode("div", {
      "class": "cm-color-picker-alpha",
      "ref": el => container = el
    }, [createVNode("div", {
      "class": "cm-color-picker-alpha-wrap",
      "style": gradientStyle(),
      "onMousedown": handleMouseDown
    }, [createVNode("div", {
      "class": "cm-color-picker-alpha-picker",
      "style": {
        left: `${left.value}%`,
        top: '0px'
      }
    }, null)])]);
  }
});

export { Alpha };
