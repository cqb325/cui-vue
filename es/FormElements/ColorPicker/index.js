import { defineComponent, ref, computed, watchEffect, createVNode, createTextVNode } from 'vue';
import { Value } from './Value.js';
import { Saturation } from './Saturation.js';
import { changeColor, toRGBAString } from './utils.js';
import { Hue } from './Hue.js';
import { Alpha } from './Alpha.js';
import { Recommend } from './Recommend.js';
import createField from '../../use/createField.js';
import Dropdown from '../../Dropdown/index.js';
import Space from '../../Space/index.js';
import Input from '../Input/index.js';
import Button from '../../Button/index.js';

const index = /* @__PURE__ */ defineComponent({
  name: 'ColorPicker',
  props: {
    name: {
      type: String,
      default: ''
    },
    modelValue: {
      type: String,
      default: ''
    },
    transfer: {
      type: Boolean,
      default: false
    },
    inline: {
      type: Boolean,
      default: false
    },
    align: {
      type: String,
      default: 'bottomLeft'
    },
    disabled: {
      type: Boolean,
      default: false
    },
    alpha: {
      type: Boolean,
      default: false
    },
    size: {
      type: String,
      default: ''
    },
    recommend: {
      type: Boolean,
      default: false
    },
    colors: {
      type: Array
    },
    asFormField: {
      type: Boolean,
      default: true
    }
  },
  emits: ['change', 'update:modelValue'],
  setup(props, {
    emit
  }) {
    const open = ref(false);
    const align = props.align ?? 'bottomLeft';
    const value = createField(props, emit, '');
    const val = ref(changeColor(value.value || '#2D8CF0'));
    const confirmVal = ref('');
    let oldHue = val.value;
    const classList = computed(() => ({
      'cm-color-picker': true,
      [`cm-color-picker-${props.size}`]: props.size,
      'cm-color-picker-disabled': props.disabled
    }));
    const inlineClassList = computed(() => ({
      'cm-color-picker-wrap': true,
      'cm-color-picker-inline': props.inline,
      'cm-color-picker-disabled': props.disabled
    }));
    const onColorChange = data => {
      colorChange(data);
    };
    const colorChange = (data, _oldHue) => {
      oldHue = val.value.hsl.h;
      val.value = changeColor(data, _oldHue || oldHue);
    };
    const onConfirm = () => {
      open.value = false;
      value.value = confirmVal.value;
      emit('change', confirmVal.value);
    };
    const onClear = () => {
      open.value = false;
      value.value = '';
      emit('change', '');
    };
    watchEffect(() => {
      if (props.alpha) {
        confirmVal.value = toRGBAString(val.value.rgba);
      } else {
        confirmVal.value = val.value.hex;
      }
    });
    watchEffect(() => {
      const v = changeColor(confirmVal.value);
      val.value = v;
    });
    const renderMain = () => {
      return createVNode(Space, {
        "dir": "v"
      }, {
        default: () => [createVNode(Saturation, {
          "value": val.value,
          "onChange": onColorChange
        }, null), createVNode(Hue, {
          "value": val.value,
          "onChange": onColorChange
        }, null), props.alpha && createVNode(Alpha, {
          "value": val.value,
          "onChange": onColorChange
        }, null), props.recommend && createVNode(Recommend, {
          "colors": props.colors,
          "onChange": onColorChange
        }, null), createVNode("div", {
          "class": "cm-color-picker-confirm"
        }, [createVNode(Space, {
          "dir": "h"
        }, {
          default: () => [createVNode(Input, {
            "size": "small",
            "class": "cm-color-picker-input",
            "modelValue": confirmVal.value,
            "onUpdate:modelValue": $event => confirmVal.value = $event
          }, null), createVNode(Button, {
            "size": "small",
            "type": "default",
            "onClick": onClear
          }, {
            default: () => [createTextVNode("\u6E05\u9664")]
          }), createVNode(Button, {
            "size": "small",
            "type": "primary",
            "onClick": onConfirm
          }, {
            default: () => [createTextVNode("\u786E\u5B9A")]
          })]
        })])]
      });
    };
    return () => props.inline ? createVNode("div", {
      "class": inlineClassList.value
    }, [renderMain()]) : createVNode("div", {
      "class": classList.value
    }, [createVNode(Dropdown, {
      "transfer": props.transfer,
      "align": align,
      "disabled": props.disabled,
      "trigger": "click",
      "modelValue": open.value,
      "onUpdate:modelValue": $event => open.value = $event,
      "menu": createVNode("div", {
        "class": "cm-color-picker-wrap"
      }, [renderMain()])
    }, {
      default: () => [createVNode(Value, {
        "disabled": props.disabled,
        "currentValue": val.value,
        "value": value.value,
        "open": open.value
      }, null)]
    })]);
  }
});

export { index as default };
