import { defineComponent, ref, watchEffect, computed, createVNode, createTextVNode } from 'vue';
import { FeatherX } from 'cui-vue-icons/feather';

const Value = /* @__PURE__ */ defineComponent({
  name: "Value",
  props: {
    open: {
      type: Boolean
    },
    disabled: {
      type: Boolean
    },
    currentValue: {
      type: Object
    },
    value: {
      type: String
    },
    name: {
      type: String
    }
  },
  setup(props) {
    const bgColorStyle = ref({});
    watchEffect(() => {
      const style = props.open ? {
        background: `rgba(${props.currentValue.rgba.r},${props.currentValue.rgba.g},${props.currentValue.rgba.b},${props.currentValue.rgba.a})`
      } : {
        background: props.value
      };
      bgColorStyle.value = style;
    });
    const classList = computed(() => ({
      'cm-color-picker-value': true
    }));
    return () => createVNode("div", {
      "class": classList.value,
      "tab-index": "0"
    }, [createVNode("span", {
      "style": {
        width: '0px',
        "font-size": '12px',
        visibility: 'hidden',
        'line-height': 'initial'
      }
    }, [createTextVNode("A")]), createVNode("input", {
      "type": "hidden",
      "name": props.name,
      "value": props.value
    }, null), createVNode("div", {
      "class": "cm-select-color-wrap"
    }, [bgColorStyle.value.background ? createVNode("div", {
      "class": "cm-select-color",
      "style": bgColorStyle.value
    }, null) : createVNode("div", {
      "class": "cm-select-color cm-select-color-empty"
    }, [createVNode(FeatherX, null, null)])])]);
  }
});

export { Value };
