import { defineComponent, inject, ref, createVNode, mergeProps } from 'vue';
import DatePane from './DatePane.js';
import TimePane from '../TimePicker/TimePane.js';
import dayjs from 'dayjs';
import { FeatherCalendar, FeatherClock } from 'cui-vue-icons/feather';

const DateTimePane = /* @__PURE__ */ defineComponent({
  name: "DateTimePane",
  props: {
    name: String,
    value: [String, Date]
  },
  setup(props) {
    const ctx = inject('CMDatepickerContext', null);
    const tab = ref('date');
    const val = () => ctx.store.currentMonth[props.name === 'end' ? 1 : 0];
    const displayDate = () => {
      return dayjs(props.value || new Date()).format('YYYY-MM-DD');
    };
    const displayTime = () => {
      return dayjs(val()).format('HH:mm:ss');
    };
    const selectTab = atab => {
      tab.value = atab;
    };
    const onSelectTime = (type, num, name) => {
      const v = new Date(val());
      if (type === 'hour') {
        v.setHours(num);
      }
      if (type === 'minute') {
        v.setMinutes(num);
      }
      if (type === 'second') {
        v.setSeconds(num);
      }
      ctx && ctx.onSelectTime(v, props.name);
    };
    return () => createVNode("div", {
      "class": "cm-date-picker-datetime"
    }, [createVNode("div", {
      "class": "cm-datetime-content"
    }, [tab.value === 'date' ? createVNode(DatePane, props, null) : null, tab.value === 'time' ? createVNode(TimePane, mergeProps(props, {
      "format": ctx.format,
      "header": "\u9009\u62E9\u65F6\u95F4",
      "value": val(),
      "onSelectTime": onSelectTime
    }), null) : null]), createVNode("div", {
      "class": "cm-datetime-switch"
    }, [createVNode("div", {
      "class": {
        "cm-datetime-switch-item": true,
        'active': tab.value === 'date'
      },
      "onClick": selectTab.bind(null, 'date')
    }, [createVNode(FeatherCalendar, {
      "size": 12
    }, null), displayDate()]), createVNode("div", {
      "class": {
        "cm-datetime-switch-item": true,
        'active': tab.value === 'time'
      },
      "onClick": selectTab.bind(null, 'time')
    }, [createVNode(FeatherClock, {
      "size": 12
    }, null), displayTime()])])]);
  }
});

export { DateTimePane as default };
