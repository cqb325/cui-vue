import { defineComponent, createVNode, Fragment } from 'vue';
import MonthPane from './MonthPane.js';

const MonthRangePane = /* @__PURE__ */ defineComponent({
  name: 'MonthRangePane',
  props: {
    value: [String, Array]
  },
  setup(props, context) {
    const val1 = () => props.value ? props.value[0] : '';
    const val2 = () => props.value ? props.value[1] : '';
    return () => createVNode(Fragment, null, [createVNode(MonthPane, {
      "name": "start",
      "value": val1()
    }, null), createVNode(MonthPane, {
      "name": "end",
      "value": val2()
    }, null)]);
  }
});

export { MonthRangePane as default };
