import { defineComponent, createVNode, Fragment } from 'vue';
import DatePane from './DatePane.js';

const DateRangePane = /* @__PURE__ */ defineComponent({
  name: 'DateRangePane',
  props: {
    value: [String, Array]
  },
  setup(props) {
    const val1 = () => props.value[0];
    const val2 = () => props.value[1];
    return () => createVNode(Fragment, null, [createVNode(DatePane, {
      "name": "start",
      "value": val1()
    }, null), createVNode(DatePane, {
      "name": "end",
      "value": val2()
    }, null)]);
  }
});

export { DateRangePane as default };
