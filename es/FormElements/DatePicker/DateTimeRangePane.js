import { defineComponent, createVNode, Fragment } from 'vue';
import DateTimePane from './DateTimePane.js';

const DateTimeRangePane = /* @__PURE__ */ defineComponent({
  name: 'DateTimeRangePane',
  props: {
    value: Array
  },
  setup(props, context) {
    const val1 = () => props.value && props.value[0];
    const val2 = () => props.value && props.value[1];
    return () => createVNode(Fragment, null, [createVNode(DateTimePane, {
      "name": "start",
      "value": val1()
    }, null), createVNode(DateTimePane, {
      "name": "end",
      "value": val2()
    }, null)]);
  }
});

export { DateTimeRangePane as default };
