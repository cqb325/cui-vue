import { defineComponent, computed, ref, createVNode } from 'vue';
import RateItem from './Item.js';
import createField from '../../use/createField.js';

const index = /* @__PURE__ */ defineComponent({
  name: 'Rate',
  props: {
    disabled: {
      type: Boolean
    },
    icon: {
      type: Object
    },
    modelValue: {
      type: Number
    },
    count: {
      type: Number
    },
    allowHalf: {
      type: Boolean
    },
    name: {
      type: String
    },
    asFormField: {
      type: Boolean,
      default: true
    }
  },
  emits: ['change', 'update:modelValue'],
  setup(props, {
    emit,
    slots
  }) {
    const classList = computed(() => ({
      'cm-rate': true,
      'cm-rate-disabled': props.disabled
    }));
    if (!props.icon || slots.icon) {
      console.warn('need icon slot or property');
      return null;
    }
    const value = createField(props, emit);
    const current = ref(value.value);
    const allowHalf = props.allowHalf || false;
    const onMouseEnter = val => {
      current.value = val;
    };
    const onMouseEnterHalf = (val, e) => {
      if (!allowHalf) {
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      current.value = val;
    };
    const onMouseLeave = () => {
      current.value = value.value;
    };
    const onClickStar = val => {
      value.value = val;
      emit('change', val);
    };
    const onClickHalfStar = (val, e) => {
      e.preventDefault();
      e.stopPropagation();
      if (!allowHalf) {
        return;
      }
      value.value = val;
      emit('change', val);
    };
    const count = props.count || 5;
    const stars = [];
    for (let i = 0; i < count; i++) {
      stars.push({
        id: i,
        value: i
      });
    }
    return () => createVNode("div", {
      "class": classList.value,
      "onMouseleave": onMouseLeave
    }, [stars.map((item, index) => {
      return createVNode(RateItem, {
        "index": index,
        "onMouseEnterHalf": onMouseEnterHalf,
        "onClickHalfStar": onClickHalfStar,
        "onMouseEnter": onMouseEnter,
        "onClickStar": onClickStar,
        "icon": props.icon || slots.icon?.(),
        "allowHalf": allowHalf,
        "current": current.value
      }, null);
    }), createVNode("span", null, [slots.default?.()])]);
  }
});

export { index as default };
