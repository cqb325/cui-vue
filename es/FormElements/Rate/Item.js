import { defineComponent, createVNode } from 'vue';

const RateItem = /* @__PURE__ */ defineComponent({
  name: "RateItem",
  props: {
    icon: {
      type: Object
    },
    index: {
      type: Number
    },
    allowHalf: {
      type: Boolean
    },
    current: {
      type: Number
    }
  },
  emits: ['mouseEnter', 'mouseEnterHalf', 'clickHalfStar', 'clickStar'],
  setup(props, {
    emit
  }) {
    const className = () => {
      let half = false;
      let full = false;
      if (props.index <= props.current - 1) {
        full = true;
      }
      if (props.index > props.current - 1 && props.index < props.current) {
        half = true;
      }
      return {
        'cm-rate-star': true,
        'cm-rate-star-zero': !full && !half,
        'cm-rate-star-half': props.allowHalf && half,
        'cm-rate-star-full': full
      };
    };
    const onMouseEnter = index => {
      emit('mouseEnter', index);
    };
    const onMouseEnterHalf = (index, e) => {
      emit('mouseEnterHalf', index, e);
    };
    const onClickStar = index => {
      emit('clickStar', index);
    };
    const onClickHalfStar = (index, e) => {
      emit('clickHalfStar', index, e);
    };
    return () => createVNode("div", {
      "class": className()
    }, [createVNode("span", {
      "onMouseenter": onMouseEnter.bind(null, props.index + 1),
      "onClick": onClickStar.bind(null, props.index + 1)
    }, [props.icon]), props.allowHalf ? createVNode("span", {
      "class": "cm-rate-star-content",
      "onMouseenter": onMouseEnterHalf.bind(null, props.index + 0.5),
      "onClick": onClickHalfStar.bind(null, props.index + 0.5)
    }, [props.icon]) : null]);
  }
});

export { RateItem as default };
