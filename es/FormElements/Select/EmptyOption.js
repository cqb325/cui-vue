import { defineComponent, computed, createVNode } from 'vue';

const EmptyOption = /* @__PURE__ */ defineComponent({
  name: 'EmptyOption',
  props: {
    checked: Boolean,
    data: Object
  },
  emits: ['click'],
  setup(props, {
    emit
  }) {
    const classList = computed(() => ({
      'cm-select-option': true,
      'cm-select-option-active': props.checked
    }));
    const value = props.data.value;
    return () => createVNode("li", {
      "class": classList.value,
      "onClick": () => emit('click', value)
    }, [props.data.label]);
  }
});

export { EmptyOption as default };
