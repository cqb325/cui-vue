import { defineComponent, inject, onUnmounted } from 'vue';

const Option = /* @__PURE__ */ defineComponent({
  name: 'Option',
  props: {
    style: {
      type: Object
    },
    class: {
      type: Object
    },
    disabled: {
      type: Boolean
    },
    value: {
      type: [String, Number, Boolean]
    },
    label: {
      type: String
    }
  },
  setup(props) {
    const ctx = inject('CMSelectContext', null);
    ctx.addOption({
      ...props
    });
    onUnmounted(() => {
      ctx.removeOption({
        value: props.value
      });
    });
    return () => null;
  }
});

export { Option as default };
