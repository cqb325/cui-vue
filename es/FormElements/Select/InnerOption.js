import { defineComponent, computed, createVNode } from 'vue';

const InnerOption = /* @__PURE__ */ defineComponent({
  name: 'InnerOption',
  props: {
    data: {
      type: Object
    },
    checked: {
      type: Boolean
    },
    disabled: {
      type: Boolean
    },
    textField: {
      type: String
    },
    valueField: {
      type: String
    },
    visible: {
      type: Boolean
    },
    value: {
      type: [String, Number, Boolean]
    },
    label: {
      type: String
    },
    emptyOption: {
      type: Boolean
    },
    renderOption: {
      type: Function
    }
  },
  emits: ['click'],
  setup(props, {
    emit
  }) {
    const classList = computed(() => ({
      'cm-select-option': true,
      'cm-select-group-wrap': props.data.group,
      'cm-select-option-active': props.checked,
      'cm-select-option-disabled': props.data.disabled
    }));
    const onClick = () => {
      if (props.disabled) {
        return;
      }
      emit('click', value, props.data);
    };
    const value = props.data[props.valueField];
    return () => props.visible ? createVNode("li", {
      "class": classList.value,
      "onClick": onClick
    }, [props.renderOption ? props.renderOption(props.data) : props.data[props.textField]]) : null;
  }
});

export { InnerOption as default };
