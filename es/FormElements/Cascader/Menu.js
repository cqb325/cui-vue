import { defineComponent, createVNode } from 'vue';
import { Item } from './Item.js';
import Empty from '../../Empty/index.js';

const Menu = /* @__PURE__ */ defineComponent({
  name: 'CascaderMenu',
  props: {
    data: {
      type: Array
    },
    store: {
      type: Object
    },
    trigger: {
      type: String
    },
    emptyText: {
      type: String
    }
  },
  setup(props) {
    const data = () => props.data;
    return () => createVNode("div", {
      "class": {
        "cm-cascader-list": true,
        'cm-cascader-list-empty': !data().length
      }
    }, [data().length ? data().map(item => {
      return props.store.store.nodeMap[item] && createVNode(Item, {
        "trigger": props.trigger,
        "data": props.store.store.nodeMap[item],
        "store": props.store
      }, null);
    }) : createVNode("div", {
      "class": "cm-cascader-empty"
    }, [createVNode(Empty, {
      "width": 100,
      "text": props.emptyText
    }, null)])]);
  }
});

export { Menu };
