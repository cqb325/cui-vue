import { defineComponent, inject, computed, createVNode } from 'vue';
import { CascaderContextKey } from './index.js';

const Option = /* @__PURE__ */ defineComponent({
  props: {
    data: {
      type: Array
    },
    seperator: {
      type: String
    },
    store: {
      type: Object
    },
    filter: {
      type: Boolean
    }
  },
  setup(props) {
    const {
      store
    } = props;
    const ctx = inject(CascaderContextKey, {});
    const title = computed(() => props.data.map(item => item[store.titleField]).join(' / '));
    const onClick = () => {
      const lastItem = props.data[props.data.length - 1];
      if (lastItem.disabled) {
        return;
      }
      const vals = props.data.map(item => item[store.valueField]);
      if (ctx?.multi) {
        store.checkNode(vals[vals.length - 1], true);
        if (props.filter) {
          ctx?.clearQuery('');
        }
      } else {
        store.selectedKey.value = vals;
        ctx?.onSelect(props.data[props.data.length - 1]);
      }
    };
    const classList = computed(() => ({
      'cm-cascader-item': true,
      'cm-cascader-item-checked': props.data[props.data.length - 1].checked,
      'cm-cascader-item-disabled': props.data[props.data.length - 1].disabled
    }));
    return () => createVNode("div", {
      "class": classList.value,
      "onClick": onClick
    }, [title.value]);
  }
});

export { Option };
