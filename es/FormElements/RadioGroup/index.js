import { defineComponent, computed, ref, watch, reactive, onMounted, watchEffect, createVNode } from 'vue';
import Radio from '../Radio/index.js';
import formFieldRef from '../../use/formFieldRef.js';

const index = /* @__PURE__ */ defineComponent({
  props: {
    block: {
      type: Boolean
    },
    name: {
      type: String
    },
    value: {
      type: [String, Number, Boolean]
    },
    modelValue: {
      type: [String, Number, Boolean],
      default: undefined
    },
    disabled: {
      type: Boolean
    },
    data: {
      type: Array
    },
    type: {
      type: String
    },
    textField: {
      type: String
    },
    valueField: {
      type: String
    },
    stick: {
      type: Boolean
    },
    asFormField: {
      type: Boolean,
      default: true
    }
  },
  emits: ['change', 'update:modelValue'],
  setup(props, {
    emit
  }) {
    const classList = computed(() => ({
      'cm-radio-group': true,
      'cm-radio-group-stack': props.block,
      'cm-radio-group-stick': props.stick
    }));
    const value = formFieldRef(props, emit);
    const thumbStyle = ref({});
    const wrap = ref(null);
    const _onChange = (checked, v) => {
      if (props.disabled) {
        return;
      }
      value.value = v;
      props.data.forEach(item => {
        store[item[valueField]] = item[valueField] === v ? checked : false;
      });
      emit('change', v);
    };
    watch(() => value.value, val => {
      props.data.forEach(item => {
        store[item[valueField]] = item[valueField] === val;
      });
    });
    const textField = props.textField ?? 'label';
    const valueField = props.valueField ?? 'value';
    const store = reactive({});
    props.data.forEach(item => {
      store[item[valueField]] = item[valueField] === value.value;
    });
    onMounted(() => {
      watchEffect(() => {
        const val = value.value ?? "";
        let currentIndex = -1;
        for (let i = 0; i < props.data.length; i++) {
          const item = props.data[i];
          const checked = val === item[valueField];
          checked ? currentIndex = i : false;
        }
        if (wrap.value) {
          const eles = wrap.value.querySelectorAll('.cm-radio');
          const ele = eles[currentIndex];
          if (!ele) {
            return;
          }
          const rect = ele.getBoundingClientRect();
          const wrapRect = wrap.value.getBoundingClientRect();
          const left = rect.left - wrapRect.left;
          const width = rect.width;
          const ret = {
            width: `${width}px`,
            left: `${left}px`
          };
          thumbStyle.value = ret;
        }
      });
    });
    return () => createVNode("div", {
      "class": classList.value,
      "ref": wrap
    }, [props.stick ? createVNode("div", {
      "class": "cm-radio-group-thumb",
      "style": thumbStyle.value
    }, null) : null, props.data.map(item => {
      return createVNode(Radio, {
        "disabled": props.disabled || item.disabled,
        "type": props.type || 'radio',
        "value": item[valueField],
        "modelValue": store[item[valueField]],
        "onUpdate:modelValue": $event => store[item[valueField]] = $event,
        "label": item[textField],
        "onChange": _onChange
      }, null);
    })]);
  }
});

export { index as default };
