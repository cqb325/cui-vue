import { defineComponent, computed, createVNode } from 'vue';
import Cell from './Cell.js';

const TimePane = /* @__PURE__ */ defineComponent({
  name: 'TimePane',
  props: {
    value: {
      type: [String, Object]
    },
    format: String,
    header: [String, Object],
    footer: [String, Object],
    name: String,
    hourStep: Number,
    minuteStep: Number,
    secondStep: Number
  },
  emits: ['selectTime'],
  setup(props, ctx) {
    const hour = computed(() => {
      return props.value instanceof Date ? props.value.getHours() : undefined;
    });
    const minute = computed(() => {
      return props.value instanceof Date ? props.value.getMinutes() : undefined;
    });
    const second = computed(() => {
      return props.value instanceof Date ? props.value.getSeconds() : undefined;
    });

    // 检查格式是否需要显示小时、分钟、秒
    const hasHour = computed(() => props.format.includes('H'));
    const hasMinute = computed(() => props.format.includes('m'));
    const hasSecond = computed(() => props.format.includes('s'));
    return () => createVNode("div", {
      "class": "cm-time-picker-pane"
    }, [props.header && createVNode("div", {
      "class": "cm-time-picker-header"
    }, [props.header]), createVNode("div", {
      "class": "cm-time-picker-options"
    }, [hasHour.value && createVNode(Cell, {
      "max": 24,
      "type": "hour",
      "value": hour.value,
      "step": props.hourStep,
      "name": props.name,
      "onSelectTime": ctx.emit.bind(ctx, 'selectTime')
    }, null), hasMinute.value && createVNode(Cell, {
      "max": 60,
      "type": "minute",
      "value": minute.value,
      "step": props.minuteStep,
      "name": props.name,
      "onSelectTime": ctx.emit.bind(ctx, 'selectTime')
    }, null), hasSecond.value && createVNode(Cell, {
      "max": 60,
      "type": "second",
      "value": second.value,
      "step": props.secondStep,
      "name": props.name,
      "onSelectTime": ctx.emit.bind(ctx, 'selectTime')
    }, null)]), props.footer && createVNode("div", {
      "class": "cm-time-picker-footer"
    }, [props.footer])]);
  }
});

export { TimePane as default };
