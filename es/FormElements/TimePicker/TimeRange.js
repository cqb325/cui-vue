import { defineComponent, createVNode, Fragment, mergeProps } from 'vue';
import TimePane from './TimePane.js';

const TimeRange = /* @__PURE__ */ defineComponent({
  name: "TimeRange",
  props: {
    header: Array,
    footer: Array,
    value: [String, Array],
    format: String,
    minuteStep: Number,
    hourStep: Number,
    secondStep: Number
  },
  setup(props, ctx) {
    const {
      header,
      footer,
      value,
      ...others
    } = props;
    return () => createVNode(Fragment, null, [createVNode(TimePane, mergeProps({
      "value": props.value[0],
      "header": props.header[0],
      "footer": props.footer && props.footer.length && props.footer[0]
    }, others, {
      "name": "start"
    }), null), createVNode(TimePane, mergeProps({
      "value": props.value[1],
      "header": props.header[1],
      "footer": props.footer && props.footer.length && props.footer[1]
    }, others, {
      "name": "end"
    }), null)]);
  }
});

export { TimeRange as default };
