import { defineComponent, computed, watch, createVNode, createTextVNode } from 'vue';
import Loading from '../../inner/Loading.js';
import formFieldRef from '../../use/formFieldRef.js';

const index = /* @__PURE__ */ defineComponent({
  name: 'Switch',
  props: {
    size: {
      type: String
    },
    disabled: {
      type: Boolean
    },
    name: {
      type: String
    },
    modelValue: {
      type: Boolean
    },
    loading: {
      type: Boolean
    },
    labels: {
      type: Array
    },
    values: {
      type: Array
    },
    round: {
      type: Boolean,
      default: true
    },
    icon: {
      type: [Object, Array]
    },
    colors: {
      type: Array
    },
    asFormField: {
      type: Boolean,
      default: true
    },
    onBeforeChange: {
      type: Function
    }
  },
  emits: ['update:modelValue', 'change'],
  setup(props, {
    emit
  }) {
    const checked = formFieldRef(props, emit, props.modelValue ?? false);
    const classList = computed(() => ({
      'cm-switch': true,
      [`cm-switch-${props.size}`]: props.size,
      'cm-switch-disabled': props.disabled,
      'cm-switch-checked': checked.value,
      'cm-switch-loading': props.loading,
      'cm-switch-round': props.round ?? true
    }));
    watch(() => props.modelValue, value => {
      checked.value = value;
    });
    const style = computed(() => ({
      '--cm-switch-default-color': props.colors && props.colors[0],
      '--cm-switch-active-color': props.colors && props.colors[1]
    }));
    const labels = props.labels || [];
    const values = props.values || [true, false];
    const toggleSwitch = async () => {
      if (props.disabled) {
        return;
      }
      if (props.loading) {
        return;
      }
      let ret = true;
      if (props.onBeforeChange) {
        ret = await props.onBeforeChange(checked.value);
      }
      if (ret) {
        const flag = !checked.value;
        const v = flag ? values[0] : values[1];
        emit('change', v);
        checked.value = flag;
        emit('update:modelValue', flag);
      }
    };
    const icon = computed(() => {
      if (checked.value) {
        if (props.icon && props.icon instanceof Array) {
          return props.icon[1];
        }
        return props.icon;
      } else {
        if (props.icon && props.icon instanceof Array) {
          return props.icon[0];
        }
        return props.icon;
      }
    });
    return () => createVNode("div", {
      "class": classList.value,
      "style": style.value,
      "tab-index": "0",
      "onClick": toggleSwitch
    }, [createVNode("span", {
      "style": {
        width: '0px',
        "font-size": '12px',
        visibility: 'hidden'
      }
    }, [createTextVNode("A")]), createVNode("span", {
      "class": "cm-switch-inner-placeholder"
    }, [createVNode("span", null, [createVNode("span", {
      "class": "cm-switch-inner-button-placeholder"
    }, null), labels[0]]), createVNode("span", null, [createVNode("span", {
      "class": "cm-switch-inner-button-placeholder"
    }, null), labels[1]])]), createVNode("span", {
      "class": "cm-switch-inner"
    }, [icon.value ? createVNode("span", {
      "class": "cm-switch-inner-icon"
    }, [icon.value]) : null, createVNode("span", {
      "class": "cm-switch-label cm-switch-label-left"
    }, [labels[0]]), createVNode("span", {
      "class": "cm-switch-label cm-switch-label-right"
    }, [labels[1]])]), props.loading ? createVNode(Loading, null, null) : null, createVNode("input", {
      "name": props.name,
      "type": "hidden",
      "value": checked.value ? values[0] : values[1]
    }, null)]);
  }
});

export { index as default };
