import { defineComponent, computed, createVNode } from 'vue';

const TimelineItem = /* @__PURE__ */ defineComponent({
  name: 'TimelineItem',
  // vue component name
  props: {
    color: {
      type: String,
      default: 'blue'
    },
    class: {
      type: String
    },
    fill: {
      type: Boolean,
      default: undefined
    },
    icon: {
      type: [String, Object]
    },
    time: {
      type: String
    }
  },
  setup(props, {
    slots
  }) {
    const classList = computed(() => ({
      'cm-timeline-item-head': true,
      [`cm-timeline-item-head-${props.color}`]: props.color,
      'cm-timeline-item-head-custom': props.icon,
      'cm-timeline-item-head-fill': props.fill
    }));
    return () => createVNode("div", {
      "class": "cm-timeline-item"
    }, [createVNode("div", {
      "class": "cm-timeline-item-tail"
    }, null), createVNode("div", {
      "class": classList.value
    }, [props.icon || slots.icon?.()]), createVNode("div", {
      "class": "cm-timeline-item-content"
    }, [slots.default?.(), props.time ? createVNode("div", {
      "class": "cm-timeline-time"
    }, [props.time]) : null])]);
  }
});

export { TimelineItem as default };
