import { defineComponent, computed, createVNode } from 'vue';
import TimelineItem from './TimelineItem.js';

const Timeline = /* @__PURE__ */ defineComponent({
  name: 'Timeline',
  props: {
    mode: {
      type: String
    }
  },
  setup(props, {
    slots
  }) {
    const classList = computed(() => ({
      'cm-timeline': true,
      [`cm-timeline-${props.mode}`]: props.mode
    }));
    return () => createVNode("div", {
      "class": classList.value
    }, [slots.default?.()]);
  }
});
Timeline.Item = TimelineItem;

export { Timeline as default };
