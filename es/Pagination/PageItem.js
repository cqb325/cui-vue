import { defineComponent, createVNode } from 'vue';

const PageItem = /* @__PURE__ */ defineComponent({
  name: 'PageItem',
  props: {
    active: Boolean,
    currentIndex: [Number, String]
  },
  emits: ['click'],
  setup(props, {
    emit
  }) {
    const onClick = () => {
      emit('click');
    };
    const classList = () => ({
      'cm-pagination-num': true,
      'cm-pagination-item-active': props.active
    });
    return () => createVNode("li", {
      "onClick": onClick,
      "class": classList()
    }, [props.currentIndex]);
  }
});

export { PageItem as default };
