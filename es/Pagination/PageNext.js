import { defineComponent, createVNode } from 'vue';
import { FeatherChevronRight } from 'cui-vue-icons/feather';

const PageNext = /* @__PURE__ */ defineComponent({
  name: 'PageNext',
  props: {
    currentIndex: Number,
    disabled: Boolean
  },
  emits: ['click'],
  setup(props, {
    emit
  }) {
    const onClick = () => {
      emit('click');
    };
    const classList = () => ({
      'cm-pagination-num': true,
      'cm-pagination-next': true,
      'cm-pagination-num-disabled': props.disabled
    });
    return () => createVNode("li", {
      "onClick": onClick,
      "class": classList()
    }, [createVNode(FeatherChevronRight, null, null)]);
  }
});

export { PageNext as default };
