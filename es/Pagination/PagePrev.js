import { defineComponent, createVNode } from 'vue';
import { FeatherChevronLeft } from 'cui-vue-icons/feather';

const PagePrev = /* @__PURE__ */ defineComponent({
  name: 'PagePrev',
  props: {
    currentIndex: Number,
    current: Number
  },
  emits: ['click'],
  setup(props, {
    emit
  }) {
    const onClick = () => {
      emit('click');
    };
    const classList = () => ({
      'cm-pagination-num': true,
      'cm-pagination-prev': true,
      'cm-pagination-num-disabled': props.current
    });
    return () => createVNode("li", {
      "onClick": onClick,
      "class": classList()
    }, [createVNode(FeatherChevronLeft, null, null)]);
  }
});

export { PagePrev as default };
