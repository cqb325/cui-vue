import { defineComponent, provide, createVNode } from 'vue';

const TableStyleLayoutContextKey = Symbol('TableStyleLayoutContextKey');
const index = /* @__PURE__ */ defineComponent({
  name: 'TableStyleLayout',
  props: {
    labelWidth: {
      type: Number,
      default: 100
    }
  },
  setup(props, {
    slots
  }) {
    provide(TableStyleLayoutContextKey, {
      labelWidth: props.labelWidth
    });
    return () => createVNode("div", {
      "class": "cm-table-style-layout"
    }, [slots.default?.()]);
  }
});

export { TableStyleLayoutContextKey, index as default };
