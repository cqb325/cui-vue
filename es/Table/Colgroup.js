import { defineComponent, inject, createVNode } from 'vue';
import { TableContextKey } from './index.js';

const Colgroup = /* @__PURE__ */ defineComponent({
  name: 'Colgroup',
  props: {
    data: {
      type: Object,
      required: true
    }
  },
  setup(props) {
    const ctx = inject(TableContextKey, null);
    return () => createVNode("colgroup", {
      "class": "cm-table-colgroup"
    }, [ctx.store.columns.map((col, index) => createVNode("col", {
      "key": index,
      "class": "cm-table-col",
      "width": col._width
    }, null))]);
  }
});

export { Colgroup as default };
