import { defineComponent, computed, watchEffect, onMounted, onUnmounted, createVNode } from 'vue';
import Colgroup from './Colgroup.js';
import Cell from './Cell.js';

const Summary = /* @__PURE__ */ defineComponent({
  name: 'Summary',
  props: ['data', 'summaryMethod', 'onResizeSummary'],
  setup(props) {
    let summary;
    const summaryData = computed(() => {
      let row = {};
      if (props.summaryMethod) {
        row = props.summaryMethod(props.data.columns, props.data.data);
      } else {
        props.data.columns.forEach((col, index) => {
          const key = col.name;
          if (index === 0) {
            row[key] = '合计';
            return;
          }
          const values = props.data.data.map(item => Number(item[key]));
          if (!values.every(value => isNaN(value))) {
            const v = values.reduce((prev, curr) => {
              const value = Number(curr);
              if (!isNaN(value)) {
                return prev + curr;
              } else {
                return prev;
              }
            }, 0);
            row[key] = v;
          } else {
            row[key] = '';
          }
        });
      }
      return row;
    });
    watchEffect(() => {
    });
    const onEntryResize = entry => {
      const el = entry.target;
      if (el.classList.contains("cm-table-summary")) {
        const rect = el.getBoundingClientRect();
        const tableH = el.children[0].getBoundingClientRect().height;
        props.onResizeSummary(rect.width, tableH);
        summary.style.height = tableH + 'px';
      } else {
        // setTimeout, header变化让body设置height后再计算是否有垂直滚动条
        setTimeout(() => {
          const rect = el.getBoundingClientRect();
          const parentRect = el.closest(".cm-table-body").getBoundingClientRect();
          if (rect.height > parentRect.height) {
            summary.style.overflowY = 'scroll';
          } else {
            summary.style.overflowY = '';
          }
        });
      }
    };
    onMounted(() => {
      const ro = new ResizeObserver(entries => {
        entries.forEach(entry => onEntryResize(entry));
      });
      ro.observe(summary);
      const parent = summary.closest('.cm-table');
      const body = parent.querySelector('.cm-table-body-wrap');
      ro.observe(body);
      onUnmounted(() => {
        ro.unobserve(summary);
        ro.unobserve(body);
      });
    });
    return createVNode("div", {
      "class": "cm-table-summary",
      "ref": summary
    }, [createVNode("table", null, [createVNode(Colgroup, {
      "data": props.data
    }, null), createVNode("thead", {
      "style": {
        display: 'none'
      }
    }, [createVNode("tr", null, [props.data.columns.map((col, index) => {
      return createVNode(Cell, {
        "column": col,
        "type": "th",
        "placeholder": true,
        "colIndex": index,
        "checkedAll": props.data.checkedAll
      }, null);
    })])]), createVNode("tbody", null, [createVNode("tr", null, [props.data.columns.map((col, index) => {
      return createVNode(Cell, {
        "type": "td",
        "summary": true,
        "data": summaryData.value,
        "column": col,
        "colIndex": index,
        "index": index,
        "showFixedLeft": props.data.showFixedLeft,
        "showFixedRight": props.data.showFixedRight
      }, null);
    })])])])]);
  }
});

export { Summary as default };
