import { defineComponent, ref, watchEffect, createVNode, inject, computed } from 'vue';
import Cell from './Cell.js';
import { TableContextKey } from './index.js';
import Colgroup from './Colgroup.js';
import VirtualListCore from '../virtual-list/core.js';

const Row = /* @__PURE__ */ defineComponent({
  name: 'Row',
  props: {
    store: {
      type: Object
    },
    data: {
      type: Object
    },
    index: {
      type: Number
    }
  },
  setup(props) {
    const ctx = inject(TableContextKey, null);
    const onRowClick = () => {
      if (props.data._type === 'expandChildren') {
        return;
      }
      // 设置了highlight属性才能进行高亮
      if (ctx && ctx.highlight) {
        ctx.onSelectRow(props.data);
      }
    };
    const classList = computed(() => ({
      'cm-table-row': true,
      'cm-table-row-ood': props.index % 2 === 0,
      'cm-table-row-even': props.index % 2 !== 0,
      'cm-table-row-selected': props.data._highlight
    }));
    const rowStyle = computed(() => ({
      display: props.data._show ? '' : 'none'
    }));
    return () => createVNode("tr", {
      "class": classList.value,
      "onClick": onRowClick,
      "style": rowStyle.value
    }, [props.data._type === 'expandChildren' ? createVNode(Cell, {
      "type": "td",
      "data": props.data,
      "column": props.data.column,
      "index": props.index,
      "showFixedLeft": props.store.showFixedLeft,
      "showFixedRight": props.store.showFixedRight,
      "colSpan": props.store.columns.length
    }, null) : props.store.columns.map((column, columnIndex) => {
      let [rowSpan, colSpan] = [1, 1];
      if (ctx && ctx.spanMethod) {
        const ret = ctx.spanMethod(props.data, column, props.index, columnIndex);
        if (ret) {
          [rowSpan, colSpan] = ret;
        }
      }
      return rowSpan && colSpan ? createVNode(Cell, {
        "type": "td",
        "data": props.data,
        "column": column,
        "index": props.index,
        "colIndex": columnIndex,
        "showFixedLeft": props.store.showFixedLeft,
        "showFixedRight": props.store.showFixedRight,
        "rowSpan": rowSpan,
        "colSpan": colSpan
      }, null) : null;
    })]);
  }
});
const EmprtyRow = /* @__PURE__ */ defineComponent({
  name: 'EmptyRow',
  props: {
    store: Object
  },
  setup(props) {
    const ctx = inject('CMTableContext', null);
    return () => createVNode("tr", null, [createVNode("td", {
      "colspan": props.store.columns.length
    }, [createVNode("div", {
      "class": "cm-table-emprty-cell"
    }, [ctx?.empty || '暂无数据'])])]);
  }
});
const Body = /* @__PURE__ */ defineComponent({
  name: 'Body',
  props: {
    data: {
      type: Object
    },
    height: {
      type: Number
    },
    virtual: {
      type: Boolean
    }
  },
  emits: ['scroll'],
  setup(props, {
    emit
  }) {
    const body = ref();
    const height = ref();
    const width = () => {
      const columns = props.data.columns;
      let total = 0;
      columns.forEach(item => {
        total += item._width || 0;
      });
      return total;
    };
    watchEffect(() => {
      // 数据改变也需要重刷
      props.data.data;
      const hh = props.data.headerSize.height;
      const summaryH = props.data.summarySize.height;
      if (props.virtual) {
        const scrollElHeight = props.height ?? document.documentElement.clientHeight;
        height.value = scrollElHeight - hh - summaryH;
      } else {
        setTimeout(() => {
          const content = body.value.querySelector('.cm-table-body-wrap');
          const contentH = content.getBoundingClientRect().height;
          if (props.height && contentH > props.height - hh - summaryH) {
            const bodyH = props.height - hh - summaryH;
            height.value = bodyH;
          }
        });
      }
    });
    const handleScroll = () => {
      emit('scroll', body.value.scrollLeft, body.value.clientWidth, body.value.scrollWidth);
    };
    const contentElement = ref();
    const bodyElement = ref();
    return () => createVNode("div", {
      "class": "cm-table-body",
      "ref": body,
      "onScroll": handleScroll,
      "style": {
        display: 'block',
        'width': '100%',
        overflow: 'auto',
        height: height.value + 'px',
        position: 'relative'
      }
    }, [props.virtual ? createVNode("div", {
      "ref": contentElement,
      "style": {
        'min-width': '100%',
        width: width() + 'px',
        'will-change': 'transform',
        'box-sizing': 'border-box',
        'contain': 'strict',
        position: 'absolute',
        top: 0,
        left: 0
      }
    }, [createVNode("table", {
      "class": "cm-table-body-wrap"
    }, [createVNode(Colgroup, {
      "data": props.data
    }, null), createVNode("thead", {
      "style": {
        display: 'none'
      }
    }, [createVNode("tr", null, [props.data.columns.map((col, index) => {
      return createVNode(Cell, {
        "column": col,
        "type": "th",
        "placeholder": true,
        "colIndex": index,
        "checkedAll": props.data.checkedAll
      }, null);
    })])]), createVNode("tbody", {
      "ref": bodyElement
    }, [body.value ? createVNode(VirtualListCore, {
      "scrollElement": body.value,
      "contentElement": contentElement.value,
      "bodyElement": bodyElement.value,
      "items": props.data.data,
      "itemEstimatedSize": 50,
      "maxHeight": height.value || props.height
    }, {
      default: scope => {
        return createVNode(Row, {
          "key": scope.item.id,
          "data": scope.item,
          "index": scope.index,
          "store": props.data
        }, null);
      }
    }) : null, !props.data.data || !props.data.data.length ? createVNode(EmprtyRow, {
      "store": props.data
    }, null) : null])])]) : createVNode("table", {
      "class": "cm-table-body-wrap",
      "ref": bodyElement
    }, [createVNode(Colgroup, {
      "data": props.data
    }, null), createVNode("thead", {
      "style": {
        display: 'none'
      }
    }, [createVNode("tr", null, [props.data.columns.map((col, index) => {
      return createVNode(Cell, {
        "column": col,
        "type": "th",
        "placeholder": true,
        "colIndex": index,
        "checkedAll": props.data.checkedAll
      }, null);
    })])]), createVNode("tbody", null, [props.data.data.map((rowData, index) => {
      return createVNode(Row, {
        "key": rowData.id,
        "data": rowData,
        "index": index,
        "store": props.data
      }, null);
    }), !props.data.data || !props.data.data.length ? createVNode(EmprtyRow, {
      "store": props.data
    }, null) : null])])]);
  }
});

export { EmprtyRow, Row, Body as default };
