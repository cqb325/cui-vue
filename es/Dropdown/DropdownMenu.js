import { defineComponent, inject, computed, createVNode } from 'vue';

const DropdownMenu = /* @__PURE__ */ defineComponent({
  name: "DropdownMenu",
  setup(props, {
    slots
  }) {
    const ctx = inject('dropdownConext', {});
    const style = computed(() => ({
      'background': ctx?.gradient ? `linear-gradient(${ctx.gradient?.join(',')})` : ''
    }));
    return () => createVNode("ul", {
      "class": "cm-dropdown-list",
      "style": style.value
    }, [slots.default && slots.default()]);
  }
});

export { DropdownMenu as default };
