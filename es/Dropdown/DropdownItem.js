import { defineComponent, computed, inject, createVNode } from 'vue';
import { isColor } from '../utils/assist.js';
import { FeatherChevronRight } from 'cui-vue-icons/feather';

const DropdownItem = /* @__PURE__ */ defineComponent({
  name: 'DropdownItem',
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    divided: {
      type: Boolean,
      default: false
    },
    name: {
      type: String,
      default: ''
    },
    icon: {
      type: Object,
      default: null
    },
    arrow: {
      type: Boolean,
      default: false
    },
    data: {
      type: Object,
      default: null
    },
    theme: {
      type: String,
      default: ''
    },
    selected: {
      type: Boolean,
      default: false
    }
  },
  setup(props, {
    slots
  }) {
    const theme = isColor(props.theme) ? '' : props.theme;
    const classList = computed(() => ({
      'cm-dropdown-item': true,
      'cm-dropdown-item-disabled': props.disabled,
      'cm-dropdown-item-divided': props.divided,
      'cm-dropdown-item-selected': props.selected,
      'cm-dropdown-item-with-arrow': props.arrow,
      [`cm-dropdown-item-${theme}`]: theme
    }));
    const ctx = inject("dropdownConext", {});
    const onClick = e => {
      if (props.disabled) {
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      ctx?.onSelect(props.name);
    };
    const style = computed(() => ({
      '--cui-dropdown-text-color': isColor(props.theme) ? props.theme : ''
    }));
    return () => createVNode("li", {
      "class": classList.value,
      "style": style.value,
      "onClick": onClick
    }, [props.icon ? createVNode("span", {
      "class": "cm-dropdown-item-icon"
    }, [props.icon]) : null, slots.default && slots.default(), props.arrow ? createVNode(FeatherChevronRight, {
      "class": "cm-dropdown-item-arrow"
    }, null) : null]);
  }
});

export { DropdownItem as default };
