import { defineComponent, inject, computed, createVNode } from 'vue';
import { RowContext } from '../Row/index.js';
import { createGrid, createOffset } from '../use/createGridOffset.js';

const responsiveTypes = ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'];
const index = /* @__PURE__ */ defineComponent({
  name: 'Col',
  props: {
    grid: {
      type: Number
    },
    push: {
      type: Number
    },
    pull: {
      type: Number
    },
    offset: {
      type: Number
    },
    flex: {
      type: String
    },
    fixWidth: {
      type: Boolean
    },
    xs: {
      type: [Number, Object]
    },
    sm: {
      type: [Number, Object]
    },
    md: {
      type: [Number, Object]
    },
    lg: {
      type: [Number, Object]
    },
    xl: {
      type: [Number, Object]
    },
    xxl: {
      type: [Number, Object]
    }
  },
  setup(props, {
    slots
  }) {
    const ctx = inject(RowContext, {});
    // const respClassList: any = {};
    // const offsetClassList: any = {};

    // 响应式类名计算
    const {
      respClassList,
      offsetClassList
    } = computed(() => {
      const resp = {};
      const offset = {};
      responsiveTypes.forEach(type => {
        const value = props[type];
        if (value) {
          const gridValue = typeof value === 'number' ? value : value.grid;
          const gridClass = createGrid(gridValue, type);
          if (gridClass) resp[gridClass] = true;
          const offsetValue = typeof value === 'object' ? value.offset : 0;
          const offsetClass = createOffset(offsetValue, type);
          if (offsetClass) offset[offsetClass] = true;
        }
      });
      return {
        respClassList: resp,
        offsetClassList: offset
      };
    }).value;

    // responsiveTypes.forEach((type: string) => {
    //     if (props[type as keyof ColProps]) {
    //         const w = typeof props[type as keyof ColProps] === 'number'
    //             ? props[type as keyof ColProps] : (props[type as keyof ColProps] as ColResponsiveProps).grid;
    //         const respClass: string = createGrid(w as number, type as responsiveType);
    //         if (respClass) {
    //             respClassList[respClass] = true;
    //         }
    //         const offset = typeof props[type as keyof ColProps] === 'object'
    //             ? (props[type as keyof ColProps] as ColResponsiveProps).offset : 0;

    //         const offsetClass: string = createOffset(offset, type as responsiveType);
    //         if (offsetClass) {
    //             offsetClassList[offsetClass] = true;
    //         }
    //     }
    // });
    const style = computed(() => {
      const isResponsive = Object.keys(respClassList).length > 0;
      const isResponsiveOffset = Object.keys(offsetClassList).length > 0;
      const style = {};
      if (!isResponsive) {
        style.flex = `0 0 ${(props.grid || 1) * 100}%`;
        if (props.fixWidth) {
          style['max-width'] = `${(props.grid || 1) * 100}%`;
        }
      }

      // 偏移量
      if (props.push) style.left = `${props.push * 100}%`;
      if (props.pull) style.right = `${props.pull * 100}%`;
      if (props.offset && !isResponsiveOffset) {
        style['margin-left'] = `${props.offset * 100}%`;
      }

      // 间距处理
      const gutter = ctx?.gutter ? Array.isArray(ctx.gutter) ? ctx.gutter[0] : ctx.gutter : undefined;
      const halfGutter = gutter ? `${gutter / 2}px` : '0';
      if (gutter) {
        style['padding-left'] = halfGutter;
        style['padding-right'] = halfGutter;
      }

      // 自定义flex
      if (props.flex) {
        style.flex = props.flex.includes(' ') ? props.flex : `0 0 ${props.flex}`;
      }
      return style;
    });
    const classList = computed(() => ({
      'cm-col': true,
      ...respClassList,
      ...offsetClassList
    }));
    return () => createVNode("div", {
      "class": classList.value,
      "style": style.value
    }, [slots.default?.()]);
  }
});

export { index as default };
