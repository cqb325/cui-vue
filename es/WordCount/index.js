import { defineComponent, createVNode, Fragment, createTextVNode } from 'vue';
import Progress from '../Progress/index.js';

const WordCount = /* @__PURE__ */ defineComponent({
  name: 'WordCount',
  props: {
    total: {
      type: Number,
      default: 0
    },
    value: {
      type: String,
      default: ''
    },
    prefix: {
      type: String,
      default: ''
    },
    prefixOverflow: {
      type: String,
      default: ''
    },
    suffix: {
      type: String,
      default: ''
    },
    suffixOverflow: {
      type: String,
      default: ''
    },
    circle: {
      type: Boolean,
      default: false
    },
    overflow: {
      type: Boolean,
      default: false
    },
    radius: {
      type: Number,
      default: 10
    }
  },
  setup(props) {
    const overflow = () => {
      const val = props.value ?? '';
      return val.length > props.total;
    };
    const overNumber = () => {
      const val = props.value ?? '';
      return props.overflow && overflow() ? val.length - props.total : val.length;
    };
    const percent = () => {
      const val = props.value ?? '';
      return Math.min(val.length / props.total * 100, 100);
    };
    const radius = props.radius ?? 10;
    return () => createVNode("div", {
      "class": "cm-word-count"
    }, [props.circle ? createVNode(Progress, {
      "type": "circle",
      "radius": radius,
      "strokeWidth": 1,
      "hidePercent": true,
      "value": percent()
    }, null) : createVNode(Fragment, null, [createVNode("span", {
      "class": {
        "cm-word-count-prefix": true,
        'cm-word-count-overflow': overflow()
      }
    }, [overflow() ? props.prefixOverflow : props.prefix]), createVNode("span", {
      "class": overflow() ? 'cm-word-count-overflow' : ''
    }, [overNumber()]), createVNode("span", null, [createTextVNode("/")]), createVNode("span", null, [props.total]), createVNode("span", {
      "class": {
        "cm-word-count-suffix": true,
        'cm-word-count-overflow': overflow()
      }
    }, [overflow() ? props.suffixOverflow : props.suffix])])]);
  }
});

export { WordCount as default };
