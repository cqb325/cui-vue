import { defineComponent, computed, createVNode } from 'vue';

const Space = /* @__PURE__ */ defineComponent({
  name: 'Space',
  props: {
    dir: {
      type: String
    },
    wrap: {
      type: Boolean
    },
    inline: {
      type: Boolean
    },
    size: {
      type: Number
    },
    align: {
      type: String
    },
    justify: {
      type: String
    },
    id: {
      type: String
    },
    title: {
      type: String
    }
  },
  setup(props, {
    slots
  }) {
    const dir = computed(() => props.dir ?? 'h');
    const size = computed(() => props.size ?? 8);
    const align = computed(() => props.align ?? '');
    const classList = computed(() => ({
      'cm-space': true,
      [`cm-space-${dir.value}`]: dir.value,
      [`cm-space-align-${align.value}`]: align.value,
      [`cm-space-justify-${props.justify}`]: !!props.justify,
      'cm-space-wrap': props.wrap,
      'cm-space-inline': props.inline
    }));
    const newStyle = computed(() => ({
      ['gap']: size.value + 'px'
    }));
    return () => createVNode("div", {
      "class": classList.value,
      "style": newStyle.value,
      "id": props.id,
      "title": props.title
    }, [slots.default && slots.default()]);
  }
});

export { Space as default };
