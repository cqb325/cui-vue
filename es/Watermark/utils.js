function getStyleStr(style) {
  return Object.keys(style).map((key) => `${key}: ${style[key]};`).join(" ");
}
function getPixelRatio() {
  return window.devicePixelRatio || 1;
}
function rotateWatermark(ctx, rotateX, rotateY, rotate) {
  ctx.translate(rotateX, rotateY);
  ctx.rotate(Math.PI / 180 * Number(rotate));
  ctx.translate(-rotateX, -rotateY);
}
const reRendering = (mutation, watermarkElement) => {
  let flag = false;
  if (mutation.removedNodes.length) {
    flag = Array.from(mutation.removedNodes).some((node) => node === watermarkElement);
  }
  if (mutation.type === "attributes" && mutation.target === watermarkElement) {
    flag = true;
  }
  return flag;
};

export { getPixelRatio, getStyleStr, reRendering, rotateWatermark };
