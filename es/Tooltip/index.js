import { defineComponent, ref, createVNode, mergeProps } from 'vue';
import { innerPopupDefinedProps, InnerPopup } from '../inner/InnerPopup.js';

const Tooltip = /* @__PURE__ */ defineComponent({
  name: "Tooltip",
  props: innerPopupDefinedProps,
  emits: ['update:modelValue', 'visibleChange'],
  setup(props, {
    slots,
    emit,
    attrs,
    expose
  }) {
    const pop = ref();
    expose({
      updatePosition() {
        pop.value?.updatePosition();
      }
    });
    const align = props.align ?? 'top';
    const arrow = props.arrow ?? true;
    return () => createVNode(InnerPopup, mergeProps({
      "ref": pop
    }, props, attrs, {
      "clsPrefix": "cm-tooltip",
      "varName": "tooltip",
      "align": align,
      "arrow": arrow,
      "confirm": false,
      "onUpdate:modelValue": val => emit('update:modelValue', val),
      "onVisibleChange": val => emit('visibleChange', val)
    }), {
      default: () => [slots.default?.()]
    });
  }
});

export { Tooltip as default };
