import { defineComponent, ref, createVNode, mergeProps } from 'vue';
import { innerPopupDefinedProps, InnerPopup } from '../inner/InnerPopup.js';

const Popover = /* @__PURE__ */ defineComponent({
  name: "Popover",
  props: innerPopupDefinedProps,
  emits: ['update:modelValue', 'visibleChange'],
  setup(props, {
    slots,
    emit,
    attrs,
    expose
  }) {
    const pop = ref();
    expose({
      updatePosition() {
        pop.value?.updatePosition();
      }
    });
    return () => createVNode(InnerPopup, mergeProps({
      "ref": pop
    }, props, attrs, {
      "theme": props.theme || "light",
      "onUpdate:modelValue": val => emit('update:modelValue', val),
      "onVisibleChange": val => emit('visibleChange', val)
    }), {
      default: () => [slots.default?.()]
    });
  }
});

export { Popover as default };
