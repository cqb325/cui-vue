import { defineComponent, inject, computed, createVNode } from 'vue';
import { useClickAnimating } from '../use/useClickAnimating.js';
import Loading from '../inner/Loading.js';
import { BUTTON_GROUP_CONTEXT } from '../ButtonGroup/index.js';

const Button = /* @__PURE__ */ defineComponent({
  name: 'Button',
  props: {
    type: {
      type: String
    },
    theme: {
      type: String
    },
    shape: {
      type: String
    },
    disabled: {
      type: Boolean,
      default: undefined
    },
    block: {
      type: Boolean
    },
    size: {
      type: String
    },
    active: {
      type: Boolean
    },
    loading: {
      type: Boolean
    },
    icon: {
      type: Object
    },
    iconAlign: {
      type: String,
      default: 'left'
    }
  },
  emits: ['click'],
  setup(props, {
    emit,
    slots
  }) {
    const [animating, setAnimate] = useClickAnimating();
    const ctx = inject(BUTTON_GROUP_CONTEXT, {});
    const type = computed(() => props.type || ctx?.type);
    const size = computed(() => props.size || ctx?.size);
    const theme = computed(() => props.theme || ctx?.theme);
    const shape = computed(() => props.shape || ctx?.shape || 'square');
    const disabled = computed(() => props.disabled || ctx?.disabled);
    const classes = computed(() => ({
      'cm-button': true,
      [`cm-button-icon-${props.iconAlign}`]: true,
      'cm-click-animating': animating,
      'cm-button-disabled': disabled.value,
      'cm-button-block': props.block,
      [`cm-button-${type.value}`]: type.value,
      [`cm-button-theme-${theme.value}`]: theme.value,
      [`cm-button-${size.value}`]: size.value,
      'cm-button-active': props.active,
      'cm-button-circle': shape.value === 'circle',
      'cm-button-round': shape.value === 'round',
      'cm-button-icon-only': !!props.icon && !slots.default,
      'cm-button-empty': !props.icon && !slots.default
    }));
    const handleClick = e => {
      if (disabled.value) {
        return;
      }
      setAnimate();
      emit('click', e);
    };
    return () => createVNode("button", {
      "type": "button",
      "class": classes.value,
      "title": props.title,
      "disabled": disabled.value,
      "onClick": handleClick
    }, [props.loading ? createVNode(Loading, null, null) : (props.icon || slots.icon) && props.iconAlign === 'left' ? createVNode("span", {
      "class": "cm-button-icon"
    }, [props.icon || slots.icon()]) : null, slots.default && slots.default(), (props.icon || slots.icon) && props.iconAlign === 'right' ? createVNode("span", {
      "class": "cm-button-icon"
    }, [props.icon || slots.icon()]) : null]);
  }
});

export { Button as default };
