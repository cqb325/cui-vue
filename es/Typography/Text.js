import { defineComponent, computed, createVNode } from 'vue';

const Text = /* @__PURE__ */ defineComponent({
  name: 'Text',
  props: ['type', 'gradient', 'size', 'onCopy', 'mark', 'disabled', 'link', 'code', 'underline', 'deleted', 'strong', 'size', 'icon'],
  setup(props, {
    slots
  }) {
    const size = () => props.size || 'normal';
    const classList = computed(() => ({
      'cm-text': true,
      [`cm-text-${props.type}`]: props.type,
      'cm-text-disabled': props.disabled,
      'cm-text-underline': props.underline,
      'cm-text-link': props.link,
      'cm-text-deleted': props.deleted,
      'cm-text-strong': props.strong,
      [`cm-text-${size()}`]: size()
    }));
    const style = computed(() => ({
      'background-image': props.gradient ? `linear-gradient(${props.gradient.join(',')})` : '',
      '-webkit-text-fill-color': props.gradient ? 'transparent' : ''
    }));
    return () => createVNode("span", {
      "class": classList.value,
      "style": style.value
    }, [props.mark ? createVNode("mark", null, [slots.default && slots.default()]) : props.code ? createVNode("code", null, [slots.default && slots.default()]) : props.link ? createVNode("a", {
      "href": props.link
    }, [props.icon, createVNode("span", null, [slots.default && slots.default()])]) : slots.default && slots.default()]);
  }
});

export { Text as default };
