import { defineComponent, computed, createVNode } from 'vue';

const Link = /* @__PURE__ */ defineComponent({
  name: 'Link',
  props: {
    type: {
      type: String
    },
    disabled: {
      type: Boolean
    },
    link: {
      type: String
    },
    icon: {
      type: Object
    },
    mark: {
      type: Boolean
    },
    code: {
      type: Boolean
    },
    underline: {
      type: Boolean
    },
    deleted: {
      type: Boolean
    },
    strong: {
      type: Boolean
    },
    size: {
      type: String
    },
    gradient: {
      type: Array
    }
  },
  setup(props, {
    slots
  }) {
    const size = () => props.size || 'normal';
    const classList = computed(() => ({
      'cm-text cm-text-link': true,
      [`cm-text-${props.type}`]: props.type,
      'cm-text-disabled': props.disabled,
      'cm-text-underline': props.underline,
      'cm-text-deleted': props.deleted,
      'cm-text-strong': props.strong,
      [`cm-text-${size()}`]: size()
    }));
    const style = computed(() => ({
      'background-image': props.gradient ? `linear-gradient(${props.gradient.join(',')})` : '',
      '-webkit-text-fill-color': props.gradient ? 'transparent' : ''
    }));
    return () => createVNode("span", {
      "class": classList.value
    }, [createVNode("a", {
      "style": style.value,
      "href": props.link
    }, [props.icon, slots.default?.()])]);
  }
});

export { Link as default };
