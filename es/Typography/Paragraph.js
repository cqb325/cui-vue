import { defineComponent, ref, computed, createVNode } from 'vue';
import useCopy from '../use/useCopy.js';
import { FeatherCheck, FeatherCopy } from 'cui-vue-icons/feather';

const Paragraph = /* @__PURE__ */ defineComponent({
  name: 'Paragraph',
  props: ['size', 'type', 'spacing', 'copyText', 'copyable'],
  emits: ['copy'],
  setup(props, {
    slots,
    emit
  }) {
    const copyed = ref(false);
    const size = computed(() => props.size || 'normal');
    const classList = computed(() => ({
      'cm-typograghy-paragraph': true,
      [`cm-typograghy-paragraph-${size.value}`]: size.value,
      [`cm-typograghy-paragraph-${props.type}`]: !!props.type,
      'cm-typograghy-extended': props.spacing === 'extended'
    }));
    const target = ref(null);
    async function onCopy() {
      const ret = await useCopy(props.copyText ?? target.value.innerText);
      copyed.value = ret;
      if (ret) {
        emit('copy');
        setTimeout(() => {
          copyed.value = false;
        }, 4000);
      }
    }
    return () => createVNode("p", {
      "class": classList.value,
      "ref": target
    }, [slots.default && slots.default(), props.copyable ? copyed.value ? createVNode("span", {
      "class": "cm-typograghy-copyed"
    }, [createVNode(FeatherCheck, null, null)]) : createVNode("span", {
      "class": "cm-typograghy-copy",
      "onClick": onCopy
    }, [createVNode(FeatherCopy, null, null)]) : null]);
  }
});

export { Paragraph as default };
