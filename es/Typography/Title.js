import { defineComponent, computed, createVNode } from 'vue';

const Title = /* @__PURE__ */ defineComponent({
  name: 'Title',
  props: ['heading', 'type', 'inline', 'prefix', 'gradient', 'prefixWidth', 'prefixGap', 'prefixColor'],
  setup(props, {
    slots
  }) {
    const heading = computed(() => props.heading || 1);
    const classList = computed(() => ({
      'cm-typograghy-title': true,
      [`cm-typograghy-h${heading.value}`]: true,
      'cm-typograghy-title-inline': props.inline,
      [`cm-typograghy-title-prefix-${props.prefix}`]: props.prefix
    }));
    const style = computed(() => ({
      'background-image': props.gradient ? `linear-gradient(${props.gradient.join(',')})` : '',
      '-webkit-text-fill-color': props.gradient ? 'transparent' : '',
      [`--cm-title-prefix-width`]: props.prefixWidth ?? (props.prefix === 'bar' ? 4 : 8),
      [`--cm-title-prefix-gap`]: props.prefixGap ?? 16,
      [`--cm-title-prefix-color`]: typeof props.prefixColor === 'string' ? props.prefixColor : "",
      [`--cm-title-prefix-gradient`]: props.prefixColor instanceof Array ? props.prefixColor.join(',') : ""
    }));
    const options = [() => createVNode("h1", {
      "class": classList.value,
      "style": style.value
    }, [slots.default && slots.default()]), () => createVNode("h2", {
      "class": classList.value,
      "style": style.value
    }, [slots.default && slots.default()]), () => createVNode("h3", {
      "class": classList.value,
      "style": style.value
    }, [slots.default && slots.default()]), () => createVNode("h4", {
      "class": classList.value,
      "style": style.value
    }, [slots.default && slots.default()]), () => createVNode("h5", {
      "class": classList.value,
      "style": style.value
    }, [slots.default && slots.default()]), () => createVNode("h6", {
      "class": classList.value,
      "style": style.value
    }, [slots.default && slots.default()])];
    return () => options[heading.value - 1]();
  }
});

export { Title as default };
