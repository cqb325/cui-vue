import { defineComponent, computed, ref, watch, provide, createVNode } from 'vue';
import Item from './Item.js';

const Accordion = /* @__PURE__ */ defineComponent({
  name: 'Accordion',
  props: {
    multi: {
      type: Boolean
    },
    activeKey: {
      type: [String, Number, Array]
    },
    flex: {
      type: Boolean
    }
  },
  emits: ['select'],
  setup(props, {
    emit,
    slots
  }) {
    const classList = computed(() => ({
      'cm-accordion': true,
      'cm-flex-accordion': props.flex
    }));
    const activeKey = ref(props.activeKey ?? props.multi ? [] : '');
    watch(() => props.activeKey, newVal => {
      activeKey.value = newVal;
    });
    const onSelect = (name, open, v) => {
      emit('select', name, open, v);
    };
    const ctx = {
      flex: props.flex,
      multi: props.multi,
      activeKey,
      onSelect: onSelect
    };
    provide('CMAccordionContext', ctx);
    return () => createVNode("div", {
      "class": classList.value
    }, [slots.default?.()]);
  }
});
Accordion.Item = Item;

export { Accordion as default };
